﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Tracing;
using selenium.core.Framework.Browser;

namespace selenium.beeline {
    [Binding]
    public class SpecFlowHooks {
        private static string GetArtifactsDirectory() {
            return Path.Combine(Directory.GetCurrentDirectory(), "testresults");

            //a[h3/span[@title='ChangeNumber']]/descendant::tr[descendant::span[contains(@title,'CorrectInitialValues')]]/following-sibling::tr[1]/descendant::pre
        }

        [BeforeTestRun]
        public static void BeforeAllTests()
        {
            // Очистим папку с артифактами тестового запуска
            string artifactDirectory = GetArtifactsDirectory();
            if (Directory.Exists(artifactDirectory))
                Directory.Delete(artifactDirectory, true);
            Directory.CreateDirectory(artifactDirectory);
        }

        [BeforeScenario]
        public static void BeforeTest() {
            BeelineSeleniumContext.Inst.Browser.Cookies.Clear();
        }

        [AfterScenario]
        public void AfterTest() {
            if (ScenarioContext.Current.TestError != null)
                TakeScreenshot();
        }

        private void TakeScreenshot() {
            try {
                string fileNameBase = MakeAtrifactFileName(FeatureContext.Current.FeatureInfo.Title.ToIdentifier(),
                                                           ScenarioContext.Current.ScenarioInfo.Title.ToIdentifier());

                // Сохранить исходный код страницы
                string artifactDirectory = GetArtifactsDirectory();
                string pageSource = BeelineSeleniumContext.Inst.Browser.Get.PageSource;
                string sourceFilePath = Path.Combine(artifactDirectory, fileNameBase + "_source.html");
                File.WriteAllText(sourceFilePath, pageSource, Encoding.UTF8);
                Console.WriteLine("Page source: {0}", new Uri(sourceFilePath));

                // Сохранить скриншот
                Bitmap screenshot = BeelineSeleniumContext.Inst.Browser.Get.Screenshot();
                string screenshotFilePath = Path.Combine(artifactDirectory, fileNameBase + "_screenshot.png");
                screenshot.Save(screenshotFilePath, ImageFormat.Png);
                Console.WriteLine("Screenshot: {0}", new Uri(screenshotFilePath));
            }
            catch (Exception ex) {
                Console.WriteLine("Error while taking screenshot: {0}", ex);
            }
        }

        private string MakeAtrifactFileName(string featureId, string scenarioId) {
            return string.Format("{0}_{1}_{2}", featureId, scenarioId, DateTime.Now.ToString("yyyyMMdd_HHmmss"));
        }
    }
}
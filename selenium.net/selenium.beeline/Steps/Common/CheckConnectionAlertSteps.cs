﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Products.Home.Tariffs.Internet;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Common {
    [Binding]
    [Scope(Feature = "Connect house application")]
    public class CheckConnectionAlertSteps:BeelineSteps {
        public CheckConnectionAlert Alert {
            get { return Browser.State.HtmlAlertAs<CheckConnectionAlert>(); }
        }

        [Given(@"\[Connection alert] is visible")]
        public void GivenConnectionAlertIsVisible() {
            var page = Browser.Go.ToPage<InternetTariffsPage>();
            page.Tariffs.GoToRandomTariffDetails();
            var tariffPage = Browser.State.PageAs<InternetTariffPage>();
            tariffPage.OpenCheckConnectionAlert();
        }

        [When(@"\[Connection alert] Type in address\(street:'(.*)', house:'(.*)'\)")]
        public void WhenConnectionAlertTypeInAddressStreetHome(string street, string house) {
            Alert.FillForm(street, house);
        }

        [When(@"\[Connection alert] Click by \[Send application] button")]
        public void WhenConnectionAlertClickBySendApplicationButton() {
            Alert.SendApplicationButton.ClickAndWaitWhileAjax();
        }

        [Then(@"\[Connection alert] is not visible")]
        public void ThenConnectionAlertIsNotVisible() {
            Assert.False(Browser.State.HtmlAlertIs<CheckConnectionAlert>());
        }
    }
}

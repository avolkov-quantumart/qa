﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Products;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Common {
    [Binding]
    [Scope(Feature = "GlobalSearch")]
    public class GlobalSearchSteps : BeelinePageSteps<BeelinePageBase> {
        [Given(@"\[global search] field is visible")]
        public void GivenGlobalSearchFieldIsVisible() {
            GoTo<ProductsPage>();
        }

        [When(@"\[global search] Search for '(.*)'")]
        public void WhenTypeInInGlobalSearchField(string query) {
            Page.Navigation.Search.SearchFor(query);
        }
    }
}
﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Common.Alerts;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Common {
    [Binding]
    [Scope(Feature = "Connect house application")]
    public class ConnectHouseApplicationAlertSteps : BeelineSteps {
        public ConnectHouseApplicationAlert Alert {
            get { return Browser.State.HtmlAlertAs<ConnectHouseApplicationAlert>(); }
        }

        [Then(@"\[Connect house application] is visible")]
        public void ThenConnectHouseApplicationIsVisible() {
            Alert.AssertVisible();
        }

        [Then(@"\[Connect house application] Street is '(.*)'")]
        public void ThenConnectHouseApplicationStreetIs(string street) {
            Alert.Street.AssertEqual(street);
        }

        [Then(@"\[Connect house application] House is '(.*)'")]
        public void ThenConnectHouseApplicationHouseIs(string house) {
            Alert.House.AssertEqual(house);
        }

        [When(@"\[Connect house application] Type in '(.*)' in \[appartment] field")]
        public void WhenConnectHouseApplicationTypeInInAppartmentField(int apartment) {
            Alert.Apartment.TypeIn(apartment);
        }

        [When(@"\[Connect house application] Type in '(.*)' in \[last name] field")]
        public void WhenConnectHouseApplicationTypeInInLastNameField(string lastName) {
            Alert.LastName.TypeIn(lastName);
        }

        [When(@"\[Connect house application] Type in '(.*)' in \[first name] field")]
        public void WhenConnectHouseApplicationTypeInInFirstNameField(string firstName) {
            Alert.FirstName.TypeIn(firstName);
        }

        [When(@"\[Connect house application] Type in '(.*)' in \[patronymic name] field")]
        public void WhenConnectHouseApplicationTypeInInPatronymicNameField(string patronymicName) {
            Alert.PatronymicName.TypeIn(patronymicName);
        }

        [When(@"\[Connect house application] Type in '(.*)' in \[email] field")]
        public void WhenConnectHouseApplicationTypeInField(string email) {
            Alert.Email.TypeIn(email);
        }

        [When(@"\[Connect house application] Type in '(.*)' in \[phone] field")]
        public void WhenConnectHouseApplicationTypeInInPhoneField(string phone) {
            Alert.PhoneNumber.TypeIn(phone);
        }

        [When(@"\[Connect house application] Click by \[send application] button")]
        public void WhenConnectHouseApplicationClickBySendApplicationButton() {
            Alert.SendButton.ClickAndWaitWhileAjax();
        }

        [Then(@"\[Connect house application] Application was sent message is visible")]
        public void ThenConnectHouseApplicationConnectionApplicationWasSentAlertIsVisible() {
            Alert.ApplicationWasSentMessage.AssertIsVisible();
        }
    }
}
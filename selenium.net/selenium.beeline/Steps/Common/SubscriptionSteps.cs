using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using QA.Beeline.DAL;
using TechTalk.SpecFlow;
using selenium.beeline.DB;
using selenium.beeline.Pages.Customers.press;
using selenium.beeline.Pages.newssubscriptions;
using selenium.beeline.Steps.Base;
using selenium.core.Auxiliary;
using selenium.core.Exceptions;

namespace selenium.beeline.Steps.Common {
    [Binding]
    [Scope(Tag = "subscription")]
    public class SubscriptionSteps : BeelineSteps {
        #region Given

        [Given(@"There is no subscription for '(.*)'")]
        public void GivenThereIsNoSubscriptionFor(string email) {
            QPManager.RemoveSubscriptions(QPManager.GetSubscriptions(email));
        }

        [Given(@"User made subscription to '(.*)'")]
        public void UserSubscribesTo(string email) {
            MailHelper.DeleteMessages(email);
            var newsPage = GoTo<NewsPage>();
            newsPage.Subscribe(email);
            FD.subscription.subscription_email = email;
        }

        [Given(@"User subscribed to news for '(.*)'")]
        public void GivenUserSubscribedToNews(string email) {
            UserSubscription activeSubscription = QPManager.GetActiveSubscription(email);
            if (activeSubscription == null) {
                UserSubscribesTo(email);
                UserClicksByActivationLinkInActivationLetter(email);
                activeSubscription = QPManager.GetActiveSubscription(email);
                if (activeSubscription == null)
                    throw Throw.FrameworkException("�� ������� ������� �������� �� ������� ��� {0}", email);
            }
            FD.subscription.active_subscription_id = activeSubscription.Id;
        }

        [Given(@"User cancels subscription for '(.*)'")]
        public void GivenUserRemovesSubscription(string email) {
            MailHelper.DeleteMessages(email);
            var newsPage = GoTo<NewsPage>();
            newsPage.RemoveSubscription(email);
            FD.subscription.subscription_email = email;
        }

        #endregion

        #region When

        [When(@"User clicks by \[activation link] in activation letter at '(.*)'")]
        public void UserClicksByActivationLinkInActivationLetter(string email) {
            SubscriptionActivationEmail activateSubscriptionPage = OpenActivationLetter(email);
            activateSubscriptionPage.ActivationLink.Click();
            FD.subscription.subscription_email = email;
        }

        [When(@"User clicks by \[change themes] link in activation letter at '(.*)'")]
        public void WhenUserClicksByChangeThemesLinkInActivationLetter(string email) {
            SubscriptionActivationEmail page = OpenActivationLetter(email);
            FD.subscription.subscription_themes = page.GetThemes();
            page.ChangeThemesLink.Click();
            FD.subscription.subscription_email = email;
        }

        [When(@"User confirms subscription cancellation in letter at '(.*)'")]
        public void WhenUserConfirmsSubscriptionCancellationInLetter(string email) {
            SubscriptionCancelationEmail page = OpenCancellationLetter(email);
            page.ConfirmationLink.Click();
            FD.subscription.subscription_email = email;
        }

        [When(@"User clicks by \[subscribe link] in change subscription themes letter at '(.*)'")]
        public void WhenUserClicksBySubscribeLinkInChangeSubscriptionThemesLetterAt(string email) {
            ChangeSubscriptionThemesEmail changeSubscriptionThemesPage = OpenChangeThemesLetter(email);
            changeSubscriptionThemesPage.ChangeLink.Click();
            FD.subscription.subscription_themes = FD.subscription.new_subscription_themes;
        }

        [When(@"User changes themes on \[subscription management] page")]
        public void WhenUserChangesThemesOnSubscriptionManagementPage() {
            var page = Browser.State.PageAs<SubscriptionManagementPage>();
            page.Email.TypeIn(FD.subscription.subscription_email);
            page.SelectRandomThemes(5);
            page.SubscribeCaptcha.SetDefault();
            FD.subscription.new_subscription_themes = page.GetSelectedThemes();
            page.SubscribeButton.ClickAndWaitWhileAjax();
        }

        #endregion

        #region Then

        [Then(@"Check mail for activation letter message is visible on \[subscription management] page")]
        public void ThenCheckMailForActivationLetterMessageIsVisibleOnSubscriptionManagementPage() {
            var page = Browser.State.PageAs<SubscriptionManagementPage>();
            page.AssertCheckMailForActivationMessage();
        }

        [Then(@"Check mail for activation letter message is visible")]
        public void ThenCheckMailForActivationLetterMessageIsVisible() {
            var newsPage = Browser.State.PageAs<NewsPage>();
            newsPage.SubscribeAlert.AssertCheckMailForActivationMessage();
        }

        private SubscriptionCancelationEmail OpenCancellationLetter(string email) {
            if (!Browser.State.PageIs<SubscriptionCancelationEmail>())
                return Browser.Go.ToEmail<SubscriptionCancelationEmail>(email, "������������� ����(.*)� ��������");
            return Browser.State.PageAs<SubscriptionCancelationEmail>();
        }

        private SubscriptionActivationEmail OpenActivationLetter(string email) {
            if (!Browser.State.PageIs<SubscriptionActivationEmail>())
                return Browser.Go.ToEmail<SubscriptionActivationEmail>(email, "������������� ����(.*)��� �� �������");
            return Browser.State.PageAs<SubscriptionActivationEmail>();
        }

        private ChangeSubscriptionThemesEmail OpenChangeThemesLetter(string email) {
            if (!Browser.State.PageIs<ChangeSubscriptionThemesEmail>()) {
                return Browser.Go.ToEmail<ChangeSubscriptionThemesEmail>(email,
                                                                         "������������� ����(.*)�� �������� �� �������");
            }
            return Browser.State.PageAs<ChangeSubscriptionThemesEmail>();
        }

        [Then(@"Subscription activation letter came to '(.*)'")]
        public void ThenSubscriptionActivationLetterCameTo(string email) {
            SubscriptionActivationEmail activateSubscriptionPage = OpenActivationLetter(email);
            List<string> themes = activateSubscriptionPage.GetThemes();
            Assert.That(FD.subscription.subscription_themes, Is.EquivalentTo(themes),
                        "���� �������� � ������ ��������� �� ������������� ��������� ������������� �����");
        }

        [Then(@"Change subscription themes letter came to '(.*)'")]
        public void ThenChangeSubscriptionThemesLetterCameTo(string email) {
            ChangeSubscriptionThemesEmail changeSubscriptionThemesPage = OpenChangeThemesLetter(email);
            List<string> oldThemes = changeSubscriptionThemesPage.GetOldThemes();
            Assert.That(FD.subscription.subscription_themes, Is.EquivalentTo(oldThemes),
                        "������������ ������ ������ ������� � ������ ������������� ��������� �������");
            List<string> newThemes = changeSubscriptionThemesPage.GetNewThemes();
            Assert.That(FD.subscription.new_subscription_themes, Is.EquivalentTo(newThemes),
                        "������������ ������ ����� ������� � ������ ������������� ��������� �������");
        }

        [Then(@"\[Subscription successfull] page is visible")]
        public void ThenSubscriptionSuccessfullPageIsVisible() {
            var page = Browser.State.PageAs<ActivationResultPage>();
            Assert.IsNotNull(page, "�� ������� �� �������� ������������� ���������");
            page.AssertSubscriptionIsActivated(FD.subscription.subscription_email);
        }

        [Then("Subscription activation record exists in DB")]
        private void ThenSubscriptionActivationExistsInDB() {
            List<ConfirmationRequest> subscriptionActivation =
                QPManager.GetSubscriptionActivations(FD.subscription.subscription_email);
            Assert.IsNotNull(subscriptionActivation, "� �� �� ��������� ������ �� ��������� �������� ��� email '{0}'",
                             FD.subscription.subscription_email);
        }

        [Then(@"\[Old activation link] page is visible")]
        public void ThenOldActivationLinkPageIsVisible() {
            var page = Browser.State.PageAs<ActivationResultPage>();
            Assert.IsNotNull(page, "�� ������� �� �������� ������������� ���������");
            page.AssertActivationLinkIsOld();
        }

        [Then(@"Check mail for cancellation letter message is visible")]
        public void ThenCheckMailForCancellationLetterMessageIsVisible() {
            var newsPage = Browser.State.PageAs<NewsPage>();
            newsPage.SubscribeAlert.AssertCheckMailForCancellationMessage();
        }

        [Then(@"Cancel subscription confirmation letter came to '(.*)'")]
        public void ThenRemoveSubscriptionConfirmationLetterCameTo(string email) {
            SubscriptionCancelationEmail subscriptionCancelationEmail = OpenCancellationLetter(email);
            Assert.True(subscriptionCancelationEmail.ConfirmationLink.IsVisible(),
                        "� ������ ������ �������� �� ������������ ������ �������������");
        }

        [Then(@"\[Is deleted] is true for canceled subscription")]
        public void ThenIsDeletedFlagIsTrueForSubscriptionTo() {
            UserSubscription subscription = QPManager.GetSubscription(FD.subscription.active_subscription_id);
            Assert.True(subscription.IsDeleted != null, "� ���� �� ���������� ���� '������' ��� �������� '{0}'",
                        FD.subscription.subscription_email);
        }

        [Then(@"Canceled subscription has no themes")]
        public void ThenRemovedSubscriptionHasNoThemes() {
            UserSubscription subscription = QPManager.GetSubscription(FD.subscription.active_subscription_id);
            Assert.IsEmpty(QPManager.GetSubscriptionThemes(subscription),
                           "���� �� �������� �� �������� '{0}' � ����", FD.subscription.subscription_email);
        }

        [Then(@"Subscription in DB contains correct themes")]
        public void ThenSubscriptionInDBContainsCorrectThemes() {
            UserSubscription subscription = QPManager.GetActiveSubscription(FD.subscription.subscription_email);
            Assert.IsNotNull(subscription, "� ���� �� ��������� ������ � �������� ��� email '{0}'",
                             FD.subscription.subscription_email);
            List<string> themes = QPManager.GetSubscriptionThemeNames(subscription);
            Assert.That(FD.subscription.subscription_themes, Is.EquivalentTo(themes),
                        "������ ��� � �� ��� email '{0}' �� ��������� � ������, ���������� ��� ���������� ��������",
                        FD.subscription.subscription_email);
        }

        #endregion

        private List<int> GetSubscriptions(string email) {
            return QPManager.GetSubscriptions(email).Select(s => s.Id).ToList();
        }

    }
}
﻿using TechTalk.SpecFlow;
using selenium.beeline.DB;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Products;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Component {
    [Binding]
    [Scope(Feature = "Feedback")]
    public class FeedbackWidgetSteps : BeelinePageSteps<BeelinePageBase> {
        public string _name;

        public FeedbackComponent Feedback {
            get { return Page.Header.Support.Feedback; }
        }

        public HelpRobotComponent Help {
            get { return Page.Header.Support.Help; }
        }

        [Given(@"\[Short numbers swindle form] is visible")]
        public void GivenShortNumbersSwingleFormIsVisible() {
            GoTo<ProductsPage>();
            Page.Header.OpenSupportWidget().OpenFeedbackComponent().OpenSwindleTab();
            Feedback.Swindle.ShortNumbersRadio.SelectAndWaitWhileAjaxRequests();
        }

        [When(@"\[Short numbers swindle form] Type in '(.*)' in \[phone] field")]
        public void WhenShortNumbersSwindleFormTypeInInPhoneField(string phone) {
            Feedback.Swindle.Phone.TypeIn(phone);
        }

        [When(@"\[Short numbers swindle form] Type in captcha")]
        public void WhenShortNumbersSwingleFormTypeInCaptcha() {
            Feedback.Swindle.Captcha.SetDefault();
        }

        [When(@"\[Short numbers swindle form] Click by \[Get password] button")]
        public void WhenShortNumbersSwingleFormClickByGetPasswordButton() {
            Feedback.Swindle.GetPasswordButton.ClickAndWaitWhileProgress();
        }

        [When(@"\[Short numbers swindle form] Type in password")]
        public void WhenShortNumbersSwindleFormTypeInPassword() {
            string code = DBManager.GetLastSmsCode();
            Feedback.Swindle.Password.TypeIn(code);
        }

        [When(@"\[Short numbers swindle form] Click by \[Continue] button")]
        public void WhenShortNumbersSwindleFormClickByContinueButton() {
            Feedback.Swindle.ContinueButton.ClickAndWaitWhileAjax();
        }

        [Then(@"\[Short numbers swindle form] Success form is visible")]
        public void ThenShortNumbersSwindleFormSuccessFormIsVisible() {
            Feedback.Complaint.AssertSuccessFormIsVisible(_name);
        }

        [Given(@"\[Complaint form] is visible")]
        public void GivenComplaintFormIsVisible() {
            GoTo<ProductsPage>();
            Page.Header.OpenSupportWidget().OpenFeedbackComponent().OpenComplaintTab();
        }

        [Given(@"\[Complaint form] Theme '(.*)' is selected")]
        public void GivenComplaintFormThemeIsSelected(string theme) {
            Feedback.Complaint.Theme.SelectItem(theme);
        }

        [Given(@"\[Complaint form] Type in '(.*)' in \[your question] field")]
        public void GivenComplaintFormTypeInInYourQuestionField(string complaintText) {
            Feedback.Complaint.Body.TypeIn(complaintText);
        }

        [Given(@"\[Complaint form] Type in '(.*)' in \[name] field")]
        public void GivenComplaintFormTypeInInNameField(string name) {
            Feedback.Complaint.Name.TypeIn(name);
            _name = name;
        }

        [Given(@"\[Complaint form] Type in '(.*)' in \[phone] field")]
        public void GivenComplaintFormTypeInInPhoneField(string phone) {
            Feedback.Complaint.Phone.TypeIn(phone);
        }

        [Given(@"\[Complaint form] Type in '(.*)' in \[email] field")]
        public void GivenComplaintFormTypeInInEmailField(string email) {
            Feedback.Complaint.Email.TypeIn(email);
        }

        [Given(@"\[Complaint form] Type in captcha")]
        public void GivenComplaintFormTypeInCaptcha() {
            Feedback.Complaint.Captcha.SetDefault();
        }

//        [Given(@"\[Complaint form] Load jpeg file")]
//        public void GivenComplaintFormLoadJpegFile() {
//            _complaintForm.LoadFile("/test_files/test_img.jpeg");
//        }
//
//        [Given(@"\[Complaint form] Load doc file")]
//        public void GivenComplaintFormLoadDocFile() {
//            ScenarioContext.Current.Pending();
//        }

        [Given(@"\[Complaint form] Click by \[send] button")]
        public void GivenComplaintFormClickBySendButton() {
            Feedback.Complaint.SendButton.ClickAndWaitWhileAjax();
        }

        [Then(@"\[Complaint form] Success form is visible")]
        public void ThenComplaintFormSuccessFormIsVisible() {
            Feedback.Complaint.AssertSuccessFormIsVisible(_name);
        }

        [Then(@"\[Question form] Success form is visible")]
        public void ThenQuestionFormFeedbackWidgetSuccessFormIsVisible() {
            Feedback.Question.AssertSuccessFormIsVisible(_name);
        }

//        [Then(@"Complaint letter came to email from site settings")]
//        public void ThenComplaintLetterCameToEmailFromSiteSettings() {
//            ScenarioContext.Current.Pending();
//        }

        [Given(@"\[Question form] is visible")]
        public void GivenQuestionFormIsVisible() {
            GoTo<ProductsPage>();
            Page.Header.OpenSupportWidget().OpenFeedbackComponent().OpenQuestionTab();
        }

        [Given(@"\[Question form] Theme '(.*)' is selected")]
        public void GivenQuestionFormThemeIsSelected(string theme) {
            Feedback.Question.Theme.SelectItem(theme);
        }

        [Given(@"\[Question form] Subtheme '(.*)' is selected")]
        public void GivenQuestionFormSubthemeIsSelected(string subtheme) {
            Feedback.Question.Subtheme.SelectItem(subtheme);
        }

        [Given(@"\[Question form] Type in '(.*)' in \[name] field")]
        public void GivenQuestionFormTypeInInNameField(string name) {
            Feedback.Question.Name.TypeIn(name);
            _name = name;
        }

        [Given(@"\[Question form] Type in '(.*)' in \[your question] field")]
        public void GivenQuestionFormTypeInInYourQuestionField(string question) {
            Feedback.Question.Body.TypeIn(question);
        }

        [Given(@"\[Question form] Type in '(.*)' in \[phone] field")]
        public void GivenQuestionFormTypeInInPhoneField(string phone) {
            Feedback.Question.Phone.TypeIn(phone);
        }

        [Given(@"\[Question form] Type in '(.*)' in \[email] field")]
        public void GivenQuestionFormTypeInInEmailField(string email) {
            Feedback.Question.Email.TypeIn(email);
        }

        [Given(@"\[Question form] Type in captcha")]
        public void GivenQuestionFormTypeInCaptcha() {
            Feedback.Question.Captcha.SetDefault();
        }

        [Given(@"\[Question form] Click by \[send] button")]
        public void GivenQuestionFormClickBySendButton() {
            Feedback.Question.SendButton.ClickAndWaitWhileAjax();
        }
    }
}
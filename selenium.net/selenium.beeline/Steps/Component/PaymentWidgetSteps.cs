﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Common.ServiceWidgets;
using selenium.beeline.Pages.Products.Home;
using selenium.beeline.Pages.Products.Mobile;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Component {
    [Binding]
    [Scope(Feature = "Payment")]
    public class PaymentWidgetSteps : BeelineSteps {
        private PaymentWidget _paymentWidget;

        [Given(@"\[Payment widget] for home beeline is visible")]
        public void GivenPaymentWidgetIsVisible() {
            GoTo<HomePage>();
            var homePage = Browser.State.PageAs<HomePage>();
            _paymentWidget = homePage.Services.OpenService<PaymentWidget>(BeelineServiceType.Payment);
        }

        [Given(@"\[Payment widget] for mobile beeline is visible")]
        public void GivenPaymentWidgetForMobileBeelineIsVisible() {
            GoTo<MobilePage>();
            var homePage = Browser.State.PageAs<MobilePage>();
            _paymentWidget = homePage.Services.OpenService<PaymentWidget>(BeelineServiceType.Payment);
        }

        [When(@"\[Payment widget] Type in random number in \[account number] field")]
        public void WhenTypeInRandomNumberInAccountNumberField() {
            FD.payment.AccountNumber = _paymentWidget.AccountNumber.TypeInRandomNumber(7);
        }

        [When(@"\[Payment widget] Type '(.*)' in \[phone] field")]
        public void WhenPaymentWidgetTypeInBeelineNumberInPhoneField(string phone) {
            _paymentWidget.Phone.TypeIn(phone);
        }

        [When(@"\[Payment widget] Type in '(.*)' in \[payment sum] field")]
        public void WhenTypeInInPaymentSumField(int sum) {
            _paymentWidget.Sum.TypeIn(sum);
            FD.payment.Sum = sum;
        }

        [Then(@"\[Payment widget] \[payment sum] field value is '(.*)'")]
        public void ThenPaymentWidgetPaymentSumFieldValueIs(string expected) {
            _paymentWidget.Sum.AssertEqual(expected);
        }

        [When(@"\[Payment widget] Press \[pay] button")]
        public void WhenPressPayButton() {
            _paymentWidget.PayButton.ClickAndWaitWhileAjax(true);
        }

        [Then(@"\[Payment widget] \[Type in beeline number] error is visible")]
        public void ThenPaymentWidgetTypeInBeelineNumberErrorIsVisible() {
            _paymentWidget.Phone.ValidationHint.AssertNotBeelineErrorVisible();
        }

    }
}
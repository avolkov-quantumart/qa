﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common.ServiceWidgets;
using selenium.beeline.Pages.Products.Mobile.Services;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Component {
    [Binding]
    [Scope(Feature = "SendSms")]
    public class SendSmsWidgetSteps : BeelineSteps {
        private SendSmsWidget _sendSmsComponent;

        [Given(@"\[Send sms widget] is visible")]
        public void GivenSendSmsWidgetIsVisible() {
            var page = GoTo<CellPhoneServicesPage>();
            _sendSmsComponent = page.Services.OpenService<SendSmsWidget>(BeelineServiceType.SendSms);
        }

        [When(@"\[Send sms widget] Type in '(.*)' in \[phone] field")]
        public void WhenTypeInPhone(string phone) {
            _sendSmsComponent.Phone.TypeIn(phone);
        }

        [When(@"\[Send sms widget] Type in '(.*)' in \[body] field")]
        public void WhenSendSmsWidgetTypeInInBodyField(string body) {
            _sendSmsComponent.SmsBody.TextArea.TypeIn(body);
        }

        [When(@"\[Send sms widget] Type in captcha")]
        public void WhenTypeInCaptcha() {
            _sendSmsComponent.Captcha.SetDefault();
        }

        [When(@"\[Send sms widget] Click by \[Send message button]")]
        public void WhenClickBySendMessageButton() {
            _sendSmsComponent.SendButton.ClickAndWaitWhileAjax();
        }

        [Then(@"\[Send sms widget] Sms status form for '(.*)' is visible")]
        public void ThenSmsStatusFormForIsVisible(string phone) {
            _sendSmsComponent.SmsStatusForm.AssertIsVisible(phone);
        }

        [Then(@"\[Send sms widget] Sms status is \[in progress]")]
        public void ThenSmsStatusIsInProgress() {
            _sendSmsComponent.SmsStatusForm.AssertStatusIsInProgress();
        }

        [When(@"\[Send sms widget] Wait while Sms status is \[in progress]")]
        public void WhenWaitWhileSmsStatusIsInProgress() {
            _sendSmsComponent.SmsStatusForm.WaitWhileInProgres();
        }

        [When(@"\[Send sms widget] Click by \[Update link]")]
        public void WhenClickByUpdateLink() {
            _sendSmsComponent.SmsStatusForm.UpdateLink.ClickAndWaitWhileAjaxRequests();
        }

        [Then(@"\[Send sms widget] Sms status is \[sended]")]
        public void ThenSmsStatusForIsSended() {
            _sendSmsComponent.SmsStatusForm.AssertStatusIsSended();
        }
    }
}
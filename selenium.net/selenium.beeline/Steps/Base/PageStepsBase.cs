using System;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;

namespace selenium.beeline.Steps.Base {
    public abstract class PageStepsBase<P>:StepsBase where P: IPage {
        protected P Page {
            get { return Browser.State.PageAs<P>(); }
        }

        /// <summary>
        /// �������� FixtureSetup - ������ ������� ���������� ���� ��� ����� ����� ������� ������
        /// </summary>
        /// <remarks>
        /// �.�. ��� ����� ������ ����������� �������� ���������������, �� �� ����� ������������� ��� ������� �������� � ������ ��� ��������� � ������ FixtureSetup
        /// �� ���� ������ ������ �������� ����� ���������� ������ � ���� ���������, ��� ������� ��� ����� �� ����� ��������� �� ��� �������������� ��������
        /// </remarks>
        /// <param name="action">��������, ������� ����� ��������� ���� ���</param>
        protected void PreparePage(Action action) {
            if (Browser.State.PageIs<P>())
                return;
            action.Invoke();
            Browser.State.PageAs<P>();
            if (!Browser.State.PageIs<P>())
                throw Throw.FrameworkException("�� ������� �� �������� '{0}'", typeof (P).Name);
        }
    }
}
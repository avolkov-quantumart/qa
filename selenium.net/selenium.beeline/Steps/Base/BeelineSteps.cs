using selenium.core.Framework.Browser;
using selenium.core.Logging;

namespace selenium.beeline.Steps.Base {
    public abstract class BeelineSteps : StepsBase {
        protected override Browser Browser {
            get { return BeelineSeleniumContext.Inst.Browser; }
        }

        protected override TestLogger Log {
            get { return BeelineSeleniumContext.Inst.Log; }
        }
    }
}
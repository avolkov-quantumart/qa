using selenium.core.Exceptions;
using selenium.core.Framework.Browser;
using selenium.core.Framework.Page;
using selenium.core.Logging;

namespace selenium.beeline.Steps {
    public abstract class StepsBase {
        protected abstract Browser Browser { get; }
        protected abstract TestLogger Log { get; }

        public T GoTo<T>(bool update = true, bool waitForAjax = true, bool ajaxInevitable=false) where T : IPage {
            if (!Browser.State.PageIs<T>() || update)
                Browser.Go.ToPage<T>();
            if (!Browser.State.PageIs<T>())
                throw Throw.FrameworkException("�� ������� �� �������� '{0}'", typeof (T).Name);
            if (waitForAjax)
                Browser.Wait.WhileAjax(ajaxInevitable: ajaxInevitable);
            return Browser.State.PageAs<T>();
        }
    }
}
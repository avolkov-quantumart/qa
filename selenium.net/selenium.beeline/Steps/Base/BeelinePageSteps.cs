using selenium.beeline.Steps.Common;
using selenium.core.Framework.Browser;
using selenium.core.Framework.Page;
using selenium.core.Logging;

namespace selenium.beeline.Steps.Base {
    public abstract class BeelinePageSteps<P> : PageStepsBase<P> where P : IPage {
        protected override Browser Browser {
            get { return BeelineSeleniumContext.Inst.Browser; }
        }

        protected override TestLogger Log {
            get { return BeelineSeleniumContext.Inst.Log; }
        }

        public BrowserAction Action {
            get { return Browser.Action; }
        }

        public BrowserAlert Alert {
            get { return Browser.Alert; }
        }

        public BrowserFind Find {
            get { return Browser.Find; }
        }

        public BrowserGet Get {
            get { return Browser.Get; }
        }

        public BrowserGo Go {
            get { return Browser.Go; }
        }

        public BrowserIs Is {
            get { return Browser.Is; }
        }

        public BrowserState State {
            get { return Browser.State; }
        }

        public BrowserWait Wait {
            get { return Browser.Wait; }
        }

        public BrowserJs Js {
            get { return Browser.Js; }
        }

        public BrowserWindow Window {
            get { return Browser.Window; }
        }

        private BrowserCookies Cookies {
            get { return Browser.Cookies; }
        }
    }
}
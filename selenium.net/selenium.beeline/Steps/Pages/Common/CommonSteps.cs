﻿using TechTalk.SpecFlow;
using selenium.core.Framework.Browser;
using selenium.core.Logging;

namespace selenium.beeline.Steps.Pages.Common {
    [Binding]
    internal class CommonSteps {
        protected Browser Browser {
            get { return BeelineSeleniumContext.Inst.Browser; }
        }

        protected TestLogger Log {
            get { return BeelineSeleniumContext.Inst.Log; }
        }

        [Given(@"I cleared all cookies")]
        public void GivenIClearedAllCookies() {
            Browser.Cookies.Clear();
        }
    }
}

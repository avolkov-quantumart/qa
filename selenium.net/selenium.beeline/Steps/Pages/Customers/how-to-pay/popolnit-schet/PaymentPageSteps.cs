﻿using TechTalk.SpecFlow;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Customers.how_to_pay.popolnit_schet {
    [Binding]
    [Scope(Feature = "Payment")]
    public class PaymentPageSteps : BeelinePageSteps<PaymentPage> {
        [Given(@"\[Payment page] is visible")]
        public void GivenPaymentPageIsVisible() {
            GoTo<PaymentPage>();
        }

        [Given(@"\[Payment page] Home payment tab is selected")]
        public void GivenHomePaymentIsSelected() {
            Page.OpenHomeTab();
        }

        [When(@"\[Payment page] Type in random number in \[account number] field")]
        public void WhenTypeInRandomNumberInAccountNumberField() {
            FD.payment.AccountNumber = Page.HomeTab.PaymentBlock.AccountNumber.TypeInRandomNumber(7);
        }

        [When(@"\[Payment page] Type in '(.*)' in \[payment sum] field")]
        public void WhenTypeInInPaymentSumField(int sum) {
            Page.HomeTab.PaymentBlock.Sum.TypeIn(sum);
            FD.payment.Sum = sum;
        }

        [Then(@"\[Payment page] \[payment sum] field is '(.*)'")]
        public void ThenPaymentPagePaymentSumFieldIs(string expected) {
            Page.HomeTab.PaymentBlock.Sum.AssertEqual(expected);
        }

        [When(@"\[Payment page] Click by \[go to payment button]")]
        public void WhenClickByGoToPaymentButton() {
            Page.GoToPaymentButton.ClickAndWaitForRedirect();
        }

        [Given(@"\[Payment page] Mobile payment tab is selected")]
        public void GivenMobilePaymentIsSelected() {
            Page.OpenMobileTab();
        }

        [When(@"\[Payment page] Type '(.*)' in \[phone] field")]
        public void WhenTypeInPhoneField(string phone) {
            Page.MobileTab.Phone.TypeIn(phone);
        }
    }
}
﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Customers.how;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Customers.how_to_pay.popolnit_schet {
    [Binding]
    [Scope(Feature = "OnlinePayment")]

    public class OnlinePaymentPageSteps : BeelinePageSteps<OnlinePaymentPage> {

        [Given(@"\[Mobile payment page] Mobile tab is visible")]
        public void GivenMobilePaymentPageMobileTabIsVisible() {
            GoTo<OnlinePaymentPage>();
            Page.OpenMobileTab();
        }

        [Then(@"\[Mobile payment page] Instruction for mobile payment is displayed")]
        public void ThenMobilePaymentPageInstructionForMobilePaymentIsDisplayed() {
            Page.AssertMobilePaymentInstructionIsDisplayed();
        }

        [Given(@"\[Mobile payment page] Home tab is visible")]
        public void GivenMobilePaymentPageHomeTabIsVisible() {
            GoTo<OnlinePaymentPage>();
            Page.OpenHomeTab();
        }

        [Then(@"\[Mobile payment page] Instruction for home payment is displayed")]
        public void ThenMobilePaymentPageInstructionForHomePaymentIsDisplayed() {
            Page.AssertHomePaymentInstructionIsDisplayed();
        }

        [Given(@"\[Mobile payment page] Wifi tab is visible")]
        public void GivenMobilePaymentPageWifiTabIsVisible() {
            GoTo<OnlinePaymentPage>();
            Page.OpenWifiTab();
        }

        [Then(@"\[Mobile payment page] Instruction for wifi payment is displayed")]
        public void ThenMobilePaymentPageInstructionForWifiPaymentIsDisplayed() {
            Page.AssertWifiPaymentInstructionIsDisplayed();
        }
    }
}
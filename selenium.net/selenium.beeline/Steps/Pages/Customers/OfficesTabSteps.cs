﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using core.Extensions;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Customers.BeelineOnMap;
using selenium.beeline.Steps.Base;
using selenium.core.Exceptions;

namespace selenium.beeline.Steps.Pages.Customers {
    [Binding]
    [Scope(Feature = "FilterOffices")]
    [Scope(Feature = "OfficeLoad")]
    [Scope(Feature = "KnowHowOffices")]
    public class OfficesTabSteps : BeelinePageSteps<BeelineOnMapPage> {
        private OfficeDetails _currentListOffice;
        private IMapPoint _currentMapPoint;
        private List<string> _listOffices;

        [When(@"Offices list is visible")]
        public void WhenOfficesListIsVisible() {
            GoTo<BeelineOnMapPage>(ajaxInevitable: true);
            Wait.WhileElementVisible(".layout-grid");
            Page.OfficesTab.OpenOfficesList();
        }

        [When(@"Some office load chart opened")]
        public void WhenSomeOfficeLoadChartOpened() {
            WhenOfficesListIsVisible();
            _currentListOffice = Page.OfficesTab.OfficesList.FindRandom(office => office.LoadMarker.IsVisible());
            if (_currentListOffice == null)
                throw Throw.FrameworkException("В списке отсутствуют оффисы, содержащие маркер загрузки.");
            _currentListOffice.OpenLoadChart();
        }

        [Then(@"Sun icon is on the current hour")]
        public void ThenSunIconIsOnTheCurrentHour() {
            _currentListOffice.LoadChart.AssertSunIsUnder(DateTime.Now);
        }

        [Then(@"Slider is on the current hour")]
        public void ThenSliderIsOnTheCurrentHour() {
            _currentListOffice.LoadChart.AssertSelectedTimeIs(DateTime.Now);
        }

        [Then(@"Slider color match the office load mark")]
        public void ThenSliderColorMatchTheOfficeLoadMark() {
            EOfficeLoad markerLoad = _currentListOffice.LoadMarker.GetLoad();
            _currentListOffice.LoadChart.AssertTimeLoadIs(DateTime.Now, markerLoad);
        }

        [Given(@"Offices list is visible")]
        public void GivenOfficesListIsVisible() {
            GoTo<BeelineOnMapPage>(ajaxInevitable:true);
            Page.OfficesTab.OpenOfficesList();
        }

        [When(@"Search by '(.*)'")]
        public void WhenSearchByAddress(string address) {
            _listOffices = Page.OfficesTab.OfficesList.GetIds();
            Page.OfficesTab.Search.TypeInAndSelectFirst(address, true);
        }

        [Then(@"Offices list changed")]
        public void ThenOfficesListChanged() {
            List<string> current = Page.OfficesTab.OfficesList.GetIds();
            if (current.Count != _listOffices.Count)
                return;
            if (current.Any(office => !_listOffices.Contains(office)))
                return;
            Assert.Fail("Список оффисов не изменился");
        }

        [Then(@"Offices list is not empty")]
        public void ThenOfficesListIsNotEmpty() {
            List<string> current = Page.OfficesTab.OfficesList.GetIds();
            Assert.AreNotEqual(0, current.Count, "Список оффисов пуст");
        }

        [Given(@"Offices map is visible")]
        public void GivenOfficesMapIsVisible() {
            GoTo<BeelineOnMapPage>();
            Page.OfficesTab.OpenOfficesMap();
        }

        [Then(@"'(.*)' is displayed on map")]
        public void ThenTileIsDisplayedOnMap(string tile) {
            Page.OfficesTab.OfficesMap.AssertHasTile(tile);
        }

        [Then(@"'(.*)' contains office")]
        public void ThenTileContainsOffice(string tile) {
            Page.OfficesTab.OfficesMap.AssertTileContainsPoint(tile);
        }

        [When(@"Click by office on '(.*)'")]
        public void WhenClickByOfficeOnTile(string tile) {
            Page.OfficesTab.OfficesMap.ClickByPointOnTile(tile);
        }

        [Then(@"Office ballon is displayed")]
        public void ThenOfficeBallonIsDisplayed() {
            var officeBallon = Page.OfficesTab.OfficesMap.GetBalloon<OfficeDetailsBallon>();
            officeBallon.AssertVisible();
        }

        [Then(@"Load color match the marker text")]
        public void ThenOfficeLoadMatchTheQueueLength() {
            foreach (var office in Page.OfficesTab.OfficesList.GetItems()) {
                if (!office.LoadMarker.IsVisible())
                    continue;
                office.LoadMarker.AssertLoadColorMatchTheMarkerText();
            }
        }

        [Then(@"Offices with queue length are exist")]
        public void ThenOfficesWithQueueLengthAreExist() {
            Assert.True(Page.OfficesTab.OfficesList.GetItems().Any(o => o.LoadMarker.HasQueueLength()));
        }

        [Then(@"Offices with usual load are exist")]
        public void ThenOfficesWithUsualLoadAreExist() {
            Assert.True(Page.OfficesTab.OfficesList.GetItems().Any(o => o.LoadMarker.HasUsualLoad()));
        }

        [When(@"\[Beeline on map page] Check only Know How offices in filter")]
        public void WhenCheckOnlyKnowHowOfficesInFilter() {
            Page.OfficesTab.Filter.SelectOnly(OfficeFilterOption.knowhow);
        }

        [Then(@"\[Beeline on map page] Map contains Know How offices")]
        public void ThenContainsKnowHowOffices() {
            Page.OfficesTab.OfficesMap.AssertContainsKnowHowOffices();
        }

        [When(@"\[Beeline on map page] Open services list")]
        public void WhenOpenServicesList() {
            _currentListOffice = Page.OfficesTab.OfficesList.RandomItem();
            _currentListOffice.OpenServicesList();
        }

        [Then(@"\[Beeline on map page] Services list contains Know How service")]
        public void ThenServicesListContainsKnowHowService() {
            _currentListOffice.ServicesList.AssertContainsKnowHowService();
        }

        [When(@"\[Beeline on map page] Open office ballon")]
        public void WhenOpenOfficeBallon() {
            _currentMapPoint = Page.OfficesTab.OfficesMap.GetPoints<KnowHowMapPoint>().RandomItem();
            _currentMapPoint.OpenBalloon();
        }

        [Then(@"\[Beeline on map page] Office ballon has \[Share link]")]
        public void ThenOfficeBallonHasShareLink() {
            var ballon = Page.OfficesTab.OfficesMap.GetBalloon<OfficeDetailsBallon>();
            ballon.ShareLink.AssertIsVisible();
        }
    }
}
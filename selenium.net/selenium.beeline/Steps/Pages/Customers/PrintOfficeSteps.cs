﻿using System.Collections.Generic;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Customers.BeelineOnMap;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;
using selenium.core.Exceptions;
using selenium.core.Tests;

namespace selenium.beeline.Steps.Pages.Customers {
    [Binding]
    public class PrintOfficeSteps : BeelinePageSteps<PrintOfficeMapPage> {
        private static string _officeAddress;
        private static string _metroStation;
        private static string _findHint;
        private static List<NearestOffice> _nearestOffices;
        private static List<OfficeService> _services;

        [When(@"I am on print office map page")]
        public void WhenIAmOnPrintOfficeMapPage() {
            PreparePage(
                () => {
                    var beelineOnMapPage = GoTo<BeelineOnMapPage>(ajaxInevitable:true);
                    beelineOnMapPage.OfficesTab.OpenOfficesList();
                    OfficeDetails officeDetails =
                        beelineOnMapPage.OfficesTab.OfficesList.FindRandom(office => office.LoadMarker.IsVisible());
                    if (officeDetails==null)
                        throw Throw.FrameworkException("В списке отсутствуют оффисы, содержащие маркер загрузки.");
                    _officeAddress = officeDetails.GetAddress();
                    _metroStation = officeDetails.GetMetroStation();
                    _findHint = officeDetails.GetFindHint();
                    _nearestOffices = officeDetails.GetNearestOfficeAdresses();
                    _services = officeDetails.GetServices();
                    officeDetails.OpenOfficeMap();
                    officeDetails.OfficeMap.PrintLink.Click();
                });
        }

        [Then(@"map is visible")]
        public void ThenMapIsVisible() {
            Page.AssertMapIsVisible();
        }

        [Then(@"address is correct")]
        public void ThenAddressIsCorrect() {
            Page.AssertAddressIs(_officeAddress);
        }

        [Then(@"metro station is correct")]
        public void ThenMetroStationIsCorrect() {
            Page.AssertMetroStationIs(_metroStation);
        }

        [Then(@"find hint is correct")]
        public void ThenFindHintIsCorrect() {
            Page.AssertFindHintIs(_findHint);
        }

        [Then(@"nearest offices are correct")]
        public void ThenNearestOfficesAreCorrect() {
            Page.AssertNearestOfficesIs(_nearestOffices);
        }

        [Then(@"Page has valid print button")]
        public void ThenPageHasValidPrintButton() {
            TestHelper.AssertHasPrintAction(BeelineSeleniumContext.Inst, "span.button.sub.label");
        }

        [Then(@"Page has valid services list")]
        public void ThenPageHasValidServicesList() {
            Page.AssertServicesIs(_services);
        }
    }
}
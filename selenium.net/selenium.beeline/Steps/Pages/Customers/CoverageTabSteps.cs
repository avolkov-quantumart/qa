﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Customers.BeelineOnMap;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;

namespace selenium.beeline.Steps.Pages.Customers {
    [Binding]
    [Scope(Feature = "Coverage")]
    public class CoverageTabSteps : BeelinePageSteps<BeelineOnMapPage> {
        private int _freePointsCount;

        [Given(@"\[Coverage zones] tab is opened")]
        public void GivenCoverageZonesTabIsOpened() {
            GoTo<BeelineOnMapPage>(ajaxInevitable: true);
            Page.CoverageToggle.SelectAndWaitWhileAjax();
        }

        [When(@"Select 2G coverage")]
        public void WhenSelect2GCoverage() {
            Page.CoverageTab.Radio2G.Select(2000);
        }

        [When(@"Select 3G coverage")]
        public void WhenSelect3GCoverage() {
            Page.CoverageTab.Radio3G.Select(2000);
        }

        [When(@"Select 4G coverage")]
        public void WhenSelect4GCoverage() {
            Page.CoverageTab.Radio4G.Select(2000);
        }

        [Then(@"2G coverage percent greater than (.*)")]
        public void Then2GCoveragePersentGreaterThan(int coverage) {
            decimal actual = Page.CoverageTab.CoverageMap.Get2GCoveragePercent();
            Assert.Greater(actual, coverage, "Низкий процент покрытия");
        }

        [Then(@"3G coverage percent greater than (.*)")]
        public void Then3GCoveragePersentGreaterThan(int coverage) {
            decimal actual = Page.CoverageTab.CoverageMap.Get3GCoveragePercent();
            Assert.Greater(actual, coverage, "Низкий процент покрытия");
        }

        [Then(@"4G coverage percent greater than (.*)")]
        public void Then4GCoveragePercentGreaterThan(int coverage) {
            decimal actual = Page.CoverageTab.CoverageMap.Get4GCoveragePercent();
            Assert.Greater(actual, coverage, "Низкий процент покрытия");
        }

        [When(@"Select 3G metro coverage")]
        public void WhenSelect3GMetroCoverage() {
            Page.CoverageTab.RadioMetro.SelectAndWaitWhileAjaxRequests();
            Page.CoverageTab.Metro3GCoverageToggle.SelectAndWaitWhileAjax();
        }

        [When(@"Select 2G metro coverage")]
        public void WhenSelect2GMetroCoverage() {
            Page.CoverageTab.RadioMetro.SelectAndWaitWhileAjaxRequests();
            Page.CoverageTab.Metro2GCoverageToggle.SelectAndWaitWhileAjax();
        }

        [Then(@"3G metro coverage image is visible")]
        public void Then3GMetroCoverageImageIsVisible() {
            Page.CoverageTab.AssertMetro3GCoverageIsVisible();
        }

        [Then(@"2G metro coverage image is visible")]
        public void Then2GMetroCoverageImageIsVisible() {
            Page.CoverageTab.AssertMetro2GCoverageIsVisible();
        }

        [When(@"Select free wifi coverage")]
        public void WhenSelectFreeWifiCoverage() {
            Page.CoverageTab.RadioWifi.SelectAndWaitWhileAjaxRequests(2000);
            Page.CoverageTab.FreeWifiToggle.SelectAndWaitWhileAjax(1000, true);
            _freePointsCount = Page.CoverageTab.WifiMap.GetPointsCount<WifiPoint>();
        }

        [Then(@"Map contains more then (.*) wifi points")]
        public void ThenMapContainsMoreThenWifiPoints(int count) {
            Page.CoverageTab.WifiMap.AssertPointsCountGreaterThan(count);
        }

        [When(@"Click by wifi point")]
        public void WhenClickByWifiPoint() {
            Page.CoverageTab.WifiMap.ClickByRandomPoint<WifiPoint>();
        }

        [Then(@"Wifi baloon is displayed")]
        public void ThenWifiBaloonIsDisplayed() {
            Page.CoverageTab.WifiMap.AssertBaloonIsVisible();
        }

        [When(@"Select paid wifi coverage")]
        public void WhenSelectPaidWifiCoverage() {
            Page.CoverageTab.RadioWifi.SelectAndWaitWhileAjaxRequests();
            Page.CoverageTab.PaidWifiToggle.SelectAndWaitWhileAjax(1000, true);
        }

        [Then(@"Paid wifi points count more then free")]
        public void ThenPaidWifiPointsCountMoreTthenFree() {
            int current = Page.CoverageTab.WifiMap.GetPointsCount<WifiPoint>();
            Assert.Greater(current, _freePointsCount, "Количество wifi точек на карте не увеличилось");
        }
    }
}
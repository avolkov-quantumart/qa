﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using core.Extensions;
using selenium.beeline.Pages.Customers;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Customers {
    [Binding]
    public class SearchPageSteps : BeelinePageSteps<SearchPage> {
        private int _fullMatchIndex; // индекс первого результата, который не содержит все слова поисковой фразы

        private int _partialMatchIndex;
        // индекс первого результата, который не содержит хотя бы одного слова поисковой фразы

        private int _precizeMatchIndex;
        // индекс первого результата, который не содержит точное вхождение поисковой фразы

        private List<SearchResult> _resultsList;

        private List<SearchResult> ResultsList {
            get { return _resultsList ?? (_resultsList = Page.SearchResults.GetItems()); }
            set { _resultsList = value; }
        }

        [Then(@"Tabs '(.*)' are visible")]
        public void ThenTabsAreVisible(string tabsString) {
            string[] tabs = tabsString.Split(' ');
            foreach (string tab in tabs)
                Page.Categories.AssertTabIsVisible(tab);
        }

        [Then(@"All tabs contain results")]
        public void ThenAllTabsContainResults() {
            Page.Categories.AssertEachCategoryHasResults();
        }

        [Then(@"\[All] tab is selected")]
        public void ThenAllTabIsSelected() {
            Page.Categories.AssertTabIsSelected("Все");
        }

        [Then(@"\[All] tab results count is equal to sum count on other tabs")]
        public void ThenAllTabResultsCountIsEqualToSumCountOnOtherTabs() {
            int allCount = Page.Categories.GetTabResultsCount("Все");
            IEnumerable<string> otherTabNames = Page.Categories.GetTabNames().Where(
                n => string.Compare(n, "Все", StringComparison.OrdinalIgnoreCase) != 0);
            int otherCount = otherTabNames.Sum(n => Page.Categories.GetTabResultsCount(n));
            Assert.AreEqual(allCount, otherCount,
                            "Количество результатов на вкладке 'Все' не равно количеству результатов на остальных вкладках");
        }

        [Then(@"All results are from the same region")]
        public void ThenAllResultsAreFromTheSameRegion() {
            IEnumerable<string> regions = ResultsList.Select(r => r.GetRegion());
            Assert.AreEqual(1, regions.Distinct().Count(), "В результатах есть страницы из разных регионов: {0}",
                            regions.AsString());
        }

        [Then(@"First results contains phrase '(.*)'")]
        public void ThenFirstResultsContainsPhrase(string phrase) {
            _precizeMatchIndex = 0;
            for (; _precizeMatchIndex < ResultsList.Count; _precizeMatchIndex++) {
                if (!ResultsList[_precizeMatchIndex].ContainsPhrase(phrase))
                    break;
            }
        }

        [Then(@"Second results contains all words from '(.*)'")]
        public void ThenSecondResultsContainsAllWordsFrom(string phrase) {
            if (_precizeMatchIndex == ResultsList.Count)
                return; // Все результаты содержат точное вхождение поисковой фразы
            _fullMatchIndex = _precizeMatchIndex;
            for (; _fullMatchIndex < ResultsList.Count; _fullMatchIndex++) {
                SearchResult searchResult = ResultsList[_fullMatchIndex];
                Assert.False(searchResult.ContainsPhrase(phrase),
                             "Вторая группа содержит результат удовлетворяющий свойствам первой группы(точное вхождение поисковой фразы)");
                if (!searchResult.ContainsAllPhraseWords(phrase))
                    break;
            }
        }

        [Then(@"Third results contains one word from '(.*)'")]
        public void ThenThirdResultsContsinsOneWordFrom(string phrase) {
            if (_fullMatchIndex == ResultsList.Count)
                return; // Все результаты содержат все слова поисковой фразы
            _partialMatchIndex = _fullMatchIndex;
            for (; _partialMatchIndex < ResultsList.Count; _partialMatchIndex++) {
                SearchResult searchResult = ResultsList[_partialMatchIndex];
                Assert.False(searchResult.ContainsPhrase(phrase),
                             "Третья группа содержит результат удовлетворяющий свойствам первой группы(точное вхождение поисковой фразы)");
                Assert.False(searchResult.ContainsAllPhraseWords(phrase),
                             "Третья группа содержит результат удовлетворяющий свойствам второй группы(вхождение всех слов поисковой фразы)");
                if (!searchResult.ContainsSomePhraseWords(phrase))
                    break;
            }
        }

        [Then(@"No results wich does not contain words from '(.*)'")]
        public void ThenNoResultsWichDoesNotContainWordsFrom(string p0) {
            Assert.AreEqual(ResultsList.Count, _partialMatchIndex,
                            "В списке начиная с индекса {0} есть результаты не содержащие ни одного слова поисковой фразы",
                            _partialMatchIndex);
        }

        [Then(@"No result duplications")]
        public void ThenNoResultDuplications() {
            IEnumerable<string> ids = ResultsList.Select(r => r.ID);
            IEnumerable<string> distinctIds = ids.Distinct();
            Assert.AreEqual(ids.Count(), distinctIds.Count(), "Список содержит дублирующиеся результаты: {0}",
                            ids.Except(distinctIds).AsString());
        }

        [Then(@"Select random tab from '(.*)'")]
        public void ThenSelectRandomTabFrom(string tabs) {
            string random = tabs.Split(' ').ToList().RandomItem();
            Page.Categories.SelectTab(random);
            ResultsList = Page.SearchResults.GetItems();
        }

        [Then(@"Results category marker match to tab category")]
        public void ThenResultsCategoryMarkerMatchToTabCategory() {
            string currentCategory = Page.Categories.Selected;
            foreach (SearchResult result in ResultsList)
                Assert.Contains(currentCategory, result.GetTags(), "Результат {0} не принадлежит категории {1}",
                                result.ID, currentCategory);
        }

        [Then(@"Clients partition is selected")]
        public void ThenClientsPartitionIsSelected() {
            Page.ClientsPartitionToggle.AssertIsSelected();
        }

        [Then(@"All result links are on Client site")]
        public void ThenAllResultLinksAreOnClientSite() {
            foreach (SearchResult result in ResultsList)
                result.AssertOnClientSite();
        }

        [When(@"Select \[Buisness] partition")]
        public void WhenSelectBuisnessPartition() {
            Page.BuisnessToggle.SelectAndWaitWhileAjax();
            ResultsList = Page.SearchResults.GetItems();
        }

        [Then(@"All result links are to Buisness site")]
        public void ThenAllResultLinksAreToBuisnessSite() {
            foreach (SearchResult result in ResultsList)
                result.AssertOnBuisnessSite();
        }

        [When(@"Select \[Partners] partition")]
        public void WhenSelectPartnersPartition() {
            Page.PartnersPartitionToggle.Select();
            ResultsList = Page.SearchResults.GetItems();
        }

        [Then(@"All result links are to Partners site")]
        public void ThenAllResultLinksAreToPartnersSite() {
            foreach (SearchResult result in ResultsList)
                result.AssertOnPartnersSite();
        }

        [Given(@"\[search page] is visible")]
        public void GivenSearchPageIsVisible() {
            GoTo<SearchPage>();
        }

        [When(@"Set \[Search in all regions] checkbox")]
        public void WhenSetSearchInAllRegionsCheckbox() {
            Page.AllRegionsCheckbox.Select();
        }

        [When(@"\[search page] Search for '(.*)'")]
        public void WhenSearchPageSearchFor(string query) {
            Page.SearchFor(query);
            ResultsList = Page.SearchResults.GetItems();
        }

        [Then(@"Results list contains results for different regions")]
        public void ThenResultsListContainsResultsForDifferentRegions() {
            IEnumerable<string> regions = ResultsList.Select(r => r.GetRegion());
            Assert.AreNotEqual(1, regions.Distinct().Count(), "Все результаты из одного региона: {0}",
                               regions.First());
        }
    }
}
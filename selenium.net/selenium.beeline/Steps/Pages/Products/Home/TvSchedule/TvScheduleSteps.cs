﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Base.Tariffs;
using selenium.beeline.Pages.Products.Home.Tariffs.Tv;
using selenium.beeline.Pages.Products.Home.TvSchedule;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;

namespace selenium.beeline.Steps.Pages.Products.Home.TvSchedule {
    [Binding]
    [Scope(Feature = "TvSchedule")]
    [Scope(Feature = "TariffTvSchedule")]
    public class TvScheduleSteps : BeelineSteps {
        private string _category;
        private DateTime _date;
        private TvDayTime.EDayTime _dayTime;
        private Dictionary<string, List<TvProgram>> _schedule;

        public TvScheduleFilter Filter {
            get { return Browser.State.PageAs<IHasTvSchedule>().Filter; }
        }

        public TvScheduleComponent Schedule {
            get { return Browser.State.PageAs<IHasTvSchedule>().Schedule; }
        }

        [Given(@"\[Tv Schedule] on \[Tv tariff] page is visible")]
        public void GivenTvScheduleOnTvTariffPageIsVisible() {
            OpenTvScheduleOnTariffPage();
        }

        [Given(@"\[Tv Schedule filter] has more then one genre")]
        public void GivenTvScheduleFilterHasMoreThenOneGenre() {
            while (Filter.Genres.GetGenres().Count < 2)
                OpenTvScheduleOnTariffPage();
        }

        private void OpenTvScheduleOnTariffPage() {
            GoTo<TvTariffsPage>();
            var tvTariffsPage = Browser.State.PageAs<TvTariffsPage>();
            var excluded = new List<string>();
            do {
                ITariff randomTariff = tvTariffsPage.Tariffs.GoToRandomTariffDetails(excluded);
                var tvTariffPage = Browser.State.PageAs<TvTariffPage>();
                if (!tvTariffPage.TvScheduleButton.IsVisible())
                    Browser.Go.Back();
                else {
                    tvTariffPage.OpenTvSchedule();
                    break;
                }
                excluded.Add(randomTariff.ID);
            } while (true);
        }

        [Given(@"\[TV schedule] page is opened")]
        public void GivenTVSchedulePageIsOpened() {
            GoTo<TvSchedulePage>();
            Filter.Date.SelectToday();  // Иначе программа может быть недоступна
        }

        [When(@"Type in '(.*)' in search field")]
        public void WhenTypeInInSearchField(string query) {
            Filter.Genres.DeselectAll();
            Filter.TypeInSearchQuery(query);
        }

        [When(@"Select random category")]
        public void WhenSelectRandomCategory() {
            _category = Filter.Genres.SelectRandom();
            _schedule = Schedule.GetData();
        }

        [When(@"Select random date")]
        public void WhenSelectRandomDate() {
            _schedule = SelectDateWithAvailableSchedule(out _date);
        }

        /// <summary>
        /// Выбрать дату для которой доступна программа
        /// </summary>
        private Dictionary<string, List<TvProgram>> SelectDateWithAvailableSchedule(out DateTime date,
                                                                                    params DateTime[] exclude) {
            List<DateTime> excludeList = exclude.ToList();
            Dictionary<string, List<TvProgram>> schedule;
            do {
                date = Filter.Date.SelectRandom();
                schedule = Schedule.GetData();
                excludeList.Add(date);
            } while (schedule.Values.All(c => c.Count == 0) && excludeList.Count < 7);
            return schedule;
        }

        [When(@"Select random time")]
        public void WhenSelectRandomTime() {
            _dayTime = Filter.DayTime.SelectRandom();
            _schedule = Schedule.GetData();
        }

        [Then(@"At least one program contains text '(.*)'")]
        public void ThenAtLeastOneProgramContainsText(string searchQuery) {
            Schedule.AssertProgramsFilteredBy(searchQuery);
        }

        [When(@"Select another category")]
        public void WhenSelectAnotherCategory() {
            Filter.Genres.SelectRandom(_category);
        }

        [Then(@"Channels are changed")]
        public void ThenChannelsAreChanged() {
            Dictionary<string, List<TvProgram>> schedule2 = Schedule.GetData();
            if (_schedule.Count != schedule2.Count)
                return; // Количество каналов в расписании изменилось
            if (_schedule.Keys.Any(channelName => !schedule2.ContainsKey(channelName)))
                return; // Изменился список каналов в расписании
            Assert.Fail("Список каналов в расписании не изменился");
        }

        [Then(@"Programs list changed")]
        public void ThenProgramsListChanged() {
            Dictionary<string, List<TvProgram>> schedule2 = Schedule.GetData();
            Assert.AreEqual(_schedule.Count, schedule2.Count,
                            "Количество каналов в расписании не должно было измениться");
            foreach (string channelName in _schedule.Keys) {
                if (!schedule2.ContainsKey(channelName))
                    Assert.Fail("Список каналов не должен был измениться");
                List<TvProgram> tvPrograms = _schedule[channelName];
                List<TvProgram> tvPrograms2 = schedule2[channelName];
                if (tvPrograms.Count != tvPrograms2.Count)
                    return; // Количество программ изменилось
                for (int i = 0; i < tvPrograms.Count; i++) {
                    if (tvPrograms[i].Time != tvPrograms2[i].Time ||
                        tvPrograms[i].Name != tvPrograms2[i].Name)
                        return; // Состав программ в расписании изменился
                }
            }
            Assert.Fail("Расписание не изменилось");
        }

        [When(@"Select another date")]
        public void WhenSelectAnotherDate() {
            Filter.Date.SelectRandom(_date);
        }

        [Then(@"Displayed schedule for selected time")]
        public void ThenDisplayedScheduleForSelectedTime() {
            Schedule.AssertDayTimeIs(_dayTime);
        }
    }
}
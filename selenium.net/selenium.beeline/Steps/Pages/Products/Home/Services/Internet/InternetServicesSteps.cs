﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Home.Services.Internet;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Home.Services.Internet {

    [Binding]
    [Scope(Feature = "InternetService")]
    public class InternetServicesSteps : BeelinePageSteps<InternetServicesPage> {

        [Then(@"\[Services list] is visible")]
        public void ThenServicesListIsVisible() {
            GoTo<InternetServicesPage>();
            Page.Services.IsVisible();
        }

        [When(@"\[Services list] select random service")]
        public void WhenServicesListSelectRandomService() {
            Page.Services.GoToRandomTariffDetails();
        }

    }
}

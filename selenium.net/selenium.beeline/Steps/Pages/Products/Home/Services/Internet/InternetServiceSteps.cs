﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Home.Services.Internet;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Home.Services.Internet {

    [Binding]
    [Scope(Feature = "InternetService")]
    public class InternetServiceSteps : BeelinePageSteps<InternetServicePage> {

        [Then(@"\[Service details page] is visible")]
        public void ThenServiceDetailsPageIsVisible() {
            Page.ServiceDetails.IsVisible();
        }

        [When(@"\[Service details page] click by \[ToBuyButton]")]
        public void WhenServiceDetailsPageClickByToBuyButton() {
            if (Page.BillWidget.ToBuyButton.IsVisible()) {
                Page.BillWidget.ToBuyButton.ClickAndWaitWhileAjax(true);
            } else {
                Page.ClickByToBuyButton();
            }
        }

        [When(@"\[Service details page] click by \[OrderButton]")]
        public void WhenServiceDetailsPageClickByOrderButton() {
            FD.service = Page.ServiceDetails.Name;
            Page.BasketWidget.OrderButton.ClickAndWaitWhileAjax(true);
        }

    }
}

using TechTalk.SpecFlow;
using selenium.beeline.Pages.Base.Tariffs;
using selenium.beeline.Pages.Products.Home.Tariffs;
using selenium.beeline.Pages.Products.Home.Tariffs.Internet;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Home {
    [Binding]
    [Scope]
    public class ChangeTariffSteps : BeelinePageSteps<InternetTariffsPage> {
        private string _firstTariffName;
        private string _secondTariffName;

        [Given(@"Compare tariffs alert is visible")]
        public void GivenCompareTariffsAlertIsVisible() {
            Browser.Go.ToPage<InternetTariffsPage>();
            ITariff tariff = Page.Tariffs.GoToRandomTariffDetails();
            _firstTariffName = tariff.Name;
            var tariffPage = Browser.State.PageAs<InternetTariffPage>();
            tariffPage.AddTariffToBusket("���������", "52");
            tariffPage = tariffPage.GoToOtherTariffOfSameType();
            _secondTariffName = tariffPage.TariffDetails.Name;
            tariffPage.OpenCompareTariffsAlert();
        }

        [When(@"I select new tariff")]
        public void WhenISelectNewTariff() {
            Browser.State.HtmlAlertAs<CompareTariffsAlert>().SelectAnotherTariff(_firstTariffName);
        }

        [Then(@"Tariff in basket changed")]
        public void ThenTariffInBasketChanged() {
            Browser.State.PageAs<InternetTariffPage>().Basket.AssertTariffIs(_secondTariffName, "����� �� ���������");
        }
    }
}
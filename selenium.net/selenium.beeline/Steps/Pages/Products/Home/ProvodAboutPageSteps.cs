﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using core.Extensions;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Products.Home;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Home {
    [Binding]
    [Scope(Feature = "NavigateToTariff")]
    public class ProvodAboutPageSteps : BeelinePageSteps<ProvodAboutPage> {
        [Given(@"I am on ProvodAboutPage")]
        public void GivenIAmOnProvodAboutPage() {
            GoTo<ProvodAboutPage>();
        }

        [Given(@"I click by connect button for some tariff")]
        public void GivenIClickByConnectButtonForSomeTariff() {
            ProvodTariff provodTariff = Page.Tariffs.GetItems().RandomItem();
            Log.WriteValue("currenttariff", provodTariff.GetTariffName());
            provodTariff.ConnectButton.ClickAndWaitForRedirect();
        }

        [Then(@"I go to details page of tariff")]
        public void ThenIGoToDetailsPageOfTariff() {
            Assert.IsTrue(Browser.State.PageIs<TariffPageBase>(), "Не перешли на страницу описание тарифа");
        }

        [Then(@"tariff name is correct")]
        public void ThenTariffNameIsCorrect() {
            var tariffPage = Browser.State.PageAs<TariffPageBase>();
            tariffPage.AssertTariffNameIs(Log.GetValue<string>("currenttariff"));
        }
    }
}
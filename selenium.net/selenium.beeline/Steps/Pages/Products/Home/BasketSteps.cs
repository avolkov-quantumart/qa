﻿using System;
using System.Threading;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Home {

    [Binding]
    [Scope(Feature = "TariffKit")]
    [Scope(Feature = "TvTariff")]
    [Scope(Feature = "InternetTariff")]
    [Scope(Feature = "InternetService")]
    class BasketSteps : BeelinePageSteps<BasketPage> {

        private string apartmentNumber;

        [Then(@"\[Products list tab] is visible")]
        public void WhenProductsListTabIsVisible() {
            Page.AssertTabIsVisible("Список покупок");
        }

        [Then(@"\[Products list tab] tariff is correct")]
        public void ThenProductsListTabTariffIsCorrect() {
            Page.AssertTariffIsCorrect();
        }

        [Then(@"\[Products list tab] service is correct")]
        public void ThenProductsListTabServiceIsCorrect() {
            Page.AssertServiceIsCorrect();
        }

        [When(@"\[Products list tab] click by \[RentTvConsoleRadioButton]")]
        public void WhenProductsListTabClickByRentTvConsoleRadioButton() {
            Page.ClickByRadioButton();
        }
    
        [When(@"\[Products list tab] click by \[ContactDetailsButton]")]
        public void WhenProductsListTabClickByContactDetailsButton() {
            Page.ContactDetailsButton.ClickAndWaitWhileAjax(true);
        }

        [Then(@"\[Contact info tab] is visible")]
        public void WhenContactInfoTabIsVisible() {
            Page.AssertTabIsVisible("Контактные данные");
        }

        [When(@"\[Contact info tab] type '(.*)' in \[username] field")]
        public void WhenContactInfoTabTypeInUsernameField(string userName) {
            Page.UserNameInput.TypeIn(userName);
        }

        [When(@"\[Contact info tab] type '(.*)' in \[emailaddress] field")]
        public void WhenContactInfoTabTypeInEmailaddressField(string emailAddress) {
            Page.EmailAddressInput.TypeIn(emailAddress);
        }

        [When(@"\[Contact info tab] type '(.*)' in \[phonenumber] field")]
        public void WhenContactInfoTabTypeInPhonenumberField(string phoneNumber) {
            Page.PhoneNumberInput.TypeIn(phoneNumber);
        }

        [When(@"\[Contact info tab] generate and type apartment number in \[apartmentnumber] field")]
        public void WhenContactInfoTabGenerateAndTypeApartmentNumberInApartmentnumberField() {
            do {
                GenerateRandomApartmentNumber();
                if (!Page.WrongApartmentWebText.IsVisible()) {
                    break;
                }
            } while (true);
            Page.ApartmentInput.TypeIn(apartmentNumber);
        }

        [When(@"\[Contact info tab] click by \[ConfirmButton]")]
        public void WhenContactInfoTabClickByConfirmButton() {
            do {
                Thread.Sleep(100);
                if (Page.ConfirmButton.IsEnabled()) {
                    break;
                }
            } while (true);
            Page.ConfirmButton.Click();
        }

        [Then(@"\[Confirm tab] is visible")]
        public void ThenConfirmTabIsVisible() {
            Page.AssertTabIsVisible("Подтверждение заказа");
        }

        [Then(@"\[Confirm tab] tariff is correct")]
        public void ThenConfirmTabTariffIsCorrect() {
            Page.AssertTariffIsCorrect();
        }

        [Then(@"\[Confirm tab] service is correct")]
        public void ThenConfirmTabServiceIsCorrect() {
            Page.AssertServiceIsCorrect();
        }

        [Then(@"\[Confirm tab] name is '(.*)', email is '(.*)', phone is '(.*)'")]
        public void ThenConfirmTabNameIsEmailIsPhoneIs(string userName, string userEmail, string userPhone) {
            Page.AssertUserDetailsIs(userName, userEmail, userPhone);
        }

        [Then(@"\[Confirm tab] address is '(.*)','(.*)'")]
        public void ThenConfirmTabAddressIs(string streetName, string houseNumber) {
            Page.AssertAddressIs(streetName, houseNumber, apartmentNumber);
        }

        [When(@"\[Confirm tab] click by \[MakeOrderButton]")]
        public void WhenConfirmTabClickByMakeOrderButton() {
            Page.MakeOrderButton.Click();
        }

        [Then(@"\[Connection success page] is displayed")]
        public void ThenConnectionSuccessPageIsDisplayed() {
            Page.HomeRequestResultForm.IsVisible();
        }

        private void GenerateRandomApartmentNumber() {
            Random rand = new Random();
            apartmentNumber = rand.Next(30000).ToString();
        }
    }
}

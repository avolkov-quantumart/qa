﻿using NUnit.Framework;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Products.Home;
using selenium.beeline.Pages.Products.Home.Tariffs.Kit;
using selenium.beeline.Steps.Pages.Base;

namespace selenium.beeline.Steps.Pages.Home {
    [TestFixture]
    public class TariffAccessibilityForAddress : BeelineTestBase<KitTariffsPage> {
        [SetUp]
        public void TestSetUp() {
            Page = Browser.Go.ToPage<KitTariffsPage>();
        }

        [Test]
        public void TariffInaccessible() {
            // .Arrange
            CheckConnectionAlert alert = Page.TariffAccessibilityWidget.OpenCheckTariffAccessibilityAlert();
            // .Act
            alert.FillForm("лен", "1");
            // .Assert
            alert.AssertTariffInaccessible();
        }

        [Test]
        public void TariffAccessible()
        {
            // .Arrange
            CheckConnectionAlert alert = Page.TariffAccessibilityWidget.OpenCheckTariffAccessibilityAlert();
            // .Act
            alert.FillForm("мит", "52");
            // .Assert
            alert.AssertTariffAccessible();
        }
    }
}
﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Home.Tariffs.Tv;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Home.Tariffs.Tv {
    [Binding]
    [Scope(Feature = "TvTariff")]
    public class TvTariffsSteps : BeelinePageSteps<TvTariffsPage> {

        [Given(@"\[Tariff list] is visible")]
        public void GivenTariffListIsVisible() {
            GoTo<TvTariffsPage>();
            Page.Tariffs.IsVisible();
        }

        [When(@"\[Tariff list] select random tariff")]
        public void WhenTariffListSelectRandomTvTariff() {
            Page.Tariffs.GoToRandomTariffDetails();
        }
    }
}

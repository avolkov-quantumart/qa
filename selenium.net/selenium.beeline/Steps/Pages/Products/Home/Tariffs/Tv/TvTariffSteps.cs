﻿using System.Collections.Generic;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Home.Tariffs.Tv;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Home.Tariffs.Tv {
    [Binding]
    [Scope(Feature = "TvTariff")]
    public class TvTariffSteps : BeelinePageSteps<TvTariffPage>{

        private static List<string> _excludedTariffs;

        [Then(@"\[Tariff details page] is visible")]
        public void ThenTariffDetailsPageIsVisible() {
            Page.TariffDetails.IsVisible();
            _excludedTariffs = new List<string>();
        }

        [When(@"\[Tariff details page] click by \[ToBuyButton]")]
        public void WhenTariffDetailsPageClickByToBuyButton() {
            Page.ClickByToBuyButton(_excludedTariffs);
        }

        [Then(@"\[Check connection alert] is visible")]
        public void ThenCheckConnectionAlertIsVisible() {
            if (!Page.AccessibilityWidget.IsAddressSelected()) {
                Page.CheckConnectionAlert.IsVisible();
            }
        }

        [When(@"\[Check connection alert] type '(.*)' '(.*)' in field and accept")]
        public void WhenCheckConnectionAlertTypeInFieldAndAccept(string street, string house)
        {
            if (!Page.AccessibilityWidget.IsAddressSelected()) {
                Page.CheckConnectionAlert.SetValidAddress(street, house);
            }
        }

        [When(@"\[Basket widget] click by \[OrderButton]")]
        public void WhenBasketWidgetClickByOrderButton() {
            Page.ClickByOrderButton(_excludedTariffs);
        }
    }
}

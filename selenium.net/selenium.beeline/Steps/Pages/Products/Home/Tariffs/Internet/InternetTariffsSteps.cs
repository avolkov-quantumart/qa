﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Home.Tariffs.Internet;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Home.Tariffs.Internet {

    [Binding]
    [Scope(Feature = "InternetTariff")]
    [Scope(Feature = "InternetService")]
    public class InternetTariffsSteps : BeelinePageSteps<InternetTariffsPage> {

        [Given(@"\[Tariff list] is visible")]
        public void GivenTariffListIsVisible() {
            GoTo<InternetTariffsPage>();
            Page.Tariffs.IsVisible();
        }

        [When(@"\[Tariff list] select random tariff")]
        public void WhenTariffListSelectRandomTariffKit() {
            Page.Tariffs.GoToRandomTariffDetails();
        }
    }
}

﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Home.Tariffs.Internet;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Home.Tariffs.Internet {
    [Binding]
    [Scope(Feature = "InternetTariff")]
    [Scope(Feature = "InternetService")]
    public class InternetTariffSteps : BeelinePageSteps<InternetTariffPage> {

        [Then(@"\[Tariff details page] is visible")]
        public void ThenTariffDetailsPageIsVisible() {
            Page.TariffDetails.IsVisible();
            FD.tariff = Page.TariffDetails.Name;
        }

        [When(@"\[Tariff details page] click by \[ToBuyButton]")]
        public void WhenTariffDetailsPageClickByToBuyButton()
        {
            Page.BillWidget.ToBuyButton.ClickAndWaitWhileAjax(true);
        }

        [Then(@"\[Check connection alert] is visible")]
        public void ThenCheckConnectionAlertIsVisible() {
            if (!Page.AccessibilityWidget.IsAddressSelected()) {
                Page.CheckConnectionAlert.IsVisible();
            }
        }

        [When(@"\[Check connection alert] type '(.*)' '(.*)' in field and accept")]
        public void WhenCheckConnectionAlertTypeInFieldAndAccept(string streetName, string houseNumber) {
            if (!Page.AccessibilityWidget.IsAddressSelected()) {
                Page.CheckConnectionAlert.SetValidAddress(streetName, houseNumber);
            }
        }

        [When(@"\[Basket widget] click by \[OrderButton]")]
        public void WhenBasketWidgetClickByOrderButton() {
            Page.BasketWidget.OrderButton.ClickAndWaitWhileAjax(true);
        }

    }
}

﻿using System.Collections.Generic;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Home.Tariffs.Kit;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Home.Tariffs.Kit {
    [Binding]
    [Scope(Feature = "TariffKit")]
    public class KitTariffSteps : BeelinePageSteps<KitTariffPage> {

        private static List<string> _excludedTariffs;

        [Then(@"\[Tariff details page] is visible")]
        public void ThenTariffDetailsPageIsVisible() {
            Page.TariffDetails.IsVisible();
            _excludedTariffs = new List<string>();
        }

        [When(@"\[Tariff details page] click by \[ToBuyButton]")]
        public void WhenTariffDetailsPageClickByToBuyButton() {
            if (Page.BillWidget.ToBuyButton.IsVisible()) {;
                Page.BillWidget.ToBuyButton.Click();
            }
        }

        [Then(@"\[Check connection alert] is visible")]
        public void ThenCheckConnectionAlertIsVisible() {
            if (!Page.AccessibilityWidget.IsAddressSelected()) {
                Page.CheckConnectionAlert.IsVisible();         
            }
        }

        [When(@"\[Check connection alert] type '(.*)' '(.*)' in field and accept")]
        public void WhenCheckConnectionAlertTypeInFieldAndAccept(string streetName, string houseNumber) {
            if (!Page.AccessibilityWidget.IsAddressSelected()) {
                Page.CheckConnectionAlert.SetValidAddress(streetName, houseNumber);    
            }
        }

        [When(@"\[Basket widget] click by \[OrderButton]")]
        public void WhenBasketWidgetClickByOrderButton() {
            Page.ClickByOrderButton(_excludedTariffs);
        }
    }
}
﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Home.Tariffs.Kit;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Home.Tariffs.Kit {
    [Binding]
    [Scope(Feature = "TariffKit")]
    public class KitTariffsSteps : BeelinePageSteps<KitTariffsPage> {

        [Given(@"\[Tariff list] is visible")]
        public void GivenTariffListIsVisible() {
            GoTo<KitTariffsPage>();
            Page.Tariffs.IsVisible();
        }

        [When(@"\[Tariff list] select random kit")]
        public void WhenTariffListSelectRandomTariffKit() {
            Page.Tariffs.GoToRandomTariffDetails();
        }

    }
}

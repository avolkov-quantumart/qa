﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Products;
using selenium.beeline.Pages.Products.Mobile;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;
using selenium.beeline.Steps.Pages.Help.Mobile;
using selenium.core.Exceptions;

namespace selenium.beeline.Steps.Pages.Products {
    [Binding]
    [Scope(Feature = "SwitchLanguage")]
    public class SwitchLanguageSteps : BeelinePageSteps<BeelinePageBase> {
        [Given(@"I am on any Beeline page")]
        public void GivenIAmOnAnyBeelinePage() {
            Browser.Go.ToPage<ProductsPage>();
        }

        [Given(@"Current language is not English")]
        public void GivenCurrentLanguageIsNotEnglish() {
            if (Page.Header.GetCultureLanguage() == SiteLanguage.English)
                throw Throw.TestException("Язык сайта изначально английский");
        }

        [When(@"I change language to English")]
        public void WhenIChangeLanguageToEnglish() {
            LanguageSelector languageSelector = Page.Header.OpenLanguageSelector();
            languageSelector.Select(SiteLanguage.English);
        }

        [Then(@"Current language is English")]
        public void ThenCurrentLanguageIsEnglish() {
            Assert.IsTrue(Browser.State.PageIs<ForGuestsPage>(), "Не перешли на страницу For guests");
            Page.AssertLanguage(SiteLanguage.English);
        }
    }
}

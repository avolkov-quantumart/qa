﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;

namespace selenium.beeline.Steps.Pages.Products {
    [Binding]
    public class ProductsPageSteps : BeelinePageSteps<ProductsPage> {
        [Given(@"I am on ProductsPage")]
        public void GivenIAmOnProductsPage() {
            GoTo<ProductsPage>();
        }

        [When(@"I click by current region")]
        public void WhenIClickByCurrentRegion() {
            Page.Header.CurrentRegion.Click();
        }

        [Then(@"Regions selector is visibe")]
        public void ThenRegionsSelectorIsVisibe() {
            Page.Header.RegionSelector.AssertVisible();
        }

        [Given(@"Regions selector opened")]
        public void GivenRegionsSelectorOpened() {
            Page.Header.OpenRegionsSelector();
        }

        [When(@"I type in '(.*)' in search field")]
        public void WhenITypeInInSearchField(string region) {
            Page.Header.RegionSelector.Search.TypeIn(region);
        }

        [Then(@"Drop down list contains '(.*)'")]
        public void ThenDropDownListContains(string region) {
            Page.Header.RegionSelector.DropList.WaitForVisible();
            Page.Header.RegionSelector.DropList.AssertContains(region);
        }

        [Given(@"I type in '(.*)' in search field")]
        public void GivenITypeInInSearchField(string value) {
            Page.Header.RegionSelector.Search.TypeIn(value);
        }

        [When(@"I select '(.*)' from drop down list")]
        public void WhenISelectFromDropDownList(string item) {
            Page.Header.RegionSelector.DropList.Select(item);
            Browser.State.PageAs<ProductsPage>();
        }

        [Then(@"I am on ProductsPage")]
        public void ThenIAmOnProductsPage() {
            Assert.IsTrue(Browser.State.PageIs<ProductsPage>());
        }

        [Then(@"Subdomain is '(.*)'")]
        public void ThenSubdomainIs(string subdomain) {
            Assert.AreEqual(Page.BaseUrlInfo.SubDomain, subdomain, "Поддомен страницы не соответствует требуемому");
        }

        [Then(@"Current region is '(.*)'")]
        public void ThenCurrentRegionIs(string region) {
            Page.AssertRegion(region);
        }

        [Then(@"And carousel is visible")]
        public void ThenAndCarouselIsVisible() {
            Page.Carousel.AssertVisible();
        }
    }
}
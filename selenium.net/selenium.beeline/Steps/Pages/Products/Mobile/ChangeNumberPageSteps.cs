﻿using System.Collections.Generic;
using NUnit.Framework;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Mobile;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;
using selenium.beeline.Steps.Pages.Help.Mobile;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Steps.Pages.Mobile {
    [Binding]
    [Scope(Feature = "ChangeNumber")]
    [Scope(Feature = "Кнопка \"Далее к смене номера\" в контенте страницы смены номера")]
    [Scope(Feature = "TableWithSuggestedNumbers")]
    public class ChangeNumberPageSteps : BeelinePageSteps<ChangeNumberPage> {
        [When(@"I navigate on ChangeNumberPage")]
        public void WhenINavigateOnChangeNumberPage() {
            GoTo<ChangeNumberPage>(update: true, waitForAjax: true);
        }

        [Then(@"All fields are empty")]
        public void ThenAllFieldsAreEmpty() {
            Page.SourceData.PhoneNumber.AssertIsEmpty();
        }

        [Given(@"I am on ChangeNumberPage")]
        public void GivenIAmOnChangeNumberPage() {
            GoTo<ChangeNumberPage>(waitForAjax:true);
        }

        [StepDefinition(@"I typed in some Beeline number")]
        public void ITypedInSomeBeelineNumber() {
            Page.SourceData.PhoneNumber.FillWithRandomBeelineNumber();
        }

        [Then(@"SHOW NUMBERS button is enabled")]
        public void ThenSHOWNUMBERSButtonIsEnabled() {
            Page.SourceData.ShowNumbersButton.AssertEnabled();
        }

        [When(@"I clear number field")]
        public void WhenIClearNumberField() {
            Page.SourceData.PhoneNumber.Clear();
            Browser.Action.ChangeFocus();
        }

        [Then(@"Empty phone number validation hint is shown")]
        public void ThenEmptyPhoneNumberValidationHintIsShown() {
            Page.SourceData.PhoneNumber.ValidationHint.AssertPhoneIsEmptyVisible();
        }

        [When(@"I click by SHOW NUMBERS button")]
        public void WhenIClickBySHOWNUMBERSButton() {
            Page.SourceData.ShowNumbersButton.ClickAndWaitWhileAjax();
        }

        [Then(@"Table with suggested numbers is visible")]
        public void ThenTableWithSuggestedNumbersIsVisible() {
            Page.PhonesGrid.AssertVisible();
        }

        [Given(@"Go to table with suggested numbers")]
        public void GivenGoToTableWithSuggestedNumbers() {
            GoTo<ChangeNumberPage>(waitForAjax: true);
            if (Page.SourceData.IsVisible()) {
                Page.SourceData.PhoneNumber.FillWithRandomBeelineNumber();
                Page.SourceData.ShowNumbersButton.ClickAndWaitWhileAjax();
            }
        }

        [Then(@"Each column has at least one number")]
        public void ThenEachColumnHasAtLeastOneNumber() {
            Page.PhonesGrid.AssertIsNotEmpty();
        }

        [Then(@"Tab '(.*)' is visible")]
        public void ThenTabIsVisible(string tabName) {
            Page.Tabs.AssertTabIsVisible(tabName);
        }

        [Then(@"Tab '(.*)' is selected")]
        public void ThenTabIsSelected(string tabName)
        {
            Page.Tabs.AssertTabIsSelected(tabName);
        }

        [Then(@"Current user number is the same as user typed in")]
        public void ThenCurrentUserNumberIsTheSameAsUserTypedIn() {
            Page.UserNumber.AssertEqual(Log.GetValue<string>("userphone"));
        }

        [When(@"Refresh page")]
        public void WhenRefreshPage()
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"Table with suggested numbers is visible")]
        public void GivenTableWithSuggestedNumbersIsVisible() {
            GoTo<ChangeNumberPage>(update: true, waitForAjax: true);
            Page.OpenTableWithSuggestedNumbers();
        }

        [Given(@"Table with suggested numbers has selected number")]
        public void GivenTableWithSuggestedNumbersHasSelectedNumber() {
            if(!Page.PhonesGrid.HasSelectedNumber())
                Page.PhonesGrid.SelectRandomNumber();
        }

        [When(@"I select some number from table")]
        public void WhenISelectSomeNumberFromTable() {
            Page.PhonesGrid.SelectRandomNumber();
        }

        [Then(@"Summary widget contains params of selected number")]
        public void ThenSummaryWidgetContainsParamsOfSelectedNumber() {
            Page.SummaryWidget.AssertNewNumberIs(Page.PhonesGrid.GetSelectedNumber());
            Page.SummaryWidget.AssertPriceIs(Page.PhonesGrid.GetPriceOfSelectedNumber());
        }

        [When(@"Click by SHOW MORE button")]
        public void WhenClickBySHOWMOREButton() {
            Log.WriteValue("currentphones", Page.PhonesGrid.GetPhones());
            Page.ShowMoreButton.ClickAndWaitWhileAjax();
        }

        [Then(@"New numbers added to table")]
        public void ThenNewNumbersAddedToTable() {
            List<string> newPhones = Page.PhonesGrid.GetPhones();
            var oldPhones = Log.GetValue<List<string>>("currentphones");
            Assert.IsTrue(newPhones.Count > oldPhones.Count, "Количество номеров в таблице не увеличилось");
        }

        [When(@"I click by FURTHER TO NUMBER CHANGING button")]
        public void WhenIClickByFURTHERTONUMBERCHANGINGButton() {
            ((WebButton) Page.SummaryWidget.ChangeNumberButton).ClickAndWaitWhileAjax();
        }

        [Then(@"GET CONFIRMATION CODE window shown")]
        public void ThenGETCONFIRMATIONCODEWindowShown() {
            Assert.IsNotNull(Browser.State.HtmlAlertAs<GetConfirmationCodeAlert>(),
                             "Окно отправки кода подтверждения для смены номера не отобразилось");
        }

        [Then(@"GET CONFIRMATION CODE has correct number")]
        public void ThenGETCONFIRMATIONCODEHasCorrectNumber() {
            Page.GetConfirmationCodeAlert.AssertNewNumberIs(Log.GetValue<string>("selectedphone"));
            Page.GetConfirmationCodeAlert.AssertCurrentNumberIs(Log.GetValue<string>("userphone"));
        }

        [Then(@"GET CONFIRMATION CODE has correct price")]
        public void ThenGETCONFIRMATIONCODEHasCorrectPrice() {
            Page.GetConfirmationCodeAlert.AssertPriceIs(Page.PhonesGrid.GetPriceOfSelectedNumber());
        }
    }
}
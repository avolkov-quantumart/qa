﻿using System.Threading;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Mobile.Services.Details;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Mobile.Services.Details {
    [Binding]
    [Scope(Feature = "SendSms")]
    public class SendSmsPageSteps : BeelinePageSteps<SendSmsPage> {
        [Given(@"\[Send sms page] is visible")]
        public void GivenSendSmsPageIsVisible() {
            GoTo<SendSmsPage>();
        }

        [When(@"\[Send sms page] Type in '(.*)' in \[phone] field")]
        public void WhenTypeInInPhoneField(string phone) {
            Page.SendSmsComponent.Phone.TypeIn(phone);
        }

        [When(@"\[Send sms page] Type in '(.*)' in \[body] field")]
        public void WhenTypeInInBodyField(string body) {
            Page.SendSmsComponent.SmsBody.TypeIn(body);
        }

        [When(@"\[Send sms page] Type in captcha")]
        public void WhenTypeInCaptcha() {
            Page.SendSmsComponent.Captcha.SetDefault();
        }

        [When(@"\[Send sms page] Click by \[Send message button]")]
        public void WhenClickBySendMessageButton() {
            Page.SendSmsComponent.SendButton.ClickAndWaitForRedirect();
        }

        [Then(@"\[Send sms page] Sms status form for '(.*)' is visible")]
        public void ThenSmsStatusFormForIsVisible(string phone) {
            Page.SendSmsComponent.SmsStatusForm.AssertIsVisible(phone);
        }

        [Then(@"\[Send sms page] Sms status is \[in progress]")]
        public void ThenSmsStatusIsInProgress() {
            Page.SendSmsComponent.SmsStatusForm.AssertStatusIsInProgress();
        }

        [When(@"\[Send sms page] Wait while Sms status is \[in progress]")]
        public void WhenWaitWhileSmsStatusIsInProgress() {
            Page.SendSmsComponent.SmsStatusForm.WaitWhileInProgres();
        }

        [When(@"\[Send sms page] Click by \[Update link]")]
        public void WhenClickByUpdateLink() {
            Page.SendSmsComponent.SmsStatusForm.UpdateLink.ClickAndWaitWhileAjaxRequests();
        }

        [Then(@"\[Send sms page] Sms status is \[sended]")]
        public void ThenSmsStatusIsSended() {
            Page.SendSmsComponent.SmsStatusForm.AssertStatusIsSended();
        }

        [Then(@"Left symbols count is '(.*)'")]
        public void ThenLeftSymbolsCountIs(int count) {
            Page.SendSmsComponent.AssertLefSymbolsCountIs(count);
        }
        
        [Then(@"\[body] field is highlited with red")]
        public void ThenBodyFieldIsHighlitedWithRed() {
            Page.SendSmsComponent.SmsBody.AssertIsInvalid();
        }

        [When(@"\[Send sms page] Click by \[translit checkbox]")]
        public void WhenSendSmsPageClickByTranslitCheckbox() {
            Page.SendSmsComponent.TranslitCheckbox.Click();
        }

        [Then(@"\[body] field text is '(.*)'")]
        public void ThenBodyFieldTextIs(string text)
        {
            Page.SendSmsComponent.SmsBody.TextArea.AssertEqual(text);
        }

    }
}
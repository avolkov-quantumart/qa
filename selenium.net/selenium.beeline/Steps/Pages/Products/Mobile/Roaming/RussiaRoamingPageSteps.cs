﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Mobile.Roaming;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;
using selenium.beeline.Steps.Pages.Help.Mobile;

namespace selenium.beeline.Steps.Pages.Products.Mobile.Roaming
{
    [Binding]
    [Scope(Feature = "Russia roaming")]
    internal class RussiaRoamingPageSteps : BeelinePageSteps<RussiaRoamingPage> {
        [Given(@"\[Russia roaming] page is visible")]
        public void GivenRussiaRoamingPageIsVisible() {
            GoTo<RussiaRoamingPage>();
        }

        [When(@"Set '(.*)' to \[destination] field")]
        public void WhenSetDestinationField(string destination) {
            Page.Params.Destination.TypeInAndSelectFirst(destination);
        }

        [When(@"Click by \[calculate] button")]
        public void WhenClickByCalculateButton() {
            Page.Params.CalculateButton.ClickAndWaitWhileAjax();
        }

        [Then(@"Prices for correct tariff are displayed")]
        public void ThenPricesForCorrectTariffAreDisplayed() {
            Page.Prices.AssertVisible();
        }
    }
}

﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Mobile.Roaming;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;
using selenium.beeline.Steps.Pages.Help.Mobile;

namespace selenium.beeline.Steps.Pages.Products.Mobile.Roaming
{
    [Binding]
    [Scope(Feature = "Global calls")]
    public class GlobalCallsPageSteps : BeelinePageSteps<GlobalCallsPage> {
        [Given(@"\[Global calls] page is opened")]
        public void GivenGlobalCallsPageIsOpened() {
            GoTo<GlobalCallsPage>();
        }

        [When(@"Set '(.*)' to \[destination] field")]
        public void WhenSetDestinationField(string destination) {
            Page.Params.Destination.TypeInAndSelectFirst(destination);
        }

        [When(@"Select random tariff")]
        public void WhenSelectRandomTariff() {
            FD.tariff = Page.Params.SelectRandomTariff();
        }

        [When(@"Click by \[calculate] button")]
        public void WhenClickByCalculateButton() {
            Page.Params.CalculateButton.ClickAndWaitWhileAjax();
        }

        [Then(@"Prices for correct tariff are displayed")]
        public void ThenPricesForCorrectTariffAreDisplayed() {
            Page.Prices.AssertVisible();
            Page.Prices.AssertTariffNameIs(FD.tariff);
        }
    }
}

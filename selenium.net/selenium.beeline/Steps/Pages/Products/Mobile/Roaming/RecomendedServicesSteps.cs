﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Mobile.Roaming;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.Products.Mobile.Roaming {
    [Binding]
    [Scope(Feature = "Global calls")]
    [Scope(Feature = "Global roaming")]
    public class RecomendedServicesSteps : BeelineSteps {
        public RecommendedServices RecommendedServices {
            get { return Browser.State.PageAs<IHasRecommendedServices>().RecommendedServices; }
        }

        [When(@"\[Recommended services] Select serivce '(.*)'")]
        public void WhenSelectSerivce(string serviceName) {
            RecommendedServices.SelectService(serviceName);
        }

        [Then(@"\[Recommended services] Services '(.*)' are disabled")]
        public void ThenServicesAreDisabled(string serviceNames) {
            foreach (var serviceName in serviceNames.Split(','))
                RecommendedServices.AssertServiceIsDisabled(serviceName);
        }
    }
}

﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Products.Mobile.Roaming;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;
using selenium.beeline.Steps.Pages.Help.Mobile;

namespace selenium.beeline.Steps.Pages.Products.Mobile.Roaming
{
    [Binding]
    [Scope(Feature = "Global roaming")]
    public class GlobalRoamingPageSteps : BeelinePageSteps<GlobalRoamingPage> {
        [Given(@"\[Global roaming] page is visible")]
        public void GivenGlobalRoamingPageIsVisible() {
            GoTo<GlobalRoamingPage>();
        }

        [When(@"Set '(.*)' to \[destination] field")]
        public void WhenSetЛондонToDestinationField(string destination) {
            Page.Params.Destination.TypeInAndSelectFirst(destination);
        }

        [When(@"Click by \[calculate] button")]
        public void WhenClickByCalculateButton() {
            Page.Params.CalculateButton.ClickAndWaitWhileAjax();
        }

        [Then(@"Prices for correct tariff are displayed")]
        public void ThenPricesForCorrectTariffAreDisplayed() {
            Page.Prices.AssertVisible();
        }
    }
}

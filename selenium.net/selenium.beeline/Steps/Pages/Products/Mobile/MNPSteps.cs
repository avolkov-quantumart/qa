﻿using TechTalk.SpecFlow;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Products.Mobile.MNP;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;

namespace selenium.beeline.Steps.Pages.Mobile {
    [Binding]
    [Scope(Feature = "MNP Application")]
    public class MNPSteps : BeelinePageSteps<MNPPage> {
        [Given(@"\[Mnp] page is opened")]
        public void GivenMnpPageIsOpened() {
            GoTo<MNPPage>();
        }

        [When(@"Type in random phone with code '(.*)'")]
        public void WhenTypeInRandomPhoneWithCode(string code) {
            Page.ApplicationForm.PhoneNumber.Code.TypeIn(code);
            Page.ApplicationForm.PhoneNumber.Phone.TypeInRandomNumber(7);
            Page.ApplicationForm.UseSamePhoneForContact();
        }

        [When(@"Type in name: '(.*)', '(.*)', '(.*)'")]
        public void WhenTypeInName(string firstName, string lastName, string patronymicName) {
            Page.ApplicationForm.FirstName.TypeIn(firstName);
            Page.ApplicationForm.LastName.TypeIn(lastName);
            Page.ApplicationForm.PatronimicName.TypeIn(patronymicName);
        }

        [When(@"Select courier delivery")]
        public void WhenSelectCourierDelivery() {
            Page.ApplicationForm.SelectCourierDelivary();
        }

        [When(@"Type in address: '(.*)', '(.*)', '(.*)', '(.*)'")]
        public void WhenTypeInAddress(string city, string street, int building, int apartment) {
            Page.ApplicationForm.City.TypeIn(city);
            Page.ApplicationForm.Street.TypeIn(street);
            Page.ApplicationForm.Building.TypeIn(building);
            Page.ApplicationForm.Apartment.TypeIn(apartment);
        }

        [When(@"Type in captcha")]
        public void WhenTypeInCpatcha() {
            Page.ApplicationForm.Captcha.SetDefault();
        }

        [When(@"Click by \[Select office] button")]
        public void WhenClickBySelectOfficeButton() {
            for (int i = 0; i < 5; i++) {
                Page.ApplicationForm.SelectOfficeButton.ClickAndWaitWhileAjax(true);
                if (Page.ApplicationForm.PhoneNumber.ValidationHint.IsVisible()) {
                    // некорректный номер телефона
                    Page.ApplicationForm.PhoneNumber.Phone.TypeInRandomNumber(7);
                    Page.ApplicationForm.Captcha.SetDefault();
                    continue;
                }
                break;
            }
        }

        [Then(@"\[Select office] component is displayed")]
        public void ThenSelectOfficeComponentIsDisplayed() {
            Page.OfficesTab.AssertVisible();
        }

        [Then(@"\[Select portation tariff] component is displayed")]
        public void ThenSelectPortationTariffComponentIsDisplayed() {
            Page.SelectTariff.AssertVisible();
        }

        [When(@"Select random portation tariff")]
        public void WhenSelectRandomPortationTariff() {
            Page.SelectTariff.SelectRandom();
        }

        [Then(@"Application accepted form is displayed")]
        public void ThenSuccesFormIsDisplayed() {
            Page.RequestResultForm.AssertVisible();
            Page.RequestResultForm.AssertApplicationAccepted();
        }

        [When(@"Click by \[send application] button for random office")]
        public void WhenClickBySendApplicationButtonForRandomOffice() {
            var officesMap = Page.OfficesTab.OpenOfficesMap();
            officesMap.ClickByRandomPoint<MapPoint>();
            var balloon = officesMap.GetBalloon<SendMnpApplicationBallon>();
            balloon.Submit.ClickAndWaitWhileAjax();
        }
    }
}
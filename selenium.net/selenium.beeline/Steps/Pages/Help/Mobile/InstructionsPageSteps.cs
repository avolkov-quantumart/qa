using NUnit.Framework;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Help.Mobile;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;
using selenium.core.Tests;

namespace selenium.beeline.Steps.Pages.Help.Mobile {
    [Binding]
    [Scope(Feature = "InternetInstructions")]
    public class InstructionsPageSteps : BeelinePageSteps<InternetInstructionsPage> {
        [Given(@"I am on InstructionsPage")]
        public void GivenIAmOnInstructionsPage() {
            Browser.Go.ToPage<InternetInstructionsPage>();
        }

        [When(@"I typed in '(.*)'")]
        [Scope(Feature = "InternetInstructions")]
        public void WhenITypedIn(string phoneModel) {
            Page.PhoneModel.TypeInAndSelectFirst(phoneModel);
        }

        [Then(@"Phone icon is '(.*)'")]
        public void ThenPhoneIconIs(string phoneIcon) {
            Page.AssertIconFileIs(phoneIcon);
        }

        [Then(@"Button Find instruction activated")]
        public void ThenButtonFindInstructionActivated() {
            Page.AssertFindInstructionButtonEnabled();
        }

        [When(@"I typed in phone model with automatic instruction")]
        public void WhenITypedInPhoneModelWithAutomaticInstruction() {
            TestHelper.AssertAction(() => Page.FindAutomaticInstruction(),
                                    "�� ������������ ������ �� �������� �������������� ��������");
        }

        [Then(@"Automatic instruction visible")]
        public void ThenAutomaticInstructionVisible() {
            Assert.IsTrue(Page.AutomaticInstruction.IsVisible());
        }

        [Then(@"Automatic instruction contains correct phone model")]
        [Scope(Scenario = "Find automatic instruction")]
        public void ThenAutomaticInstructionContainsCorrectPhoneModel() {
            string phoneModel = Page.PhoneModel.GetText().Trim();
            Page.AutomaticInstruction.AssertName(phoneModel);
        }

        [Then(@"Manual instruction contains correct phone model")]
        [Scope(Scenario = "Find manual instruction")]
        public void ThenManualInstructionContainsCorrectPhoneModel() {
            string phoneModel = Page.PhoneModel.GetText().Trim();
            Page.ManualInstruction.AssertName(phoneModel);
        }

        [When(@"I typed in phone model with manual instruction")]
        public void WhenITypedInPhoneModelWithManualInstruction() {
            TestHelper.AssertAction(() => Page.FindManualInstruction(),
                                    "�� ������������ ���������� �� ������ ���������");
        }

        [Then(@"Manual instruction visible")]
        public void ThenManualInstructionVisible() {
            Assert.IsTrue(Page.ManualInstruction.IsVisible());
        }

        [Then(@"Contains not empty steps")]
        public void ThenContainsNotEmptySteps() {
            Page.ManualInstruction.AssertContainsSteps();
        }

        [When(@"Click by Send Sms link in instruction")]
        public void WhenClickBySendSmsLinkInInstruction() {
            Page.AutomaticInstruction.OpenSendSmsSettingsAlert();
        }

        [Then(@"Send Sms window opened")]
        public void ThenSendSmsWindowOpened() {
            Assert.IsTrue(Browser.State.HtmlAlertIs<SendSmsSettingsAlert>());
        }

        [Then(@"Contains correct phone number")]
        public void ThenContainsCorrectPhoneNumber() {
            string phoneModel = Page.PhoneModel.GetText().Trim();
            Browser.State.HtmlAlertAs<SendSmsSettingsAlert>().AssertModelPhone(phoneModel);
        }

        [Then(@"Send button disabled")]
        public void ThenSendButtonDisabled() {
            Browser.State.HtmlAlertAs<SendSmsSettingsAlert>().AssertSendButtonDisabled();
        }
    }
}
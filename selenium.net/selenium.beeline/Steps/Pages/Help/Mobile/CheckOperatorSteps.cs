﻿using System;
using NUnit.Framework;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Help.Mobile;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;

namespace selenium.beeline.Steps.Pages.Help.Mobile {
    [Binding]
    public class CheckOperatorSteps : BeelinePageSteps<CheckOperatorPage> {
        private string _curCaptchaImage;

        [Given(@"I am on MNP Page")]
        public void GivenIAmOnMNPPage() {
            GoTo<CheckOperatorPage>();
        }

        [When(@"I type in number '(.*)'")]
        public void WhenITypeInNumber(Decimal number) {
            Page.PhoneNumber.Phone.TypeIn(number);
        }

        [When(@"I type in cpatcha '(.*)'")]
        public void WhenITypeInCpatcha(int value) {
            Page.Captcha.TypeIn(value);
        }

        [When(@"I click by CHECK button")]
        public void WhenIClickByCHECKButton() {
            Page.CheckButton.ClickAndWaitWhileAjax();
        }

        [When(@"I type in code '(.*)'")]
        public void WhenITypeInCode(int code) {
            Page.PhoneNumber.Code.TypeIn(code);
        }

        [Then(@"Operator is '(.*)'")]
        public void ThenOperatorIs(string value) {
            Page.AssertOperatorIs(value);
        }

        [Then(@"Region is '(.*)'")]
        public void ThenRegionIs(string value) {
            Page.AssertRegionIs(value);
        }

        [When(@"I navigate to MNP page")]
        public void WhenINavigateToMNPPage() {
            GoTo<CheckOperatorPage>(true);
        }

        [Then(@"Check button is disabled")]
        public void ThenCheckButtonIsDisabled() {
            Page.CheckButton.AssertDisabled();
        }

        [Then(@"All fields are empty")]
        [Scope(Feature = "CheckOperator")]
        public void ThenAllFieldsAreEmpty() {
            Page.AssertAllFieldsAreEmpty();
        }

        [When(@"I fill all fields with some values")]
        public void WhenIFillAllFieldsWithSomeValues() {
            Page.RandomFill();
        }

        [Then(@"Check button gets activated")]
        public void ThenCheckButtonGetsActivated() {
            Page.CheckButton.AssertEnabled();
        }

        [Given(@"Phone is not empty")]
        public void GivenPhoneIsNotEmpty() {
            Page.PhoneNumber.Phone.TypeInRandomNumber(7);
        }

        [When(@"I clear phone field")]
        public void WhenIClearPhoneField() {
            Page.PhoneNumber.Clear();
            Browser.Action.ChangeFocus();
        }

        [When(@"I remove one symbol from code field")]
        public void WhenIRemoveOneSymbolFromCodeField() {
            Page.PhoneNumber.RemoveLastFromCode();
        }

        [Then(@"Phone is empty message shown")]
        public void ThenPhoneIsEmptyMessageShown() {
            Page.PhoneNumber.ValidationHint.AssertPhoneIsEmptyVisible();
        }

        [Given(@"Captcha is not empty")]
        public void GivenCaptchaIsNotEmpty() {
            Page.Captcha.TypeInRandomNumber(5);
        }

        [When(@"I remove one symbol from captcha")]
        public void WhenIRemoveOneSymbolFromCaptcha() {
            Page.Captcha.RemoveLast(true);
        }

        [Then(@"CAPTCHA MUST HAVE 5 SYMBOLS message shown")]
        public void ThenCAPTCHAMUSTHAVE5SYMBOLSMessageShown() {
            Page.CaptchaValidationHint.AssertCaptchaMustHave5SymbolsVisible();
        }

        [When(@"I clear captcha field")]
        public void WhenIClearCaptchaField() {
            Page.Captcha.Clear();
            Browser.Action.ChangeFocus();
            Page.CaptchaValidationHint.AssertCaptchaIsEmptyVisible();
        }

        [Then(@"CAPTCHA IS EMPTY message shown")]
        public void ThenCAPTCHAISEMPTYMessageShown() {

        }

        [When(@"I remember current captcha image")]
        public void WhenIRememberCurrentCaptchaImage() {
            _curCaptchaImage = Page.CaptchaImage.GetFileName();
        }

        [When(@"I click update captcha link")]
        public void WhenIClickUpdateCaptchaLink() {
            Page.UpdateCaptchaLink.Click();
        }

        [Then(@"Captcha image is changed")]
        public void ThenCaptchaImageIsChanged() {
            Assert.AreNotEqual(_curCaptchaImage, Page.CaptchaImage.GetFileName(), "Каптча не обновилась");
        }
    }
}
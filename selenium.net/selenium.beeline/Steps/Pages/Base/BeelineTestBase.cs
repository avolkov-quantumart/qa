﻿using selenium.core.Framework.Browser;
using selenium.core.Framework.Page;
using selenium.core.Logging;
using selenium.core.Tests;

namespace selenium.beeline.Steps.Pages.Base {
    public abstract class BeelineTestBase<T> : TestBase<T> where T : IPage {
        protected override Browser Browser {
            get { return BeelineSeleniumContext.Inst.Browser; }
        }

        protected override TestLogger Log {
            get { return BeelineSeleniumContext.Inst.Log; }
        }
    }
}
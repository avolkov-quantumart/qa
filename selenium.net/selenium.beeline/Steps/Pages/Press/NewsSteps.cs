﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using core.Extensions;
using selenium.beeline.Pages.Customers.press;
using selenium.beeline.Steps.Base;
using selenium.beeline.Steps.Common;

namespace selenium.beeline.Steps.Pages.Press {
    [Binding]
    public class NewsSteps : BeelinePageSteps<NewsPage> {
        private Article _article;
        private Dictionary<string, Article> _articles;

        [Given(@"\[News] page is opened")]
        public void GivenNewsPageIsOpened() {
            GoTo<NewsPage>();
        }

        [When(@"I select not current year")]
        public void WhenISelectRandomYear() {
            _articles = Page.NewsList.GetArticles();
            Page.YearFilter.SelectNewYear();
        }

        [Then(@"New articles are displayed")]
        public void ThenNewArticlesAreDisplayed() {
            Dictionary<string, Article> current = Page.NewsList.GetArticles();
            if (current.Keys.Any(title => !_articles.ContainsKey(title)))
                return;
            Assert.Fail("Не отобразилось новых статей");
        }

        [When(@"I select random month")]
        public void WhenISelectRandomMonth() {
            _articles = Page.NewsList.GetArticles();
            Page.MonthFilter.SelectRandom();
        }

        [Given(@"Selected not current month")]
        public void GivenSelectedNotCurrentMonth() {
            Page.YearFilter.SelectRandom(year => year > 2012);      // после 2012 на тестовом кластере только тестовые новости
        }

        [Then(@"Displayed news for selected month")]
        public void ThenDisplayedNewsForSelectedMonth() {
            Dictionary<string, Article> articles = Page.NewsList.GetArticles();
            int currentMonth = Page.MonthFilter.Selected;
            foreach (Article article in articles.Values) {
                Assert.AreEqual(currentMonth, article.Date.Month,
                                "В списке отображается новость не за выбранный в фильтре месяц");
            }
        }

        [When(@"Scroll down until load progress is visible")]
        public void WhenScrollDownUntilLoadProgressIsVisible() {
            _articles = Page.NewsList.GetArticles();
            Browser.Action.ScrollToBottom();
        }

        [Then(@"News count increased")]
        public void ThenNewsCountIncreased() {
            int articlesCount = Page.NewsList.GetArticlesCount();
            Assert.Greater(articlesCount, _articles.Count, "Количество статей не увеличилось");
        }

        [When(@"Navigate to not first article")]
        public void NavigateToRandomArticle() {
            _article = Page.NewsList.GoToNotFirstArticle();
        }

        [When(@"Click by \[next news] button")]
        public void WhenClickByNextNewsButton() {
            var articlePage = Browser.State.PageAs<ArticlePage>();
            _article = new Article(articlePage.NextLink.AnchorText);
            articlePage.NextLink.Click();
        }

        [Then(@"Navigated to correct article")]
        public void NavigatedToCorrectArticle() {
            Assert.True(Browser.State.PageIs<ArticlePage>(), "Не перешли на страницу новости");
            Assert.AreEqual(_article.Title, Browser.State.PageAs<ArticlePage>().Title.Text,
                            "Должны были перейти на новость с другим заголовком");
        }

        [When(@"Click by \[previous news] button")]
        public void WhenClickByPreviousNewsButton() {
            var articlePage = Browser.State.PageAs<ArticlePage>();
            _article = new Article(articlePage.PreviousLink.AnchorText);
            articlePage.PreviousLink.Click();
        }
    }
}
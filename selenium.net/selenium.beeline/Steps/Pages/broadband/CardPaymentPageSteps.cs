﻿using System.Web.UI;
using NUnit.Framework;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.broadband;
using selenium.beeline.Steps.Base;

namespace selenium.beeline.Steps.Pages.broadband {
    [Binding]
    [Scope(Feature = "Payment")]
    public class CardPaymentPageSteps : BeelineSteps {
        [Then(@"\[Home credit card payment page] is visible")]
        public void ThenCreditCardPaymentPageIsVisible() {
            Assert.IsNotNull(Browser.State.PageIs<HomeCardPaymentPage>(),
                             "Не перешли на страницу оплаты кредитной картой");
        }

        [Then(@"\[Mobile credit card payment page] is visible")]
        public void ThenMobileCardPaymentPageIsVisible() {
            Assert.IsNotNull(Browser.State.PageIs<HomeCardPaymentPage>(),
                             "Не перешли на страницу оплаты кредитной картой");
        }

        [Then(@"\[Credit card payment page] \[account number] starts with '(.*)'")]
        public void ThenAccountNumberStartsWith(string code) {
            var page = Browser.State.PageAs<HomeCardPaymentPage>();
            var accountNumber = page.AccountNumber.Text;
            Assert.True(accountNumber.StartsWith(code), "Номер счета не начинается с '{0}'", code);
        }

        [Then(@"\[Credit card payment page] \[account number] contains specified earlier value")]
        public void ThenAccountNumberContainsSpecifiedEarlierValue() {
            var page = Browser.State.PageAs<HomeCardPaymentPage>();
            var accountNumber = page.AccountNumber.Text;
            // отрезать код номера счета
            accountNumber = accountNumber.Substring(3, accountNumber.Length - 3);
            Assert.AreEqual(FD.payment.AccountNumber, accountNumber, "Некорректный номер счета на странице оплаты");
        }

        [Then(@"\[Credit card payment page] \[payment sum] contains specified earlier value")]
        public void ThenPaymentSumContainsSpecifiedEarlierValue() {
            var page = Browser.State.PageAs<HomeCardPaymentPage>();
            Assert.AreEqual(FD.payment.Sum.ToString(), page.Sum.Text, "Некорректная сумма оплаты");
        }

        [Then(@"\[Credit card payment page] \[Phone] is '(.*)'")]
        public void ThenPhoneIs(string phone) {
            var page = Browser.State.PageAs<MobileCardPaymentPage>();
            page.Phone.AssertEqual(phone);
        }

        [Then(@"\[Credit card payment page] \[payment sum] is '(.*)'")]
        public void ThenPaymentSumIs(int sum) {
            var page = Browser.State.PageAs<MobileCardPaymentPage>();
            page.Sum.AssertEqual(sum.ToString());
        }
    }
}
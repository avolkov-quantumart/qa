﻿using System.Collections.Generic;

namespace selenium.beeline
{
    /// <summary>
    /// Features Data
    /// Хранилище данных для совместного использования несколькими шагами сценариев
    /// </summary>
    public static class FD {
        public static readonly SubscriptionFD subscription = new SubscriptionFD();
        public static readonly PaymentFD payment = new PaymentFD();
        public static string tariff;
        public static string service;
    }

    public class PaymentFD {
        public string PhoneNumber;
        public string AccountNumber;
        public int Sum;
    }

    public class SubscriptionFD {
        public string subscription_email;
        public List<string> subscription_themes;
        public List<string> new_subscription_themes;
        public int active_subscription_id;
    }
}

﻿Feature: CheckOperator
	In order to save my money
	As a user
	I want to know operator of the number I wanna call to

Scenario Outline: Operator is valid
	Given I am on MNP Page
	When I type in code '<code>'
	And I type in number '<number>'
	And I type in cpatcha '<captcha>'
	And I click by CHECK button
	Then Operator is '<operator>'
	And Region is '<region>'
	Examples: 
	| case    | code | number  | operator | captcha | region                        |
	| mts     | 916  | 1112233 | МТС      | 99999   | г. Москва, Московская область |
	| beeline | 964  | 1112233 | Билайн   | 99999   | Иркутская область             |
	| megafon | 920  | 7470157 | Мегафон  | 99999   | Тульская область              |

Scenario: CHECK button disabled
	When I navigate to MNP page
	Then Check button is disabled
	And All fields are empty

Scenario: Activate CHECK button after filling data
	When I navigate to MNP page
	And I fill all fields with some values
	Then Check button gets activated

Scenario: Show PHONE IS EMPTY message after remove one symbol from code field
	Given I am on MNP Page
	And Phone is not empty
	When I remove one symbol from code field
	Then Phone is empty message shown
	And Check button is disabled

Scenario: Show PHONE IS EMPTY message after remove one symbol from number field
	Given I am on MNP Page
	And Phone is not empty
	When I clear phone field
	Then Phone is empty message shown
	And Check button is disabled

Scenario: Show CAPTCHA IS EMPTY message after clear captcha field
	Given I am on MNP Page
	And Captcha is not empty
	When I clear captcha field
	Then CAPTCHA IS EMPTY message shown
	And Check button is disabled

Scenario: Show CAPTCHA MUST HAVE 5 SYMBOLS message after clear captcha field
	Given I am on MNP Page
	And Captcha is not empty
	When I remove one symbol from captcha
	Then CAPTCHA MUST HAVE 5 SYMBOLS message shown
	And Check button is disabled

Scenario: Update captcha image
	Given I am on MNP Page
	When I remember current captcha image
	And I click update captcha link
	Then Captcha image is changed

﻿Feature: MNP Application

Scenario Outline: Change number in office
	Given [Mnp] page is opened
	When Type in random phone with code '<code>'
	And Type in name: '<фамилия>', '<имя>', '<отчество>'
	And Type in captcha
	And Click by [Select office] button
	Then [Select office] component is displayed
	When Click by [send application] button for random office
	Then Application accepted form is displayed
	Examples: 
	| code | фамилия | имя     | отчество     |
	| 915  | Фролов  | Евгений | Владимирович |

Scenario Outline: Change number with courier
	Given [Mnp] page is opened
	When Type in random phone with code '<code>'
	And Type in name: '<фамилия>', '<имя>', '<отчество>'
	And Select courier delivery
	And Type in address: '<city>', '<street>', '<building>', '<apartment>'
	And Type in captcha
	And Click by [Select office] button
	Then [Select portation tariff] component is displayed
	When Select random portation tariff
	Then Application accepted form is displayed
	Examples: 
	| code | фамилия | имя     | отчество     | city   | street    | building | apartment |
	| 915  | Фролов  | Евгений | Владимирович | Москва | Митинская | 52       | 1         |
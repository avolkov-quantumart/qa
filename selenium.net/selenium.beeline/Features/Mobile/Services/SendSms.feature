﻿Feature: SendSms

Scenario Outline: Send sms from widget
	Given [Send sms widget] is visible
	When [Send sms widget] Type in '<phone>' in [phone] field
	And [Send sms widget] Type in '<smsbody>' in [body] field
	And [Send sms widget] Type in captcha
	And [Send sms widget] Click by [Send message button]
	Then [Send sms widget] Sms status form for '<phone>' is visible
	And [Send sms widget] Sms status is [in progress]
	When [Send sms widget] Wait while Sms status is [in progress]
	And [Send sms widget] Click by [Update link]
	Then [Send sms widget] Sms status is [sended]
	Examples:
	| phone      | smsbody                   |
	| 9062246353 | hello from sendsms widget |

Scenario Outline: Send sms from page
	Given [Send sms page] is visible
	When [Send sms page] Type in '<phone>' in [phone] field
	And [Send sms page] Type in '<smsbody>' in [body] field
	And [Send sms page] Type in captcha
	And [Send sms page] Click by [Send message button]
	Then [Send sms page] Sms status form for '<phone>' is visible
	And [Send sms page] Sms status is [in progress]
	When [Send sms page] Wait while Sms status is [in progress]
	And [Send sms page] Click by [Update link]
	Then [Send sms page] Sms status is [sended]
	Examples:
	| phone      | smsbody                 |
	| 9062246353 | hello from sendsms page |

Scenario Outline: Left symbols count
	Given [Send sms page] is visible
	When [Send sms page] Type in '<smsbody>' in [body] field
	Then Left symbols count is '<count>'
	Examples: 
	| case           | smsbody                                                                     | count |
	| latin          | aaabbbccce                                                                  | 130   |
	| cyrillic       | ааабббвввг                                                                  | 60    |
	| mix            | ёёёёёwwwww                                                                  | 60    |
	| negative count | ёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёё12345 | -5    |

Scenario Outline: Invalid sms body
	Given [Send sms page] is visible
	When [Send sms page] Type in '<smsbody>' in [body] field
	Then [body] field is highlited with red
	Examples: 
	| case     | smsbody                                                                     |
	| overflow | ёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёёё12345 |
				 
Scenario Outline: Translit
	Given [Send sms page] is visible
	When [Send sms page] Type in '<text before>' in [body] field
	And [Send sms page] Click by [translit checkbox]
	Then [body] field text is '<text after>'
	And Left symbols count is '<count>'
	Examples: 
	| text before   | text after    | count |
	| русский текст | russkii tekst | 127   |


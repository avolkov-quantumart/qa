﻿Feature: ChangeNumber
	In order to have phone number wich is easyer to remember
	As a user
	I want to be able to change my phone number

Background: 
	Given I cleared all cookies

Scenario: Correct initial values
	When I navigate on ChangeNumberPage
	Then All fields are empty

#Scenario: SHOW NUMBERS button activation
#	Given I am on ChangeNumberPage
#	When I typed in some Beeline number
#	Then SHOW NUMBERS button is enabled

Scenario: Empty number validation
	Given I am on ChangeNumberPage
	And I typed in some Beeline number
	When I clear number field
	Then Empty phone number validation hint is shown

Scenario: Summary widget updating
	Given Table with suggested numbers is visible
	When I select some number from table
	Then Summary widget contains params of selected number

Scenario: SHOW MORE button adds new numbers
	Given Table with suggested numbers is visible
	When Click by SHOW MORE button
	Then New numbers added to table

Scenario: Show GET CONFIRMATION CODE window
	Given Table with suggested numbers is visible
	And Table with suggested numbers has selected number
	When I click by FURTHER TO NUMBER CHANGING button
	Then GET CONFIRMATION CODE window shown
	And GET CONFIRMATION CODE has correct number
	And GET CONFIRMATION CODE has correct price


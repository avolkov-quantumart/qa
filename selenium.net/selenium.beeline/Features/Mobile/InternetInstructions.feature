Feature: InternetInstructions
	In order to know how to set up internet on my phone
	As a user
	I want to get an instruction

Scenario: Activate show instruction button
	Given I am on InstructionsPage
	When I typed in 'Motorola C380'
	Then Button Find instruction activated

Scenario: PhoneIconDisplayed
	Given I am on InstructionsPage
	When I typed in 'Motorola C380'
	Then Phone icon is '0ac7613b-35b9-48c1-8a99-976033967c64%7B%7B$$%7D%7D600-600.jpg'
	When I typed in 'Motorola C390'
	Then Phone icon is '4acb0a24-d9aa-4234-8aa3-c1fb69535a5c%7B%7B$$%7D%7D600-600.jpg'

Scenario: Find automatic instruction
	Given I am on InstructionsPage
	When I typed in phone model with automatic instruction
	Then Automatic instruction visible 
	And Automatic instruction contains correct phone model

Scenario: Find manual instruction
	Given I am on InstructionsPage
	When I typed in phone model with manual instruction
	Then Manual instruction visible
	And Manual instruction contains correct phone model
	And Contains not empty steps

Scenario: Open window to send instruction in Sms
	Given I am on InstructionsPage
	When I typed in phone model with automatic instruction
	And Click by Send Sms link in instruction
	Then Send Sms window opened
	And Contains correct phone number
	And Send button disabled

	

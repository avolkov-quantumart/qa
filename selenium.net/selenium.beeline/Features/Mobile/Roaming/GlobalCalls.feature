﻿Feature: Global calls

Scenario Outline: Calculate prices
	Given [Global calls] page is opened
	When Set '<destination>' to [destination] field
	And Select random tariff
	And Click by [calculate] button
	Then Prices for correct tariff are displayed
	Examples:
	| destination |
	| Лондон      |

Scenario Outline: Interexcluded services
	Given [Global calls] page is opened
	When Set '<destination>' to [destination] field
	And Select random tariff
	And Click by [calculate] button
	And [Recommended services] Select serivce '<checkedservice1>'
	Then [Recommended services] Services '<disabledservices>' are disabled
	Examples:
	| destination | checkedservice1    | disabledservices |
	| Тула        | Комфортный роуминг | Мультипасс       |
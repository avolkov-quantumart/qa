﻿Feature: Russia roaming

Scenario Outline: Calculate prices
	Given [Russia roaming] page is visible
	When Set '<destination>' to [destination] field
	And Click by [calculate] button
	Then Prices for correct tariff are displayed
	Examples:
	| destination |
	| Тула        |
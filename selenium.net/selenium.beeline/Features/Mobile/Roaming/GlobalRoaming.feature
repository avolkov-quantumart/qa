﻿Feature: Global roaming

Scenario Outline: Calculate prices
	Given [Global roaming] page is visible
	When Set '<destination>' to [destination] field
	And Click by [calculate] button
	Then Prices for correct tariff are displayed
	Examples:
	| destination |
	| Лондон      |

Scenario Outline: Interexcluded services
	Given [Global roaming] page is visible
	When Set '<destination>' to [destination] field
	And Click by [calculate] button
	And [Recommended services] Select serivce '<checkedservice1>'
	Then [Recommended services] Services '<disabledservices>' are disabled
	Examples:
	| destination | checkedservice1    | disabledservices |
	| Стокгольм   | Комфортный роуминг | Мультипасс       |
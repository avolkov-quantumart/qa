﻿Feature: TableWithSuggestedNumbers
	In order to be able to select easily select new number
	As a user
	I want the interface to select number was shown

Background: 
	Given Go to table with suggested numbers

Scenario: Table with suggested numbers is visible
	Then Table with suggested numbers is visible

Scenario: Table with suggested numbers is not empty
	Then Each column has at least one number

Scenario Outline: Tabs are visible
	Then Tab '<tabname>' is visible
	Examples: 
	| tabname          |
	| Случайные номера |
	| Группа цифр      |
	| Слова            |
	| Похожий номер    |
	| Конструктор      |
	| Дата             |

Scenario: Random nubmers tab is selected
	Then Tab 'Случайные номера' is selected

Scenario: Current number is visible
	Then Current user number is the same as user typed in


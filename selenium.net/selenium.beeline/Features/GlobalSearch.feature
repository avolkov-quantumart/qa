﻿Feature: GlobalSearch

Scenario Outline: Results are relevant
	Given [global search] field is visible
	When [global search] Search for '<phrase>'
	Then Tabs '<tabs>' are visible
	And All tabs contain results
	And [All] tab is selected
	And [All] tab results count is equal to sum count on other tabs
	#And All results are from the same region
	And First results contains phrase '<phrase>'
	And Second results contains all words from '<phrase>'
	And Third results contains one word from '<phrase>'
	And No results wich does not contain words from '<phrase>'
	And No result duplications
	And Select random tab from '<tabs>'
	Then Results category marker match to tab category
	Examples:
	| phrase              | tabs                                      |
	| тариф ноль сомнений | Новости Тарифы Услуги Другое              |
	| роуминг в Турци     | Новости Тарифы Услуги Другое Помощь Акции |

Scenario: Switch partition
	Given [global search] field is visible
	When [global search] Search for 'роуминг'
	Then Clients partition is selected
	And All result links are on Client site
	When Select [Buisness] partition 
	Then All result links are to Buisness site
	When Select [Partners] partition
	Then All result links are to Partners site

Scenario: Regional search
	Given [search page] is visible
	When Set [Search in all regions] checkbox
	And [search page] Search for 'роуминг'
	Then Results list contains results for different regions
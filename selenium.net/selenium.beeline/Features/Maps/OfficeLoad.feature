Feature: OfficeLoad

Scenario: Sun icon is on the current hour
	When Some office load chart opened
	Then Sun icon is on the current hour

Scenario: Slider is on the current hour
	When Some office load chart opened
	Then Slider is on the current hour

Scenario: Slider color match the office load mark
	When Some office load chart opened
	Then Slider color match the office load mark

Scenario: Load color match the marker text
	When Offices list is visible
	Then Load color match the marker text

Scenario: Offices with queue length are exist
	When Offices list is visible
	Then Offices with queue length are exist

Scenario: Offices with usual load are exist
	When Offices list is visible
	Then Offices with usual load are exist


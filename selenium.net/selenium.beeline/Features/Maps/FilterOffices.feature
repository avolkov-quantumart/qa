﻿Feature: FilterOffices

Scenario Outline: Search by address in list
	Given Offices list is visible
	When Search by '<address>'
	Then Offices list changed
	And Offices list is not empty
Examples: 
	| address      |
	| Митинская 52 |

Scenario Outline: Search by address on map
	Given Offices map is visible
	When Search by '<address>'
	Then '<tile>' is displayed on map
	And '<tile>' contains office
	When Click by office on '<tile>'
	Then Office ballon is displayed
Examples: 
	| address      | tile                                                                                                       |
	| Митинская 52 | http://h03tiles.tmcrussia.com/map/lv13/00/000/004/946/000/005/605.png?key=6f4c0ee05cb084784e90f6e311d87d75 |


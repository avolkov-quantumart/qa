﻿Feature: PrintOfficeMapPage

Background:
	When I am on print office map page

Scenario: Map is visible
	Then map is visible

Scenario: Address is correct
	Then address is correct

Scenario: Metro station is correct
	Then metro station is correct

Scenario: Find description is correct
	Then find hint is correct

Scenario: Nearest offices are correct
	Then nearest offices are correct

Scenario: Page has valid print button
	Then Page has valid print button

Scenario: Page has valid services list
	Then Page has valid services list
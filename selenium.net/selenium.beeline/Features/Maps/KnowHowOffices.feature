﻿Feature: KnowHowOffices

Scenario: Filter know how offices
	Given Offices map is visible
	When [Beeline on map page] Check only Know How offices in filter
	Then [Beeline on map page] Map contains Know How offices

Scenario: Office services list contains Know How service
	Given Offices list is visible
	When [Beeline on map page] Check only Know How offices in filter
	And [Beeline on map page] Open services list
	Then [Beeline on map page] Services list contains Know How service

Scenario: [Share link] in Know How office ballon
	Given Offices map is visible
	When [Beeline on map page] Check only Know How offices in filter
	And [Beeline on map page] Open office ballon
	Then [Beeline on map page] Office ballon has [Share link]


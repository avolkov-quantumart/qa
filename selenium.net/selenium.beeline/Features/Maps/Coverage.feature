﻿Feature: Coverage

Background: 
	Given [Coverage zones] tab is opened

Scenario: NG coverage
	When Select 2G coverage
	Then 2G coverage percent greater than 85
	When Select 3G coverage
	Then 3G coverage percent greater than 85
	When Select 4G coverage
	Then 4G coverage percent greater than 6

Scenario: Metro coverage
	When Select 3G metro coverage
	Then 3G metro coverage image is visible
	When Select 2G metro coverage
	Then 2G metro coverage image is visible

Scenario: Wifi coverage
	When Select free wifi coverage
	Then Map contains more then 700 wifi points
	When Click by wifi point
	Then Wifi baloon is displayed
	When Select paid wifi coverage
	Then Map contains more then 7000 wifi points
	And Paid wifi points count more then free

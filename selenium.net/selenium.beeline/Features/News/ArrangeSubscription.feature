﻿@subscription
Feature: ArrangeSubscription

Scenario Outline: ArrangeSubscription 
	Given There is no subscription for '<email>'
	And User made subscription to '<email>'
	Then Check mail for activation letter message is visible
	And Subscription activation record exists in DB
	And Subscription activation letter came to '<email>'
	When User clicks by [activation link] in activation letter at '<email>'
	Then [Subscription successfull] page is visible
	And Subscription in DB contains correct themes
	When User clicks by [activation link] in activation letter at '<email>'
	Then [Old activation link] page is visible
	When User clicks by [change themes] link in activation letter at '<email>'
	And User changes themes on [subscription management] page
	Then Check mail for activation letter message is visible on [subscription management] page
	And Change subscription themes letter came to '<email>'
	When User clicks by [subscribe link] in change subscription themes letter at '<email>'
	Then Subscription in DB contains correct themes
	Examples:
	| email                         |
	| quantumart.selenium@gmail.com |
﻿Feature: Navigation

Scenario: Filter by date
	Given [News] page is opened
	When I select not current year
	Then New articles are displayed
	Then Displayed news for selected month

Scenario: Filter by month
	Given [News] page is opened
	When I select random month
	Then Displayed news for selected month

Scenario: Dynamic load
	Given [News] page is opened
	And Selected not current month
	When Scroll down until load progress is visible
	Then News count increased

Scenario: Navigate between news
	Given [News] page is opened
	When Navigate to not first article
	Then Navigated to correct article
	When Click by [next news] button
	Then Navigated to correct article
	When Click by [previous news] button
	Then Navigated to correct article
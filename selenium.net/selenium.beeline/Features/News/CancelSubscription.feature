﻿Feature: CancelSubscription

@subscription
Scenario Outline: CancelSubscription
	Given User subscribed to news for '<email>'
	And User cancels subscription for '<email>'
	Then Check mail for cancellation letter message is visible
	And Cancel subscription confirmation letter came to '<email>'
	When User confirms subscription cancellation in letter at '<email>'
	Then [Is deleted] is true for canceled subscription
	And Canceled subscription has no themes
	Examples: 
	| email                         |
	| quantumart.selenium@gmail.com |
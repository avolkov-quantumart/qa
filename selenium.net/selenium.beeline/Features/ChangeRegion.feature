﻿Feature: ChangeRegion
	As a user
	In order to get information about tariffs in my region
	I want to be able to change region of site

Scenario: Open regions selector
	Given I am on ProductsPage
	When I click by current region
	Then Regions selector is visibe

Scenario: Search for region
	Given I am on ProductsPage
	And Regions selector opened
	When I type in 'комсомольск' in search field
	Then Drop down list contains 'Комсомольск-на-Амуре'

Scenario: Change region
	Given I am on ProductsPage
	And Regions selector opened
	And I type in 'комсомольск' in search field
	When I select 'Комсомольск-на-Амуре' from drop down list
	Then I am on ProductsPage
	And Subdomain is 'komsomolsk-na-amure'
	And Current region is 'Комсомольск-на-Амуре'
	And And carousel is visible


﻿Feature: Connect house application

Scenario: Send house connection application
	Given [Connection alert] is visible
	When [Connection alert] Type in address(street:'ореховый проезд', house:'35к4')
	And [Connection alert] Click by [Send application] button
	Then [Connection alert] is not visible
	Then [Connect house application] is visible
	And [Connect house application] Street is 'Ореховый проезд'
	And [Connect house application] House is '35к4'
	When [Connect house application] Type in '1' in [appartment] field
	And [Connect house application] Type in 'Боярская' in [last name] field
	And [Connect house application] Type in 'Елизавета' in [first name] field
	And [Connect house application] Type in 'Михайловна' in [patronymic name] field
	And [Connect house application] Type in 'quantumart.selenium@gmail.com' in [email] field
	And [Connect house application] Type in '9062446353' in [phone] field
	And [Connect house application] Click by [send application] button
	Then [Connect house application] Application was sent message is visible
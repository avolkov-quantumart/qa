﻿Feature: InternetTariff

Scenario Outline: ConnectInternetTariff
	# Page with list of internet tariffs | InternetTariffsSteps.cs
	Given [Tariff list] is visible
	When [Tariff list] select random tariff
	# Page with internet tariff | InternetTariffSteps.cs
	Then [Tariff details page] is visible
	When [Tariff details page] click by [ToBuyButton]
	Then [Check connection alert] is visible
	When [Check connection alert] type '<street>' '<house>' in field and accept
	And [Basket widget] click by [OrderButton]
	# Basket page with tabs | BasketSteps.cs
	Then [Products list tab] is visible
	And [Products list tab] tariff is correct
	When [Products list tab] click by [ContactDetailsButton]
	Then [Contact info tab] is visible
	When [Contact info tab] type '<name>' in [username] field
	And [Contact info tab] type '<email>' in [emailaddress] field
	And [Contact info tab] type '<phone>' in [phonenumber] field
	And [Contact info tab] generate and type apartment number in [apartmentnumber] field
	And [Contact info tab] click by [ConfirmButton]
	Then [Confirm tab] is visible
	And [Products list tab] tariff is correct
	And [Confirm tab] name is '<name>', email is '<email>', phone is '<phone>'
	And [Confirm tab] address is '<street>','<house>'
	When [Confirm tab] click by [MakeOrderButton]
	Then [Connection success page] is displayed

	Examples: 
	| street          | house | name                | email                | phone      |
	| Митинская улица | 52    | Иосиф Виссарионович | stalin1878@soviet.su | 2565121024 |
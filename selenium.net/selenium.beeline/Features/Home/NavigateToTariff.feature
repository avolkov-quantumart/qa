﻿Feature: NavigateToTariff

Scenario: Redirect to tariff from ProvodAboutPage
	Given I am on ProvodAboutPage
	And I click by connect button for some tariff
	Then I go to details page of tariff
	And tariff name is correct

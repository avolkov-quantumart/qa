Feature: TvSchedule

Scenario: Filter by query
	Given [TV schedule] page is opened
	When Type in '�����' in search field
	Then At least one program contains text '�����'

Scenario: Filter by category
	Given [TV schedule] page is opened
	When Select random category
	And Select another category
	Then Channels are changed

Scenario: Filter by date
	Given [TV schedule] page is opened
	When Select random date
	And Select another date
	Then Programs list changed

Scenario: Filter by time
	Given [TV schedule] page is opened
	When Select random time
	Then Displayed schedule for selected time
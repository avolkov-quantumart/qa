﻿Feature: TariffTvSchedule

Background: 
	Given [Tv Schedule] on [Tv tariff] page is visible

Scenario: Filter by category
	Given [Tv Schedule filter] has more then one genre	
	When Select random category
	And Select another category
	Then Channels are changed

Scenario: Filter by date
	When Select random date
	And Select another date
	Then Programs list changed

Scenario: Filter by time
	When Select random time
	Then Displayed schedule for selected time
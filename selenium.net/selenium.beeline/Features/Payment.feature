﻿Feature: Payment

Scenario Outline: Home payment from widget
	Given [Payment widget] for home beeline is visible
	When [Payment widget] Type in random number in [account number] field
	And [Payment widget] Type in '<sum>' in [payment sum] field
	And [Payment widget] Press [pay] button
	Then [Home credit card payment page] is visible
	And [Credit card payment page] [account number] starts with '<code>'
	And [Credit card payment page] [account number] contains specified earlier value
	And [Credit card payment page] [payment sum] contains specified earlier value
	Examples: 
	| code | sum |
	| 089  | 100 |

Scenario Outline: Home payment from page
	Given [Payment page] is visible
	And [Payment page] Home payment tab is selected
	When [Payment page] Type in random number in [account number] field
	And [Payment page] Type in '<sum>' in [payment sum] field
	And [Payment page] Click by [go to payment button] 
	Then [Home credit card payment page] is visible
	And [Credit card payment page] [account number] starts with '<code>'
	And [Credit card payment page] [account number] contains specified earlier value
	And [Credit card payment page] [payment sum] contains specified earlier value
	Examples: 
	| code | sum |
	| 089  | 100 |

Scenario Outline: Mobile payment from widget
	Given [Payment widget] for mobile beeline is visible
	When [Payment widget] Type '<phone>' in [phone] field
	And [Payment widget] Type in '<sum>' in [payment sum] field
	And [Payment widget] Press [pay] button
	Then [Mobile credit card payment page] is visible
	And [Credit card payment page] [Phone] is '<phone>'
	And [Credit card payment page] [payment sum] is '<sum>'
	Examples: 
	| case     | phone      | sum |
	| simple   | 9062446353 | 100 |
	| replace6 | 6061111111 | 100 |

Scenario Outline: Mobile payment from page
	Given [Payment page] is visible
	And [Payment page] Mobile payment tab is selected
	When [Payment page] Type '<phone>' in [phone] field
	And [Payment page] Type in '<sum>' in [payment sum] field
	And [Payment page] Click by [go to payment button]
	Then [Mobile credit card payment page] is visible
	And [Credit card payment page] [Phone] is '<phone>'
	And [Credit card payment page] [payment sum] is '<sum>'
	Examples: 
	| phone      | sum |
	| 9062446353 | 100 |

Scenario: Mobile payment from page(sum field length)
	Given [Payment page] is visible
	When [Payment page] Type in '12345678' in [payment sum] field
	Then [Payment page] [payment sum] field is '12345'

Scenario: Mobile payment from widget(sum field length)
	Given [Payment widget] for mobile beeline is visible
	When [Payment widget] Type in '12345678' in [payment sum] field
	Then [Payment widget] [payment sum] field value is '12345'

Scenario Outline: Mobile payment from widget(beeline number validation)
	Given [Payment widget] for mobile beeline is visible
	When [Payment widget] Type '<phone>' in [phone] field
	And [Payment widget] Type in '<sum>' in [payment sum] field
	And [Payment widget] Press [pay] button
	Then [Payment widget] [Type in beeline number] error is visible
	Examples: 
	| case     | phone      | sum |
	| simple   | 1111111111 | 100 |

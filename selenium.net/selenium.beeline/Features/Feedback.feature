﻿Feature: Feedback

Scenario: Swindle
	Given [Short numbers swindle form] is visible
	When [Short numbers swindle form] Type in '9062446353' in [phone] field
	And [Short numbers swindle form] Type in captcha
	And [Short numbers swindle form] Click by [Get password] button
	And [Short numbers swindle form] Type in password
	And [Short numbers swindle form] Click by [Continue] button
	Then [Short numbers swindle form] Success form is visible

# Чтобы заработало нужно указать следующие параметры для строки подключения beeline_SMS_Transferer
# ms-webtst009.smstransferer.dbo.ConfirmData логин nwbautotest пароль 123456

Scenario: Complaint
	Given [Complaint form] is visible
	And [Complaint form] Theme 'Мобильная связь' is selected
	And [Complaint form] Type in 'Elam Harnish' in [name] field
	And [Complaint form] Type in 'I hate bees' in [your question] field
	And [Complaint form] Type in '9062446353' in [phone] field
	And [Complaint form] Type in 'quantumart.selenium@gmail.com' in [email] field
	And [Complaint form] Type in captcha
	And [Complaint form] Click by [send] button
	Then [Complaint form] Success form is visible

Scenario: Question
	Given [Question form] is visible
	And [Question form] Theme 'Мобильная связь' is selected
    And [Question form] Subtheme 'Оплата услуг' is selected
	And [Question form] Type in 'Elam Harnish' in [name] field
	And [Question form] Type in 'Two bee or not two bee?' in [your question] field
	And [Question form] Type in '9062446353' in [phone] field
	And [Question form] Type in 'quantumart.selenium@gmail.com' in [email] field
	And [Question form] Type in captcha
	And [Question form] Click by [send] button
	Then [Question form] Success form is visible

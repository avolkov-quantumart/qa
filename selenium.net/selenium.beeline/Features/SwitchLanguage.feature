﻿Feature: SwitchLanguage

Scenario: SwitchLanguageToEnglish
	Given I am on any Beeline page
	And Current language is not English
	When I change language to English
	Then Current language is English

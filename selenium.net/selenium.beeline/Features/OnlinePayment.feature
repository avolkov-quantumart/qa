﻿Feature: OnlinePayment

Scenario: Mobile payment
	Given [Mobile payment page] Mobile tab is visible
	Then [Mobile payment page] Instruction for mobile payment is displayed

Scenario: Home payment
	Given [Mobile payment page] Home tab is visible
	Then [Mobile payment page] Instruction for home payment is displayed

Scenario: Wifi payment
	Given [Mobile payment page] Wifi tab is visible
	Then [Mobile payment page] Instruction for wifi payment is displayed


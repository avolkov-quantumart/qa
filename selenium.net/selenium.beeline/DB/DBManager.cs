﻿using System.Linq;

namespace selenium.beeline.DB
{
    public class DBManager
    {
        /// <summary>
        /// Получить из базы последний код подтверждения
        /// </summary>
        public static string GetLastSmsCode() {
            var smsTransferer = new beeline_SMS_Transferer();
            ConfirmData lastConfirmData = smsTransferer.ConfirmData.OrderByDescending(cd => cd.cdate).First();
            return lastConfirmData.code;
        }
    }
}

using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using QA.Beeline.DAL;

namespace selenium.beeline.DB {
    public class QPManager {
        public static UserSubscription GetActiveSubscription(string email) {
            return LinqHelper.Context.UserSubscriptions.SingleOrDefault(
                s => s.IsActive == true && s.IsDeleted != true && s.Email == email);
        }

        public static UserSubscription GetNotActiveSubscription(string email) {
            return LinqHelper.Context.UserSubscriptions.SingleOrDefault(
                s => s.IsActive == false && s.IsDeleted != true && s.Email == email);
        }

        public static List<UserSubscription> GetSubscriptions(string email) {
            return LinqHelper.Context.UserSubscriptions.Where(s => s.Email == email).ToList();
        }

        public static List<string> GetSubscriptionThemeNames(UserSubscription subscription) {
            return GetSubscriptionThemes(subscription).Where(t => t.IsActive != false)
                .Select(t => t.NewsCategory.Title).ToList();
        }

        /// <summary>
        /// �������� ���� �������� ��������� ��������
        /// </summary>
        public static IEnumerable<SubscriptionCategory> GetSubscriptionThemes(UserSubscription subscription) {
            return LinqHelper.Context.SubscriptionCategories.Where(c => c.Subscriber.Id == subscription.Id).ToList();
        }

        /// <summary>
        /// �������� ������� �� ��������� ��������
        /// </summary>
        public static List<ConfirmationRequest> GetSubscriptionActivations(string email) {
            // ������ �� ��������� ���������� �� ���� ������� ���� �������� �� ����� ������������
            // �.�. ���� ���� UserSubscription.IsActive = 0
            var notActiveSubscription = GetNotActiveSubscription(email);
            return GetSubscriptionActivations(notActiveSubscription);
        }

        public static List<ConfirmationRequest> GetSubscriptionActivations(UserSubscription subscription) {
            return LinqHelper.Context.ConfirmationRequests.Where(r => r.Subscription.Id == subscription.Id).ToList();
        }

        /// <summary>
        /// ������� �������� ��� ���������� email ���� ����������
        /// </summary>
        public static void RemoveSubscriptions(List<UserSubscription> subscriptions) {
            // ������� ���� ��������
            foreach (var userSubscription in subscriptions) {
                for (int i = userSubscription.SubscribedCategories.Count - 1; i >= 0; i--)
                    LinqHelper.Context.SubscriptionCategories.DeleteOnSubmit(userSubscription.SubscribedCategories[i]);
            }

            // ������� ������ �� ��������� ��������
            foreach (var userSubscription in subscriptions) {
                for (int i = userSubscription.ConfirmationRequests.Count - 1; i >= 0; i--)
                    LinqHelper.Context.ConfirmationRequests.DeleteOnSubmit(userSubscription.ConfirmationRequests[i]);
            }

            // ������� ��������
            foreach (var userSubscription in subscriptions) {
                LinqHelper.Context.UserSubscriptions.DeleteOnSubmit(userSubscription);
            }

            // ��������� ��������� � ��
            LinqHelper.Context.SubmitChanges();
        }

        public static UserSubscription GetSubscription(int subscriptionID) {
            return LinqHelper.Context.UserSubscriptions.SingleOrDefault(s => s.Id == subscriptionID);
        }
    }

//    [TestFixture]
//    public class TestQPManager {
//        [Test]
//        public void GetSubscription() {
//            UserSubscription userSubscription = QPManager.GetActiveSubscription("quantumart.selenium@gmail.com");
//            Assert.IsNotNull(userSubscription);
//        }
//    }
}
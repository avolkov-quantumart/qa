using System;

namespace QA.Beeline.DAL {
    public static partial class LinqHelper {
        [ThreadStatic]
        private static QPDataContext _context;

        public static QPDataContext Context {
            get { return _context ?? (_context = QPDataContext.Create()); }
        }
    }
}
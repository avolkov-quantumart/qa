﻿using System.Configuration;
using System.Data.Linq;
using QA.Core.Data;
using QA.Core.Data.Repository;

namespace QA.Beeline.DAL
{
    /// <summary>
    /// Контект к БД QP
    /// </summary>
    public partial class QPUnitOfWork : L2SqlUnitOfWorkBase<QPDataContext>
    {
        /// <summary>
        /// Конструирует объект
        /// </summary>
        /// <param name="connectionString">Строка подключения</param>
        public QPUnitOfWork(string connectionString) :
            base(connectionString, null)
        {
        }

        /// <summary>
        /// Конструирует объект
        /// </summary>
        /// <param name="connectionString">Строка подключения</param>
        /// <param name="mappingSource">Источник маппинга</param>
        public QPUnitOfWork(string connectionString, IXmlMappingResolver mappingSource) :
            base(connectionString, mappingSource)
        {
        }

        /// <summary>
        /// Конструирует объект 
        /// </summary> 
        /// <param name="connectionString">Строка подключения</param> 
        /// <param name="mappingSource">Источник маппинга</param> 
        /// <param name="siteName">Имя сайта</param> 
        public QPUnitOfWork(string connectionString, string siteName, IXmlMappingResolver mappingSource) :
            base(connectionString, siteName, mappingSource)
        {
        }


        /// <summary>
        /// Выполняется во время конструирования объекта
        /// </summary>
        protected override void OnCreated()
        {
            QPDataContext.DefaultConnectionString = ConfigurationManager
                .ConnectionStrings[_connectionStringName].ConnectionString;

            if (!string.IsNullOrEmpty(_siteName))
            {
                //QPDataContext.DefaultSiteName = _siteName;
            }

            if (_mappingSource != null)
            {
#warning не является потокобезопасным, закомментировал
                //QPDataContext.DefaultXmlMappingSource = _mappingSource.GetCurrentMapping();
            }
        }

        /// <summary>
        /// Создает контекст
        /// </summary>
        /// <returns></returns>
        protected override QPDataContext CreateContext()
        {
            var context = string.IsNullOrEmpty(_siteName) ?
                LinqHelper.Create(
                    _connectionStringName,
                    QPDataContext.DefaultXmlMappingSource) :

                LinqHelper.Create(
                _connectionStringName,
                    _siteName,
                QPDataContext.DefaultXmlMappingSource);

            DataLoadOptions dlo = new DataLoadOptions();

            //NOTE: not use n-to-many

            //dlo.LoadWith<MarketingRegion>(c => c.ExternalRegionMappingsToMarketingRegions);
            dlo.LoadWith<ExternalRegionMappingToMarketingRegion>(c => c.ExternalRegionMapping);
            dlo.LoadWith<ExternalRegionMappingToMarketingRegion>(c => c.MarketingRegion);
            dlo.LoadWith<ExternalRegionMapping>(c => c.ExternalSystem);
            
            dlo.LoadWith<MarketingRegion>(c => c.SiteProductToMarketingRegions);
            dlo.LoadWith<SiteProductToMarketingRegion>(c => c.SiteProduct);

            dlo.LoadWith<Poll>(c => c.Questions);
            dlo.LoadWith<PollQuestion>(c => c.Answers);
            dlo.LoadWith<UserPollAnswer>(c => c.Question);
            dlo.LoadWith<UserPollAnswer>(c => c.Answer);
            dlo.LoadWith<UserSubscription>(x => x.SubscribedCategories);
            dlo.LoadWith<UserSubscription>(x => x.ConfirmationRequests);

            //dlo.LoadWith<Action>(x => x.ActionInRegions);
            //dlo.LoadWith<Action>(x => x.ActionDeviceTypes);
            //dlo.LoadWith<Action>(x => x.ActionProducts);
            dlo.LoadWith<ActionDeviceType>(x => x.DeviceType);

			dlo.LoadWith<PrivelegeAndBonus>(x => x.PrivelegeAndBonusInRegions);
			dlo.LoadWith<PrivelegeAndBonus>(x => x.PrivelegeAndBonusDeviceTypes);
			dlo.LoadWith<PrivelegeAndBonus>(x => x.PrivelegeAndBonusProducts);
            
            //dlo.LoadWith<MarketingMobileTariff>(x => x.MarketingTariffDeviceTypes);
            //dlo.LoadWith<MarketingMobileTariff>(x => x.PreOpenParamGroups);
            
            dlo.LoadWith<MarketingTariffDeviceType>(x => x.DeviceType);
            dlo.LoadWith<MarketingTariffDeviceType>(x => x.MarketingMobileTariff);

            dlo.LoadWith<MarketingMobileTariff>(x => x.MobileTariffInCategories);

            dlo.LoadWith<MarketingMobileService>(x => x.Prefix);
            
            dlo.LoadWith<MarketingMobileServiceDeviceType>(x => x.DeviceType);
            dlo.LoadWith<MarketingMobileServiceDeviceType>(x => x.MarketingMobileService);

            //dlo.LoadWith<MarketingMobileService>(x => x.MobileServiceInCategories);
            //dlo.LoadWith<MobileServiceInCategory>(x => x.MobileServiceCategory);
            //dlo.LoadWith<MarketingMobileService>(x => x.ServiceInTariffParameterGroups);
            
            dlo.LoadWith<MobileServiceCategory>(x => x.MobileServiceFamilyInCategories);
            dlo.LoadWith<MobileServiceCategory>(x => x.MobileServiceInCategories);

            dlo.LoadWith<MobileTariffFamilyInCategory>(x => x.MobileTariffCategory);
            //dlo.LoadWith<MobileTariffCategory>(x => x.MobileTariffFamilyInCategories);
            //dlo.LoadWith<MobileTariffCategory>(x => x.MobileTariffInCategories);
            //dlo.LoadWith<MobileServiceFamily>(x => x.MobileServiceFamilyInCategories);
            dlo.LoadWith<MobileServiceFamily>(x => x.MarketingSign);
            //dlo.LoadWith<MobileTariffFamily>(x => x.MobileTariffFamilyInCategories);
            dlo.LoadWith<MobileTariffFamily>(x => x.MarketingSign);
            dlo.LoadWith<MutualMobileServiceGroup>(x => x.MutualMobileServices);
            dlo.LoadWith<MutualMobileService>(x => x.MarketingMobileService);
            dlo.LoadWith<MobileParamsGroupTab>(x => x.MobileParamGroupInTabs);
            dlo.LoadWith<MobileParamsGroupTab>(x => x.Tab);
            dlo.LoadWith<QuickLinksTitle>(x => x.Groups);

            dlo.LoadWith<TariffGuideQuestion>(x => x.Answers);
            dlo.LoadWith<TariffGuideQuestion>(x => x.GuideQuestionDeviceTypes);

            dlo.LoadWith<ProvodServiceCategory>(x => x.ProvodServiceFamilyInCategories);
            dlo.LoadWith<ProvodServiceCategory>(x => x.MarketingProvodServiceInCategories);
            
            dlo.LoadWith<MarketingMobileServiceMobileDeviceType>(x => x.MobileDevice);
            dlo.LoadWith<MobileServiceByRegionUsageType>(x => x.MobileServiceUsageTypes);
            
            dlo.LoadWith<InternetTariff>(x => x.MarketingTariff);
            dlo.LoadWith<InternetTariff>(x => x.SubscriptionFee);
            dlo.LoadWith<InternetTariff>(x => x.Params);
            dlo.LoadWith<InternetTariffCategory>(x => x.InternetTariffFamilyInCategories);
            dlo.LoadWith<InternetTariffCategory>(x => x.MarketingInternetTariffInCategories);
            dlo.LoadWith<PhoneTariff>(x => x.MarketingTariff);
            dlo.LoadWith<PhoneTariff>(x => x.SubscriptionFee);
            dlo.LoadWith<PhoneTariffCategory>(x => x.PhoneTariffFamilyInCategories);
            dlo.LoadWith<PhoneTariffCategory>(x => x.MarketingPhoneTariffInCategories);
            dlo.LoadWith<InternetTariffFamily>(x => x.InternetTariffFamilyInCategories);
            dlo.LoadWith<PhoneTariffFamily>(x => x.PhoneTariffFamilyInCategories);
            dlo.LoadWith<ProvodServiceFamily>(x => x.ProvodServiceFamilyInCategories);
            dlo.LoadWith<MarketingInternetTariff>(x => x.MarketingInternetTariffInCategories);
            dlo.LoadWith<MarketingInternetTariff>(x => x.MarketingSign);
            dlo.LoadWith<MarketingInternetTariff>(x => x.Family);
            dlo.LoadWith<MarketingInternetTariff>(x => x.MarketingProvodKits);
            dlo.LoadWith<MarketingPhoneTariff>(x => x.MarketingPhoneTariffInCategories);
            dlo.LoadWith<MarketingPhoneTariff>(x => x.Family);
            dlo.LoadWith<MarketingPhoneTariff>(x => x.MarketingSign);
            dlo.LoadWith<MarketingProvodService>(x => x.MarketingProvodServiceInCategories);
            dlo.LoadWith<MarketingProvodService>(x => x.MarketingProvodServiceInProducts);
            dlo.LoadWith<TVChannel>(x => x.TVChannelsInMarketingPackages);
            dlo.LoadWith<TVChannel>(x => x.TVChannelsInRegions);
            dlo.LoadWith<TVChannel>(x => x.TVProgramChannelsTVChannels);
            dlo.LoadWith<TVChannel>(x => x.TVChannelsInThemes);
            dlo.LoadWith<TVChannelRegion>(x => x.TVProgramChannelsInRegions);

            dlo.LoadWith<TVPackage>(x => x.MarketingPackage);
            dlo.LoadWith<TVPackage>(x => x.SubscriptionFeeType);
            dlo.LoadWith<MarketingTVPackage>(x => x.Category);
            dlo.LoadWith<MarketingTVPackage>(x => x.Family);
            dlo.LoadWith<MarketingTVPackage>(x => x.MarketingSign);
            dlo.LoadWith<MarketingTVPackage>(x => x.MarketingTVPackageInThemes);

            dlo.LoadWith<ProvodKit>(x => x.MarketKit);
            dlo.LoadWith<ProvodKit>(x => x.SubscriptionFeeType);
            dlo.LoadWith<MarketingProvodKit>(x => x.Family);
            dlo.LoadWith<MarketingProvodKit>(x => x.MarketingSign);

            dlo.LoadWith<DeviceType>(x => x.SiteProducts);
            dlo.LoadWith<SiteProduct>(x => x.DefaultDeviceType);

            dlo.LoadWith<HelpCenterParam>(x => x.HelpCenterParamInDeviceTypes);

            dlo.LoadWith<ArchiveTariff>(x => x.Tabs);

            dlo.LoadWith<ArchiveService>(x => x.Bookmarks);

            dlo.LoadWith<RoamingTariffZone>(x => x.TariffParams);
            dlo.LoadWith<RoamingTariffZone>(x => x.MarketingMobileServiceInRoamingTariffs);

            dlo.LoadWith<CpaShortNumber>(x => x.Partner);

            dlo.LoadWith<SearchAnnouncement>(x => x.SearchAnnouncementInRegions);
            dlo.LoadWith<SearchSuggestion>(x => x.SearchSuggestInRegions);

            dlo.LoadWith<ServiceForIternetTariff>(x => x.Service);
            dlo.LoadWith<MarketingProvodService>(x => x.Family);
            dlo.LoadWith<MarketingProvodService>(x => x.MarketingSign);
            dlo.LoadWith<ProvodServiceParam>(x => x.ParamGroup);
            dlo.LoadWith<ServiceForPhoneTariff>(x => x.Service);
            dlo.LoadWith<ProvodService>(x => x.MarketingService);
            dlo.LoadWith<ProvodService>(x => x.SubscriptionFeeType);
            dlo.LoadWith<ProvodServiceInRegion>(x => x.MarketingRegion);
            dlo.LoadWith<InternetTariff>(x => x.SubscriptionFeeType);
            dlo.LoadWith<PhoneTariff>(x => x.SubscriptionFeeType);
            dlo.LoadWith<MarketingPhoneTariff>(x => x.UnavailableServicesIDs);
            dlo.LoadWith<InternetTariffParam>(x => x.Group);

            dlo.LoadWith<MarketingInternetTariff>(x => x.TitleFormat);
            dlo.LoadWith<MarketingTVPackage>(x => x.TitleFormat);
            dlo.LoadWith<MarketingProvodService>(x => x.TitleFormat);
            dlo.LoadWith<MarketingPhoneTariff>(x => x.TitleFormat);
            dlo.LoadWith<MarketingProvodKit>(x => x.TitleFormat);
            dlo.LoadWith<MarketingInternetTariff>(x => x.Services);
            dlo.LoadWith<MarketingPhoneTariff>(x => x.Services);
            //dlo.LoadWith<MarketingProvodService>(x => x.ProvodServices);
            dlo.LoadWith<MarketingProvodKit>(x => x.PhoneTariff);
            //dlo.LoadWith<MarketingProvodKit>(x => x.InternetTariff);

            dlo.LoadWith<HelpDeviceType>(c => c.HelpDeviceInRegions);
            
            dlo.LoadWith<RoamingOptionCalculation>(x => x.RoamingTariffsInOptions);
            dlo.LoadWith<RoamingOptionCalculation>(x => x.RoamingOptionInRegions);

            dlo.LoadWith<NewsArticle>(c => c.NewsInRegions);

            context.LoadOptions = dlo;
            //TODO: надо раскомментарить!
            //context.DeferredLoadingEnabled = false;

            

            return context;
        }
    }
}

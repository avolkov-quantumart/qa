﻿using System.Data;
using System.Data.Linq.Mapping;
using QA.Core.Data.Repository;
using QA.Core.Web;

namespace QA.Beeline.DAL {
    /// <summary>
    /// Расширение контекста
    /// </summary>
    public partial class QPDataContext {
        /// <summary>
        /// Создает контектс
        /// </summary>
        /// <param name="connection">Экземпляр подклчюения к БД</param>
        /// <param name="mappingSource">Источник маппинга стурктуры</param>
        /// <returns></returns>
        public static QPDataContext Create(IDbConnection connection, MappingSource mappingSource) {
            var ctx = new QPDataContext(connection, mappingSource);
            ctx.SiteName = DefaultSiteName;
            ctx.ConnectionString = connection.ConnectionString;
            return ctx;
        }
    }

    /// <summary>
    /// Расширение хелпера linq
    /// </summary>
    public static partial class LinqHelper {
        /// <summary>
        /// Менеджер подключений к БД
        /// </summary>
        private static readonly RequestLocal<ConnectionManager> _connManager =
            new RequestLocal<ConnectionManager>(() => new ConnectionManager());

        /// <summary>
        /// Создает контекст
        /// </summary>
        /// <param name="connectionName">Имя подключение к БД</param>
        /// <param name="mappingSource">Источник маппинга</param>
        /// <returns></returns>
        public static QPDataContext Create(string connectionName, MappingSource mappingSource) {
            IDbConnection conn = _connManager.Value.GetConnection(connectionName);
            QPDataContext ctx = QPDataContext.Create(conn, mappingSource);
            if (_context == null) _context = ctx;
            return ctx;
        }

        /// <summary>
        /// Создает контекст
        /// </summary>
        /// <param name="connectionName">Имя подключение к БД</param>
        /// <param name="mappingSource">Источник маппинга</param>
        /// <param name="siteName">Имя сайта</param>
        /// <returns></returns>
        public static QPDataContext Create(string connectionName, string siteName, MappingSource mappingSource) {
            IDbConnection conn = _connManager.Value.GetConnection(connectionName);
            QPDataContext ctx = QPDataContext.Create(conn, siteName, mappingSource);
            if (_context == null) _context = ctx;
            return ctx;
        }
    }
}
﻿
using Quantumart.QPublishing.Database;
using System.Collections;
using System.Web;
using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;


namespace QA.Beeline.DAL 
{


public partial class QPDataContext
{


    
	private Hashtable PackGeoRegion(GeoRegion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "GeoRegion", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Parent_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "GeoRegion", "Parent"), instance.Parent_ID.ToString()); }

   if (instance.OldId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "GeoRegion", "OldId"), instance.OldId.ToString()); }

   if (instance.OldParentId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "GeoRegion", "OldParentId"), instance.OldParentId.ToString()); }

   if (instance.MarketingRegion_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "GeoRegion", "MarketingRegion"), instance.MarketingRegion_ID.ToString()); }

   if (instance.MniId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "GeoRegion", "MniId"), instance.MniId.ToString()); }

		return Values;
	}
	

//	partial void InsertGeoRegion(GeoRegion instance) 
//	{	
//		
//		Cnn.ExternalTransaction = Transaction;
//		Hashtable Values = PackGeoRegion(instance);
//		HttpFileCollection cl = (HttpFileCollection)null;
//		DateTime created = DateTime.Now;
//		instance.LoadStatusType();
//		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "GeoRegion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
//		instance.Created = created;
//		instance.Modified = created;
//            
//	}
//
//	partial void UpdateGeoRegion(GeoRegion instance)
//	{	
//		
//		Cnn.ExternalTransaction = Transaction;
//		Hashtable Values = PackGeoRegion(instance);
//		HttpFileCollection cl = (HttpFileCollection)null;
//		DateTime modified = DateTime.Now;
//		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "GeoRegion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
//		instance.Modified = modified;
//            		
//	}			

	partial void DeleteGeoRegion(GeoRegion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMarketingRegion(MarketingRegion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Parent_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "Parent"), instance.Parent_ID.ToString()); }

   if (instance.Url != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "Url"), ReplaceUrls(instance.Url)); }

   if (instance.IsPopular != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "IsPopular"), ((bool)instance.IsPopular) ? "1" : "0"); }

   if (instance.IsMain != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "IsMain"), ((bool)instance.IsMain) ? "1" : "0"); }

   if (instance.Has3G != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "Has3G"), ((bool)instance.Has3G) ? "1" : "0"); }

   if (instance.IsB2B != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "IsB2B"), ((bool)instance.IsB2B) ? "1" : "0"); }

   if (instance.Lat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "Lat"), instance.Lat.ToString()); }

   if (instance.Lon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "Lon"), instance.Lon.ToString()); }

   if (instance.Lon1 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "Lon1"), instance.Lon1.ToString()); }

   if (instance.Lat1 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "Lat1"), instance.Lat1.ToString()); }

   if (instance.Lon2 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "Lon2"), instance.Lon2.ToString()); }

   if (instance.Lat2 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "Lat2"), instance.Lat2.ToString()); }

   if (instance.GMT != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "GMT"), instance.GMT.ToString()); }

   if (instance.RegionAddress != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "RegionAddress"), ReplaceUrls(instance.RegionAddress)); }

   if (instance.RegionPhone != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "RegionPhone"), ReplaceUrls(instance.RegionPhone)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "Order"), instance.Order.ToString()); }

   if (instance.TitlePreposition != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "TitlePreposition"), ReplaceUrls(instance.TitlePreposition)); }

   if (instance.TitlePrepositionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "TitlePrepositionEngl"), ReplaceUrls(instance.TitlePrepositionEngl)); }

   if (instance.TitlePrepositionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegion", "TitlePrepositionTat"), ReplaceUrls(instance.TitlePrepositionTat)); }

		return Values;
	}
	

	partial void InsertMarketingRegion(MarketingRegion instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingRegion(instance);
		System.Web.HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingRegion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMarketingRegion(MarketingRegion instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingRegion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMarketingRegion(MarketingRegion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackGeoRegionsIPAddress(GeoRegionsIPAddress instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.GeoRegion_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "GeoRegionsIPAddress", "GeoRegion"), instance.GeoRegion_ID.ToString()); }

   if (instance.IpFrom != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "GeoRegionsIPAddress", "IpFrom"), instance.IpFrom.ToString()); }

   if (instance.IpTo != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "GeoRegionsIPAddress", "IpTo"), instance.IpTo.ToString()); }

   if (instance.OldGniId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "GeoRegionsIPAddress", "OldGniId"), instance.OldGniId.ToString()); }

   if (instance.OldId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "GeoRegionsIPAddress", "OldId"), instance.OldId.ToString()); }

		return Values;
	}
	

	partial void InsertGeoRegionsIPAddress(GeoRegionsIPAddress instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackGeoRegionsIPAddress(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "GeoRegionsIPAddress"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateGeoRegionsIPAddress(GeoRegionsIPAddress instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackGeoRegionsIPAddress(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "GeoRegionsIPAddress"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteGeoRegionsIPAddress(GeoRegionsIPAddress instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSiteProduct(SiteProduct instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Mask != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "Mask"), ReplaceUrls(instance.Mask)); }

   if (instance.RegularExpression != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "RegularExpression"), ReplaceUrls(instance.RegularExpression)); }

   if (instance.SiteNum != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "SiteNum"), instance.SiteNum.ToString()); }

   if (instance.StageMask != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "StageMask"), ReplaceUrls(instance.StageMask)); }

   if (instance.StageTitle != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "StageTitle"), ReplaceUrls(instance.StageTitle)); }

   if (instance.DefaultRegion_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "DefaultRegion"), instance.DefaultRegion_ID.ToString()); }

   if (instance.OldId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "OldId"), instance.OldId.ToString()); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DefaultDeviceType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "DefaultDeviceType"), instance.DefaultDeviceType_ID.ToString()); }

   if (instance.PrepositionalTitle != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "PrepositionalTitle"), ReplaceUrls(instance.PrepositionalTitle)); }

   if (instance.PrepositionalTitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "PrepositionalTitleEngl"), ReplaceUrls(instance.PrepositionalTitleEngl)); }

   if (instance.PrepositionalTitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "PrepositionalTitleTat"), ReplaceUrls(instance.PrepositionalTitleTat)); }

   if (instance.FeedbackDefaultTheme_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteProduct", "FeedbackDefaultTheme"), instance.FeedbackDefaultTheme_ID.ToString()); }

		return Values;
	}
	

	partial void InsertSiteProduct(SiteProduct instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSiteProduct(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SiteProduct"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSiteProduct(SiteProduct instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSiteProduct(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SiteProduct"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSiteProduct(SiteProduct instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackQPAbstractItem(QPAbstractItem instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Name != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "Name"), ReplaceUrls(instance.Name)); }

   if (instance.Parent_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "Parent"), instance.Parent_ID.ToString()); }

   if (instance.IsVisible != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "IsVisible"), ((bool)instance.IsVisible) ? "1" : "0"); }

   if (instance.IsPage != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "IsPage"), ((bool)instance.IsPage) ? "1" : "0"); }

   if (instance.ZoneName != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "ZoneName"), ReplaceUrls(instance.ZoneName)); }

   if (instance.AllowedUrlPatterns != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "AllowedUrlPatterns"), ReplaceUrls(instance.AllowedUrlPatterns)); }

   if (instance.DeniedUrlPatterns != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "DeniedUrlPatterns"), ReplaceUrls(instance.DeniedUrlPatterns)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.Discriminator_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "Discriminator"), instance.Discriminator_ID.ToString()); }

   if (instance.ContentId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "ContentId"), instance.ContentId.ToString()); }

   if (instance.VersionOf_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "VersionOf"), instance.VersionOf_ID.ToString()); }

   if (instance.Culture_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "Culture"), instance.Culture_ID.ToString()); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

   if (instance.Keywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "Keywords"), ReplaceUrls(instance.Keywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.Tags != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "Tags"), ReplaceUrls(instance.Tags)); }

   if (instance.IsInSiteMap != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "IsInSiteMap"), ((bool)instance.IsInSiteMap) ? "1" : "0"); }

   if (instance.IndexOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "IndexOrder"), instance.IndexOrder.ToString()); }

   if (instance.ExtensionId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPAbstractItem", "ExtensionId"), instance.ExtensionId.ToString()); }

		return Values;
	}
	

	partial void InsertQPAbstractItem(QPAbstractItem instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQPAbstractItem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QPAbstractItem"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateQPAbstractItem(QPAbstractItem instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQPAbstractItem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QPAbstractItem"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteQPAbstractItem(QPAbstractItem instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackQPDiscriminator(QPDiscriminator instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPDiscriminator", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Name != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPDiscriminator", "Name"), ReplaceUrls(instance.Name)); }

   if (instance.PreferredContentId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPDiscriminator", "PreferredContentId"), instance.PreferredContentId.ToString()); }

   if (instance.CategoryName != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPDiscriminator", "CategoryName"), ReplaceUrls(instance.CategoryName)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPDiscriminator", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.IconUrl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPDiscriminator", "IconUrl"), instance.IconUrl); }

   if (instance.IsPage != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPDiscriminator", "IsPage"), ((bool)instance.IsPage) ? "1" : "0"); }

   if (instance.AllowedZones != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPDiscriminator", "AllowedZones"), ReplaceUrls(instance.AllowedZones)); }

   if (instance.FilterPartByUrl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPDiscriminator", "FilterPartByUrl"), ((bool)instance.FilterPartByUrl) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertQPDiscriminator(QPDiscriminator instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQPDiscriminator(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QPDiscriminator"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateQPDiscriminator(QPDiscriminator instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQPDiscriminator(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QPDiscriminator"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteQPDiscriminator(QPDiscriminator instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackQPCulture(QPCulture instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPCulture", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Name != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPCulture", "Name"), ReplaceUrls(instance.Name)); }

   if (instance.Icon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPCulture", "Icon"), instance.Icon); }

		return Values;
	}
	

	partial void InsertQPCulture(QPCulture instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQPCulture(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QPCulture"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateQPCulture(QPCulture instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQPCulture(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QPCulture"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteQPCulture(QPCulture instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackItemTitleFormat(ItemTitleFormat instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Value != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ItemTitleFormat", "Value"), ReplaceUrls(instance.Value)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ItemTitleFormat", "Description"), ReplaceUrls(instance.Description)); }

		return Values;
	}
	

	partial void InsertItemTitleFormat(ItemTitleFormat instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackItemTitleFormat(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ItemTitleFormat"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateItemTitleFormat(ItemTitleFormat instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackItemTitleFormat(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ItemTitleFormat"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteItemTitleFormat(ItemTitleFormat instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 

	partial void InsertQPRegion(QPRegion instance) 
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
			
	}

	partial void UpdateQPRegion(QPRegion instance)
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
					
	}			

	partial void DeleteQPRegion(QPRegion instance)
	{
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
				
	}
 
	private Hashtable PackPoll(Poll instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Poll", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.MarketCodes != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Poll", "MarketCodes"), ReplaceUrls(instance.MarketCodes)); }

   if (instance.StartDate != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Poll", "StartDate"), instance.StartDate.ToString()); }

   if (instance.EndDate != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Poll", "EndDate"), instance.EndDate.ToString()); }

		return Values;
	}
	

	partial void InsertPoll(Poll instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPoll(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "Poll"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePoll(Poll instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPoll(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "Poll"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePoll(Poll instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPollQuestion(PollQuestion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PollQuestion", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.Poll_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PollQuestion", "Poll"), instance.Poll_ID.ToString()); }

   if (instance.NumberOfAnswers != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PollQuestion", "NumberOfAnswers"), instance.NumberOfAnswers.ToString()); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PollQuestion", "SortOrder"), instance.SortOrder.ToString()); }

		return Values;
	}
	

	partial void InsertPollQuestion(PollQuestion instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPollQuestion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PollQuestion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePollQuestion(PollQuestion instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPollQuestion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PollQuestion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePollQuestion(PollQuestion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPollAnswer(PollAnswer instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PollAnswer", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.Question_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PollAnswer", "Question"), instance.Question_ID.ToString()); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PollAnswer", "SortOrder"), instance.SortOrder.ToString()); }

		return Values;
	}
	

	partial void InsertPollAnswer(PollAnswer instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPollAnswer(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PollAnswer"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePollAnswer(PollAnswer instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPollAnswer(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PollAnswer"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePollAnswer(PollAnswer instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackUserPollAnswer(UserPollAnswer instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.User != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserPollAnswer", "User"), ReplaceUrls(instance.User)); }

   if (instance.Answer_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserPollAnswer", "Answer"), instance.Answer_ID.ToString()); }

   if (instance.Question_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserPollAnswer", "Question"), instance.Question_ID.ToString()); }

   if (instance.Poll_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserPollAnswer", "Poll"), instance.Poll_ID.ToString()); }

   if (instance.MarketCode != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserPollAnswer", "MarketCode"), ReplaceUrls(instance.MarketCode)); }

   if (instance.AnswerCode != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserPollAnswer", "AnswerCode"), ReplaceUrls(instance.AnswerCode)); }

		return Values;
	}
	

	partial void InsertUserPollAnswer(UserPollAnswer instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackUserPollAnswer(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "UserPollAnswer"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateUserPollAnswer(UserPollAnswer instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackUserPollAnswer(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "UserPollAnswer"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteUserPollAnswer(UserPollAnswer instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 

	partial void InsertTrailedAbstractItem(TrailedAbstractItem instance) 
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
			
	}

	partial void UpdateTrailedAbstractItem(TrailedAbstractItem instance)
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
					
	}			

	partial void DeleteTrailedAbstractItem(TrailedAbstractItem instance)
	{
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
				
	}
 
	private Hashtable PackQPObsoleteUrl(QPObsoleteUrl instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Url != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPObsoleteUrl", "Url"), ReplaceUrls(instance.Url)); }

   if (instance.AbstractItem_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPObsoleteUrl", "AbstractItem"), instance.AbstractItem_ID.ToString()); }

		return Values;
	}
	

	partial void InsertQPObsoleteUrl(QPObsoleteUrl instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQPObsoleteUrl(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QPObsoleteUrl"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateQPObsoleteUrl(QPObsoleteUrl instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQPObsoleteUrl(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QPObsoleteUrl"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteQPObsoleteUrl(QPObsoleteUrl instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRegionTag(RegionTag instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RegionTag", "Title"), ReplaceUrls(instance.Title)); }

		return Values;
	}
	

	partial void InsertRegionTag(RegionTag instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRegionTag(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RegionTag"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRegionTag(RegionTag instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRegionTag(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RegionTag"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRegionTag(RegionTag instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRegionTagValue(RegionTagValue instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.RegionTag_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RegionTagValue", "RegionTag"), instance.RegionTag_ID.ToString()); }

   if (instance.Value != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RegionTagValue", "Value"), ReplaceUrls(instance.Value)); }

		return Values;
	}
	

	partial void InsertRegionTagValue(RegionTagValue instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRegionTagValue(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RegionTagValue"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRegionTagValue(RegionTagValue instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRegionTagValue(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RegionTagValue"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRegionTagValue(RegionTagValue instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSearchSuggestion(SearchSuggestion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SearchSuggestion", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Priority != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SearchSuggestion", "Priority"), instance.Priority.ToString()); }

		return Values;
	}
	

	partial void InsertSearchSuggestion(SearchSuggestion instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSearchSuggestion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SearchSuggestion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSearchSuggestion(SearchSuggestion instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSearchSuggestion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SearchSuggestion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSearchSuggestion(SearchSuggestion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSearchResult(SearchResult instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Query != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SearchResult", "Query"), ReplaceUrls(instance.Query)); }

   if (instance.PageUrl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SearchResult", "PageUrl"), ReplaceUrls(instance.PageUrl)); }

   if (instance.IsTop != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SearchResult", "IsTop"), ((bool)instance.IsTop) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertSearchResult(SearchResult instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSearchResult(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SearchResult"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSearchResult(SearchResult instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSearchResult(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SearchResult"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSearchResult(SearchResult instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSiteSection(SiteSection instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteSection", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteSection", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteSection", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteSection", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertSiteSection(SiteSection instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSiteSection(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SiteSection"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSiteSection(SiteSection instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSiteSection(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SiteSection"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSiteSection(SiteSection instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackNewsCategory(NewsCategory instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsCategory", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsCategory", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsCategory", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertNewsCategory(NewsCategory instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackNewsCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "NewsCategory"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateNewsCategory(NewsCategory instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackNewsCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "NewsCategory"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteNewsCategory(NewsCategory instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackNewsArticle(NewsArticle instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.StartPublishDate != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "StartPublishDate"), instance.StartPublishDate.ToString()); }

   if (instance.EndPublishDate != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "EndPublishDate"), instance.EndPublishDate.ToString()); }

   if (instance.Category_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "Category"), instance.Category_ID.ToString()); }

   if (instance.SiteProduct_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "SiteProduct"), instance.SiteProduct_ID.ToString()); }

   if (instance.ShowOnWeb != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "ShowOnWeb"), ((bool)instance.ShowOnWeb) ? "1" : "0"); }

   if (instance.ShowInSelfService != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "ShowInSelfService"), ((bool)instance.ShowInSelfService) ? "1" : "0"); }

   if (instance.ExcludeFromSubscription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "ExcludeFromSubscription"), ((bool)instance.ExcludeFromSubscription) ? "1" : "0"); }

   if (instance.IsUrgent != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "IsUrgent"), ((bool)instance.IsUrgent) ? "1" : "0"); }

   if (instance.PaymentSystem != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "PaymentSystem"), ReplaceUrls(instance.PaymentSystem)); }

   if (instance.Currency != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "Currency"), ReplaceUrls(instance.Currency)); }

   if (instance.BusinessSegment != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "BusinessSegment"), ReplaceUrls(instance.BusinessSegment)); }

   if (instance.Preview != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "Preview"), ReplaceUrls(instance.Preview)); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.Tariff != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "Tariff"), ReplaceUrls(instance.Tariff)); }

   if (instance.FileCTN != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "FileCTN"), ReplaceUrls(instance.FileCTN)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.PreviewEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "PreviewEngl"), ReplaceUrls(instance.PreviewEngl)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.PreviewTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "PreviewTat"), ReplaceUrls(instance.PreviewTat)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "TextTat"), ReplaceUrls(instance.TextTat)); }

   if (instance.MetaKeywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "MetaKeywords"), ReplaceUrls(instance.MetaKeywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "Image"), instance.Image); }

   if (instance.TileImage != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "TileImage"), instance.TileImage); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NewsArticle", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

		return Values;
	}
	

	partial void InsertNewsArticle(NewsArticle instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackNewsArticle(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "NewsArticle"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateNewsArticle(NewsArticle instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackNewsArticle(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "NewsArticle"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteNewsArticle(NewsArticle instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackNotificationTemplate(NotificationTemplate instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NotificationTemplate", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TemplateCode != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NotificationTemplate", "TemplateCode"), ReplaceUrls(instance.TemplateCode)); }

   if (instance.Subject != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "NotificationTemplate", "Subject"), ReplaceUrls(instance.Subject)); }

		return Values;
	}
	

	partial void InsertNotificationTemplate(NotificationTemplate instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackNotificationTemplate(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "NotificationTemplate"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateNotificationTemplate(NotificationTemplate instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackNotificationTemplate(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "NotificationTemplate"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteNotificationTemplate(NotificationTemplate instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRoamingTariffZone(RoamingTariffZone instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffZone", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffZone", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffZone", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffZone", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffZone", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffZone", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffZone", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffZone", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.InterCityRegion_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffZone", "InterCityRegion"), instance.InterCityRegion_ID.ToString()); }

   if (instance.HiddenByDefault != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffZone", "HiddenByDefault"), ((bool)instance.HiddenByDefault) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertRoamingTariffZone(RoamingTariffZone instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingTariffZone(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingTariffZone"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRoamingTariffZone(RoamingTariffZone instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingTariffZone(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingTariffZone"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRoamingTariffZone(RoamingTariffZone instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRoamingCountryZone(RoamingCountryZone instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Country_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingCountryZone", "Country"), instance.Country_ID.ToString()); }

   if (instance.TariffZone_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingCountryZone", "TariffZone"), instance.TariffZone_ID.ToString()); }

   if (instance.RoamingOperator_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingCountryZone", "RoamingOperator"), instance.RoamingOperator_ID.ToString()); }

   if (instance.LocalRoamingOperator_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingCountryZone", "LocalRoamingOperator"), instance.LocalRoamingOperator_ID.ToString()); }

   if (instance.LocalRoamingRegion_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingCountryZone", "LocalRoamingRegion"), instance.LocalRoamingRegion_ID.ToString()); }

		return Values;
	}
	

	partial void InsertRoamingCountryZone(RoamingCountryZone instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingCountryZone(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingCountryZone"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRoamingCountryZone(RoamingCountryZone instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingCountryZone(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingCountryZone"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRoamingCountryZone(RoamingCountryZone instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackUserSubscription(UserSubscription instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Email != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserSubscription", "Email"), ReplaceUrls(instance.Email)); }

   if (instance.Login != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserSubscription", "Login"), ReplaceUrls(instance.Login)); }

   if (instance.IsActive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserSubscription", "IsActive"), ((bool)instance.IsActive) ? "1" : "0"); }

   if (instance.IsDeleted != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserSubscription", "IsDeleted"), ((bool)instance.IsDeleted) ? "1" : "0"); }

   if (instance.IsOld != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserSubscription", "IsOld"), ((bool)instance.IsOld) ? "1" : "0"); }

   if (instance.MarketRegion_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserSubscription", "MarketRegion"), instance.MarketRegion_ID.ToString()); }

   if (instance.NewRegion_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserSubscription", "NewRegion"), instance.NewRegion_ID.ToString()); }

   if (instance.IsHtml != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserSubscription", "IsHtml"), ((bool)instance.IsHtml) ? "1" : "0"); }

   if (instance.FailedDeliveries != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "UserSubscription", "FailedDeliveries"), instance.FailedDeliveries.ToString()); }

		return Values;
	}
	

	partial void InsertUserSubscription(UserSubscription instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackUserSubscription(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "UserSubscription"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateUserSubscription(UserSubscription instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackUserSubscription(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "UserSubscription"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteUserSubscription(UserSubscription instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSubscriptionCategory(SubscriptionCategory instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.NewsCategory_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionCategory", "NewsCategory"), instance.NewsCategory_ID.ToString()); }

   if (instance.Subscriber_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionCategory", "Subscriber"), instance.Subscriber_ID.ToString()); }

   if (instance.IsActive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionCategory", "IsActive"), ((bool)instance.IsActive) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertSubscriptionCategory(SubscriptionCategory instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSubscriptionCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SubscriptionCategory"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSubscriptionCategory(SubscriptionCategory instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSubscriptionCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SubscriptionCategory"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSubscriptionCategory(SubscriptionCategory instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackConfirmationRequest(ConfirmationRequest instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Code != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ConfirmationRequest", "Code"), ReplaceUrls(instance.Code)); }

   if (instance.Subscription_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ConfirmationRequest", "Subscription"), instance.Subscription_ID.ToString()); }

   if (instance.Action != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ConfirmationRequest", "Action"), instance.Action.ToString()); }

		return Values;
	}
	

	partial void InsertConfirmationRequest(ConfirmationRequest instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackConfirmationRequest(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ConfirmationRequest"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateConfirmationRequest(ConfirmationRequest instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackConfirmationRequest(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ConfirmationRequest"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteConfirmationRequest(ConfirmationRequest instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackFeedbackTheme(FeedbackTheme instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.SendToEmail != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "SendToEmail"), ReplaceUrls(instance.SendToEmail)); }

   if (instance.ShowHasContract != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "ShowHasContract"), ((bool)instance.ShowHasContract) ? "1" : "0"); }

   if (instance.ShowLogin != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "ShowLogin"), ((bool)instance.ShowLogin) ? "1" : "0"); }

   if (instance.ShowBeelinePhone != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "ShowBeelinePhone"), ((bool)instance.ShowBeelinePhone) ? "1" : "0"); }

   if (instance.ShowAttachmentsForEmail != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "ShowAttachmentsForEmail"), ((bool)instance.ShowAttachmentsForEmail) ? "1" : "0"); }

   if (instance.ShowAttachmentsForCall != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "ShowAttachmentsForCall"), ((bool)instance.ShowAttachmentsForCall) ? "1" : "0"); }

   if (instance.FilterThemeId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "FilterThemeId"), ReplaceUrls(instance.FilterThemeId)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "Order"), instance.Order.ToString()); }

   if (instance.Source != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "Source"), ReplaceUrls(instance.Source)); }

   if (instance.OnlineChatId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackTheme", "OnlineChatId"), ReplaceUrls(instance.OnlineChatId)); }

		return Values;
	}
	

	partial void InsertFeedbackTheme(FeedbackTheme instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackTheme(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackTheme"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateFeedbackTheme(FeedbackTheme instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackTheme(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackTheme"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteFeedbackTheme(FeedbackTheme instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackFeedbackType(FeedbackType instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.FilterTypeId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "FilterTypeId"), ReplaceUrls(instance.FilterTypeId)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "Order"), instance.Order.ToString()); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "TextTat"), ReplaceUrls(instance.TextTat)); }

   if (instance.SuccessTitle != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "SuccessTitle"), ReplaceUrls(instance.SuccessTitle)); }

   if (instance.SuccessTitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "SuccessTitleEngl"), ReplaceUrls(instance.SuccessTitleEngl)); }

   if (instance.SuccessTitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "SuccessTitleTat"), ReplaceUrls(instance.SuccessTitleTat)); }

   if (instance.SuccessText != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "SuccessText"), ReplaceUrls(instance.SuccessText)); }

   if (instance.SuccessTextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "SuccessTextEngl"), ReplaceUrls(instance.SuccessTextEngl)); }

   if (instance.SuccessTextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackType", "SuccessTextTat"), ReplaceUrls(instance.SuccessTextTat)); }

		return Values;
	}
	

	partial void InsertFeedbackType(FeedbackType instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackType"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateFeedbackType(FeedbackType instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackType"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteFeedbackType(FeedbackType instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackFeedbackSubtheme(FeedbackSubtheme instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubtheme", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.FeedbackType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubtheme", "FeedbackType"), instance.FeedbackType_ID.ToString()); }

   if (instance.Theme_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubtheme", "Theme"), instance.Theme_ID.ToString()); }

   if (instance.FeedbackSubthemeGroup_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubtheme", "FeedbackSubthemeGroup"), instance.FeedbackSubthemeGroup_ID.ToString()); }

   if (instance.ContactType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubtheme", "ContactType"), ReplaceUrls(instance.ContactType)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubtheme", "Order"), instance.Order.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubtheme", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubtheme", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertFeedbackSubtheme(FeedbackSubtheme instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackSubtheme(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackSubtheme"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateFeedbackSubtheme(FeedbackSubtheme instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackSubtheme(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackSubtheme"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteFeedbackSubtheme(FeedbackSubtheme instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackFeedbackQueue(FeedbackQueue instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.SourceID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackQueue", "SourceID"), ReplaceUrls(instance.SourceID)); }

   if (instance.Theme_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackQueue", "Theme"), instance.Theme_ID.ToString()); }

		return Values;
	}
	

	partial void InsertFeedbackQueue(FeedbackQueue instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackQueue(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackQueue"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateFeedbackQueue(FeedbackQueue instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackQueue(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackQueue"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteFeedbackQueue(FeedbackQueue instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackTVChannel(TVChannel instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TopOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "TopOrder"), instance.TopOrder.ToString()); }

   if (instance.IsOnline != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "IsOnline"), ((bool)instance.IsOnline) ? "1" : "0"); }

   if (instance.OnlineId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "OnlineId"), ReplaceUrls(instance.OnlineId)); }

   if (instance.LogoImage != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "LogoImage"), instance.LogoImage); }

   if (instance.GrayLogo != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "GrayLogo"), instance.GrayLogo); }

   if (instance.GrayLogoSmall != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "GrayLogoSmall"), instance.GrayLogoSmall); }

   if (instance.SmallLogo != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "SmallLogo"), instance.SmallLogo); }

   if (instance.TinyLogo != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "TinyLogo"), instance.TinyLogo); }

   if (instance.MinAge != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "MinAge"), instance.MinAge.ToString()); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.VideoPreviewImage != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "VideoPreviewImage"), instance.VideoPreviewImage); }

   if (instance.PromoVideo != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "PromoVideo"), instance.PromoVideo); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.Languages != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannel", "Languages"), ReplaceUrls(instance.Languages)); }

		return Values;
	}
	

	partial void InsertTVChannel(TVChannel instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVChannel(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVChannel"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateTVChannel(TVChannel instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVChannel(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVChannel"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteTVChannel(TVChannel instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRegionFeedbackGroup(RegionFeedbackGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Code != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RegionFeedbackGroup", "Code"), ReplaceUrls(instance.Code)); }

   if (instance.FeedbackType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RegionFeedbackGroup", "FeedbackType"), instance.FeedbackType_ID.ToString()); }

		return Values;
	}
	

	partial void InsertRegionFeedbackGroup(RegionFeedbackGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRegionFeedbackGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RegionFeedbackGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRegionFeedbackGroup(RegionFeedbackGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRegionFeedbackGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RegionFeedbackGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRegionFeedbackGroup(RegionFeedbackGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackDeviceType(DeviceType instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "Image"), instance.Image); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.IconForTariff != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "IconForTariff"), instance.IconForTariff); }

   if (instance.TitleForIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "TitleForIcon"), ReplaceUrls(instance.TitleForIcon)); }

   if (instance.TitleForIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "TitleForIconEngl"), ReplaceUrls(instance.TitleForIconEngl)); }

   if (instance.TitleForIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "TitleForIconTat"), ReplaceUrls(instance.TitleForIconTat)); }

   if (instance.DefaultTariffTileIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "DefaultTariffTileIcon"), instance.DefaultTariffTileIcon); }

   if (instance.DefaultServiceTileIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "DefaultServiceTileIcon"), instance.DefaultServiceTileIcon); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DeviceType", "Alias"), ReplaceUrls(instance.Alias)); }

		return Values;
	}
	

	partial void InsertDeviceType(DeviceType instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackDeviceType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "DeviceType"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateDeviceType(DeviceType instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackDeviceType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "DeviceType"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteDeviceType(DeviceType instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackAction(Action instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.Priority != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "Priority"), instance.Priority.ToString()); }

   if (instance.StartDate != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "StartDate"), instance.StartDate.ToString()); }

   if (instance.EndDate != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "EndDate"), instance.EndDate.ToString()); }

   if (instance.SpecialConditions != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "SpecialConditions"), ReplaceUrls(instance.SpecialConditions)); }

   if (instance.Preview != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "Preview"), ReplaceUrls(instance.Preview)); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.ProductLinks != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "ProductLinks"), ReplaceUrls(instance.ProductLinks)); }

   if (instance.IsArchive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "IsArchive"), ((bool)instance.IsArchive) ? "1" : "0"); }

   if (instance.MetaKeywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "MetaKeywords"), ReplaceUrls(instance.MetaKeywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.TileType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "TileType"), ReplaceUrls(instance.TileType)); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "Image"), instance.Image); }

   if (instance.TileImage != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "TileImage"), instance.TileImage); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.PreviewEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "PreviewEngl"), ReplaceUrls(instance.PreviewEngl)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.PreviewTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "PreviewTat"), ReplaceUrls(instance.PreviewTat)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "TextTat"), ReplaceUrls(instance.TextTat)); }

   if (instance.SpecialConditionsEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "SpecialConditionsEngl"), ReplaceUrls(instance.SpecialConditionsEngl)); }

   if (instance.SpecialConditionsTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "SpecialConditionsTat"), ReplaceUrls(instance.SpecialConditionsTat)); }

   if (instance.ProductLinksEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "ProductLinksEngl"), ReplaceUrls(instance.ProductLinksEngl)); }

   if (instance.ProductLinksTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "ProductLinksTat"), ReplaceUrls(instance.ProductLinksTat)); }

   if (instance.MobilePreview != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "MobilePreview"), ReplaceUrls(instance.MobilePreview)); }

   if (instance.MobilePreviewEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "MobilePreviewEngl"), ReplaceUrls(instance.MobilePreviewEngl)); }

   if (instance.MobilePreviewTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "MobilePreviewTat"), ReplaceUrls(instance.MobilePreviewTat)); }

   if (instance.MobileText != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "MobileText"), ReplaceUrls(instance.MobileText)); }

   if (instance.MobileTextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "MobileTextEngl"), ReplaceUrls(instance.MobileTextEngl)); }

   if (instance.MobileTextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "MobileTextTat"), ReplaceUrls(instance.MobileTextTat)); }

   if (instance.UpSaleIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Action", "UpSaleIcon"), instance.UpSaleIcon); }

		return Values;
	}
	

	partial void InsertAction(Action instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackAction(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "Action"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateAction(Action instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackAction(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "Action"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteAction(Action instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackTVPackage(TVPackage instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.INAC != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "INAC"), ReplaceUrls(instance.INAC)); }

   if (instance.MarketingPackage_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "MarketingPackage"), instance.MarketingPackage_ID.ToString()); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.Price != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "Price"), instance.Price.ToString()); }

   if (instance.SubscriptionFee != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "SubscriptionFee"), instance.SubscriptionFee.ToString()); }

   if (instance.SubscriptionFeeType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "SubscriptionFeeType"), instance.SubscriptionFeeType_ID.ToString()); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.PDF != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "PDF"), instance.PDF); }

   if (instance.PDFEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "PDFEngl"), instance.PDFEngl); }

   if (instance.PDFTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "PDFTat"), instance.PDFTat); }

   if (instance.TransferPrice != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "TransferPrice"), instance.TransferPrice.ToString()); }

   if (instance.IsArchive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "IsArchive"), ((bool)instance.IsArchive) ? "1" : "0"); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

   if (instance.DiscountCondition != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "DiscountCondition"), ReplaceUrls(instance.DiscountCondition)); }

   if (instance.MinPackagePayment != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "MinPackagePayment"), instance.MinPackagePayment.ToString()); }

   if (instance.MinTotalPackages != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "MinTotalPackages"), instance.MinTotalPackages.ToString()); }

   if (instance.EmptyPaymentReplacement != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "EmptyPaymentReplacement"), ReplaceUrls(instance.EmptyPaymentReplacement)); }

   if (instance.ChannelsCountReplacement != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackage", "ChannelsCountReplacement"), ReplaceUrls(instance.ChannelsCountReplacement)); }

		return Values;
	}
	

	partial void InsertTVPackage(TVPackage instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVPackage(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVPackage"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateTVPackage(TVPackage instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVPackage(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVPackage"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteTVPackage(TVPackage instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackExternalRegionMapping(ExternalRegionMapping instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.ExternalSystem_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ExternalRegionMapping", "ExternalSystem"), instance.ExternalSystem_ID.ToString()); }

   if (instance.RegionCode != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ExternalRegionMapping", "RegionCode"), ReplaceUrls(instance.RegionCode)); }

		return Values;
	}
	

	partial void InsertExternalRegionMapping(ExternalRegionMapping instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackExternalRegionMapping(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ExternalRegionMapping"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateExternalRegionMapping(ExternalRegionMapping instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackExternalRegionMapping(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ExternalRegionMapping"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteExternalRegionMapping(ExternalRegionMapping instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackExternalRegionSystem(ExternalRegionSystem instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ExternalRegionSystem", "Title"), ReplaceUrls(instance.Title)); }

		return Values;
	}
	

	partial void InsertExternalRegionSystem(ExternalRegionSystem instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackExternalRegionSystem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ExternalRegionSystem"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateExternalRegionSystem(ExternalRegionSystem instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackExternalRegionSystem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ExternalRegionSystem"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteExternalRegionSystem(ExternalRegionSystem instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSiteSetting(SiteSetting instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteSetting", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TextValue != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteSetting", "TextValue"), ReplaceUrls(instance.TextValue)); }

		return Values;
	}
	

	partial void InsertSiteSetting(SiteSetting instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSiteSetting(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SiteSetting"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSiteSetting(SiteSetting instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSiteSetting(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SiteSetting"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSiteSetting(SiteSetting instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPhoneCode(PhoneCode instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Code != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneCode", "Code"), instance.Code.ToString()); }

		return Values;
	}
	

	partial void InsertPhoneCode(PhoneCode instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneCode(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneCode"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePhoneCode(PhoneCode instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneCode(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneCode"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePhoneCode(PhoneCode instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackQPItemDefinitionConstraint(QPItemDefinitionConstraint instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Source_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPItemDefinitionConstraint", "Source"), instance.Source_ID.ToString()); }

   if (instance.Target_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPItemDefinitionConstraint", "Target"), instance.Target_ID.ToString()); }

   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QPItemDefinitionConstraint", "Title"), ReplaceUrls(instance.Title)); }

		return Values;
	}
	

	partial void InsertQPItemDefinitionConstraint(QPItemDefinitionConstraint instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQPItemDefinitionConstraint(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QPItemDefinitionConstraint"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateQPItemDefinitionConstraint(QPItemDefinitionConstraint instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQPItemDefinitionConstraint(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QPItemDefinitionConstraint"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteQPItemDefinitionConstraint(QPItemDefinitionConstraint instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMarketingMobileTariff(MarketingMobileTariff instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.HtmlTitle != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "HtmlTitle"), ReplaceUrls(instance.HtmlTitle)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.HtmlTitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "HtmlTitleEngl"), ReplaceUrls(instance.HtmlTitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.HtmlTitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "HtmlTitleTat"), ReplaceUrls(instance.HtmlTitleTat)); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "Image"), instance.Image); }

   if (instance.PaymentSystem != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "PaymentSystem"), ReplaceUrls(instance.PaymentSystem)); }

   if (instance.OtherPaymentSystemTariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "OtherPaymentSystemTariff"), instance.OtherPaymentSystemTariff_ID.ToString()); }

   if (instance.TariffFamily_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "TariffFamily"), instance.TariffFamily_ID.ToString()); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.Icon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "Icon"), instance.Icon); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Purpose != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "Purpose"), ReplaceUrls(instance.Purpose)); }

   if (instance.PurposeEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "PurposeEngl"), ReplaceUrls(instance.PurposeEngl)); }

   if (instance.PurposeTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "PurposeTat"), ReplaceUrls(instance.PurposeTat)); }

   if (instance.B2C != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "B2C"), ((bool)instance.B2C) ? "1" : "0"); }

   if (instance.B2B != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "B2B"), ((bool)instance.B2B) ? "1" : "0"); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

   if (instance.MetaKeywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "MetaKeywords"), ReplaceUrls(instance.MetaKeywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.TitleForFamily != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "TitleForFamily"), ReplaceUrls(instance.TitleForFamily)); }

   if (instance.TitleForFamilyEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "TitleForFamilyEngl"), ReplaceUrls(instance.TitleForFamilyEngl)); }

   if (instance.TitleForFamilyTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "TitleForFamilyTat"), ReplaceUrls(instance.TitleForFamilyTat)); }

   if (instance.CommentForFamily != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "CommentForFamily"), ReplaceUrls(instance.CommentForFamily)); }

   if (instance.CommentForFamilyTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "CommentForFamilyTat"), ReplaceUrls(instance.CommentForFamilyTat)); }

   if (instance.CommentForFamilyEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "CommentForFamilyEngl"), ReplaceUrls(instance.CommentForFamilyEngl)); }

   if (instance.IsStandardInterCityTariff != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "IsStandardInterCityTariff"), ((bool)instance.IsStandardInterCityTariff) ? "1" : "0"); }

   if (instance.MNP != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "MNP"), ((bool)instance.MNP) ? "1" : "0"); }

   if (instance.MobileDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "MobileDescription"), ReplaceUrls(instance.MobileDescription)); }

   if (instance.MobileDescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "MobileDescriptionEngl"), ReplaceUrls(instance.MobileDescriptionEngl)); }

   if (instance.MobileDescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "MobileDescriptionTat"), ReplaceUrls(instance.MobileDescriptionTat)); }

   if (instance.IsShowNoteInsteadButton != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "IsShowNoteInsteadButton"), ((bool)instance.IsShowNoteInsteadButton) ? "1" : "0"); }

   if (instance.UpSaleIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileTariff", "UpSaleIcon"), instance.UpSaleIcon); }

		return Values;
	}
	

	partial void InsertMarketingMobileTariff(MarketingMobileTariff instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingMobileTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingMobileTariff"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMarketingMobileTariff(MarketingMobileTariff instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingMobileTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingMobileTariff"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMarketingMobileTariff(MarketingMobileTariff instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackTariffGuideQuestion(TariffGuideQuestion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideQuestion", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideQuestion", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideQuestion", "TextTat"), ReplaceUrls(instance.TextTat)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideQuestion", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.B2C != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideQuestion", "B2C"), ((bool)instance.B2C) ? "1" : "0"); }

   if (instance.B2B != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideQuestion", "B2B"), ((bool)instance.B2B) ? "1" : "0"); }

   if (instance.ParentAnswer_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideQuestion", "ParentAnswer"), instance.ParentAnswer_ID.ToString()); }

   if (instance.ControlType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideQuestion", "ControlType"), ReplaceUrls(instance.ControlType)); }

		return Values;
	}
	

	partial void InsertTariffGuideQuestion(TariffGuideQuestion instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTariffGuideQuestion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TariffGuideQuestion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateTariffGuideQuestion(TariffGuideQuestion instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTariffGuideQuestion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TariffGuideQuestion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteTariffGuideQuestion(TariffGuideQuestion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackTariffGuideAnswer(TariffGuideAnswer instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideAnswer", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideAnswer", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideAnswer", "TextTat"), ReplaceUrls(instance.TextTat)); }

   if (instance.Question_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideAnswer", "Question"), instance.Question_ID.ToString()); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideAnswer", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.IsDefault != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideAnswer", "IsDefault"), ((bool)instance.IsDefault) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertTariffGuideAnswer(TariffGuideAnswer instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTariffGuideAnswer(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TariffGuideAnswer"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateTariffGuideAnswer(TariffGuideAnswer instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTariffGuideAnswer(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TariffGuideAnswer"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteTariffGuideAnswer(TariffGuideAnswer instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackTariffGuideResult(TariffGuideResult instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.MarketingSegment != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideResult", "MarketingSegment"), ReplaceUrls(instance.MarketingSegment)); }

   if (instance.DeviceType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideResult", "DeviceType"), instance.DeviceType_ID.ToString()); }

   if (instance.Answers != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideResult", "Answers"), ReplaceUrls(instance.Answers)); }

   if (instance.Tariffs != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffGuideResult", "Tariffs"), ReplaceUrls(instance.Tariffs)); }

		return Values;
	}
	

	partial void InsertTariffGuideResult(TariffGuideResult instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTariffGuideResult(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TariffGuideResult"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateTariffGuideResult(TariffGuideResult instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTariffGuideResult(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TariffGuideResult"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteTariffGuideResult(TariffGuideResult instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMobileTariffParameterGroup(MobileTariffParameterGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.Icon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "Icon"), instance.Icon); }

   if (instance.IsComparable != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "IsComparable"), ((bool)instance.IsComparable) ? "1" : "0"); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.TitleDative != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "TitleDative"), ReplaceUrls(instance.TitleDative)); }

   if (instance.TitleDativeEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "TitleDativeEngl"), ReplaceUrls(instance.TitleDativeEngl)); }

   if (instance.TitleDativeTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "TitleDativeTat"), ReplaceUrls(instance.TitleDativeTat)); }

   if (instance.GroupType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "GroupType"), ReplaceUrls(instance.GroupType)); }

   if (instance.PreOpen != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffParameterGroup", "PreOpen"), ((bool)instance.PreOpen) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertMobileTariffParameterGroup(MobileTariffParameterGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileTariffParameterGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileTariffParameterGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMobileTariffParameterGroup(MobileTariffParameterGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileTariffParameterGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileTariffParameterGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMobileTariffParameterGroup(MobileTariffParameterGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMarketingSign(MarketingSign instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingSign", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingSign", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingSign", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertMarketingSign(MarketingSign instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingSign(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingSign"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMarketingSign(MarketingSign instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingSign(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingSign"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMarketingSign(MarketingSign instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMarketingMobileService(MarketingMobileService instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.HtmlTitle != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "HtmlTitle"), ReplaceUrls(instance.HtmlTitle)); }

   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.ServiceFamily_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "ServiceFamily"), instance.ServiceFamily_ID.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.HtmlTitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "HtmlTitleEngl"), ReplaceUrls(instance.HtmlTitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.HtmlTitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "HtmlTitleTat"), ReplaceUrls(instance.HtmlTitleTat)); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.B2C != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "B2C"), ((bool)instance.B2C) ? "1" : "0"); }

   if (instance.B2B != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "B2B"), ((bool)instance.B2B) ? "1" : "0"); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "Image"), instance.Image); }

   if (instance.Icon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "Icon"), instance.Icon); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.Purpose != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "Purpose"), ReplaceUrls(instance.Purpose)); }

   if (instance.PurposeEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "PurposeEngl"), ReplaceUrls(instance.PurposeEngl)); }

   if (instance.PurposeTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "PurposeTat"), ReplaceUrls(instance.PurposeTat)); }

   if (instance.Widget != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "Widget"), ReplaceUrls(instance.Widget)); }

   if (instance.TariffOption != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "TariffOption"), ((bool)instance.TariffOption) ? "1" : "0"); }

   if (instance.TitleForFamily != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "TitleForFamily"), ReplaceUrls(instance.TitleForFamily)); }

   if (instance.TitleForFamilyEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "TitleForFamilyEngl"), ReplaceUrls(instance.TitleForFamilyEngl)); }

   if (instance.TitleForFamilyTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "TitleForFamilyTat"), ReplaceUrls(instance.TitleForFamilyTat)); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

   if (instance.MetaKeywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "MetaKeywords"), ReplaceUrls(instance.MetaKeywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.CallCenterPrice != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "CallCenterPrice"), instance.CallCenterPrice.ToString()); }

   if (instance.ArchiveTariffs != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "ArchiveTariffs"), ReplaceUrls(instance.ArchiveTariffs)); }

   if (instance.ArchiveTariffsEng != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "ArchiveTariffsEng"), ReplaceUrls(instance.ArchiveTariffsEng)); }

   if (instance.ArchiveTariffsTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "ArchiveTariffsTat"), ReplaceUrls(instance.ArchiveTariffsTat)); }

   if (instance.CommentForFamily != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "CommentForFamily"), ReplaceUrls(instance.CommentForFamily)); }

   if (instance.CommentForFamilyEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "CommentForFamilyEngl"), ReplaceUrls(instance.CommentForFamilyEngl)); }

   if (instance.CommentForFamilyTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "CommentForFamilyTat"), ReplaceUrls(instance.CommentForFamilyTat)); }

   if (instance.ShowPriceInsteadFee != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "ShowPriceInsteadFee"), ((bool)instance.ShowPriceInsteadFee) ? "1" : "0"); }

   if (instance.Prefix_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "Prefix"), instance.Prefix_ID.ToString()); }

   if (instance.MobileDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "MobileDescription"), ReplaceUrls(instance.MobileDescription)); }

   if (instance.MobileDescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "MobileDescriptionEngl"), ReplaceUrls(instance.MobileDescriptionEngl)); }

   if (instance.MobileDescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "MobileDescriptionTat"), ReplaceUrls(instance.MobileDescriptionTat)); }

   if (instance.ShortBenefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "ShortBenefit"), ReplaceUrls(instance.ShortBenefit)); }

   if (instance.OfferForIntercityCalls != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "OfferForIntercityCalls"), ((bool)instance.OfferForIntercityCalls) ? "1" : "0"); }

   if (instance.PromoteForRoaming != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "PromoteForRoaming"), ((bool)instance.PromoteForRoaming) ? "1" : "0"); }

   if (instance.Coverage != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "Coverage"), ReplaceUrls(instance.Coverage)); }

   if (instance.UpSaleIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingMobileService", "UpSaleIcon"), instance.UpSaleIcon); }

		return Values;
	}
	

	partial void InsertMarketingMobileService(MarketingMobileService instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingMobileService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingMobileService"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMarketingMobileService(MarketingMobileService instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingMobileService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingMobileService"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMarketingMobileService(MarketingMobileService instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSubscriptionFeeType(SubscriptionFeeType instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionFeeType", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionFeeType", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionFeeType", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.TextForCheck != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionFeeType", "TextForCheck"), ReplaceUrls(instance.TextForCheck)); }

   if (instance.TextForCheckEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionFeeType", "TextForCheckEngl"), ReplaceUrls(instance.TextForCheckEngl)); }

   if (instance.TextForCheckTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionFeeType", "TextForCheckTat"), ReplaceUrls(instance.TextForCheckTat)); }

   if (instance.TextForBasket != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionFeeType", "TextForBasket"), ReplaceUrls(instance.TextForBasket)); }

   if (instance.TextForBasketEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionFeeType", "TextForBasketEngl"), ReplaceUrls(instance.TextForBasketEngl)); }

   if (instance.TextForBasketTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SubscriptionFeeType", "TextForBasketTat"), ReplaceUrls(instance.TextForBasketTat)); }

		return Values;
	}
	

	partial void InsertSubscriptionFeeType(SubscriptionFeeType instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSubscriptionFeeType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SubscriptionFeeType"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSubscriptionFeeType(SubscriptionFeeType instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSubscriptionFeeType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SubscriptionFeeType"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSubscriptionFeeType(SubscriptionFeeType instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMobileServiceParameterGroup(MobileServiceParameterGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceParameterGroup", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceParameterGroup", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceParameterGroup", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceParameterGroup", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceParameterGroup", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceParameterGroup", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceParameterGroup", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.GroupType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceParameterGroup", "GroupType"), ReplaceUrls(instance.GroupType)); }

   if (instance.PreOpen != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceParameterGroup", "PreOpen"), ((bool)instance.PreOpen) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertMobileServiceParameterGroup(MobileServiceParameterGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceParameterGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceParameterGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMobileServiceParameterGroup(MobileServiceParameterGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceParameterGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceParameterGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMobileServiceParameterGroup(MobileServiceParameterGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMobileTariffFamily(MobileTariffFamily instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Signature != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "Signature"), ReplaceUrls(instance.Signature)); }

   if (instance.SignatureEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "SignatureEngl"), ReplaceUrls(instance.SignatureEngl)); }

   if (instance.SignatureTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "SignatureTat"), ReplaceUrls(instance.SignatureTat)); }

   if (instance.TitleForTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "TitleForTile"), ReplaceUrls(instance.TitleForTile)); }

   if (instance.TitleForTileEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "TitleForTileEngl"), ReplaceUrls(instance.TitleForTileEngl)); }

   if (instance.TitleForTileTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "TitleForTileTat"), ReplaceUrls(instance.TitleForTileTat)); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffFamily", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

		return Values;
	}
	

	partial void InsertMobileTariffFamily(MobileTariffFamily instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileTariffFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileTariffFamily"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMobileTariffFamily(MobileTariffFamily instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileTariffFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileTariffFamily"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMobileTariffFamily(MobileTariffFamily instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMutualMobileServiceGroup(MutualMobileServiceGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MutualMobileServiceGroup", "Title"), ReplaceUrls(instance.Title)); }

		return Values;
	}
	

	partial void InsertMutualMobileServiceGroup(MutualMobileServiceGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMutualMobileServiceGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MutualMobileServiceGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMutualMobileServiceGroup(MutualMobileServiceGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMutualMobileServiceGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MutualMobileServiceGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMutualMobileServiceGroup(MutualMobileServiceGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMobileServiceFamily(MobileServiceFamily instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Subtitle != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "Subtitle"), ReplaceUrls(instance.Subtitle)); }

   if (instance.SubtitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "SubtitleEngl"), ReplaceUrls(instance.SubtitleEngl)); }

   if (instance.SubtitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "SubtitleTat"), ReplaceUrls(instance.SubtitleTat)); }

   if (instance.TitleForTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "TitleForTile"), ReplaceUrls(instance.TitleForTile)); }

   if (instance.TitleForTileEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "TitleForTileEngl"), ReplaceUrls(instance.TitleForTileEngl)); }

   if (instance.TitleForTileTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "TitleForTileTat"), ReplaceUrls(instance.TitleForTileTat)); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

   if (instance.IsUseModernDesign != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceFamily", "IsUseModernDesign"), ((bool)instance.IsUseModernDesign) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertMobileServiceFamily(MobileServiceFamily instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceFamily"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMobileServiceFamily(MobileServiceFamily instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceFamily"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMobileServiceFamily(MobileServiceFamily instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMobileTariffCategory(MobileTariffCategory instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffCategory", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffCategory", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffCategory", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffCategory", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffCategory", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffCategory", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffCategory", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.IsMarked != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileTariffCategory", "IsMarked"), ((bool)instance.IsMarked) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertMobileTariffCategory(MobileTariffCategory instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileTariffCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileTariffCategory"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMobileTariffCategory(MobileTariffCategory instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileTariffCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileTariffCategory"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMobileTariffCategory(MobileTariffCategory instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMobileServiceCategory(MobileServiceCategory instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceCategory", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceCategory", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceCategory", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceCategory", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceCategory", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceCategory", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceCategory", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.IsMarked != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceCategory", "IsMarked"), ((bool)instance.IsMarked) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertMobileServiceCategory(MobileServiceCategory instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceCategory"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMobileServiceCategory(MobileServiceCategory instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceCategory"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMobileServiceCategory(MobileServiceCategory instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackArchiveTariff(ArchiveTariff instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTariff", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Region_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTariff", "Region"), instance.Region_ID.ToString()); }

   if (instance.SOC != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTariff", "SOC"), ReplaceUrls(instance.SOC)); }

   if (instance.Notes != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTariff", "Notes"), ReplaceUrls(instance.Notes)); }

   if (instance.Product_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTariff", "Product"), instance.Product_ID.ToString()); }

		return Values;
	}
	

	partial void InsertArchiveTariff(ArchiveTariff instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackArchiveTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ArchiveTariff"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateArchiveTariff(ArchiveTariff instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackArchiveTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ArchiveTariff"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteArchiveTariff(ArchiveTariff instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackArchiveTariffTab(ArchiveTariffTab instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTariffTab", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTariffTab", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.Tariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTariffTab", "Tariff"), instance.Tariff_ID.ToString()); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTariffTab", "SortOrder"), instance.SortOrder.ToString()); }

		return Values;
	}
	

	partial void InsertArchiveTariffTab(ArchiveTariffTab instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackArchiveTariffTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ArchiveTariffTab"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateArchiveTariffTab(ArchiveTariffTab instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackArchiveTariffTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ArchiveTariffTab"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteArchiveTariffTab(ArchiveTariffTab instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMobileServiceTab(MobileServiceTab instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceTab", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceTab", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceTab", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceTab", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertMobileServiceTab(MobileServiceTab instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceTab"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMobileServiceTab(MobileServiceTab instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceTab"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMobileServiceTab(MobileServiceTab instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMobileParamsGroupTab(MobileParamsGroupTab instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Tab_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileParamsGroupTab", "Tab"), instance.Tab_ID.ToString()); }

   if (instance.MarketingService_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileParamsGroupTab", "MarketingService"), instance.MarketingService_ID.ToString()); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileParamsGroupTab", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileParamsGroupTab", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileParamsGroupTab", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

		return Values;
	}
	

	partial void InsertMobileParamsGroupTab(MobileParamsGroupTab instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileParamsGroupTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileParamsGroupTab"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMobileParamsGroupTab(MobileParamsGroupTab instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileParamsGroupTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileParamsGroupTab"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMobileParamsGroupTab(MobileParamsGroupTab instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackTariffParamGroupPriority(TariffParamGroupPriority instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Tariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffParamGroupPriority", "Tariff"), instance.Tariff_ID.ToString()); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffParamGroupPriority", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.ParamGroup_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TariffParamGroupPriority", "ParamGroup"), instance.ParamGroup_ID.ToString()); }

		return Values;
	}
	

	partial void InsertTariffParamGroupPriority(TariffParamGroupPriority instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTariffParamGroupPriority(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TariffParamGroupPriority"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateTariffParamGroupPriority(TariffParamGroupPriority instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTariffParamGroupPriority(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TariffParamGroupPriority"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteTariffParamGroupPriority(TariffParamGroupPriority instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackArchiveService(ArchiveService instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveService", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Region_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveService", "Region"), instance.Region_ID.ToString()); }

   if (instance.IsAction != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveService", "IsAction"), ((bool)instance.IsAction) ? "1" : "0"); }

   if (instance.Note != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveService", "Note"), ReplaceUrls(instance.Note)); }

   if (instance.Price != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveService", "Price"), ReplaceUrls(instance.Price)); }

   if (instance.SOC != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveService", "SOC"), ReplaceUrls(instance.SOC)); }

		return Values;
	}
	

	partial void InsertArchiveService(ArchiveService instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackArchiveService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ArchiveService"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateArchiveService(ArchiveService instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackArchiveService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ArchiveService"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteArchiveService(ArchiveService instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackArchiveMobileServiceBookmark(ArchiveMobileServiceBookmark instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveMobileServiceBookmark", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveMobileServiceBookmark", "Order"), instance.Order.ToString()); }

   if (instance.Service_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveMobileServiceBookmark", "Service"), instance.Service_ID.ToString()); }

   if (instance.FullDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveMobileServiceBookmark", "FullDescription"), ReplaceUrls(instance.FullDescription)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveMobileServiceBookmark", "Description"), ReplaceUrls(instance.Description)); }

		return Values;
	}
	

	partial void InsertArchiveMobileServiceBookmark(ArchiveMobileServiceBookmark instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackArchiveMobileServiceBookmark(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ArchiveMobileServiceBookmark"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateArchiveMobileServiceBookmark(ArchiveMobileServiceBookmark instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackArchiveMobileServiceBookmark(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ArchiveMobileServiceBookmark"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteArchiveMobileServiceBookmark(ArchiveMobileServiceBookmark instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPrivelegeAndBonus(PrivelegeAndBonus instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.Priority != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "Priority"), instance.Priority.ToString()); }

   if (instance.Preview != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "Preview"), ReplaceUrls(instance.Preview)); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "Image"), instance.Image); }

   if (instance.TileImage != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "TileImage"), instance.TileImage); }

   if (instance.TileType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "TileType"), ReplaceUrls(instance.TileType)); }

   if (instance.SpecialConditions != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "SpecialConditions"), ReplaceUrls(instance.SpecialConditions)); }

   if (instance.MetaKeywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "MetaKeywords"), ReplaceUrls(instance.MetaKeywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.ProductLinks != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "ProductLinks"), ReplaceUrls(instance.ProductLinks)); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.PreviewEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "PreviewEngl"), ReplaceUrls(instance.PreviewEngl)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.PreviewTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "PreviewTat"), ReplaceUrls(instance.PreviewTat)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "TextTat"), ReplaceUrls(instance.TextTat)); }

   if (instance.SpecialConditionsEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "SpecialConditionsEngl"), ReplaceUrls(instance.SpecialConditionsEngl)); }

   if (instance.SpecialConditionsTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "SpecialConditionsTat"), ReplaceUrls(instance.SpecialConditionsTat)); }

   if (instance.ProductLinksTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "ProductLinksTat"), ReplaceUrls(instance.ProductLinksTat)); }

   if (instance.ProductLinksEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "ProductLinksEngl"), ReplaceUrls(instance.ProductLinksEngl)); }

   if (instance.IsBlank != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "IsBlank"), ((bool)instance.IsBlank) ? "1" : "0"); }

   if (instance.PageLayout != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "PageLayout"), ReplaceUrls(instance.PageLayout)); }

   if (instance.ChildRelativePath != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "ChildRelativePath"), ReplaceUrls(instance.ChildRelativePath)); }

   if (instance.MobilePreview != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "MobilePreview"), ReplaceUrls(instance.MobilePreview)); }

   if (instance.MobilePreviewEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "MobilePreviewEngl"), ReplaceUrls(instance.MobilePreviewEngl)); }

   if (instance.MobilePreviewTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "MobilePreviewTat"), ReplaceUrls(instance.MobilePreviewTat)); }

   if (instance.MobileText != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "MobileText"), ReplaceUrls(instance.MobileText)); }

   if (instance.MobileTextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "MobileTextEngl"), ReplaceUrls(instance.MobileTextEngl)); }

   if (instance.MobileTextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PrivelegeAndBonus", "MobileTextTat"), ReplaceUrls(instance.MobileTextTat)); }

		return Values;
	}
	

	partial void InsertPrivelegeAndBonus(PrivelegeAndBonus instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPrivelegeAndBonus(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PrivelegeAndBonus"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePrivelegeAndBonus(PrivelegeAndBonus instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPrivelegeAndBonus(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PrivelegeAndBonus"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePrivelegeAndBonus(PrivelegeAndBonus instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackFaqContent(FaqContent instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Question != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FaqContent", "Question"), ReplaceUrls(instance.Question)); }

   if (instance.Answer != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FaqContent", "Answer"), ReplaceUrls(instance.Answer)); }

   if (instance.Group_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FaqContent", "Group"), instance.Group_ID.ToString()); }

   if (instance.Urls != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FaqContent", "Urls"), ReplaceUrls(instance.Urls)); }

   if (instance.FirstInUrls != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FaqContent", "FirstInUrls"), ReplaceUrls(instance.FirstInUrls)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FaqContent", "Order"), instance.Order.ToString()); }

   if (instance.Notice != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FaqContent", "Notice"), ReplaceUrls(instance.Notice)); }

   if (instance.QuestionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FaqContent", "QuestionEngl"), ReplaceUrls(instance.QuestionEngl)); }

   if (instance.QuestionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FaqContent", "QuestionTat"), ReplaceUrls(instance.QuestionTat)); }

   if (instance.AnswerEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FaqContent", "AnswerEngl"), ReplaceUrls(instance.AnswerEngl)); }

   if (instance.AnswerTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FaqContent", "AnswerTat"), ReplaceUrls(instance.AnswerTat)); }

		return Values;
	}
	

	partial void InsertFaqContent(FaqContent instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFaqContent(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FaqContent"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateFaqContent(FaqContent instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFaqContent(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FaqContent"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteFaqContent(FaqContent instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackFaqGroup(FaqGroup instance)
	{
		Hashtable Values = new Hashtable();
		
		return Values;
	}
	

	partial void InsertFaqGroup(FaqGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFaqGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FaqGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateFaqGroup(FaqGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFaqGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FaqGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteFaqGroup(FaqGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSiteText(SiteText instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteText", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteText", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteText", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteText", "TextTat"), ReplaceUrls(instance.TextTat)); }

		return Values;
	}
	

	partial void InsertSiteText(SiteText instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSiteText(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SiteText"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSiteText(SiteText instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSiteText(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SiteText"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSiteText(SiteText instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackTVChannelTheme(TVChannelTheme instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannelTheme", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannelTheme", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannelTheme", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannelTheme", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannelTheme", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannelTheme", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Icon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannelTheme", "Icon"), instance.Icon); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannelTheme", "SortOrder"), instance.SortOrder.ToString()); }

		return Values;
	}
	

	partial void InsertTVChannelTheme(TVChannelTheme instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVChannelTheme(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVChannelTheme"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateTVChannelTheme(TVChannelTheme instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVChannelTheme(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVChannelTheme"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteTVChannelTheme(TVChannelTheme instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMarketingTVPackage(MarketingTVPackage instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "Image"), instance.Image); }

   if (instance.TileIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "TileIcon"), instance.TileIcon); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.Purpose != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "Purpose"), ReplaceUrls(instance.Purpose)); }

   if (instance.PurposeEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "PurposeEngl"), ReplaceUrls(instance.PurposeEngl)); }

   if (instance.PurposeTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "PurposeTat"), ReplaceUrls(instance.PurposeTat)); }

   if (instance.MetaKeywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "MetaKeywords"), ReplaceUrls(instance.MetaKeywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.MetaTags != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "MetaTags"), ReplaceUrls(instance.MetaTags)); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

   if (instance.IsArchive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "IsArchive"), ((bool)instance.IsArchive) ? "1" : "0"); }

   if (instance.ShowInTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "ShowInTile"), ((bool)instance.ShowInTile) ? "1" : "0"); }

   if (instance.IsTariff != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "IsTariff"), ((bool)instance.IsTariff) ? "1" : "0"); }

   if (instance.IsPopular != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "IsPopular"), ((bool)instance.IsPopular) ? "1" : "0"); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

   if (instance.RecomendedTariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "RecomendedTariff"), instance.RecomendedTariff_ID.ToString()); }

   if (instance.Family_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "Family"), instance.Family_ID.ToString()); }

   if (instance.InacParamType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "InacParamType"), instance.InacParamType_ID.ToString()); }

   if (instance.HideInList != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "HideInList"), ((bool)instance.HideInList) ? "1" : "0"); }

   if (instance.RemarksForFamily != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "RemarksForFamily"), ReplaceUrls(instance.RemarksForFamily)); }

   if (instance.RemarksForFamilyEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "RemarksForFamilyEngl"), ReplaceUrls(instance.RemarksForFamilyEngl)); }

   if (instance.RemarksForFamilyTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "RemarksForFamilyTat"), ReplaceUrls(instance.RemarksForFamilyTat)); }

   if (instance.Category_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "Category"), instance.Category_ID.ToString()); }

   if (instance.TariffType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "TariffType"), ReplaceUrls(instance.TariffType)); }

   if (instance.Collectable != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "Collectable"), ((bool)instance.Collectable) ? "1" : "0"); }

   if (instance.Discounted != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingTVPackage", "Discounted"), ((bool)instance.Discounted) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertMarketingTVPackage(MarketingTVPackage instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingTVPackage(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingTVPackage"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMarketingTVPackage(MarketingTVPackage instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingTVPackage(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingTVPackage"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMarketingTVPackage(MarketingTVPackage instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackQuickLinksTitle(QuickLinksTitle instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.PhraseText != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksTitle", "PhraseText"), ReplaceUrls(instance.PhraseText)); }

   if (instance.PhraseTextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksTitle", "PhraseTextEngl"), ReplaceUrls(instance.PhraseTextEngl)); }

   if (instance.PhraseTextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksTitle", "PhraseTextTat"), ReplaceUrls(instance.PhraseTextTat)); }

   if (instance.DefaultWord != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksTitle", "DefaultWord"), ReplaceUrls(instance.DefaultWord)); }

   if (instance.DefaultWordEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksTitle", "DefaultWordEngl"), ReplaceUrls(instance.DefaultWordEngl)); }

   if (instance.DefaultWordTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksTitle", "DefaultWordTat"), ReplaceUrls(instance.DefaultWordTat)); }

		return Values;
	}
	

	partial void InsertQuickLinksTitle(QuickLinksTitle instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQuickLinksTitle(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QuickLinksTitle"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateQuickLinksTitle(QuickLinksTitle instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQuickLinksTitle(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QuickLinksTitle"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteQuickLinksTitle(QuickLinksTitle instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackQuickLinksGroup(QuickLinksGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksGroup", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.UrlForTitle != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksGroup", "UrlForTitle"), ReplaceUrls(instance.UrlForTitle)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksGroup", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.Keyword != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksGroup", "Keyword"), ReplaceUrls(instance.Keyword)); }

   if (instance.Icon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksGroup", "Icon"), instance.Icon); }

   if (instance.KeyPhrase_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksGroup", "KeyPhrase"), instance.KeyPhrase_ID.ToString()); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QuickLinksGroup", "Text"), ReplaceUrls(instance.Text)); }

		return Values;
	}
	

	partial void InsertQuickLinksGroup(QuickLinksGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQuickLinksGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QuickLinksGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateQuickLinksGroup(QuickLinksGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQuickLinksGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QuickLinksGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteQuickLinksGroup(QuickLinksGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackProvodTariffParameterGroup(ProvodTariffParameterGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodTariffParameterGroup", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodTariffParameterGroup", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodTariffParameterGroup", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodTariffParameterGroup", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodTariffParameterGroup", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodTariffParameterGroup", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodTariffParameterGroup", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.ExpandByDefault != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodTariffParameterGroup", "ExpandByDefault"), ((bool)instance.ExpandByDefault) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertProvodTariffParameterGroup(ProvodTariffParameterGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodTariffParameterGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodTariffParameterGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateProvodTariffParameterGroup(ProvodTariffParameterGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodTariffParameterGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodTariffParameterGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteProvodTariffParameterGroup(ProvodTariffParameterGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackInternetTariffFamily(InternetTariffFamily instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffFamily", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffFamily", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffFamily", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffFamily", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffFamily", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffFamily", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.TitleForTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffFamily", "TitleForTile"), ReplaceUrls(instance.TitleForTile)); }

   if (instance.TitleForTileEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffFamily", "TitleForTileEngl"), ReplaceUrls(instance.TitleForTileEngl)); }

   if (instance.TitleForTileTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffFamily", "TitleForTileTat"), ReplaceUrls(instance.TitleForTileTat)); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffFamily", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

		return Values;
	}
	

	partial void InsertInternetTariffFamily(InternetTariffFamily instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackInternetTariffFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "InternetTariffFamily"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateInternetTariffFamily(InternetTariffFamily instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackInternetTariffFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "InternetTariffFamily"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteInternetTariffFamily(InternetTariffFamily instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPhoneTariffFamily(PhoneTariffFamily instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffFamily", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffFamily", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffFamily", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffFamily", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffFamily", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffFamily", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.TitleForTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffFamily", "TitleForTile"), ReplaceUrls(instance.TitleForTile)); }

   if (instance.TitleForTileEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffFamily", "TitleForTileEngl"), ReplaceUrls(instance.TitleForTileEngl)); }

   if (instance.TitleForTileTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffFamily", "TitleForTileTat"), ReplaceUrls(instance.TitleForTileTat)); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffFamily", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

		return Values;
	}
	

	partial void InsertPhoneTariffFamily(PhoneTariffFamily instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneTariffFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneTariffFamily"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePhoneTariffFamily(PhoneTariffFamily instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneTariffFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneTariffFamily"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePhoneTariffFamily(PhoneTariffFamily instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackProvodServiceFamily(ProvodServiceFamily instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.TextForSwitch != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "TextForSwitch"), ReplaceUrls(instance.TextForSwitch)); }

   if (instance.TextForSwitchEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "TextForSwitchEngl"), ReplaceUrls(instance.TextForSwitchEngl)); }

   if (instance.TextForSwitchTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "TextForSwitchTat"), ReplaceUrls(instance.TextForSwitchTat)); }

   if (instance.TitleForTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "TitleForTile"), ReplaceUrls(instance.TitleForTile)); }

   if (instance.TitleForTileEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "TitleForTileEngl"), ReplaceUrls(instance.TitleForTileEngl)); }

   if (instance.TitleForTileTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "TitleForTileTat"), ReplaceUrls(instance.TitleForTileTat)); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceFamily", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

		return Values;
	}
	

	partial void InsertProvodServiceFamily(ProvodServiceFamily instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodServiceFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodServiceFamily"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateProvodServiceFamily(ProvodServiceFamily instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodServiceFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodServiceFamily"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteProvodServiceFamily(ProvodServiceFamily instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackProvodServiceParameterGroup(ProvodServiceParameterGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParameterGroup", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParameterGroup", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParameterGroup", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParameterGroup", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParameterGroup", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParameterGroup", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParameterGroup", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.ExpandByDefault != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParameterGroup", "ExpandByDefault"), ((bool)instance.ExpandByDefault) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertProvodServiceParameterGroup(ProvodServiceParameterGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodServiceParameterGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodServiceParameterGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateProvodServiceParameterGroup(ProvodServiceParameterGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodServiceParameterGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodServiceParameterGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteProvodServiceParameterGroup(ProvodServiceParameterGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackInternetTariffCategory(InternetTariffCategory instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffCategory", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffCategory", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffCategory", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffCategory", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffCategory", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffCategory", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.CheckConnectionAvailable != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffCategory", "CheckConnectionAvailable"), ((bool)instance.CheckConnectionAvailable) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertInternetTariffCategory(InternetTariffCategory instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackInternetTariffCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "InternetTariffCategory"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateInternetTariffCategory(InternetTariffCategory instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackInternetTariffCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "InternetTariffCategory"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteInternetTariffCategory(InternetTariffCategory instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPhoneTariffCategory(PhoneTariffCategory instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffCategory", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffCategory", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffCategory", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffCategory", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffCategory", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffCategory", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.CheckConnectionAvailable != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffCategory", "CheckConnectionAvailable"), ((bool)instance.CheckConnectionAvailable) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertPhoneTariffCategory(PhoneTariffCategory instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneTariffCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneTariffCategory"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePhoneTariffCategory(PhoneTariffCategory instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneTariffCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneTariffCategory"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePhoneTariffCategory(PhoneTariffCategory instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackProvodServiceCategory(ProvodServiceCategory instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceCategory", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceCategory", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceCategory", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceCategory", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceCategory", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceCategory", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.CheckConnectionAvailable != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceCategory", "CheckConnectionAvailable"), ((bool)instance.CheckConnectionAvailable) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertProvodServiceCategory(ProvodServiceCategory instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodServiceCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodServiceCategory"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateProvodServiceCategory(ProvodServiceCategory instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodServiceCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodServiceCategory"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteProvodServiceCategory(ProvodServiceCategory instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMarketingInternetTariff(MarketingInternetTariff instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "Image"), instance.Image); }

   if (instance.TileIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "TileIcon"), instance.TileIcon); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.Purpose != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "Purpose"), ReplaceUrls(instance.Purpose)); }

   if (instance.PurposeEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "PurposeEngl"), ReplaceUrls(instance.PurposeEngl)); }

   if (instance.PurposeTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "PurposeTat"), ReplaceUrls(instance.PurposeTat)); }

   if (instance.MetaKeywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "MetaKeywords"), ReplaceUrls(instance.MetaKeywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.MetaTags != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "MetaTags"), ReplaceUrls(instance.MetaTags)); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

   if (instance.IsArchive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "IsArchive"), ((bool)instance.IsArchive) ? "1" : "0"); }

   if (instance.ShowInTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "ShowInTile"), ((bool)instance.ShowInTile) ? "1" : "0"); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

   if (instance.Family_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "Family"), instance.Family_ID.ToString()); }

   if (instance.HideInList != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "HideInList"), ((bool)instance.HideInList) ? "1" : "0"); }

   if (instance.RemarksForFamily != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "RemarksForFamily"), ReplaceUrls(instance.RemarksForFamily)); }

   if (instance.RemarksForFamilyEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "RemarksForFamilyEngl"), ReplaceUrls(instance.RemarksForFamilyEngl)); }

   if (instance.RemarksForFamilyTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "RemarksForFamilyTat"), ReplaceUrls(instance.RemarksForFamilyTat)); }

   if (instance.InacParamType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "InacParamType"), instance.InacParamType_ID.ToString()); }

   if (instance.AnnualContract != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "AnnualContract"), ((bool)instance.AnnualContract) ? "1" : "0"); }

   if (instance.AnnualContractFeeType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "AnnualContractFeeType"), ReplaceUrls(instance.AnnualContractFeeType)); }

   if (instance.UpSaleIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingInternetTariff", "UpSaleIcon"), instance.UpSaleIcon); }

		return Values;
	}
	

	partial void InsertMarketingInternetTariff(MarketingInternetTariff instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingInternetTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingInternetTariff"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMarketingInternetTariff(MarketingInternetTariff instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingInternetTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingInternetTariff"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMarketingInternetTariff(MarketingInternetTariff instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMarketingPhoneTariff(MarketingPhoneTariff instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "Image"), instance.Image); }

   if (instance.TileIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "TileIcon"), instance.TileIcon); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.Purpose != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "Purpose"), ReplaceUrls(instance.Purpose)); }

   if (instance.PurposeEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "PurposeEngl"), ReplaceUrls(instance.PurposeEngl)); }

   if (instance.PurposeTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "PurposeTat"), ReplaceUrls(instance.PurposeTat)); }

   if (instance.MetaKeywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "MetaKeywords"), ReplaceUrls(instance.MetaKeywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.MetaTags != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "MetaTags"), ReplaceUrls(instance.MetaTags)); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

   if (instance.IsArchive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "IsArchive"), ((bool)instance.IsArchive) ? "1" : "0"); }

   if (instance.ShowInTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "ShowInTile"), ((bool)instance.ShowInTile) ? "1" : "0"); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

   if (instance.Family_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "Family"), instance.Family_ID.ToString()); }

   if (instance.HideInList != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "HideInList"), ((bool)instance.HideInList) ? "1" : "0"); }

   if (instance.InacParamType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "InacParamType"), instance.InacParamType_ID.ToString()); }

   if (instance.RemarksForFamily != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "RemarksForFamily"), ReplaceUrls(instance.RemarksForFamily)); }

   if (instance.RemarksForFamilyEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "RemarksForFamilyEngl"), ReplaceUrls(instance.RemarksForFamilyEngl)); }

   if (instance.RemarksForFamilyTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingPhoneTariff", "RemarksForFamilyTat"), ReplaceUrls(instance.RemarksForFamilyTat)); }

		return Values;
	}
	

	partial void InsertMarketingPhoneTariff(MarketingPhoneTariff instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingPhoneTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingPhoneTariff"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMarketingPhoneTariff(MarketingPhoneTariff instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingPhoneTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingPhoneTariff"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMarketingPhoneTariff(MarketingPhoneTariff instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMarketingProvodService(MarketingProvodService instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "Image"), instance.Image); }

   if (instance.TileIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "TileIcon"), instance.TileIcon); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.Purpose != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "Purpose"), ReplaceUrls(instance.Purpose)); }

   if (instance.PurposeEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "PurposeEngl"), ReplaceUrls(instance.PurposeEngl)); }

   if (instance.PurposeTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "PurposeTat"), ReplaceUrls(instance.PurposeTat)); }

   if (instance.MetaKeywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "MetaKeywords"), ReplaceUrls(instance.MetaKeywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

   if (instance.MetaTags != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "MetaTags"), ReplaceUrls(instance.MetaTags)); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

   if (instance.IsArchive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "IsArchive"), ((bool)instance.IsArchive) ? "1" : "0"); }

   if (instance.ShowInTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "ShowInTile"), ((bool)instance.ShowInTile) ? "1" : "0"); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

   if (instance.Family_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "Family"), instance.Family_ID.ToString()); }

   if (instance.OfferInCart != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "OfferInCart"), ((bool)instance.OfferInCart) ? "1" : "0"); }

   if (instance.OfferInCartForOneTv != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "OfferInCartForOneTv"), ((bool)instance.OfferInCartForOneTv) ? "1" : "0"); }

   if (instance.OfferInCartForOneTvInKits != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "OfferInCartForOneTvInKits"), ((bool)instance.OfferInCartForOneTvInKits) ? "1" : "0"); }

   if (instance.OfferInCartForSeveralTv != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "OfferInCartForSeveralTv"), ((bool)instance.OfferInCartForSeveralTv) ? "1" : "0"); }

   if (instance.OfferInCartForSeveralTvInKits != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "OfferInCartForSeveralTvInKits"), ((bool)instance.OfferInCartForSeveralTvInKits) ? "1" : "0"); }

   if (instance.IncludeInCartForSeveralTv != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "IncludeInCartForSeveralTv"), ((bool)instance.IncludeInCartForSeveralTv) ? "1" : "0"); }

   if (instance.IncludeInCartForSeveralTvInKits != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "IncludeInCartForSeveralTvInKits"), ((bool)instance.IncludeInCartForSeveralTvInKits) ? "1" : "0"); }

   if (instance.IncludeInCartForTvTariffsWithoutInternet != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "IncludeInCartForTvTariffsWithoutInternet"), ((bool)instance.IncludeInCartForTvTariffsWithoutInternet) ? "1" : "0"); }

   if (instance.IncludeInCartForTvTariffsWithoutInternetInKits != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "IncludeInCartForTvTariffsWithoutInternetInKits"), ((bool)instance.IncludeInCartForTvTariffsWithoutInternetInKits) ? "1" : "0"); }

   if (instance.OfferInCartForInternetTariffs != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "OfferInCartForInternetTariffs"), ((bool)instance.OfferInCartForInternetTariffs) ? "1" : "0"); }

   if (instance.CommentsForFamily != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "CommentsForFamily"), ReplaceUrls(instance.CommentsForFamily)); }

   if (instance.CommentsForFamilyEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "CommentsForFamilyEngl"), ReplaceUrls(instance.CommentsForFamilyEngl)); }

   if (instance.CommentsForFamilyTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "CommentsForFamilyTat"), ReplaceUrls(instance.CommentsForFamilyTat)); }

   if (instance.HideInList != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "HideInList"), ((bool)instance.HideInList) ? "1" : "0"); }

   if (instance.MarketingEquipment_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "MarketingEquipment"), instance.MarketingEquipment_ID.ToString()); }

   if (instance.InacParamType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "InacParamType"), instance.InacParamType_ID.ToString()); }

   if (instance.AvailableInSelfService != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "AvailableInSelfService"), ((bool)instance.AvailableInSelfService) ? "1" : "0"); }

   if (instance.DiscountArray != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "DiscountArray"), ReplaceUrls(instance.DiscountArray)); }

   if (instance.ServiceType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "ServiceType"), ReplaceUrls(instance.ServiceType)); }

   if (instance.DiscountsExpand != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "DiscountsExpand"), ((bool)instance.DiscountsExpand) ? "1" : "0"); }

   if (instance.Prefix_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodService", "Prefix"), instance.Prefix_ID.ToString()); }

		return Values;
	}
	

	partial void InsertMarketingProvodService(MarketingProvodService instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingProvodService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingProvodService"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMarketingProvodService(MarketingProvodService instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingProvodService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingProvodService"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMarketingProvodService(MarketingProvodService instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackServiceForIternetTariff(ServiceForIternetTariff instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Tariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServiceForIternetTariff", "Tariff"), instance.Tariff_ID.ToString()); }

   if (instance.Service_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServiceForIternetTariff", "Service"), instance.Service_ID.ToString()); }

   if (instance.ParameterGroup_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServiceForIternetTariff", "ParameterGroup"), instance.ParameterGroup_ID.ToString()); }

   if (instance.IncludedByDefault != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServiceForIternetTariff", "IncludedByDefault"), ((bool)instance.IncludedByDefault) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertServiceForIternetTariff(ServiceForIternetTariff instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackServiceForIternetTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ServiceForIternetTariff"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateServiceForIternetTariff(ServiceForIternetTariff instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackServiceForIternetTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ServiceForIternetTariff"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteServiceForIternetTariff(ServiceForIternetTariff instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackServiceForPhoneTariff(ServiceForPhoneTariff instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Tariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServiceForPhoneTariff", "Tariff"), instance.Tariff_ID.ToString()); }

   if (instance.Service_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServiceForPhoneTariff", "Service"), instance.Service_ID.ToString()); }

   if (instance.ParameterGroup_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServiceForPhoneTariff", "ParameterGroup"), instance.ParameterGroup_ID.ToString()); }

   if (instance.IncludedByDefault != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServiceForPhoneTariff", "IncludedByDefault"), ((bool)instance.IncludedByDefault) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertServiceForPhoneTariff(ServiceForPhoneTariff instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackServiceForPhoneTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ServiceForPhoneTariff"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateServiceForPhoneTariff(ServiceForPhoneTariff instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackServiceForPhoneTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ServiceForPhoneTariff"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteServiceForPhoneTariff(ServiceForPhoneTariff instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackProvodKitFamily(ProvodKitFamily instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitFamily", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitFamily", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitFamily", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitFamily", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitFamily", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitFamily", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.TitleForTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitFamily", "TitleForTile"), ReplaceUrls(instance.TitleForTile)); }

   if (instance.TitleForTileEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitFamily", "TitleForTileEngl"), ReplaceUrls(instance.TitleForTileEngl)); }

   if (instance.TitleForTileTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitFamily", "TitleForTileTat"), ReplaceUrls(instance.TitleForTileTat)); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitFamily", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

		return Values;
	}
	

	partial void InsertProvodKitFamily(ProvodKitFamily instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodKitFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodKitFamily"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateProvodKitFamily(ProvodKitFamily instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodKitFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodKitFamily"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteProvodKitFamily(ProvodKitFamily instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackProvodKitCategory(ProvodKitCategory instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitCategory", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitCategory", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitCategory", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitCategory", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitCategory", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitCategory", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.CheckConnectionAvailable != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKitCategory", "CheckConnectionAvailable"), ((bool)instance.CheckConnectionAvailable) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertProvodKitCategory(ProvodKitCategory instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodKitCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodKitCategory"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateProvodKitCategory(ProvodKitCategory instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodKitCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodKitCategory"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteProvodKitCategory(ProvodKitCategory instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMarketingProvodKit(MarketingProvodKit instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "Image"), instance.Image); }

   if (instance.TileIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "TileIcon"), instance.TileIcon); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.Purpose != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "Purpose"), ReplaceUrls(instance.Purpose)); }

   if (instance.PurposeEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "PurposeEngl"), ReplaceUrls(instance.PurposeEngl)); }

   if (instance.PurposeTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "PurposeTat"), ReplaceUrls(instance.PurposeTat)); }

   if (instance.MetaKeywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "MetaKeywords"), ReplaceUrls(instance.MetaKeywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.MetaTags != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "MetaTags"), ReplaceUrls(instance.MetaTags)); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

   if (instance.IsArchive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "IsArchive"), ((bool)instance.IsArchive) ? "1" : "0"); }

   if (instance.ShowInTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "ShowInTile"), ((bool)instance.ShowInTile) ? "1" : "0"); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

   if (instance.Family_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "Family"), instance.Family_ID.ToString()); }

   if (instance.HideMainTariffParameters != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "HideMainTariffParameters"), ((bool)instance.HideMainTariffParameters) ? "1" : "0"); }

   if (instance.InternetTariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "InternetTariff"), instance.InternetTariff_ID.ToString()); }

   if (instance.PhoneTariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "PhoneTariff"), instance.PhoneTariff_ID.ToString()); }

   if (instance.HideInList != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "HideInList"), ((bool)instance.HideInList) ? "1" : "0"); }

   if (instance.InacParamType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "InacParamType"), instance.InacParamType_ID.ToString()); }

   if (instance.CommentsForFamily != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "CommentsForFamily"), ReplaceUrls(instance.CommentsForFamily)); }

   if (instance.CommentsForFamilyEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "CommentsForFamilyEngl"), ReplaceUrls(instance.CommentsForFamilyEngl)); }

   if (instance.CommentsForFamilyTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "CommentsForFamilyTat"), ReplaceUrls(instance.CommentsForFamilyTat)); }

   if (instance.AnnualContract != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "AnnualContract"), ((bool)instance.AnnualContract) ? "1" : "0"); }

   if (instance.AnnualContractFeeType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingProvodKit", "AnnualContractFeeType"), ReplaceUrls(instance.AnnualContractFeeType)); }

		return Values;
	}
	

	partial void InsertMarketingProvodKit(MarketingProvodKit instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingProvodKit(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingProvodKit"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMarketingProvodKit(MarketingProvodKit instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingProvodKit(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingProvodKit"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMarketingProvodKit(MarketingProvodKit instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackProvodKit(ProvodKit instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.MarketKit_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "MarketKit"), instance.MarketKit_ID.ToString()); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.Price != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "Price"), instance.Price.ToString()); }

   if (instance.SubscriptionFee != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "SubscriptionFee"), instance.SubscriptionFee.ToString()); }

   if (instance.SubscriptionFeeType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "SubscriptionFeeType"), instance.SubscriptionFeeType_ID.ToString()); }

   if (instance.MainParameterText != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "MainParameterText"), ReplaceUrls(instance.MainParameterText)); }

   if (instance.MainParameterValue != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "MainParameterValue"), ReplaceUrls(instance.MainParameterValue)); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.PDF != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "PDF"), instance.PDF); }

   if (instance.MainParameterTextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "MainParameterTextEngl"), ReplaceUrls(instance.MainParameterTextEngl)); }

   if (instance.MainParameterValueEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "MainParameterValueEngl"), ReplaceUrls(instance.MainParameterValueEngl)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.PDFEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "PDFEngl"), instance.PDFEngl); }

   if (instance.MainParameterTextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "MainParameterTextTat"), ReplaceUrls(instance.MainParameterTextTat)); }

   if (instance.MainParameterValueTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "MainParameterValueTat"), ReplaceUrls(instance.MainParameterValueTat)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.PDFTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "PDFTat"), instance.PDFTat); }

   if (instance.IsArchive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "IsArchive"), ((bool)instance.IsArchive) ? "1" : "0"); }

   if (instance.INACID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "INACID"), ReplaceUrls(instance.INACID)); }

   if (instance.InacIdArray != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "InacIdArray"), ReplaceUrls(instance.InacIdArray)); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodKit", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

		return Values;
	}
	

	partial void InsertProvodKit(ProvodKit instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodKit(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodKit"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateProvodKit(ProvodKit instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodKit(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodKit"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteProvodKit(ProvodKit instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackLocalRoamingOperator(LocalRoamingOperator instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "LocalRoamingOperator", "Title"), ReplaceUrls(instance.Title)); }

		return Values;
	}
	

	partial void InsertLocalRoamingOperator(LocalRoamingOperator instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackLocalRoamingOperator(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "LocalRoamingOperator"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateLocalRoamingOperator(LocalRoamingOperator instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackLocalRoamingOperator(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "LocalRoamingOperator"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteLocalRoamingOperator(LocalRoamingOperator instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRoamingTariffParam(RoamingTariffParam instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Parameter_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffParam", "Parameter"), instance.Parameter_ID.ToString()); }

   if (instance.TariffZone_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffParam", "TariffZone"), instance.TariffZone_ID.ToString()); }

   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffParam", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffParam", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffParam", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffParam", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.Group != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffParam", "Group"), ReplaceUrls(instance.Group)); }

   if (instance.Value != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffParam", "Value"), ReplaceUrls(instance.Value)); }

   if (instance.ValueEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffParam", "ValueEngl"), ReplaceUrls(instance.ValueEngl)); }

   if (instance.ValueTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingTariffParam", "ValueTat"), ReplaceUrls(instance.ValueTat)); }

		return Values;
	}
	

	partial void InsertRoamingTariffParam(RoamingTariffParam instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingTariffParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingTariffParam"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRoamingTariffParam(RoamingTariffParam instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingTariffParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingTariffParam"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRoamingTariffParam(RoamingTariffParam instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackTVPackageFamily(TVPackageFamily instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageFamily", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageFamily", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageFamily", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageFamily", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageFamily", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageFamily", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.TitleForTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageFamily", "TitleForTile"), ReplaceUrls(instance.TitleForTile)); }

   if (instance.TitleForTileEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageFamily", "TitleForTileEngl"), ReplaceUrls(instance.TitleForTileEngl)); }

   if (instance.TitleForTileTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageFamily", "TitleForTileTat"), ReplaceUrls(instance.TitleForTileTat)); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageFamily", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

		return Values;
	}
	

	partial void InsertTVPackageFamily(TVPackageFamily instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVPackageFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVPackageFamily"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateTVPackageFamily(TVPackageFamily instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVPackageFamily(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVPackageFamily"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteTVPackageFamily(TVPackageFamily instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSocialNetwork(SocialNetwork instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SocialNetwork", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Icon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SocialNetwork", "Icon"), instance.Icon); }

   if (instance.Url != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SocialNetwork", "Url"), ReplaceUrls(instance.Url)); }

   if (instance.IconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SocialNetwork", "IconHover"), instance.IconHover); }

		return Values;
	}
	

	partial void InsertSocialNetwork(SocialNetwork instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSocialNetwork(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SocialNetwork"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSocialNetwork(SocialNetwork instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSocialNetwork(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SocialNetwork"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSocialNetwork(SocialNetwork instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackHelpDeviceType(HelpDeviceType instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "HelpDeviceType", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "HelpDeviceType", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "HelpDeviceType", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "HelpDeviceType", "Image"), instance.Image); }

   if (instance.SiteProduct_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "HelpDeviceType", "SiteProduct"), instance.SiteProduct_ID.ToString()); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "HelpDeviceType", "SortOrder"), instance.SortOrder.ToString()); }

		return Values;
	}
	

	partial void InsertHelpDeviceType(HelpDeviceType instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackHelpDeviceType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "HelpDeviceType"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateHelpDeviceType(HelpDeviceType instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackHelpDeviceType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "HelpDeviceType"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteHelpDeviceType(HelpDeviceType instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackHelpCenterParam(HelpCenterParam instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.ShortNumber != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "HelpCenterParam", "ShortNumber"), ReplaceUrls(instance.ShortNumber)); }

   if (instance.CityNumber != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "HelpCenterParam", "CityNumber"), ReplaceUrls(instance.CityNumber)); }

   if (instance.Email != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "HelpCenterParam", "Email"), ReplaceUrls(instance.Email)); }

		return Values;
	}
	

	partial void InsertHelpCenterParam(HelpCenterParam instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackHelpCenterParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "HelpCenterParam"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateHelpCenterParam(HelpCenterParam instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackHelpCenterParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "HelpCenterParam"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteHelpCenterParam(HelpCenterParam instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackTVChannelRegion(TVChannelRegion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.TVChannel_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannelRegion", "TVChannel"), instance.TVChannel_ID.ToString()); }

   if (instance.TVProgramChannel_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVChannelRegion", "TVProgramChannel"), instance.TVProgramChannel_ID.ToString()); }

		return Values;
	}
	

	partial void InsertTVChannelRegion(TVChannelRegion instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVChannelRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVChannelRegion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateTVChannelRegion(TVChannelRegion instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVChannelRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVChannelRegion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteTVChannelRegion(TVChannelRegion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPhoneAsModemTab(PhoneAsModemTab instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemTab", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemTab", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemTab", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertPhoneAsModemTab(PhoneAsModemTab instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneAsModemTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneAsModemTab"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePhoneAsModemTab(PhoneAsModemTab instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneAsModemTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneAsModemTab"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePhoneAsModemTab(PhoneAsModemTab instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPhoneAsModemInterface(PhoneAsModemInterface instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemInterface", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemInterface", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemInterface", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Index != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemInterface", "Index"), instance.Index.ToString()); }

		return Values;
	}
	

	partial void InsertPhoneAsModemInterface(PhoneAsModemInterface instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneAsModemInterface(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneAsModemInterface"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePhoneAsModemInterface(PhoneAsModemInterface instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneAsModemInterface(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneAsModemInterface"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePhoneAsModemInterface(PhoneAsModemInterface instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackOperatingSystem(OperatingSystem instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "OperatingSystem", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Index != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "OperatingSystem", "Index"), instance.Index.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "OperatingSystem", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "OperatingSystem", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertOperatingSystem(OperatingSystem instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackOperatingSystem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "OperatingSystem"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateOperatingSystem(OperatingSystem instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackOperatingSystem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "OperatingSystem"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteOperatingSystem(OperatingSystem instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackCpaPartner(CpaPartner instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaPartner", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEng != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaPartner", "TitleEng"), ReplaceUrls(instance.TitleEng)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaPartner", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Phones != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaPartner", "Phones"), ReplaceUrls(instance.Phones)); }

   if (instance.Email != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaPartner", "Email"), ReplaceUrls(instance.Email)); }

		return Values;
	}
	

	partial void InsertCpaPartner(CpaPartner instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackCpaPartner(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "CpaPartner"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateCpaPartner(CpaPartner instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackCpaPartner(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "CpaPartner"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteCpaPartner(CpaPartner instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPhoneAsModemInstruction(PhoneAsModemInstruction instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemInstruction", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Tab_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemInstruction", "Tab"), instance.Tab_ID.ToString()); }

   if (instance.OperatingSystem_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemInstruction", "OperatingSystem"), instance.OperatingSystem_ID.ToString()); }

   if (instance.Interface_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemInstruction", "Interface"), instance.Interface_ID.ToString()); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemInstruction", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.Index != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneAsModemInstruction", "Index"), instance.Index.ToString()); }

		return Values;
	}
	

	partial void InsertPhoneAsModemInstruction(PhoneAsModemInstruction instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneAsModemInstruction(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneAsModemInstruction"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePhoneAsModemInstruction(PhoneAsModemInstruction instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneAsModemInstruction(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneAsModemInstruction"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePhoneAsModemInstruction(PhoneAsModemInstruction instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackCpaShortNumber(CpaShortNumber instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Number != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "Number"), ReplaceUrls(instance.Number)); }

   if (instance.Type != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "Type"), ReplaceUrls(instance.Type)); }

   if (instance.Tariff != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "Tariff"), ReplaceUrls(instance.Tariff)); }

   if (instance.TariffEng != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "TariffEng"), ReplaceUrls(instance.TariffEng)); }

   if (instance.TariffTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "TariffTat"), ReplaceUrls(instance.TariffTat)); }

   if (instance.Disconnect != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "Disconnect"), ReplaceUrls(instance.Disconnect)); }

   if (instance.DisconnectEng != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "DisconnectEng"), ReplaceUrls(instance.DisconnectEng)); }

   if (instance.DisconnectTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "DisconnectTat"), ReplaceUrls(instance.DisconnectTat)); }

   if (instance.Service != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "Service"), ReplaceUrls(instance.Service)); }

   if (instance.ServiceEng != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "ServiceEng"), ReplaceUrls(instance.ServiceEng)); }

   if (instance.ServiceTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "ServiceTat"), ReplaceUrls(instance.ServiceTat)); }

   if (instance.AreaOfService != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "AreaOfService"), ReplaceUrls(instance.AreaOfService)); }

   if (instance.AreaOfServiceEng != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "AreaOfServiceEng"), ReplaceUrls(instance.AreaOfServiceEng)); }

   if (instance.AreaOfServiceTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "AreaOfServiceTat"), ReplaceUrls(instance.AreaOfServiceTat)); }

   if (instance.Partner_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CpaShortNumber", "Partner"), instance.Partner_ID.ToString()); }

		return Values;
	}
	

	partial void InsertCpaShortNumber(CpaShortNumber instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackCpaShortNumber(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "CpaShortNumber"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateCpaShortNumber(CpaShortNumber instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackCpaShortNumber(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "CpaShortNumber"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteCpaShortNumber(CpaShortNumber instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackChangeNumberErrorText(ChangeNumberErrorText instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ChangeNumberErrorText", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.ServiceErrorCode != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ChangeNumberErrorText", "ServiceErrorCode"), instance.ServiceErrorCode.ToString()); }

   if (instance.ErrorText != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ChangeNumberErrorText", "ErrorText"), ReplaceUrls(instance.ErrorText)); }

   if (instance.ErrorTextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ChangeNumberErrorText", "ErrorTextEngl"), ReplaceUrls(instance.ErrorTextEngl)); }

   if (instance.ErrorTextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ChangeNumberErrorText", "ErrorTextTat"), ReplaceUrls(instance.ErrorTextTat)); }

   if (instance.ServiceUnavailable != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ChangeNumberErrorText", "ServiceUnavailable"), ((bool)instance.ServiceUnavailable) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertChangeNumberErrorText(ChangeNumberErrorText instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackChangeNumberErrorText(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ChangeNumberErrorText"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateChangeNumberErrorText(ChangeNumberErrorText instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackChangeNumberErrorText(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ChangeNumberErrorText"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteChangeNumberErrorText(ChangeNumberErrorText instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackEquipmentParamsTab(EquipmentParamsTab instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsTab", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsTab", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsTab", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertEquipmentParamsTab(EquipmentParamsTab instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentParamsTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentParamsTab"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateEquipmentParamsTab(EquipmentParamsTab instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentParamsTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentParamsTab"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteEquipmentParamsTab(EquipmentParamsTab instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackEquipmentTab(EquipmentTab instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.ParamsTab_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentTab", "ParamsTab"), instance.ParamsTab_ID.ToString()); }

   if (instance.MarketingEquipment_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentTab", "MarketingEquipment"), instance.MarketingEquipment_ID.ToString()); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentTab", "Order"), instance.Order.ToString()); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentTab", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentTab", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentTab", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

		return Values;
	}
	

	partial void InsertEquipmentTab(EquipmentTab instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentTab"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateEquipmentTab(EquipmentTab instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentTab"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteEquipmentTab(EquipmentTab instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackEquipmentType(EquipmentType instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Product_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "Product"), instance.Product_ID.ToString()); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "Order"), instance.Order.ToString()); }

   if (instance.MaySortList != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "MaySortList"), ((bool)instance.MaySortList) ? "1" : "0"); }

   if (instance.Image != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "Image"), instance.Image); }

   if (instance.DefaultImage != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "DefaultImage"), instance.DefaultImage); }

   if (instance.StillTitle1 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "StillTitle1"), ReplaceUrls(instance.StillTitle1)); }

   if (instance.StillTitle2 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "StillTitle2"), ReplaceUrls(instance.StillTitle2)); }

   if (instance.StillTitle3 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentType", "StillTitle3"), ReplaceUrls(instance.StillTitle3)); }

		return Values;
	}
	

	partial void InsertEquipmentType(EquipmentType instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentType"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateEquipmentType(EquipmentType instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentType"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteEquipmentType(EquipmentType instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackEquipmentCategory(EquipmentCategory instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Type_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentCategory", "Type"), instance.Type_ID.ToString()); }

   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentCategory", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentCategory", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentCategory", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentCategory", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentCategory", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentCategory", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentCategory", "Order"), instance.Order.ToString()); }

   if (instance.Marked != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentCategory", "Marked"), ((bool)instance.Marked) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertEquipmentCategory(EquipmentCategory instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentCategory"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateEquipmentCategory(EquipmentCategory instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentCategory"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteEquipmentCategory(EquipmentCategory instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackEquipmentParam(EquipmentParam instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.MarketingEquipment_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "MarketingEquipment"), instance.MarketingEquipment_ID.ToString()); }

   if (instance.Group_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "Group"), instance.Group_ID.ToString()); }

   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Value != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "Value"), ReplaceUrls(instance.Value)); }

   if (instance.ValueEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "ValueEngl"), ReplaceUrls(instance.ValueEngl)); }

   if (instance.ValueTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "ValueTat"), ReplaceUrls(instance.ValueTat)); }

   if (instance.ShowInTiles != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "ShowInTiles"), ((bool)instance.ShowInTiles) ? "1" : "0"); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParam", "Order"), instance.Order.ToString()); }

		return Values;
	}
	

	partial void InsertEquipmentParam(EquipmentParam instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentParam"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateEquipmentParam(EquipmentParam instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentParam"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteEquipmentParam(EquipmentParam instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackEquipmentParamsGroup(EquipmentParamsGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Type_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsGroup", "Type"), instance.Type_ID.ToString()); }

   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsGroup", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsGroup", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsGroup", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsGroup", "Order"), instance.Order.ToString()); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsGroup", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsGroup", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsGroup", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.IsPriceGroup != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentParamsGroup", "IsPriceGroup"), ((bool)instance.IsPriceGroup) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertEquipmentParamsGroup(EquipmentParamsGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentParamsGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentParamsGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateEquipmentParamsGroup(EquipmentParamsGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentParamsGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentParamsGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteEquipmentParamsGroup(EquipmentParamsGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackEquipment(Equipment instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.MarketingEquipment_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "MarketingEquipment"), instance.MarketingEquipment_ID.ToString()); }

   if (instance.Price != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "Price"), instance.Price.ToString()); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.CreditEnabled != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "CreditEnabled"), ((bool)instance.CreditEnabled) ? "1" : "0"); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

   if (instance.IsArchived != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "IsArchived"), ((bool)instance.IsArchived) ? "1" : "0"); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.PdfFile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "PdfFile"), instance.PdfFile); }

   if (instance.PdfFileEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "PdfFileEngl"), instance.PdfFileEngl); }

   if (instance.PdfFileTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Equipment", "PdfFileTat"), instance.PdfFileTat); }

		return Values;
	}
	

	partial void InsertEquipment(Equipment instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipment(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "Equipment"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateEquipment(Equipment instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipment(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "Equipment"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteEquipment(Equipment instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMarketingEquipment(MarketingEquipment instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Category_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "Category"), instance.Category_ID.ToString()); }

   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Price != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "Price"), instance.Price.ToString()); }

   if (instance.TileImage != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "TileImage"), instance.TileImage); }

   if (instance.IsApplication != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "IsApplication"), ((bool)instance.IsApplication) ? "1" : "0"); }

   if (instance.ApplicationIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "ApplicationIcon"), instance.ApplicationIcon); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "Order"), instance.Order.ToString()); }

   if (instance.MarketingSign_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "MarketingSign"), instance.MarketingSign_ID.ToString()); }

   if (instance.AppStoreLink != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "AppStoreLink"), ReplaceUrls(instance.AppStoreLink)); }

   if (instance.GooglePlayLink != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "GooglePlayLink"), ReplaceUrls(instance.GooglePlayLink)); }

   if (instance.FileToDownload != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "FileToDownload"), instance.FileToDownload); }

   if (instance.IsArchived != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "IsArchived"), ((bool)instance.IsArchived) ? "1" : "0"); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.Destination != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "Destination"), ReplaceUrls(instance.Destination)); }

   if (instance.DestinationEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "DestinationEngl"), ReplaceUrls(instance.DestinationEngl)); }

   if (instance.DestinationTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "DestinationTat"), ReplaceUrls(instance.DestinationTat)); }

   if (instance.PdfFile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "PdfFile"), instance.PdfFile); }

   if (instance.PdfFileEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "PdfFileEngl"), instance.PdfFileEngl); }

   if (instance.PdfFileTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "PdfFileTat"), instance.PdfFileTat); }

   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.MetaKeywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "MetaKeywords"), ReplaceUrls(instance.MetaKeywords)); }

   if (instance.MetaDescription != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "MetaDescription"), ReplaceUrls(instance.MetaDescription)); }

   if (instance.TitleFormat_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "TitleFormat"), instance.TitleFormat_ID.ToString()); }

   if (instance.UpSaleIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingEquipment", "UpSaleIcon"), instance.UpSaleIcon); }

		return Values;
	}
	

	partial void InsertMarketingEquipment(MarketingEquipment instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingEquipment(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingEquipment"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMarketingEquipment(MarketingEquipment instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingEquipment(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingEquipment"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMarketingEquipment(MarketingEquipment instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackEquipmentImage(EquipmentImage instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.ImageFile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentImage", "ImageFile"), instance.ImageFile); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentImage", "Order"), instance.Order.ToString()); }

   if (instance.PreviewFile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "EquipmentImage", "PreviewFile"), instance.PreviewFile); }

		return Values;
	}
	

	partial void InsertEquipmentImage(EquipmentImage instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentImage(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentImage"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateEquipmentImage(EquipmentImage instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackEquipmentImage(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "EquipmentImage"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteEquipmentImage(EquipmentImage instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPaymentServiceFilter(PaymentServiceFilter instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PaymentServiceFilter", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PaymentServiceFilter", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PaymentServiceFilter", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.UrlTitle != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PaymentServiceFilter", "UrlTitle"), ReplaceUrls(instance.UrlTitle)); }

   if (instance.HasPaymentForm != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PaymentServiceFilter", "HasPaymentForm"), ((bool)instance.HasPaymentForm) ? "1" : "0"); }

   if (instance.ServiceBlockText != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PaymentServiceFilter", "ServiceBlockText"), ReplaceUrls(instance.ServiceBlockText)); }

   if (instance.ServiceBlockTextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PaymentServiceFilter", "ServiceBlockTextEngl"), ReplaceUrls(instance.ServiceBlockTextEngl)); }

   if (instance.ServiceBlockTextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PaymentServiceFilter", "ServiceBlockTextTat"), ReplaceUrls(instance.ServiceBlockTextTat)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PaymentServiceFilter", "Order"), instance.Order.ToString()); }

		return Values;
	}
	

	partial void InsertPaymentServiceFilter(PaymentServiceFilter instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPaymentServiceFilter(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PaymentServiceFilter"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePaymentServiceFilter(PaymentServiceFilter instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPaymentServiceFilter(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PaymentServiceFilter"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePaymentServiceFilter(PaymentServiceFilter instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackINACParamType(INACParamType instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "INACParamType", "Title"), ReplaceUrls(instance.Title)); }

		return Values;
	}
	

	partial void InsertINACParamType(INACParamType instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackINACParamType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "INACParamType"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateINACParamType(INACParamType instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackINACParamType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "INACParamType"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteINACParamType(INACParamType instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackInternetTariff(InternetTariff instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.MarketingTariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "MarketingTariff"), instance.MarketingTariff_ID.ToString()); }

   if (instance.Price != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "Price"), instance.Price.ToString()); }

   if (instance.TransferPrice != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "TransferPrice"), instance.TransferPrice.ToString()); }

   if (instance.SubscriptionFee != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "SubscriptionFee"), instance.SubscriptionFee.ToString()); }

   if (instance.SubscriptionFeeType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "SubscriptionFeeType"), instance.SubscriptionFeeType_ID.ToString()); }

   if (instance.PDF != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "PDF"), instance.PDF); }

   if (instance.PDFEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "PDFEngl"), instance.PDFEngl); }

   if (instance.PDFTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "PDFTat"), instance.PDFTat); }

   if (instance.IsArchive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "IsArchive"), ((bool)instance.IsArchive) ? "1" : "0"); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.INACID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "INACID"), ReplaceUrls(instance.INACID)); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariff", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

		return Values;
	}
	

	partial void InsertInternetTariff(InternetTariff instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackInternetTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "InternetTariff"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateInternetTariff(InternetTariff instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackInternetTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "InternetTariff"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteInternetTariff(InternetTariff instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackInternetTariffParam(InternetTariffParam instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Tariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "Tariff"), instance.Tariff_ID.ToString()); }

   if (instance.Group_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "Group"), instance.Group_ID.ToString()); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.Value != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "Value"), ReplaceUrls(instance.Value)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "TextTat"), ReplaceUrls(instance.TextTat)); }

   if (instance.ValueEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "ValueEngl"), ReplaceUrls(instance.ValueEngl)); }

   if (instance.ValueTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "ValueTat"), ReplaceUrls(instance.ValueTat)); }

   if (instance.MainInCard != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "MainInCard"), ((bool)instance.MainInCard) ? "1" : "0"); }

   if (instance.ShowInTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "ShowInTile"), ((bool)instance.ShowInTile) ? "1" : "0"); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "InternetTariffParam", "SortOrder"), instance.SortOrder.ToString()); }

		return Values;
	}
	

	partial void InsertInternetTariffParam(InternetTariffParam instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackInternetTariffParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "InternetTariffParam"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateInternetTariffParam(InternetTariffParam instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackInternetTariffParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "InternetTariffParam"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteInternetTariffParam(InternetTariffParam instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPhoneTariff(PhoneTariff instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.MarketingTariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "MarketingTariff"), instance.MarketingTariff_ID.ToString()); }

   if (instance.Price != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "Price"), instance.Price.ToString()); }

   if (instance.TransferPrice != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "TransferPrice"), instance.TransferPrice.ToString()); }

   if (instance.SubscriptionFee != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "SubscriptionFee"), instance.SubscriptionFee.ToString()); }

   if (instance.SubscriptionFeeType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "SubscriptionFeeType"), instance.SubscriptionFeeType_ID.ToString()); }

   if (instance.PDF != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "PDF"), instance.PDF); }

   if (instance.PDFEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "PDFEngl"), instance.PDFEngl); }

   if (instance.PDFTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "PDFTat"), instance.PDFTat); }

   if (instance.IsArchive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "IsArchive"), ((bool)instance.IsArchive) ? "1" : "0"); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.INACID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "INACID"), ReplaceUrls(instance.INACID)); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariff", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

		return Values;
	}
	

	partial void InsertPhoneTariff(PhoneTariff instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneTariff"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePhoneTariff(PhoneTariff instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneTariff"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePhoneTariff(PhoneTariff instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackPhoneTariffParam(PhoneTariffParam instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Tariff_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "Tariff"), instance.Tariff_ID.ToString()); }

   if (instance.Group_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "Group"), instance.Group_ID.ToString()); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "TextTat"), ReplaceUrls(instance.TextTat)); }

   if (instance.Value != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "Value"), ReplaceUrls(instance.Value)); }

   if (instance.ValueEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "ValueEngl"), ReplaceUrls(instance.ValueEngl)); }

   if (instance.ValueTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "ValueTat"), ReplaceUrls(instance.ValueTat)); }

   if (instance.MainInCard != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "MainInCard"), ((bool)instance.MainInCard) ? "1" : "0"); }

   if (instance.ShowInTile != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "ShowInTile"), ((bool)instance.ShowInTile) ? "1" : "0"); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "PhoneTariffParam", "SortOrder"), instance.SortOrder.ToString()); }

		return Values;
	}
	

	partial void InsertPhoneTariffParam(PhoneTariffParam instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneTariffParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneTariffParam"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdatePhoneTariffParam(PhoneTariffParam instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackPhoneTariffParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "PhoneTariffParam"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeletePhoneTariffParam(PhoneTariffParam instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackDiagnoseItem(DiagnoseItem instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.ItemType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "ItemType"), ReplaceUrls(instance.ItemType)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "Order"), instance.Order.ToString()); }

   if (instance.LinkedItems_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "LinkedItems"), instance.LinkedItems_ID.ToString()); }

   if (instance.Instruction != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "Instruction"), ReplaceUrls(instance.Instruction)); }

   if (instance.ShowStandardText != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "ShowStandardText"), ((bool)instance.ShowStandardText) ? "1" : "0"); }

   if (instance.PdfInstruction != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "PdfInstruction"), instance.PdfInstruction); }

   if (instance.TitleEng != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "TitleEng"), ReplaceUrls(instance.TitleEng)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.InstructionEng != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "InstructionEng"), ReplaceUrls(instance.InstructionEng)); }

   if (instance.PdfInstructionEng != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "PdfInstructionEng"), instance.PdfInstructionEng); }

   if (instance.PdfInstructionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "PdfInstructionTat"), instance.PdfInstructionTat); }

   if (instance.InstructionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseItem", "InstructionTat"), ReplaceUrls(instance.InstructionTat)); }

		return Values;
	}
	

	partial void InsertDiagnoseItem(DiagnoseItem instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackDiagnoseItem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "DiagnoseItem"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateDiagnoseItem(DiagnoseItem instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackDiagnoseItem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "DiagnoseItem"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteDiagnoseItem(DiagnoseItem instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMutualTVPackageGroup(MutualTVPackageGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MutualTVPackageGroup", "Title"), ReplaceUrls(instance.Title)); }

		return Values;
	}
	

	partial void InsertMutualTVPackageGroup(MutualTVPackageGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMutualTVPackageGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MutualTVPackageGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMutualTVPackageGroup(MutualTVPackageGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMutualTVPackageGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MutualTVPackageGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMutualTVPackageGroup(MutualTVPackageGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackTVPackageCategory(TVPackageCategory instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageCategory", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageCategory", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageCategory", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageCategory", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageCategory", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageCategory", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.CheckConnectionAvailable != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TVPackageCategory", "CheckConnectionAvailable"), ((bool)instance.CheckConnectionAvailable) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertTVPackageCategory(TVPackageCategory instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVPackageCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVPackageCategory"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateTVPackageCategory(TVPackageCategory instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTVPackageCategory(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TVPackageCategory"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteTVPackageCategory(TVPackageCategory instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackProvodServiceParamTab(ProvodServiceParamTab instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParamTab", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParamTab", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParamTab", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertProvodServiceParamTab(ProvodServiceParamTab instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodServiceParamTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodServiceParamTab"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateProvodServiceParamTab(ProvodServiceParamTab instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodServiceParamTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodServiceParamTab"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteProvodServiceParamTab(ProvodServiceParamTab instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackParamTabInProvodService(ParamTabInProvodService instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.MarketingService_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ParamTabInProvodService", "MarketingService"), instance.MarketingService_ID.ToString()); }

   if (instance.ParamGroup_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ParamTabInProvodService", "ParamGroup"), instance.ParamGroup_ID.ToString()); }

   if (instance.Tab_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ParamTabInProvodService", "Tab"), instance.Tab_ID.ToString()); }

		return Values;
	}
	

	partial void InsertParamTabInProvodService(ParamTabInProvodService instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackParamTabInProvodService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ParamTabInProvodService"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateParamTabInProvodService(ParamTabInProvodService instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackParamTabInProvodService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ParamTabInProvodService"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteParamTabInProvodService(ParamTabInProvodService instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackProvodServiceParam(ProvodServiceParam instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "TextTat"), ReplaceUrls(instance.TextTat)); }

   if (instance.Value != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "Value"), ReplaceUrls(instance.Value)); }

   if (instance.ValueEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "ValueEngl"), ReplaceUrls(instance.ValueEngl)); }

   if (instance.ValueTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "ValueTat"), ReplaceUrls(instance.ValueTat)); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.ProvodService_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "ProvodService"), instance.ProvodService_ID.ToString()); }

   if (instance.ParamGroup_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "ParamGroup"), instance.ParamGroup_ID.ToString()); }

   if (instance.Parent_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "Parent"), instance.Parent_ID.ToString()); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodServiceParam", "SortOrder"), instance.SortOrder.ToString()); }

		return Values;
	}
	

	partial void InsertProvodServiceParam(ProvodServiceParam instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodServiceParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodServiceParam"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateProvodServiceParam(ProvodServiceParam instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodServiceParam(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodServiceParam"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteProvodServiceParam(ProvodServiceParam instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackDiagnoseStatisticItem(DiagnoseStatisticItem instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseStatisticItem", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Version != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseStatisticItem", "Version"), ReplaceUrls(instance.Version)); }

   if (instance.Helped != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseStatisticItem", "Helped"), instance.Helped.ToString()); }

   if (instance.NotHelped != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseStatisticItem", "NotHelped"), instance.NotHelped.ToString()); }

   if (instance.Difference != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseStatisticItem", "Difference"), instance.Difference.ToString()); }

   if (instance.Amount != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "DiagnoseStatisticItem", "Amount"), instance.Amount.ToString()); }

		return Values;
	}
	

	partial void InsertDiagnoseStatisticItem(DiagnoseStatisticItem instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackDiagnoseStatisticItem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "DiagnoseStatisticItem"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateDiagnoseStatisticItem(DiagnoseStatisticItem instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackDiagnoseStatisticItem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "DiagnoseStatisticItem"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteDiagnoseStatisticItem(DiagnoseStatisticItem instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRoamingCountryGroup(RoamingCountryGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingCountryGroup", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingCountryGroup", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingCountryGroup", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertRoamingCountryGroup(RoamingCountryGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingCountryGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingCountryGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRoamingCountryGroup(RoamingCountryGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingCountryGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingCountryGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRoamingCountryGroup(RoamingCountryGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRoamingRegionFriendGroup(RoamingRegionFriendGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingRegionFriendGroup", "Title"), ReplaceUrls(instance.Title)); }

		return Values;
	}
	

	partial void InsertRoamingRegionFriendGroup(RoamingRegionFriendGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingRegionFriendGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingRegionFriendGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRoamingRegionFriendGroup(RoamingRegionFriendGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingRegionFriendGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingRegionFriendGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRoamingRegionFriendGroup(RoamingRegionFriendGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSukkRegionFriend(SukkRegionFriend instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Group_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SukkRegionFriend", "Group"), instance.Group_ID.ToString()); }

   if (instance.Region_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SukkRegionFriend", "Region"), instance.Region_ID.ToString()); }

		return Values;
	}
	

	partial void InsertSukkRegionFriend(SukkRegionFriend instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSukkRegionFriend(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SukkRegionFriend"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSukkRegionFriend(SukkRegionFriend instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSukkRegionFriend(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SukkRegionFriend"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSukkRegionFriend(SukkRegionFriend instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSukkCountryInGroup(SukkCountryInGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Groups_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SukkCountryInGroup", "Groups"), instance.Groups_ID.ToString()); }

   if (instance.Country_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SukkCountryInGroup", "Country"), instance.Country_ID.ToString()); }

		return Values;
	}
	

	partial void InsertSukkCountryInGroup(SukkCountryInGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSukkCountryInGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SukkCountryInGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSukkCountryInGroup(SukkCountryInGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSukkCountryInGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SukkCountryInGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSukkCountryInGroup(SukkCountryInGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSukkToMarketingRegion(SukkToMarketingRegion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.MarketingRegion_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SukkToMarketingRegion", "MarketingRegion"), instance.MarketingRegion_ID.ToString()); }

   if (instance.SukkRegion_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SukkToMarketingRegion", "SukkRegion"), instance.SukkRegion_ID.ToString()); }

		return Values;
	}
	

	partial void InsertSukkToMarketingRegion(SukkToMarketingRegion instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSukkToMarketingRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SukkToMarketingRegion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSukkToMarketingRegion(SukkToMarketingRegion instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSukkToMarketingRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SukkToMarketingRegion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSukkToMarketingRegion(SukkToMarketingRegion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSetupConnectionTab(SetupConnectionTab instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionTab", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionTab", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionTab", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionTab", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionTab", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionTab", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.Index != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionTab", "Index"), instance.Index.ToString()); }

		return Values;
	}
	

	partial void InsertSetupConnectionTab(SetupConnectionTab instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSetupConnectionTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SetupConnectionTab"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSetupConnectionTab(SetupConnectionTab instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSetupConnectionTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SetupConnectionTab"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSetupConnectionTab(SetupConnectionTab instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSetupConnectionGoal(SetupConnectionGoal instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionGoal", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Index != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionGoal", "Index"), instance.Index.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionGoal", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionGoal", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertSetupConnectionGoal(SetupConnectionGoal instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSetupConnectionGoal(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SetupConnectionGoal"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSetupConnectionGoal(SetupConnectionGoal instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSetupConnectionGoal(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SetupConnectionGoal"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSetupConnectionGoal(SetupConnectionGoal instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSetupConnectionInstruction(SetupConnectionInstruction instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionInstruction", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.File != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionInstruction", "File"), instance.File); }

   if (instance.Content != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionInstruction", "Content"), ReplaceUrls(instance.Content)); }

   if (instance.System_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionInstruction", "System"), instance.System_ID.ToString()); }

   if (instance.Goal_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionInstruction", "Goal"), instance.Goal_ID.ToString()); }

   if (instance.Wizard != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionInstruction", "Wizard"), instance.Wizard); }

   if (instance.FileEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionInstruction", "FileEngl"), instance.FileEngl); }

   if (instance.FileTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionInstruction", "FileTat"), instance.FileTat); }

   if (instance.Tab_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SetupConnectionInstruction", "Tab"), instance.Tab_ID.ToString()); }

		return Values;
	}
	

	partial void InsertSetupConnectionInstruction(SetupConnectionInstruction instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSetupConnectionInstruction(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SetupConnectionInstruction"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSetupConnectionInstruction(SetupConnectionInstruction instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSetupConnectionInstruction(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SetupConnectionInstruction"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSetupConnectionInstruction(SetupConnectionInstruction instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackProvodService(ProvodService instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.MarketingService_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "MarketingService"), instance.MarketingService_ID.ToString()); }

   if (instance.Price != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "Price"), instance.Price.ToString()); }

   if (instance.SubscriptionFee != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "SubscriptionFee"), instance.SubscriptionFee.ToString()); }

   if (instance.SubscriptionFeeType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "SubscriptionFeeType"), instance.SubscriptionFeeType_ID.ToString()); }

   if (instance.PDF != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "PDF"), instance.PDF); }

   if (instance.PDFEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "PDFEngl"), instance.PDFEngl); }

   if (instance.PDFTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "PDFTat"), instance.PDFTat); }

   if (instance.IsArchive != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "IsArchive"), ((bool)instance.IsArchive) ? "1" : "0"); }

   if (instance.SortOrder != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "SortOrder"), instance.SortOrder.ToString()); }

   if (instance.Legal != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "Legal"), ReplaceUrls(instance.Legal)); }

   if (instance.LegalEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "LegalEngl"), ReplaceUrls(instance.LegalEngl)); }

   if (instance.LegalTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "LegalTat"), ReplaceUrls(instance.LegalTat)); }

   if (instance.INACID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "INACID"), ReplaceUrls(instance.INACID)); }

   if (instance.Benefit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "Benefit"), ReplaceUrls(instance.Benefit)); }

   if (instance.BenefitEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "BenefitEngl"), ReplaceUrls(instance.BenefitEngl)); }

   if (instance.BenefitTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "BenefitTat"), ReplaceUrls(instance.BenefitTat)); }

   if (instance.DiscountArray != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "DiscountArray"), ReplaceUrls(instance.DiscountArray)); }

   if (instance.FamilyIcon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "FamilyIcon"), instance.FamilyIcon); }

   if (instance.FamilyIconEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "FamilyIconEngl"), instance.FamilyIconEngl); }

   if (instance.FamilyIconTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "FamilyIconTat"), instance.FamilyIconTat); }

   if (instance.FamilyIconHover != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "FamilyIconHover"), instance.FamilyIconHover); }

   if (instance.FamilyIconHoverEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "FamilyIconHoverEngl"), instance.FamilyIconHoverEngl); }

   if (instance.FamilyIconHoverTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "FamilyIconHoverTat"), instance.FamilyIconHoverTat); }

   if (instance.DiscountsByMonths != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "DiscountsByMonths"), ReplaceUrls(instance.DiscountsByMonths)); }

   if (instance.DiscountsText1 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "DiscountsText1"), ReplaceUrls(instance.DiscountsText1)); }

   if (instance.DiscountsValue1 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "DiscountsValue1"), ReplaceUrls(instance.DiscountsValue1)); }

   if (instance.DiscountsText2 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "DiscountsText2"), ReplaceUrls(instance.DiscountsText2)); }

   if (instance.DiscountsValue2 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "DiscountsValue2"), ReplaceUrls(instance.DiscountsValue2)); }

   if (instance.DiscountsText3 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "DiscountsText3"), ReplaceUrls(instance.DiscountsText3)); }

   if (instance.DiscountsValue3 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ProvodService", "DiscountsValue3"), ReplaceUrls(instance.DiscountsValue3)); }

		return Values;
	}
	

	partial void InsertProvodService(ProvodService instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodService"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateProvodService(ProvodService instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackProvodService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ProvodService"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteProvodService(ProvodService instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackWordForm(WordForm instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Key != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WordForm", "Key"), ReplaceUrls(instance.Key)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WordForm", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.EngPlural != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WordForm", "EngPlural"), ReplaceUrls(instance.EngPlural)); }

   if (instance.EngSingular != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WordForm", "EngSingular"), ReplaceUrls(instance.EngSingular)); }

   if (instance.TatSingular != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WordForm", "TatSingular"), ReplaceUrls(instance.TatSingular)); }

   if (instance.RusFirstPlural != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WordForm", "RusFirstPlural"), ReplaceUrls(instance.RusFirstPlural)); }

   if (instance.RusSecondPlural != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WordForm", "RusSecondPlural"), ReplaceUrls(instance.RusSecondPlural)); }

   if (instance.RusThirdPlural != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WordForm", "RusThirdPlural"), ReplaceUrls(instance.RusThirdPlural)); }

		return Values;
	}
	

	partial void InsertWordForm(WordForm instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackWordForm(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "WordForm"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateWordForm(WordForm instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackWordForm(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "WordForm"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteWordForm(WordForm instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackServiceForTVPackage(ServiceForTVPackage instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.TVPackage_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServiceForTVPackage", "TVPackage"), instance.TVPackage_ID.ToString()); }

   if (instance.Service_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServiceForTVPackage", "Service"), instance.Service_ID.ToString()); }

   if (instance.IncludedByDefault != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServiceForTVPackage", "IncludedByDefault"), ((bool)instance.IncludedByDefault) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertServiceForTVPackage(ServiceForTVPackage instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackServiceForTVPackage(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ServiceForTVPackage"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateServiceForTVPackage(ServiceForTVPackage instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackServiceForTVPackage(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ServiceForTVPackage"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteServiceForTVPackage(ServiceForTVPackage instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackQP_TrusteePayment(QP_TrusteePayment instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Range != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QP_TrusteePayment", "Range"), ReplaceUrls(instance.Range)); }

   if (instance.Sum != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QP_TrusteePayment", "Sum"), ReplaceUrls(instance.Sum)); }

   if (instance.MinBalance != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QP_TrusteePayment", "MinBalance"), ReplaceUrls(instance.MinBalance)); }

   if (instance.Regions_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QP_TrusteePayment", "Regions"), instance.Regions_ID.ToString()); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QP_TrusteePayment", "Order"), instance.Order.ToString()); }

   if (instance.InnerTab_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QP_TrusteePayment", "InnerTab"), instance.InnerTab_ID.ToString()); }

		return Values;
	}
	

	partial void InsertQP_TrusteePayment(QP_TrusteePayment instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQP_TrusteePayment(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QP_TrusteePayment"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateQP_TrusteePayment(QP_TrusteePayment instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQP_TrusteePayment(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QP_TrusteePayment"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteQP_TrusteePayment(QP_TrusteePayment instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackQP_TrusteePaymentTab(QP_TrusteePaymentTab instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QP_TrusteePaymentTab", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Name != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QP_TrusteePaymentTab", "Name"), ReplaceUrls(instance.Name)); }

   if (instance.MinBalance != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QP_TrusteePaymentTab", "MinBalance"), ReplaceUrls(instance.MinBalance)); }

   if (instance.IsDefault != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "QP_TrusteePaymentTab", "IsDefault"), ((bool)instance.IsDefault) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertQP_TrusteePaymentTab(QP_TrusteePaymentTab instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQP_TrusteePaymentTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QP_TrusteePaymentTab"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateQP_TrusteePaymentTab(QP_TrusteePaymentTab instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackQP_TrusteePaymentTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "QP_TrusteePaymentTab"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteQP_TrusteePaymentTab(QP_TrusteePaymentTab instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSearchAnnouncement(SearchAnnouncement instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SearchAnnouncement", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Url != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SearchAnnouncement", "Url"), ReplaceUrls(instance.Url)); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SearchAnnouncement", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.Keywords != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SearchAnnouncement", "Keywords"), ReplaceUrls(instance.Keywords)); }

   if (instance.ShowAlways != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SearchAnnouncement", "ShowAlways"), ((bool)instance.ShowAlways) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertSearchAnnouncement(SearchAnnouncement instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSearchAnnouncement(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SearchAnnouncement"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSearchAnnouncement(SearchAnnouncement instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSearchAnnouncement(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SearchAnnouncement"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSearchAnnouncement(SearchAnnouncement instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackWarrantyService(WarrantyService instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WarrantyService", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WarrantyService", "Order"), instance.Order.ToString()); }

   if (instance.Address != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WarrantyService", "Address"), ReplaceUrls(instance.Address)); }

   if (instance.Schedule != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WarrantyService", "Schedule"), ReplaceUrls(instance.Schedule)); }

   if (instance.Phone != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "WarrantyService", "Phone"), ReplaceUrls(instance.Phone)); }

		return Values;
	}
	

	partial void InsertWarrantyService(WarrantyService instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackWarrantyService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "WarrantyService"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateWarrantyService(WarrantyService instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackWarrantyService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "WarrantyService"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteWarrantyService(WarrantyService instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 

	partial void InsertSKADType(SKADType instance) 
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
			
	}

	partial void UpdateSKADType(SKADType instance)
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
					
	}			

	partial void DeleteSKADType(SKADType instance)
	{
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
				
	}
 

	partial void InsertSKADService(SKADService instance) 
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
			
	}

	partial void UpdateSKADService(SKADService instance)
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
					
	}			

	partial void DeleteSKADService(SKADService instance)
	{
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
				
	}
 

	partial void InsertSKADSpeciality(SKADSpeciality instance) 
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
			
	}

	partial void UpdateSKADSpeciality(SKADSpeciality instance)
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
					
	}			

	partial void DeleteSKADSpeciality(SKADSpeciality instance)
	{
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
				
	}
 
	private Hashtable PackSalesPointParameter(SalesPointParameter instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SalesPointParameter", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Services != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SalesPointParameter", "Services"), ReplaceUrls(instance.Services)); }

		return Values;
	}
	

	partial void InsertSalesPointParameter(SalesPointParameter instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSalesPointParameter(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SalesPointParameter"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSalesPointParameter(SalesPointParameter instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSalesPointParameter(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SalesPointParameter"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSalesPointParameter(SalesPointParameter instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSalesPointType(SalesPointType instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SalesPointType", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.SKADType_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SalesPointType", "SKADType"), instance.SKADType_ID.ToString()); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SalesPointType", "Order"), instance.Order.ToString()); }

		return Values;
	}
	

	partial void InsertSalesPointType(SalesPointType instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSalesPointType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SalesPointType"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSalesPointType(SalesPointType instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSalesPointType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SalesPointType"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSalesPointType(SalesPointType instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSalesPointSpeciality(SalesPointSpeciality instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SalesPointSpeciality", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.SKADSpeciality_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SalesPointSpeciality", "SKADSpeciality"), instance.SKADSpeciality_ID.ToString()); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SalesPointSpeciality", "Order"), instance.Order.ToString()); }

		return Values;
	}
	

	partial void InsertSalesPointSpeciality(SalesPointSpeciality instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSalesPointSpeciality(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SalesPointSpeciality"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSalesPointSpeciality(SalesPointSpeciality instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSalesPointSpeciality(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SalesPointSpeciality"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSalesPointSpeciality(SalesPointSpeciality instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSalesPointService(SalesPointService instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SalesPointService", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.SKADService_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SalesPointService", "SKADService"), instance.SKADService_ID.ToString()); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SalesPointService", "Order"), instance.Order.ToString()); }

		return Values;
	}
	

	partial void InsertSalesPointService(SalesPointService instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSalesPointService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SalesPointService"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSalesPointService(SalesPointService instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSalesPointService(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SalesPointService"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSalesPointService(SalesPointService instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackTargetUser(TargetUser instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TargetUser", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TargetUser", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "TargetUser", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertTargetUser(TargetUser instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTargetUser(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TargetUser"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateTargetUser(TargetUser instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackTargetUser(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "TargetUser"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteTargetUser(TargetUser instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackFeedbackSubthemeGroup(FeedbackSubthemeGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubthemeGroup", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubthemeGroup", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubthemeGroup", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSubthemeGroup", "Order"), instance.Order.ToString()); }

		return Values;
	}
	

	partial void InsertFeedbackSubthemeGroup(FeedbackSubthemeGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackSubthemeGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackSubthemeGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateFeedbackSubthemeGroup(FeedbackSubthemeGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackSubthemeGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackSubthemeGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteFeedbackSubthemeGroup(FeedbackSubthemeGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackArchiveTvTariff(ArchiveTvTariff instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTvTariff", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.IsTariff != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTvTariff", "IsTariff"), ((bool)instance.IsTariff) ? "1" : "0"); }

   if (instance.MonthPrice != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTvTariff", "MonthPrice"), instance.MonthPrice.ToString()); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTvTariff", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.IdINAC != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ArchiveTvTariff", "IdINAC"), instance.IdINAC.ToString()); }

		return Values;
	}
	

	partial void InsertArchiveTvTariff(ArchiveTvTariff instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackArchiveTvTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ArchiveTvTariff"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateArchiveTvTariff(ArchiveTvTariff instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackArchiveTvTariff(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ArchiveTvTariff"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteArchiveTvTariff(ArchiveTvTariff instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackObsoleteUrlRedirect(ObsoleteUrlRedirect instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ObsoleteUrlRedirect", "Order"), instance.Order.ToString()); }

   if (instance.OldUrl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ObsoleteUrlRedirect", "OldUrl"), ReplaceUrls(instance.OldUrl)); }

   if (instance.UrlToRedirect != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ObsoleteUrlRedirect", "UrlToRedirect"), ReplaceUrls(instance.UrlToRedirect)); }

   if (instance.RedirectType != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ObsoleteUrlRedirect", "RedirectType"), ReplaceUrls(instance.RedirectType)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ObsoleteUrlRedirect", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.InOnMainSite != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ObsoleteUrlRedirect", "InOnMainSite"), ((bool)instance.InOnMainSite) ? "1" : "0"); }

		return Values;
	}
	

	partial void InsertObsoleteUrlRedirect(ObsoleteUrlRedirect instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackObsoleteUrlRedirect(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ObsoleteUrlRedirect"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateObsoleteUrlRedirect(ObsoleteUrlRedirect instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackObsoleteUrlRedirect(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ObsoleteUrlRedirect"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteObsoleteUrlRedirect(ObsoleteUrlRedirect instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 

	partial void InsertArchiveViewTariff(ArchiveViewTariff instance) 
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
			
	}

	partial void UpdateArchiveViewTariff(ArchiveViewTariff instance)
	{	
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
					
	}			

	partial void DeleteArchiveViewTariff(ArchiveViewTariff instance)
	{
		
		throw new InvalidOperationException(@"Virtual Contents cannot be modified");	
				
	}
 
	private Hashtable PackMarketingRegionNoCallback(MarketingRegionNoCallback instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.MarketingRegion_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MarketingRegionNoCallback", "MarketingRegion"), instance.MarketingRegion_ID.ToString()); }

		return Values;
	}
	

	partial void InsertMarketingRegionNoCallback(MarketingRegionNoCallback instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingRegionNoCallback(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingRegionNoCallback"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMarketingRegionNoCallback(MarketingRegionNoCallback instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMarketingRegionNoCallback(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MarketingRegionNoCallback"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMarketingRegionNoCallback(MarketingRegionNoCallback instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackFeedbackCallbackTime(FeedbackCallbackTime instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackCallbackTime", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.CampaignId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackCallbackTime", "CampaignId"), instance.CampaignId.ToString()); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackCallbackTime", "Order"), instance.Order.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackCallbackTime", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackCallbackTime", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertFeedbackCallbackTime(FeedbackCallbackTime instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackCallbackTime(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackCallbackTime"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateFeedbackCallbackTime(FeedbackCallbackTime instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackCallbackTime(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackCallbackTime"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteFeedbackCallbackTime(FeedbackCallbackTime instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackFeedbackSwindleProblem(FeedbackSwindleProblem instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSwindleProblem", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSwindleProblem", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "FeedbackSwindleProblem", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertFeedbackSwindleProblem(FeedbackSwindleProblem instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackSwindleProblem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackSwindleProblem"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateFeedbackSwindleProblem(FeedbackSwindleProblem instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackFeedbackSwindleProblem(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "FeedbackSwindleProblem"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteFeedbackSwindleProblem(FeedbackSwindleProblem instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackContact(Contact instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Description != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "Description"), ReplaceUrls(instance.Description)); }

   if (instance.Email != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "Email"), ReplaceUrls(instance.Email)); }

   if (instance.Phone != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "Phone"), ReplaceUrls(instance.Phone)); }

   if (instance.Details != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "Details"), ReplaceUrls(instance.Details)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "Order"), instance.Order.ToString()); }

   if (instance.ContactsTab_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "ContactsTab"), instance.ContactsTab_ID.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.DescriptionEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "DescriptionEngl"), ReplaceUrls(instance.DescriptionEngl)); }

   if (instance.DescriptionTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "DescriptionTat"), ReplaceUrls(instance.DescriptionTat)); }

   if (instance.DetailsEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "DetailsEngl"), ReplaceUrls(instance.DetailsEngl)); }

   if (instance.DetailsTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "Contact", "DetailsTat"), ReplaceUrls(instance.DetailsTat)); }

		return Values;
	}
	

	partial void InsertContact(Contact instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackContact(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "Contact"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateContact(Contact instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackContact(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "Contact"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteContact(Contact instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackContactsTab(ContactsTab instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsTab", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsTab", "Order"), instance.Order.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsTab", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsTab", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsTab", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsTab", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsTab", "TextTat"), ReplaceUrls(instance.TextTat)); }

		return Values;
	}
	

	partial void InsertContactsTab(ContactsTab instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackContactsTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ContactsTab"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateContactsTab(ContactsTab instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackContactsTab(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ContactsTab"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteContactsTab(ContactsTab instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackContactsGroup(ContactsGroup instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsGroup", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsGroup", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.TextEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsGroup", "TextEngl"), ReplaceUrls(instance.TextEngl)); }

   if (instance.TextTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsGroup", "TextTat"), ReplaceUrls(instance.TextTat)); }

   if (instance.Order != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsGroup", "Order"), instance.Order.ToString()); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsGroup", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ContactsGroup", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertContactsGroup(ContactsGroup instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackContactsGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ContactsGroup"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateContactsGroup(ContactsGroup instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackContactsGroup(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ContactsGroup"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteContactsGroup(ContactsGroup instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackCampaignIdToRegion(CampaignIdToRegion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.CampaignId != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CampaignIdToRegion", "CampaignId"), instance.CampaignId.ToString()); }

   if (instance.FeedbackCallbackTime_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CampaignIdToRegion", "FeedbackCallbackTime"), instance.FeedbackCallbackTime_ID.ToString()); }

   if (instance.Region_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "CampaignIdToRegion", "Region"), instance.Region_ID.ToString()); }

		return Values;
	}
	

	partial void InsertCampaignIdToRegion(CampaignIdToRegion instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackCampaignIdToRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "CampaignIdToRegion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateCampaignIdToRegion(CampaignIdToRegion instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackCampaignIdToRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "CampaignIdToRegion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteCampaignIdToRegion(CampaignIdToRegion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSitemapXml(SitemapXml instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Generated != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SitemapXml", "Generated"), ReplaceUrls(instance.Generated)); }

   if (instance.Current != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SitemapXml", "Current"), ReplaceUrls(instance.Current)); }

   if (instance.Region_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SitemapXml", "Region"), instance.Region_ID.ToString()); }

		return Values;
	}
	

	partial void InsertSitemapXml(SitemapXml instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSitemapXml(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SitemapXml"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSitemapXml(SitemapXml instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSitemapXml(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SitemapXml"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSitemapXml(SitemapXml instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSiteConfig(SiteConfig instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Alias != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteConfig", "Alias"), ReplaceUrls(instance.Alias)); }

   if (instance.Parent_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SiteConfig", "Parent"), instance.Parent_ID.ToString()); }

		return Values;
	}
	

	partial void InsertSiteConfig(SiteConfig instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSiteConfig(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SiteConfig"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSiteConfig(SiteConfig instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSiteConfig(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SiteConfig"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSiteConfig(SiteConfig instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRobotsTxt(RobotsTxt instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Text != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RobotsTxt", "Text"), ReplaceUrls(instance.Text)); }

   if (instance.MarketingRegion_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RobotsTxt", "MarketingRegion"), instance.MarketingRegion_ID.ToString()); }

		return Values;
	}
	

	partial void InsertRobotsTxt(RobotsTxt instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRobotsTxt(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RobotsTxt"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRobotsTxt(RobotsTxt instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRobotsTxt(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RobotsTxt"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRobotsTxt(RobotsTxt instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMetroLine(MetroLine instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Line_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MetroLine", "Line"), instance.Line_ID.ToString()); }

   if (instance.Icon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MetroLine", "Icon"), instance.Icon); }

   if (instance.ColorName != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MetroLine", "ColorName"), ReplaceUrls(instance.ColorName)); }

		return Values;
	}
	

	partial void InsertMetroLine(MetroLine instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMetroLine(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MetroLine"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMetroLine(MetroLine instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMetroLine(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MetroLine"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMetroLine(MetroLine instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackAnnualContractSetting(AnnualContractSetting instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.FeeLimit != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "AnnualContractSetting", "FeeLimit"), instance.FeeLimit.ToString()); }

   if (instance.FamilyText != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "AnnualContractSetting", "FamilyText"), ReplaceUrls(instance.FamilyText)); }

		return Values;
	}
	

	partial void InsertAnnualContractSetting(AnnualContractSetting instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackAnnualContractSetting(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "AnnualContractSetting"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateAnnualContractSetting(AnnualContractSetting instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackAnnualContractSetting(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "AnnualContractSetting"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteAnnualContractSetting(AnnualContractSetting instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMnpCrmSetting(MnpCrmSetting instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.QueueName != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MnpCrmSetting", "QueueName"), ReplaceUrls(instance.QueueName)); }

   if (instance.CaseType0 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MnpCrmSetting", "CaseType0"), ReplaceUrls(instance.CaseType0)); }

   if (instance.CaseType1 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MnpCrmSetting", "CaseType1"), ReplaceUrls(instance.CaseType1)); }

   if (instance.CaseType2 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MnpCrmSetting", "CaseType2"), ReplaceUrls(instance.CaseType2)); }

   if (instance.CaseType3 != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MnpCrmSetting", "CaseType3"), ReplaceUrls(instance.CaseType3)); }

		return Values;
	}
	

	partial void InsertMnpCrmSetting(MnpCrmSetting instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMnpCrmSetting(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MnpCrmSetting"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMnpCrmSetting(MnpCrmSetting instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMnpCrmSetting(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MnpCrmSetting"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMnpCrmSetting(MnpCrmSetting instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMNPRequestOfficeInRegion(MNPRequestOfficeInRegion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Region_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MNPRequestOfficeInRegion", "Region"), instance.Region_ID.ToString()); }

		return Values;
	}
	

	partial void InsertMNPRequestOfficeInRegion(MNPRequestOfficeInRegion instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMNPRequestOfficeInRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MNPRequestOfficeInRegion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMNPRequestOfficeInRegion(MNPRequestOfficeInRegion instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMNPRequestOfficeInRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MNPRequestOfficeInRegion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMNPRequestOfficeInRegion(MNPRequestOfficeInRegion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMNPRequesCourierInRegion(MNPRequesCourierInRegion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Region_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MNPRequesCourierInRegion", "Region"), instance.Region_ID.ToString()); }

		return Values;
	}
	

	partial void InsertMNPRequesCourierInRegion(MNPRequesCourierInRegion instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMNPRequesCourierInRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MNPRequesCourierInRegion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMNPRequesCourierInRegion(MNPRequesCourierInRegion instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMNPRequesCourierInRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MNPRequesCourierInRegion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMNPRequesCourierInRegion(MNPRequesCourierInRegion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMNPRequestPageInRegion(MNPRequestPageInRegion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Region_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MNPRequestPageInRegion", "Region"), instance.Region_ID.ToString()); }

		return Values;
	}
	

	partial void InsertMNPRequestPageInRegion(MNPRequestPageInRegion instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMNPRequestPageInRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MNPRequestPageInRegion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMNPRequestPageInRegion(MNPRequestPageInRegion instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMNPRequestPageInRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MNPRequestPageInRegion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMNPRequestPageInRegion(MNPRequestPageInRegion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackSkadToFederalRegion(SkadToFederalRegion instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.FederalCode != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SkadToFederalRegion", "FederalCode"), ReplaceUrls(instance.FederalCode)); }

   if (instance.SkadCode != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "SkadToFederalRegion", "SkadCode"), ReplaceUrls(instance.SkadCode)); }

		return Values;
	}
	

	partial void InsertSkadToFederalRegion(SkadToFederalRegion instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSkadToFederalRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SkadToFederalRegion"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateSkadToFederalRegion(SkadToFederalRegion instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackSkadToFederalRegion(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "SkadToFederalRegion"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteSkadToFederalRegion(SkadToFederalRegion instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackServicePrefix(ServicePrefix instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServicePrefix", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServicePrefix", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "ServicePrefix", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertServicePrefix(ServicePrefix instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackServicePrefix(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ServicePrefix"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateServicePrefix(ServicePrefix instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackServicePrefix(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "ServicePrefix"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteServicePrefix(ServicePrefix instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRegionalEmail(RegionalEmail instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Email != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RegionalEmail", "Email"), ReplaceUrls(instance.Email)); }

   if (instance.Type != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RegionalEmail", "Type"), ReplaceUrls(instance.Type)); }

		return Values;
	}
	

	partial void InsertRegionalEmail(RegionalEmail instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRegionalEmail(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RegionalEmail"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRegionalEmail(RegionalEmail instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRegionalEmail(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RegionalEmail"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRegionalEmail(RegionalEmail instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRoamingParameter(RoamingParameter instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingParameter", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingParameter", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingParameter", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

		return Values;
	}
	

	partial void InsertRoamingParameter(RoamingParameter instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingParameter(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingParameter"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRoamingParameter(RoamingParameter instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingParameter(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingParameter"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRoamingParameter(RoamingParameter instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackRoamingOptionCalculation(RoamingOptionCalculation instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.TariffOption_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingOptionCalculation", "TariffOption"), instance.TariffOption_ID.ToString()); }

   if (instance.Parameter_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingOptionCalculation", "Parameter"), instance.Parameter_ID.ToString()); }

   if (instance.NewParameter_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingOptionCalculation", "NewParameter"), instance.NewParameter_ID.ToString()); }

   if (instance.ParamValue != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "RoamingOptionCalculation", "ParamValue"), ReplaceUrls(instance.ParamValue)); }

		return Values;
	}
	

	partial void InsertRoamingOptionCalculation(RoamingOptionCalculation instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingOptionCalculation(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingOptionCalculation"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateRoamingOptionCalculation(RoamingOptionCalculation instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackRoamingOptionCalculation(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "RoamingOptionCalculation"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteRoamingOptionCalculation(RoamingOptionCalculation instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMobileDevice(MobileDevice instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileDevice", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileDevice", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileDevice", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Icon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileDevice", "Icon"), instance.Icon); }

		return Values;
	}
	

	partial void InsertMobileDevice(MobileDevice instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileDevice(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileDevice"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMobileDevice(MobileDevice instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileDevice(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileDevice"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMobileDevice(MobileDevice instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMobileServiceUsageType(MobileServiceUsageType instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.Title != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceUsageType", "Title"), ReplaceUrls(instance.Title)); }

   if (instance.TitleEngl != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceUsageType", "TitleEngl"), ReplaceUrls(instance.TitleEngl)); }

   if (instance.TitleTat != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceUsageType", "TitleTat"), ReplaceUrls(instance.TitleTat)); }

   if (instance.Icon != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceUsageType", "Icon"), instance.Icon); }

		return Values;
	}
	

	partial void InsertMobileServiceUsageType(MobileServiceUsageType instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceUsageType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceUsageType"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMobileServiceUsageType(MobileServiceUsageType instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceUsageType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceUsageType"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMobileServiceUsageType(MobileServiceUsageType instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 
	private Hashtable PackMobileServiceByRegionUsageType(MobileServiceByRegionUsageType instance)
	{
		Hashtable Values = new Hashtable();
		
   if (instance.MarketingMobileService_ID != null)  { Values.Add(Cnn.GetFormNameByNetNames(SiteId, "MobileServiceByRegionUsageType", "MarketingMobileService"), instance.MarketingMobileService_ID.ToString()); }

		return Values;
	}
	

	partial void InsertMobileServiceByRegionUsageType(MobileServiceByRegionUsageType instance) 
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceByRegionUsageType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime created = DateTime.Now;
		instance.LoadStatusType();
		instance.Id = Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceByRegionUsageType"), instance.StatusType.Name, ref Values, ref cl, 0, true, 0, instance.Visible, instance.Archive, true, ref created);
		instance.Created = created;
		instance.Modified = created;
            
	}

	partial void UpdateMobileServiceByRegionUsageType(MobileServiceByRegionUsageType instance)
	{	
		
		Cnn.ExternalTransaction = Transaction;
		Hashtable Values = PackMobileServiceByRegionUsageType(instance);
		HttpFileCollection cl = (HttpFileCollection)null;
		DateTime modified = DateTime.Now;
		Cnn.AddFormToContent(SiteId, Cnn.GetContentIdByNetName(SiteId, "MobileServiceByRegionUsageType"), instance.StatusType.Name, ref Values, ref cl, (int)instance.Id, true, 0, instance.Visible, instance.Archive, true, ref modified);
		instance.Modified = modified;
            		
	}			

	partial void DeleteMobileServiceByRegionUsageType(MobileServiceByRegionUsageType instance)
	{
		
		Cnn.ExternalTransaction = Transaction;	
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'delete from content_item where content_item_id = @itemId', N'@itemId NUMERIC', @itemId = {0}", instance.Id.ToString()));
				
	}
 

	partial void InsertSiteProductToMarketingRegion(SiteProductToMarketingRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "SiteProductToMarketingRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SiteProductToMarketingRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateSiteProductToMarketingRegion(SiteProductToMarketingRegion instance)
	{
	
	}

	partial void DeleteSiteProductToMarketingRegion(SiteProductToMarketingRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertAbstractItemAbtractItemRegionArticle(AbstractItemAbtractItemRegionArticle instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "AbstractItemAbtractItemRegionArticle");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "AbstractItemAbtractItemRegionArticle", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateAbstractItemAbtractItemRegionArticle(AbstractItemAbtractItemRegionArticle instance)
	{
	
	}

	partial void DeleteAbstractItemAbtractItemRegionArticle(AbstractItemAbtractItemRegionArticle instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertTagInRegion(TagInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "TagInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TagInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateTagInRegion(TagInRegion instance)
	{
	
	}

	partial void DeleteTagInRegion(TagInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertSearchSuggestInRegion(SearchSuggestInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "SearchSuggestInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SearchSuggestInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateSearchSuggestInRegion(SearchSuggestInRegion instance)
	{
	
	}

	partial void DeleteSearchSuggestInRegion(SearchSuggestInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertNewsInRegion(NewsInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "NewsInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "NewsInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateNewsInRegion(NewsInRegion instance)
	{
	
	}

	partial void DeleteNewsInRegion(NewsInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingMobileServiceInRoamingTariff(MarketingMobileServiceInRoamingTariff instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingMobileServiceInRoamingTariff");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingMobileServiceInRoamingTariff", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingMobileServiceInRoamingTariff(MarketingMobileServiceInRoamingTariff instance)
	{
	
	}

	partial void DeleteMarketingMobileServiceInRoamingTariff(MarketingMobileServiceInRoamingTariff instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertFeedbackQueueInRegion(FeedbackQueueInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "FeedbackQueueInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "FeedbackQueueInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateFeedbackQueueInRegion(FeedbackQueueInRegion instance)
	{
	
	}

	partial void DeleteFeedbackQueueInRegion(FeedbackQueueInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertTVChannelsInRegion(TVChannelsInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "TVChannelsInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVChannelsInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateTVChannelsInRegion(TVChannelsInRegion instance)
	{
	
	}

	partial void DeleteTVChannelsInRegion(TVChannelsInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertTVChannelsInTheme(TVChannelsInTheme instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "TVChannelsInTheme");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVChannelsInTheme", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateTVChannelsInTheme(TVChannelsInTheme instance)
	{
	
	}

	partial void DeleteTVChannelsInTheme(TVChannelsInTheme instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertFeedbackGroupInRegion(FeedbackGroupInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "FeedbackGroupInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "FeedbackGroupInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateFeedbackGroupInRegion(FeedbackGroupInRegion instance)
	{
	
	}

	partial void DeleteFeedbackGroupInRegion(FeedbackGroupInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingMobileServiceDeviceType(MarketingMobileServiceDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingMobileServiceDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingMobileServiceDeviceType", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingMobileServiceDeviceType(MarketingMobileServiceDeviceType instance)
	{
	
	}

	partial void DeleteMarketingMobileServiceDeviceType(MarketingMobileServiceDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertActionDeviceType(ActionDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ActionDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ActionDeviceType", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateActionDeviceType(ActionDeviceType instance)
	{
	
	}

	partial void DeleteActionDeviceType(ActionDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertActionInRegion(ActionInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ActionInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ActionInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateActionInRegion(ActionInRegion instance)
	{
	
	}

	partial void DeleteActionInRegion(ActionInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertActionProduct(ActionProduct instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ActionProduct");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ActionProduct", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateActionProduct(ActionProduct instance)
	{
	
	}

	partial void DeleteActionProduct(ActionProduct instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertTVPackageInRegion(TVPackageInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "TVPackageInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVPackageInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateTVPackageInRegion(TVPackageInRegion instance)
	{
	
	}

	partial void DeleteTVPackageInRegion(TVPackageInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertIncludedDiscountTvPackagesInMarketingPackage(IncludedDiscountTvPackagesInMarketingPackage instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "IncludedDiscountTvPackagesInMarketingPackage");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "IncludedDiscountTvPackagesInMarketingPackage", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateIncludedDiscountTvPackagesInMarketingPackage(IncludedDiscountTvPackagesInMarketingPackage instance)
	{
	
	}

	partial void DeleteIncludedDiscountTvPackagesInMarketingPackage(IncludedDiscountTvPackagesInMarketingPackage instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertActivatedDiscountTvPackagesInMarketingPackage(ActivatedDiscountTvPackagesInMarketingPackage instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ActivatedDiscountTvPackagesInMarketingPackage");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ActivatedDiscountTvPackagesInMarketingPackage", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateActivatedDiscountTvPackagesInMarketingPackage(ActivatedDiscountTvPackagesInMarketingPackage instance)
	{
	
	}

	partial void DeleteActivatedDiscountTvPackagesInMarketingPackage(ActivatedDiscountTvPackagesInMarketingPackage instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertExternalRegionMappingToMarketingRegion(ExternalRegionMappingToMarketingRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ExternalRegionMappingToMarketingRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ExternalRegionMappingToMarketingRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateExternalRegionMappingToMarketingRegion(ExternalRegionMappingToMarketingRegion instance)
	{
	
	}

	partial void DeleteExternalRegionMappingToMarketingRegion(ExternalRegionMappingToMarketingRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingTariffDeviceType(MarketingTariffDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingTariffDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingTariffDeviceType", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingTariffDeviceType(MarketingTariffDeviceType instance)
	{
	
	}

	partial void DeleteMarketingTariffDeviceType(MarketingTariffDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMobileTariffInCategory(MobileTariffInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MobileTariffInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MobileTariffInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMobileTariffInCategory(MobileTariffInCategory instance)
	{
	
	}

	partial void DeleteMobileTariffInCategory(MobileTariffInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingMobileTariffInParamGroup(MarketingMobileTariffInParamGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingMobileTariffInParamGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingMobileTariffInParamGroup", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingMobileTariffInParamGroup(MarketingMobileTariffInParamGroup instance)
	{
	
	}

	partial void DeleteMarketingMobileTariffInParamGroup(MarketingMobileTariffInParamGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertGuideQuestionDeviceType(GuideQuestionDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "GuideQuestionDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "GuideQuestionDeviceType", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateGuideQuestionDeviceType(GuideQuestionDeviceType instance)
	{
	
	}

	partial void DeleteGuideQuestionDeviceType(GuideQuestionDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertServiceInTariffParameterGroup(ServiceInTariffParameterGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ServiceInTariffParameterGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ServiceInTariffParameterGroup", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateServiceInTariffParameterGroup(ServiceInTariffParameterGroup instance)
	{
	
	}

	partial void DeleteServiceInTariffParameterGroup(ServiceInTariffParameterGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingMobileServiceInParamGroup(MarketingMobileServiceInParamGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingMobileServiceInParamGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingMobileServiceInParamGroup", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingMobileServiceInParamGroup(MarketingMobileServiceInParamGroup instance)
	{
	
	}

	partial void DeleteMarketingMobileServiceInParamGroup(MarketingMobileServiceInParamGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMobileServiceInCategory(MobileServiceInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MobileServiceInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MobileServiceInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMobileServiceInCategory(MobileServiceInCategory instance)
	{
	
	}

	partial void DeleteMobileServiceInCategory(MobileServiceInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingMobileServiceMobileDeviceType(MarketingMobileServiceMobileDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingMobileServiceMobileDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingMobileServiceMobileDeviceType", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingMobileServiceMobileDeviceType(MarketingMobileServiceMobileDeviceType instance)
	{
	
	}

	partial void DeleteMarketingMobileServiceMobileDeviceType(MarketingMobileServiceMobileDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMobileTariffFamilyInCategory(MobileTariffFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MobileTariffFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MobileTariffFamilyInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMobileTariffFamilyInCategory(MobileTariffFamilyInCategory instance)
	{
	
	}

	partial void DeleteMobileTariffFamilyInCategory(MobileTariffFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMutualMobileService(MutualMobileService instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MutualMobileService");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MutualMobileService", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMutualMobileService(MutualMobileService instance)
	{
	
	}

	partial void DeleteMutualMobileService(MutualMobileService instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMobileServiceFamilyInCategory(MobileServiceFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MobileServiceFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MobileServiceFamilyInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMobileServiceFamilyInCategory(MobileServiceFamilyInCategory instance)
	{
	
	}

	partial void DeleteMobileServiceFamilyInCategory(MobileServiceFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMobileParamGroupInTab(MobileParamGroupInTab instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MobileParamGroupInTab");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MobileParamGroupInTab", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMobileParamGroupInTab(MobileParamGroupInTab instance)
	{
	
	}

	partial void DeleteMobileParamGroupInTab(MobileParamGroupInTab instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertPrivelegeAndBonusDeviceType(PrivelegeAndBonusDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "PrivelegeAndBonusDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "PrivelegeAndBonusDeviceType", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdatePrivelegeAndBonusDeviceType(PrivelegeAndBonusDeviceType instance)
	{
	
	}

	partial void DeletePrivelegeAndBonusDeviceType(PrivelegeAndBonusDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertPrivelegeAndBonusInRegion(PrivelegeAndBonusInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "PrivelegeAndBonusInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "PrivelegeAndBonusInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdatePrivelegeAndBonusInRegion(PrivelegeAndBonusInRegion instance)
	{
	
	}

	partial void DeletePrivelegeAndBonusInRegion(PrivelegeAndBonusInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertPrivelegeAndBonusProduct(PrivelegeAndBonusProduct instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "PrivelegeAndBonusProduct");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "PrivelegeAndBonusProduct", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdatePrivelegeAndBonusProduct(PrivelegeAndBonusProduct instance)
	{
	
	}

	partial void DeletePrivelegeAndBonusProduct(PrivelegeAndBonusProduct instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertFeedbackThemeToFaqContent(FeedbackThemeToFaqContent instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "FeedbackThemeToFaqContent");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "FeedbackThemeToFaqContent", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateFeedbackThemeToFaqContent(FeedbackThemeToFaqContent instance)
	{
	
	}

	partial void DeleteFeedbackThemeToFaqContent(FeedbackThemeToFaqContent instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertFeedbackSubThemeToFaqContent(FeedbackSubThemeToFaqContent instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "FeedbackSubThemeToFaqContent");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "FeedbackSubThemeToFaqContent", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateFeedbackSubThemeToFaqContent(FeedbackSubThemeToFaqContent instance)
	{
	
	}

	partial void DeleteFeedbackSubThemeToFaqContent(FeedbackSubThemeToFaqContent instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertFaqContentToRegion(FaqContentToRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "FaqContentToRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "FaqContentToRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateFaqContentToRegion(FaqContentToRegion instance)
	{
	
	}

	partial void DeleteFaqContentToRegion(FaqContentToRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingTVPackageInTheme(MarketingTVPackageInTheme instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingTVPackageInTheme");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingTVPackageInTheme", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingTVPackageInTheme(MarketingTVPackageInTheme instance)
	{
	
	}

	partial void DeleteMarketingTVPackageInTheme(MarketingTVPackageInTheme instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertUnavailableServiceInMarketingTVPackage(UnavailableServiceInMarketingTVPackage instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "UnavailableServiceInMarketingTVPackage");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "UnavailableServiceInMarketingTVPackage", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateUnavailableServiceInMarketingTVPackage(UnavailableServiceInMarketingTVPackage instance)
	{
	
	}

	partial void DeleteUnavailableServiceInMarketingTVPackage(UnavailableServiceInMarketingTVPackage instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingTVPackageInMutualGroup(MarketingTVPackageInMutualGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingTVPackageInMutualGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingTVPackageInMutualGroup", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingTVPackageInMutualGroup(MarketingTVPackageInMutualGroup instance)
	{
	
	}

	partial void DeleteMarketingTVPackageInMutualGroup(MarketingTVPackageInMutualGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertTVChannelsInMarketingPackage(TVChannelsInMarketingPackage instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "TVChannelsInMarketingPackage");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVChannelsInMarketingPackage", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateTVChannelsInMarketingPackage(TVChannelsInMarketingPackage instance)
	{
	
	}

	partial void DeleteTVChannelsInMarketingPackage(TVChannelsInMarketingPackage instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertQuickLinksGroupInRegion(QuickLinksGroupInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "QuickLinksGroupInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "QuickLinksGroupInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateQuickLinksGroupInRegion(QuickLinksGroupInRegion instance)
	{
	
	}

	partial void DeleteQuickLinksGroupInRegion(QuickLinksGroupInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertProvodTariffParameterGroupInProduct(ProvodTariffParameterGroupInProduct instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ProvodTariffParameterGroupInProduct");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodTariffParameterGroupInProduct", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateProvodTariffParameterGroupInProduct(ProvodTariffParameterGroupInProduct instance)
	{
	
	}

	partial void DeleteProvodTariffParameterGroupInProduct(ProvodTariffParameterGroupInProduct instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertInternetTariffFamilyInCategory(InternetTariffFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "InternetTariffFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "InternetTariffFamilyInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateInternetTariffFamilyInCategory(InternetTariffFamilyInCategory instance)
	{
	
	}

	partial void DeleteInternetTariffFamilyInCategory(InternetTariffFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertPhoneTariffFamilyInCategory(PhoneTariffFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "PhoneTariffFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "PhoneTariffFamilyInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdatePhoneTariffFamilyInCategory(PhoneTariffFamilyInCategory instance)
	{
	
	}

	partial void DeletePhoneTariffFamilyInCategory(PhoneTariffFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertProvodServiceFamilyInCategory(ProvodServiceFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ProvodServiceFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodServiceFamilyInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateProvodServiceFamilyInCategory(ProvodServiceFamilyInCategory instance)
	{
	
	}

	partial void DeleteProvodServiceFamilyInCategory(ProvodServiceFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingInternetTariffInCategory(MarketingInternetTariffInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingInternetTariffInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingInternetTariffInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingInternetTariffInCategory(MarketingInternetTariffInCategory instance)
	{
	
	}

	partial void DeleteMarketingInternetTariffInCategory(MarketingInternetTariffInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertUnavailableServicesInInternetTariff(UnavailableServicesInInternetTariff instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "UnavailableServicesInInternetTariff");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "UnavailableServicesInInternetTariff", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateUnavailableServicesInInternetTariff(UnavailableServicesInInternetTariff instance)
	{
	
	}

	partial void DeleteUnavailableServicesInInternetTariff(UnavailableServicesInInternetTariff instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingPhoneTariffInCategory(MarketingPhoneTariffInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingPhoneTariffInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingPhoneTariffInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingPhoneTariffInCategory(MarketingPhoneTariffInCategory instance)
	{
	
	}

	partial void DeleteMarketingPhoneTariffInCategory(MarketingPhoneTariffInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertUnavailableServiceInMarketingPhoneTariff(UnavailableServiceInMarketingPhoneTariff instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "UnavailableServiceInMarketingPhoneTariff");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "UnavailableServiceInMarketingPhoneTariff", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateUnavailableServiceInMarketingPhoneTariff(UnavailableServiceInMarketingPhoneTariff instance)
	{
	
	}

	partial void DeleteUnavailableServiceInMarketingPhoneTariff(UnavailableServiceInMarketingPhoneTariff instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingProvodServiceInCategory(MarketingProvodServiceInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingProvodServiceInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingProvodServiceInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingProvodServiceInCategory(MarketingProvodServiceInCategory instance)
	{
	
	}

	partial void DeleteMarketingProvodServiceInCategory(MarketingProvodServiceInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingProvodServiceInProduct(MarketingProvodServiceInProduct instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingProvodServiceInProduct");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingProvodServiceInProduct", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingProvodServiceInProduct(MarketingProvodServiceInProduct instance)
	{
	
	}

	partial void DeleteMarketingProvodServiceInProduct(MarketingProvodServiceInProduct instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertProvodKitFamilyInCategory(ProvodKitFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ProvodKitFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodKitFamilyInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateProvodKitFamilyInCategory(ProvodKitFamilyInCategory instance)
	{
	
	}

	partial void DeleteProvodKitFamilyInCategory(ProvodKitFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingProvodKitInCategory(MarketingProvodKitInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingProvodKitInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingProvodKitInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingProvodKitInCategory(MarketingProvodKitInCategory instance)
	{
	
	}

	partial void DeleteMarketingProvodKitInCategory(MarketingProvodKitInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertUnavailableServiceInMarketingProvodKit(UnavailableServiceInMarketingProvodKit instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "UnavailableServiceInMarketingProvodKit");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "UnavailableServiceInMarketingProvodKit", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateUnavailableServiceInMarketingProvodKit(UnavailableServiceInMarketingProvodKit instance)
	{
	
	}

	partial void DeleteUnavailableServiceInMarketingProvodKit(UnavailableServiceInMarketingProvodKit instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingProvodServiceInKit(MarketingProvodServiceInKit instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingProvodServiceInKit");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingProvodServiceInKit", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingProvodServiceInKit(MarketingProvodServiceInKit instance)
	{
	
	}

	partial void DeleteMarketingProvodServiceInKit(MarketingProvodServiceInKit instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertTVPackagesInProvodKit(TVPackagesInProvodKit instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "TVPackagesInProvodKit");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVPackagesInProvodKit", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateTVPackagesInProvodKit(TVPackagesInProvodKit instance)
	{
	
	}

	partial void DeleteTVPackagesInProvodKit(TVPackagesInProvodKit instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertProvodKitInRegion(ProvodKitInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ProvodKitInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodKitInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateProvodKitInRegion(ProvodKitInRegion instance)
	{
	
	}

	partial void DeleteProvodKitInRegion(ProvodKitInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertRoamingParamsInCountryGroup(RoamingParamsInCountryGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "RoamingParamsInCountryGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "RoamingParamsInCountryGroup", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateRoamingParamsInCountryGroup(RoamingParamsInCountryGroup instance)
	{
	
	}

	partial void DeleteRoamingParamsInCountryGroup(RoamingParamsInCountryGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertTVPackageFamilyInCategory(TVPackageFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "TVPackageFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVPackageFamilyInCategory", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateTVPackageFamilyInCategory(TVPackageFamilyInCategory instance)
	{
	
	}

	partial void DeleteTVPackageFamilyInCategory(TVPackageFamilyInCategory instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertHelpDeviceInRegion(HelpDeviceInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "HelpDeviceInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "HelpDeviceInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateHelpDeviceInRegion(HelpDeviceInRegion instance)
	{
	
	}

	partial void DeleteHelpDeviceInRegion(HelpDeviceInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertHelpCenterParamInDeviceType(HelpCenterParamInDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "HelpCenterParamInDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "HelpCenterParamInDeviceType", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateHelpCenterParamInDeviceType(HelpCenterParamInDeviceType instance)
	{
	
	}

	partial void DeleteHelpCenterParamInDeviceType(HelpCenterParamInDeviceType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertTVProgramChannelsInRegion(TVProgramChannelsInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "TVProgramChannelsInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVProgramChannelsInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateTVProgramChannelsInRegion(TVProgramChannelsInRegion instance)
	{
	
	}

	partial void DeleteTVProgramChannelsInRegion(TVProgramChannelsInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertEquipmentTabToParamsGroup(EquipmentTabToParamsGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "EquipmentTabToParamsGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "EquipmentTabToParamsGroup", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateEquipmentTabToParamsGroup(EquipmentTabToParamsGroup instance)
	{
	
	}

	partial void DeleteEquipmentTabToParamsGroup(EquipmentTabToParamsGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertEquipmentToRegion(EquipmentToRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "EquipmentToRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "EquipmentToRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateEquipmentToRegion(EquipmentToRegion instance)
	{
	
	}

	partial void DeleteEquipmentToRegion(EquipmentToRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingEquipmentsToMarketingEquipments(MarketingEquipmentsToMarketingEquipments instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingEquipmentsToMarketingEquipments");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingEquipmentsToMarketingEquipments", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingEquipmentsToMarketingEquipments(MarketingEquipmentsToMarketingEquipments instance)
	{
	
	}

	partial void DeleteMarketingEquipmentsToMarketingEquipments(MarketingEquipmentsToMarketingEquipments instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMarketingEquipmentToEquipmentImage(MarketingEquipmentToEquipmentImage instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MarketingEquipmentToEquipmentImage");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingEquipmentToEquipmentImage", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMarketingEquipmentToEquipmentImage(MarketingEquipmentToEquipmentImage instance)
	{
	
	}

	partial void DeleteMarketingEquipmentToEquipmentImage(MarketingEquipmentToEquipmentImage instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertInternetTariffInRegion(InternetTariffInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "InternetTariffInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "InternetTariffInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateInternetTariffInRegion(InternetTariffInRegion instance)
	{
	
	}

	partial void DeleteInternetTariffInRegion(InternetTariffInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertPhoneTariffInRegion(PhoneTariffInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "PhoneTariffInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "PhoneTariffInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdatePhoneTariffInRegion(PhoneTariffInRegion instance)
	{
	
	}

	partial void DeletePhoneTariffInRegion(PhoneTariffInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertProvodServiceParamInRegion(ProvodServiceParamInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ProvodServiceParamInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodServiceParamInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateProvodServiceParamInRegion(ProvodServiceParamInRegion instance)
	{
	
	}

	partial void DeleteProvodServiceParamInRegion(ProvodServiceParamInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertProvodServiceInRegion(ProvodServiceInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ProvodServiceInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodServiceInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateProvodServiceInRegion(ProvodServiceInRegion instance)
	{
	
	}

	partial void DeleteProvodServiceInRegion(ProvodServiceInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertSearchAnnouncementInRegion(SearchAnnouncementInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "SearchAnnouncementInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SearchAnnouncementInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateSearchAnnouncementInRegion(SearchAnnouncementInRegion instance)
	{
	
	}

	partial void DeleteSearchAnnouncementInRegion(SearchAnnouncementInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertSalesPointTypeToSKADType(SalesPointTypeToSKADType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "SalesPointTypeToSKADType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SalesPointTypeToSKADType", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateSalesPointTypeToSKADType(SalesPointTypeToSKADType instance)
	{
	
	}

	partial void DeleteSalesPointTypeToSKADType(SalesPointTypeToSKADType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertSalesPointSpecialityToSKADSpeciality(SalesPointSpecialityToSKADSpeciality instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "SalesPointSpecialityToSKADSpeciality");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SalesPointSpecialityToSKADSpeciality", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateSalesPointSpecialityToSKADSpeciality(SalesPointSpecialityToSKADSpeciality instance)
	{
	
	}

	partial void DeleteSalesPointSpecialityToSKADSpeciality(SalesPointSpecialityToSKADSpeciality instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertSalesPointServiceToSKADService(SalesPointServiceToSKADService instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "SalesPointServiceToSKADService");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SalesPointServiceToSKADService", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateSalesPointServiceToSKADService(SalesPointServiceToSKADService instance)
	{
	
	}

	partial void DeleteSalesPointServiceToSKADService(SalesPointServiceToSKADService instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertArchiveTvTariffInRegion(ArchiveTvTariffInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ArchiveTvTariffInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ArchiveTvTariffInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateArchiveTvTariffInRegion(ArchiveTvTariffInRegion instance)
	{
	
	}

	partial void DeleteArchiveTvTariffInRegion(ArchiveTvTariffInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertChannelOnTariff(ChannelOnTariff instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ChannelOnTariff");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ChannelOnTariff", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateChannelOnTariff(ChannelOnTariff instance)
	{
	
	}

	partial void DeleteChannelOnTariff(ChannelOnTariff instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertContactToMarketingRegion(ContactToMarketingRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ContactToMarketingRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ContactToMarketingRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateContactToMarketingRegion(ContactToMarketingRegion instance)
	{
	
	}

	partial void DeleteContactToMarketingRegion(ContactToMarketingRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertContactToContactsGroup(ContactToContactsGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ContactToContactsGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ContactToContactsGroup", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateContactToContactsGroup(ContactToContactsGroup instance)
	{
	
	}

	partial void DeleteContactToContactsGroup(ContactToContactsGroup instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertAnnualContractSettingInRegion(AnnualContractSettingInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "AnnualContractSettingInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "AnnualContractSettingInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateAnnualContractSettingInRegion(AnnualContractSettingInRegion instance)
	{
	
	}

	partial void DeleteAnnualContractSettingInRegion(AnnualContractSettingInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertMnpCrmSettingInRegion(MnpCrmSettingInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "MnpCrmSettingInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MnpCrmSettingInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateMnpCrmSettingInRegion(MnpCrmSettingInRegion instance)
	{
	
	}

	partial void DeleteMnpCrmSettingInRegion(MnpCrmSettingInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertRegionToEmail(RegionToEmail instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "RegionToEmail");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "RegionToEmail", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateRegionToEmail(RegionToEmail instance)
	{
	
	}

	partial void DeleteRegionToEmail(RegionToEmail instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertRoamingOptionInRegion(RoamingOptionInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "RoamingOptionInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "RoamingOptionInRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateRoamingOptionInRegion(RoamingOptionInRegion instance)
	{
	
	}

	partial void DeleteRoamingOptionInRegion(RoamingOptionInRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertRoamingTariffsInOption(RoamingTariffsInOption instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "RoamingTariffsInOption");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "RoamingTariffsInOption", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateRoamingTariffsInOption(RoamingTariffsInOption instance)
	{
	
	}

	partial void DeleteRoamingTariffsInOption(RoamingTariffsInOption instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertByRegionMobileServiceRegion(ByRegionMobileServiceRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ByRegionMobileServiceRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ByRegionMobileServiceRegion", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateByRegionMobileServiceRegion(ByRegionMobileServiceRegion instance)
	{
	
	}

	partial void DeleteByRegionMobileServiceRegion(ByRegionMobileServiceRegion instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertByRegionMobileServiceUsageType(ByRegionMobileServiceUsageType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ByRegionMobileServiceUsageType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ByRegionMobileServiceUsageType", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateByRegionMobileServiceUsageType(ByRegionMobileServiceUsageType instance)
	{
	
	}

	partial void DeleteByRegionMobileServiceUsageType(ByRegionMobileServiceUsageType instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertProduktovoenapravlenieProduktyArticle(ProduktovoenapravlenieProduktyArticle instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ProduktovoenapravlenieProduktyArticle");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProduktovoenapravlenieProduktyArticle", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateProduktovoenapravlenieProduktyArticle(ProduktovoenapravlenieProduktyArticle instance)
	{
	
	}

	partial void DeleteProduktovoenapravlenieProduktyArticle(ProduktovoenapravlenieProduktyArticle instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertItemDefinitionItemDefinitionArticle(ItemDefinitionItemDefinitionArticle instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "ItemDefinitionItemDefinitionArticle");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ItemDefinitionItemDefinitionArticle", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateItemDefinitionItemDefinitionArticle(ItemDefinitionItemDefinitionArticle instance)
	{
	
	}

	partial void DeleteItemDefinitionItemDefinitionArticle(ItemDefinitionItemDefinitionArticle instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertAvtorizovannyeservisnyetsentryProduktyArticle(AvtorizovannyeservisnyetsentryProduktyArticle instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "AvtorizovannyeservisnyetsentryProduktyArticle");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "AvtorizovannyeservisnyetsentryProduktyArticle", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateAvtorizovannyeservisnyetsentryProduktyArticle(AvtorizovannyeservisnyetsentryProduktyArticle instance)
	{
	
	}

	partial void DeleteAvtorizovannyeservisnyetsentryProduktyArticle(AvtorizovannyeservisnyetsentryProduktyArticle instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertAvtorizovannyeservisnyetsentryMarketingovyeregionyArticle(AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle instance)
	{
		Cnn.ExternalTransaction = Transaction;
		int linkId = Cnn.GetLinkIdByNetName(SiteId, "AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle", SiteId));
			
		Cnn.ProcessData(String.Format("EXEC sp_executesql N'if not exists(select * from item_link where link_id = @linkId and item_id = @itemId and linked_item_id = @linkedItemId) insert into item_to_item values(@linkId, @itemId, @linkedItemId)', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, instance.ITEM_ID, instance.LINKED_ITEM_ID));
	}

	partial void UpdateAvtorizovannyeservisnyetsentryMarketingovyeregionyArticle(AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle instance)
	{
	
	}

	partial void DeleteAvtorizovannyeservisnyetsentryMarketingovyeregionyArticle(AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle instance)
	{
		Cnn.ExternalTransaction = Transaction;
		Cnn.ProcessData(instance.RemovingInstruction);
	}
    

	partial void InsertStatusType(StatusType instance) 
	{
	}

	partial void UpdateStatusType(StatusType instance)
	{
	}			

	partial void DeleteStatusType(StatusType instance)
	{
	}
}


}

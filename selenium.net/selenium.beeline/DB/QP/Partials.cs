﻿
namespace QA.Beeline.DAL
{
    /// <summary>
    /// Контракт-метка для коллекций акций
    /// </summary>
    public interface IActionReference
    {
        int Action_ID { get; set; }
    }

    public partial class ActionProduct : IActionReference
    {

    }

    public partial class ActionDeviceType : IActionReference
    {

    }

    public partial class ActionInRegion : IActionReference
    {

    }
    
    /// <summary>
    /// Контракт-метка для коллекций услуг
    /// </summary>
    public interface IServiceReference
    {
        int MarketingMobileService_ID { get; set; }
    }

    public interface INullableServiceReference
    {
        int? MarketingMobileService_ID { get; set; }
    }

    public partial class MarketingMobileServiceDeviceType : IServiceReference
    {
        
    }

    public partial class ServiceInTariffParameterGroup : IServiceReference
    {
        
    }

    public partial class MarketingMobileServiceInParamGroup : IServiceReference
    {
        
    }
    
    public partial class MobileServiceInCategory : IServiceReference
    {
        
    }

    public partial class MarketingMobileServiceMobileDeviceType : IServiceReference
    {
        
    }

    public partial class MobileServiceByRegionUsageType : INullableServiceReference
    {
        
    }
    /// <summary>
    /// Контракт-метка для коллекций семейства тарифов
    /// </summary>
    public interface ITariffFamilyReference
    {
        int MobileTariffFamily_ID { get; set; }
    }

    public partial class MobileTariffFamilyInCategory : ITariffFamilyReference
    {
        
    }

    /// <summary>
    /// Контракт-метка для коллекций тарифов
    /// </summary>
    public interface ITariffReference
    {
        int MarketingMobileTariff_ID { get; set; }
    }

    public partial class MarketingTariffDeviceType : ITariffReference
    {
        
    }

    public partial class MarketingMobileTariffInParamGroup : ITariffReference
    {

    }
}

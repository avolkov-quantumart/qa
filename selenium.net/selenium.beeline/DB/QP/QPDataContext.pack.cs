﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA.Beeline.DAL
{
    public partial class QPDataContext
    {
        internal static readonly Func<MarketingMobileTariff, Hashtable> PackMarketingMobileTariffFunc = x => LinqHelper.Context.PackMarketingMobileTariff(x);
        internal static readonly Func<Action, Hashtable> PackActionFunc = x => LinqHelper.Context.PackAction(x);
        internal static readonly Func<MarketingTVPackage, Hashtable> PackMarketingTvPackageFunc = x => LinqHelper.Context.PackMarketingTVPackage(x);
        internal static readonly Func<TVPackage, Hashtable> PackTvPackageFunc = x => LinqHelper.Context.PackTVPackage(x); 
    }

    partial class MarketingMobileTariff : IQPContent
    {
        public Hashtable PackModifications()
        {
            return QPDataContext.PackMarketingMobileTariffFunc(this);
        }
    }
}

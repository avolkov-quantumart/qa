﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Linq.Mapping;
using System.IO;
using System.Linq;
using Quantumart.QPublishing.Database;

namespace QA.Beeline.DAL {
    public interface IQPContent {
        int Id { get; set; }
        int StatusTypeId { get; set; }
        StatusType StatusType { get; set; }
        bool StatusTypeChanged { get; set; }
        bool Visible { get; set; }
        bool Archive { get; set; }
        DateTime Created { get; set; }
        DateTime Modified { get; set; }
        int LastModifiedBy { get; set; }
    }

    public partial class QPDataContext {
        private static string _DefaultSiteName = "main_site";

        private static string _DefaultConnectionString;

        private static XmlMappingSource _DefaultXmlMappingSource;
        public static bool RemoveUploadUrlSchema = false;
        private bool _ShouldRemoveSchema;
        private string _SiteName;
        private DBConnector _cnn;
        private string sitePlaceholder = "<%=site_url%>";
        private string uploadPlaceholder = "<%=upload_url%>";

        public static string DefaultSiteName {
            get { return _DefaultSiteName; }
            set { _DefaultSiteName = value; }
        }

        public static string DefaultConnectionString {
            get {
                if (_DefaultConnectionString == null) {
                    ConnectionStringSettings obj = ConfigurationManager.ConnectionStrings["qp_database"];
                    if (obj == null)
                        throw new ApplicationException("Connection string 'qp_database' is not specified");
                    _DefaultConnectionString = obj.ConnectionString;
                }
                return _DefaultConnectionString;
            }
            set { _DefaultConnectionString = value; }
        }

        public static XmlMappingSource DefaultXmlMappingSource {
            get {
                if (_DefaultXmlMappingSource == null)
                    _DefaultXmlMappingSource = GetDefaultXmlMappingSource(null);
                return _DefaultXmlMappingSource;
            }
            set { _DefaultXmlMappingSource = value; }
        }

        public string ConnectionString { get; private set; }

        public string SiteName {
            get { return _SiteName; }
            set {
                if (!String.Equals(_SiteName, value, StringComparison.InvariantCultureIgnoreCase)) {
                    _SiteName = value;
                    SiteId = Cnn.GetSiteId(_SiteName);
                    LoadSiteSpecificInfo();
                }
            }
        }

        public Int32 SiteId { get; private set; }

        public DBConnector Cnn {
            get {
                if (_cnn == null) {
                    _cnn = (Connection != null)
                               ? new DBConnector(Connection, Transaction)
                               : new DBConnector(ConnectionString);

                    _cnn.UpdateManyToMany = false;
                }
                return _cnn;
            }
        }

        public bool ShouldRemoveSchema {
            get { return _ShouldRemoveSchema; }
            set { _ShouldRemoveSchema = value; }
        }

        public string SiteUrl {
            get { return StageSiteUrl; }
        }

        public string UploadUrl {
            get { return LongUploadUrl; }
        }

        public string LiveSiteUrl { get; private set; }

        public string LiveSiteUrlRel { get; private set; }

        public string StageSiteUrl { get; private set; }

        public string StageSiteUrlRel { get; private set; }

        public string LongUploadUrl { get; private set; }

        public string ShortUploadUrl { get; private set; }

        public Int32 PublishedId { get; private set; }

        public static XmlMappingSource GetDefaultXmlMappingSource(IDbConnection connection) {
            return XmlMappingSource.FromXml(File.ReadAllText("DB/QP/Mapping/QPDataContext_Stage.map"));
        }

        public static QPDataContext Create(IDbConnection connection, string siteName, MappingSource mappingSource) {
            var ctx = new QPDataContext(connection, mappingSource);
            ctx.SiteName = siteName;
            ctx.ConnectionString = connection.ConnectionString;
            return ctx;
        }

        public static QPDataContext Create(IDbConnection connection, string siteName) {
            return Create(connection, siteName, GetDefaultXmlMappingSource(connection));
        }

        public static QPDataContext Create(IDbConnection connection) {
            return Create(connection, DefaultSiteName);
        }

        public static QPDataContext Create(string connection, string siteName, MappingSource mappingSource) {
            var ctx = new QPDataContext(connection, mappingSource);
            ctx.SiteName = siteName;
            ctx.ConnectionString = connection;
            return ctx;
        }

        public static QPDataContext Create(string siteName, MappingSource mappingSource) {
            return Create(DefaultConnectionString, siteName, mappingSource);
        }

        public static QPDataContext Create(string connection, string siteName) {
            return Create(connection, siteName, DefaultXmlMappingSource);
        }

        public static QPDataContext Create(string connection) {
            return Create(connection, DefaultSiteName);
        }

        public static QPDataContext Create() {
            return Create(DefaultConnectionString);
        }

        public void LoadSiteSpecificInfo() {
            if (RemoveUploadUrlSchema && !_ShouldRemoveSchema)
                _ShouldRemoveSchema = RemoveUploadUrlSchema;

            LiveSiteUrl = Cnn.GetSiteUrl(SiteId, true);
            LiveSiteUrlRel = Cnn.GetSiteUrlRel(SiteId, true);
            StageSiteUrl = Cnn.GetSiteUrl(SiteId, false);
            StageSiteUrlRel = Cnn.GetSiteUrlRel(SiteId, false);
            LongUploadUrl = Cnn.GetImagesUploadUrl(SiteId, false, _ShouldRemoveSchema);
            ShortUploadUrl = Cnn.GetImagesUploadUrl(SiteId, true, _ShouldRemoveSchema);
            PublishedId = Cnn.GetMaximumWeightStatusTypeId(SiteId);
        }

        public string ReplacePlaceholders(string input) {
            string result = input;
            if (result != null) {
                result = result.Replace(uploadPlaceholder, UploadUrl);
                result = result.Replace(sitePlaceholder, SiteUrl);
            }
            return result;
        }

        public string ReplaceUrls(string input) {
            string result = input;
            if (result != null) {
                result = result.Replace(LongUploadUrl, uploadPlaceholder);
                result = result.Replace(ShortUploadUrl, uploadPlaceholder);
                result = result.Replace(LiveSiteUrl, sitePlaceholder);
                result = result.Replace(StageSiteUrl, sitePlaceholder);
                if (!String.Equals(LiveSiteUrlRel, "/")) result = result.Replace(LiveSiteUrlRel, sitePlaceholder);
                if (!String.Equals(StageSiteUrlRel, "/")) result = result.Replace(StageSiteUrlRel, sitePlaceholder);
            }
            return result;
        }
    }

    public static class UserExtensions {
        public static IEnumerable<GeoRegion> ApplyContext(this IEnumerable<GeoRegion> e, QPDataContext context) {
            foreach (GeoRegion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<GeoRegion> Published(this IQueryable<GeoRegion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<GeoRegion> ForFrontEnd(this IQueryable<GeoRegion> e) {
            return e;
        }

        public static IEnumerable<MarketingRegion> ApplyContext(this IEnumerable<MarketingRegion> e,
                                                                QPDataContext context) {
            foreach (MarketingRegion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MarketingRegion> Published(this IQueryable<MarketingRegion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MarketingRegion> ForFrontEnd(this IQueryable<MarketingRegion> e) {
            return e;
        }

        public static IEnumerable<GeoRegionsIPAddress> ApplyContext(this IEnumerable<GeoRegionsIPAddress> e,
                                                                    QPDataContext context) {
            foreach (GeoRegionsIPAddress item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<GeoRegionsIPAddress> Published(this IQueryable<GeoRegionsIPAddress> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<GeoRegionsIPAddress> ForFrontEnd(this IQueryable<GeoRegionsIPAddress> e) {
            return e;
        }

        public static IEnumerable<SiteProduct> ApplyContext(this IEnumerable<SiteProduct> e, QPDataContext context) {
            foreach (SiteProduct item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SiteProduct> Published(this IQueryable<SiteProduct> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SiteProduct> ForFrontEnd(this IQueryable<SiteProduct> e) {
            return e;
        }

        public static IEnumerable<QPAbstractItem> ApplyContext(this IEnumerable<QPAbstractItem> e, QPDataContext context) {
            foreach (QPAbstractItem item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<QPAbstractItem> Published(this IQueryable<QPAbstractItem> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<QPAbstractItem> ForFrontEnd(this IQueryable<QPAbstractItem> e) {
            return e;
        }

        public static IEnumerable<QPDiscriminator> ApplyContext(this IEnumerable<QPDiscriminator> e,
                                                                QPDataContext context) {
            foreach (QPDiscriminator item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<QPDiscriminator> Published(this IQueryable<QPDiscriminator> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<QPDiscriminator> ForFrontEnd(this IQueryable<QPDiscriminator> e) {
            return e;
        }

        public static IEnumerable<QPCulture> ApplyContext(this IEnumerable<QPCulture> e, QPDataContext context) {
            foreach (QPCulture item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<QPCulture> Published(this IQueryable<QPCulture> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<QPCulture> ForFrontEnd(this IQueryable<QPCulture> e) {
            return e;
        }

        public static IEnumerable<ItemTitleFormat> ApplyContext(this IEnumerable<ItemTitleFormat> e,
                                                                QPDataContext context) {
            foreach (ItemTitleFormat item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ItemTitleFormat> Published(this IQueryable<ItemTitleFormat> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ItemTitleFormat> ForFrontEnd(this IQueryable<ItemTitleFormat> e) {
            return e;
        }

        public static IEnumerable<QPRegion> ApplyContext(this IEnumerable<QPRegion> e, QPDataContext context) {
            foreach (QPRegion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<QPRegion> Published(this IQueryable<QPRegion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<QPRegion> ForFrontEnd(this IQueryable<QPRegion> e) {
            return e;
        }

        public static IEnumerable<Poll> ApplyContext(this IEnumerable<Poll> e, QPDataContext context) {
            foreach (Poll item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<Poll> Published(this IQueryable<Poll> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<Poll> ForFrontEnd(this IQueryable<Poll> e) {
            return e;
        }

        public static IEnumerable<PollQuestion> ApplyContext(this IEnumerable<PollQuestion> e, QPDataContext context) {
            foreach (PollQuestion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PollQuestion> Published(this IQueryable<PollQuestion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PollQuestion> ForFrontEnd(this IQueryable<PollQuestion> e) {
            return e;
        }

        public static IEnumerable<PollAnswer> ApplyContext(this IEnumerable<PollAnswer> e, QPDataContext context) {
            foreach (PollAnswer item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PollAnswer> Published(this IQueryable<PollAnswer> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PollAnswer> ForFrontEnd(this IQueryable<PollAnswer> e) {
            return e;
        }

        public static IEnumerable<UserPollAnswer> ApplyContext(this IEnumerable<UserPollAnswer> e, QPDataContext context) {
            foreach (UserPollAnswer item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<UserPollAnswer> Published(this IQueryable<UserPollAnswer> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<UserPollAnswer> ForFrontEnd(this IQueryable<UserPollAnswer> e) {
            return e;
        }

        public static IEnumerable<TrailedAbstractItem> ApplyContext(this IEnumerable<TrailedAbstractItem> e,
                                                                    QPDataContext context) {
            foreach (TrailedAbstractItem item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TrailedAbstractItem> Published(this IQueryable<TrailedAbstractItem> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TrailedAbstractItem> ForFrontEnd(this IQueryable<TrailedAbstractItem> e) {
            return e;
        }

        public static IEnumerable<QPObsoleteUrl> ApplyContext(this IEnumerable<QPObsoleteUrl> e, QPDataContext context) {
            foreach (QPObsoleteUrl item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<QPObsoleteUrl> Published(this IQueryable<QPObsoleteUrl> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<QPObsoleteUrl> ForFrontEnd(this IQueryable<QPObsoleteUrl> e) {
            return e;
        }

        public static IEnumerable<RegionTag> ApplyContext(this IEnumerable<RegionTag> e, QPDataContext context) {
            foreach (RegionTag item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RegionTag> Published(this IQueryable<RegionTag> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RegionTag> ForFrontEnd(this IQueryable<RegionTag> e) {
            return e;
        }

        public static IEnumerable<RegionTagValue> ApplyContext(this IEnumerable<RegionTagValue> e, QPDataContext context) {
            foreach (RegionTagValue item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RegionTagValue> Published(this IQueryable<RegionTagValue> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RegionTagValue> ForFrontEnd(this IQueryable<RegionTagValue> e) {
            return e;
        }

        public static IEnumerable<SearchSuggestion> ApplyContext(this IEnumerable<SearchSuggestion> e,
                                                                 QPDataContext context) {
            foreach (SearchSuggestion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SearchSuggestion> Published(this IQueryable<SearchSuggestion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SearchSuggestion> ForFrontEnd(this IQueryable<SearchSuggestion> e) {
            return e;
        }

        public static IEnumerable<SearchResult> ApplyContext(this IEnumerable<SearchResult> e, QPDataContext context) {
            foreach (SearchResult item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SearchResult> Published(this IQueryable<SearchResult> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SearchResult> ForFrontEnd(this IQueryable<SearchResult> e) {
            return e;
        }

        public static IEnumerable<SiteSection> ApplyContext(this IEnumerable<SiteSection> e, QPDataContext context) {
            foreach (SiteSection item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SiteSection> Published(this IQueryable<SiteSection> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SiteSection> ForFrontEnd(this IQueryable<SiteSection> e) {
            return e;
        }

        public static IEnumerable<NewsCategory> ApplyContext(this IEnumerable<NewsCategory> e, QPDataContext context) {
            foreach (NewsCategory item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<NewsCategory> Published(this IQueryable<NewsCategory> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<NewsCategory> ForFrontEnd(this IQueryable<NewsCategory> e) {
            return e;
        }

        public static IEnumerable<NewsArticle> ApplyContext(this IEnumerable<NewsArticle> e, QPDataContext context) {
            foreach (NewsArticle item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<NewsArticle> Published(this IQueryable<NewsArticle> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<NewsArticle> ForFrontEnd(this IQueryable<NewsArticle> e) {
            return e;
        }

        public static IEnumerable<NotificationTemplate> ApplyContext(this IEnumerable<NotificationTemplate> e,
                                                                     QPDataContext context) {
            foreach (NotificationTemplate item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<NotificationTemplate> Published(this IQueryable<NotificationTemplate> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<NotificationTemplate> ForFrontEnd(this IQueryable<NotificationTemplate> e) {
            return e;
        }

        public static IEnumerable<RoamingTariffZone> ApplyContext(this IEnumerable<RoamingTariffZone> e,
                                                                  QPDataContext context) {
            foreach (RoamingTariffZone item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RoamingTariffZone> Published(this IQueryable<RoamingTariffZone> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RoamingTariffZone> ForFrontEnd(this IQueryable<RoamingTariffZone> e) {
            return e;
        }

        public static IEnumerable<RoamingCountryZone> ApplyContext(this IEnumerable<RoamingCountryZone> e,
                                                                   QPDataContext context) {
            foreach (RoamingCountryZone item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RoamingCountryZone> Published(this IQueryable<RoamingCountryZone> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RoamingCountryZone> ForFrontEnd(this IQueryable<RoamingCountryZone> e) {
            return e;
        }

        public static IEnumerable<UserSubscription> ApplyContext(this IEnumerable<UserSubscription> e,
                                                                 QPDataContext context) {
            foreach (UserSubscription item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<UserSubscription> Published(this IQueryable<UserSubscription> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<UserSubscription> ForFrontEnd(this IQueryable<UserSubscription> e) {
            return e;
        }

        public static IEnumerable<SubscriptionCategory> ApplyContext(this IEnumerable<SubscriptionCategory> e,
                                                                     QPDataContext context) {
            foreach (SubscriptionCategory item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SubscriptionCategory> Published(this IQueryable<SubscriptionCategory> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SubscriptionCategory> ForFrontEnd(this IQueryable<SubscriptionCategory> e) {
            return e;
        }

        public static IEnumerable<ConfirmationRequest> ApplyContext(this IEnumerable<ConfirmationRequest> e,
                                                                    QPDataContext context) {
            foreach (ConfirmationRequest item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ConfirmationRequest> Published(this IQueryable<ConfirmationRequest> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ConfirmationRequest> ForFrontEnd(this IQueryable<ConfirmationRequest> e) {
            return e;
        }

        public static IEnumerable<FeedbackTheme> ApplyContext(this IEnumerable<FeedbackTheme> e, QPDataContext context) {
            foreach (FeedbackTheme item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<FeedbackTheme> Published(this IQueryable<FeedbackTheme> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<FeedbackTheme> ForFrontEnd(this IQueryable<FeedbackTheme> e) {
            return e;
        }

        public static IEnumerable<FeedbackType> ApplyContext(this IEnumerable<FeedbackType> e, QPDataContext context) {
            foreach (FeedbackType item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<FeedbackType> Published(this IQueryable<FeedbackType> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<FeedbackType> ForFrontEnd(this IQueryable<FeedbackType> e) {
            return e;
        }

        public static IEnumerable<FeedbackSubtheme> ApplyContext(this IEnumerable<FeedbackSubtheme> e,
                                                                 QPDataContext context) {
            foreach (FeedbackSubtheme item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<FeedbackSubtheme> Published(this IQueryable<FeedbackSubtheme> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<FeedbackSubtheme> ForFrontEnd(this IQueryable<FeedbackSubtheme> e) {
            return e;
        }

        public static IEnumerable<FeedbackQueue> ApplyContext(this IEnumerable<FeedbackQueue> e, QPDataContext context) {
            foreach (FeedbackQueue item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<FeedbackQueue> Published(this IQueryable<FeedbackQueue> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<FeedbackQueue> ForFrontEnd(this IQueryable<FeedbackQueue> e) {
            return e;
        }

        public static IEnumerable<TVChannel> ApplyContext(this IEnumerable<TVChannel> e, QPDataContext context) {
            foreach (TVChannel item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TVChannel> Published(this IQueryable<TVChannel> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TVChannel> ForFrontEnd(this IQueryable<TVChannel> e) {
            return e;
        }

        public static IEnumerable<RegionFeedbackGroup> ApplyContext(this IEnumerable<RegionFeedbackGroup> e,
                                                                    QPDataContext context) {
            foreach (RegionFeedbackGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RegionFeedbackGroup> Published(this IQueryable<RegionFeedbackGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RegionFeedbackGroup> ForFrontEnd(this IQueryable<RegionFeedbackGroup> e) {
            return e;
        }

        public static IEnumerable<DeviceType> ApplyContext(this IEnumerable<DeviceType> e, QPDataContext context) {
            foreach (DeviceType item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<DeviceType> Published(this IQueryable<DeviceType> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<DeviceType> ForFrontEnd(this IQueryable<DeviceType> e) {
            return e;
        }

        public static IEnumerable<Action> ApplyContext(this IEnumerable<Action> e, QPDataContext context) {
            foreach (Action item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<Action> Published(this IQueryable<Action> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<Action> ForFrontEnd(this IQueryable<Action> e) {
            return e;
        }

        public static IEnumerable<TVPackage> ApplyContext(this IEnumerable<TVPackage> e, QPDataContext context) {
            foreach (TVPackage item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TVPackage> Published(this IQueryable<TVPackage> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TVPackage> ForFrontEnd(this IQueryable<TVPackage> e) {
            return e;
        }

        public static IEnumerable<ExternalRegionMapping> ApplyContext(this IEnumerable<ExternalRegionMapping> e,
                                                                      QPDataContext context) {
            foreach (ExternalRegionMapping item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ExternalRegionMapping> Published(this IQueryable<ExternalRegionMapping> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ExternalRegionMapping> ForFrontEnd(this IQueryable<ExternalRegionMapping> e) {
            return e;
        }

        public static IEnumerable<ExternalRegionSystem> ApplyContext(this IEnumerable<ExternalRegionSystem> e,
                                                                     QPDataContext context) {
            foreach (ExternalRegionSystem item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ExternalRegionSystem> Published(this IQueryable<ExternalRegionSystem> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ExternalRegionSystem> ForFrontEnd(this IQueryable<ExternalRegionSystem> e) {
            return e;
        }

        public static IEnumerable<SiteSetting> ApplyContext(this IEnumerable<SiteSetting> e, QPDataContext context) {
            foreach (SiteSetting item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SiteSetting> Published(this IQueryable<SiteSetting> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SiteSetting> ForFrontEnd(this IQueryable<SiteSetting> e) {
            return e;
        }

        public static IEnumerable<PhoneCode> ApplyContext(this IEnumerable<PhoneCode> e, QPDataContext context) {
            foreach (PhoneCode item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PhoneCode> Published(this IQueryable<PhoneCode> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PhoneCode> ForFrontEnd(this IQueryable<PhoneCode> e) {
            return e;
        }

        public static IEnumerable<QPItemDefinitionConstraint> ApplyContext(
            this IEnumerable<QPItemDefinitionConstraint> e, QPDataContext context) {
            foreach (QPItemDefinitionConstraint item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<QPItemDefinitionConstraint> Published(this IQueryable<QPItemDefinitionConstraint> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<QPItemDefinitionConstraint> ForFrontEnd(this IQueryable<QPItemDefinitionConstraint> e) {
            return e;
        }

        public static IEnumerable<MarketingMobileTariff> ApplyContext(this IEnumerable<MarketingMobileTariff> e,
                                                                      QPDataContext context) {
            foreach (MarketingMobileTariff item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MarketingMobileTariff> Published(this IQueryable<MarketingMobileTariff> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MarketingMobileTariff> ForFrontEnd(this IQueryable<MarketingMobileTariff> e) {
            return e;
        }

        public static IEnumerable<TariffGuideQuestion> ApplyContext(this IEnumerable<TariffGuideQuestion> e,
                                                                    QPDataContext context) {
            foreach (TariffGuideQuestion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TariffGuideQuestion> Published(this IQueryable<TariffGuideQuestion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TariffGuideQuestion> ForFrontEnd(this IQueryable<TariffGuideQuestion> e) {
            return e;
        }

        public static IEnumerable<TariffGuideAnswer> ApplyContext(this IEnumerable<TariffGuideAnswer> e,
                                                                  QPDataContext context) {
            foreach (TariffGuideAnswer item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TariffGuideAnswer> Published(this IQueryable<TariffGuideAnswer> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TariffGuideAnswer> ForFrontEnd(this IQueryable<TariffGuideAnswer> e) {
            return e;
        }

        public static IEnumerable<TariffGuideResult> ApplyContext(this IEnumerable<TariffGuideResult> e,
                                                                  QPDataContext context) {
            foreach (TariffGuideResult item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TariffGuideResult> Published(this IQueryable<TariffGuideResult> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TariffGuideResult> ForFrontEnd(this IQueryable<TariffGuideResult> e) {
            return e;
        }

        public static IEnumerable<MobileTariffParameterGroup> ApplyContext(
            this IEnumerable<MobileTariffParameterGroup> e, QPDataContext context) {
            foreach (MobileTariffParameterGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MobileTariffParameterGroup> Published(this IQueryable<MobileTariffParameterGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MobileTariffParameterGroup> ForFrontEnd(this IQueryable<MobileTariffParameterGroup> e) {
            return e;
        }

        public static IEnumerable<MarketingSign> ApplyContext(this IEnumerable<MarketingSign> e, QPDataContext context) {
            foreach (MarketingSign item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MarketingSign> Published(this IQueryable<MarketingSign> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MarketingSign> ForFrontEnd(this IQueryable<MarketingSign> e) {
            return e;
        }

        public static IEnumerable<MarketingMobileService> ApplyContext(this IEnumerable<MarketingMobileService> e,
                                                                       QPDataContext context) {
            foreach (MarketingMobileService item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MarketingMobileService> Published(this IQueryable<MarketingMobileService> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MarketingMobileService> ForFrontEnd(this IQueryable<MarketingMobileService> e) {
            return e;
        }

        public static IEnumerable<SubscriptionFeeType> ApplyContext(this IEnumerable<SubscriptionFeeType> e,
                                                                    QPDataContext context) {
            foreach (SubscriptionFeeType item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SubscriptionFeeType> Published(this IQueryable<SubscriptionFeeType> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SubscriptionFeeType> ForFrontEnd(this IQueryable<SubscriptionFeeType> e) {
            return e;
        }

        public static IEnumerable<MobileServiceParameterGroup> ApplyContext(
            this IEnumerable<MobileServiceParameterGroup> e, QPDataContext context) {
            foreach (MobileServiceParameterGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MobileServiceParameterGroup> Published(this IQueryable<MobileServiceParameterGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MobileServiceParameterGroup> ForFrontEnd(this IQueryable<MobileServiceParameterGroup> e) {
            return e;
        }

        public static IEnumerable<MobileTariffFamily> ApplyContext(this IEnumerable<MobileTariffFamily> e,
                                                                   QPDataContext context) {
            foreach (MobileTariffFamily item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MobileTariffFamily> Published(this IQueryable<MobileTariffFamily> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MobileTariffFamily> ForFrontEnd(this IQueryable<MobileTariffFamily> e) {
            return e;
        }

        public static IEnumerable<MutualMobileServiceGroup> ApplyContext(this IEnumerable<MutualMobileServiceGroup> e,
                                                                         QPDataContext context) {
            foreach (MutualMobileServiceGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MutualMobileServiceGroup> Published(this IQueryable<MutualMobileServiceGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MutualMobileServiceGroup> ForFrontEnd(this IQueryable<MutualMobileServiceGroup> e) {
            return e;
        }

        public static IEnumerable<MobileServiceFamily> ApplyContext(this IEnumerable<MobileServiceFamily> e,
                                                                    QPDataContext context) {
            foreach (MobileServiceFamily item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MobileServiceFamily> Published(this IQueryable<MobileServiceFamily> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MobileServiceFamily> ForFrontEnd(this IQueryable<MobileServiceFamily> e) {
            return e;
        }

        public static IEnumerable<MobileTariffCategory> ApplyContext(this IEnumerable<MobileTariffCategory> e,
                                                                     QPDataContext context) {
            foreach (MobileTariffCategory item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MobileTariffCategory> Published(this IQueryable<MobileTariffCategory> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MobileTariffCategory> ForFrontEnd(this IQueryable<MobileTariffCategory> e) {
            return e;
        }

        public static IEnumerable<MobileServiceCategory> ApplyContext(this IEnumerable<MobileServiceCategory> e,
                                                                      QPDataContext context) {
            foreach (MobileServiceCategory item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MobileServiceCategory> Published(this IQueryable<MobileServiceCategory> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MobileServiceCategory> ForFrontEnd(this IQueryable<MobileServiceCategory> e) {
            return e;
        }

        public static IEnumerable<ArchiveTariff> ApplyContext(this IEnumerable<ArchiveTariff> e, QPDataContext context) {
            foreach (ArchiveTariff item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ArchiveTariff> Published(this IQueryable<ArchiveTariff> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ArchiveTariff> ForFrontEnd(this IQueryable<ArchiveTariff> e) {
            return e;
        }

        public static IEnumerable<ArchiveTariffTab> ApplyContext(this IEnumerable<ArchiveTariffTab> e,
                                                                 QPDataContext context) {
            foreach (ArchiveTariffTab item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ArchiveTariffTab> Published(this IQueryable<ArchiveTariffTab> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ArchiveTariffTab> ForFrontEnd(this IQueryable<ArchiveTariffTab> e) {
            return e;
        }

        public static IEnumerable<MobileServiceTab> ApplyContext(this IEnumerable<MobileServiceTab> e,
                                                                 QPDataContext context) {
            foreach (MobileServiceTab item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MobileServiceTab> Published(this IQueryable<MobileServiceTab> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MobileServiceTab> ForFrontEnd(this IQueryable<MobileServiceTab> e) {
            return e;
        }

        public static IEnumerable<MobileParamsGroupTab> ApplyContext(this IEnumerable<MobileParamsGroupTab> e,
                                                                     QPDataContext context) {
            foreach (MobileParamsGroupTab item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MobileParamsGroupTab> Published(this IQueryable<MobileParamsGroupTab> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MobileParamsGroupTab> ForFrontEnd(this IQueryable<MobileParamsGroupTab> e) {
            return e;
        }

        public static IEnumerable<TariffParamGroupPriority> ApplyContext(this IEnumerable<TariffParamGroupPriority> e,
                                                                         QPDataContext context) {
            foreach (TariffParamGroupPriority item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TariffParamGroupPriority> Published(this IQueryable<TariffParamGroupPriority> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TariffParamGroupPriority> ForFrontEnd(this IQueryable<TariffParamGroupPriority> e) {
            return e;
        }

        public static IEnumerable<ArchiveService> ApplyContext(this IEnumerable<ArchiveService> e, QPDataContext context) {
            foreach (ArchiveService item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ArchiveService> Published(this IQueryable<ArchiveService> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ArchiveService> ForFrontEnd(this IQueryable<ArchiveService> e) {
            return e;
        }

        public static IEnumerable<ArchiveMobileServiceBookmark> ApplyContext(
            this IEnumerable<ArchiveMobileServiceBookmark> e, QPDataContext context) {
            foreach (ArchiveMobileServiceBookmark item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ArchiveMobileServiceBookmark> Published(this IQueryable<ArchiveMobileServiceBookmark> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ArchiveMobileServiceBookmark> ForFrontEnd(
            this IQueryable<ArchiveMobileServiceBookmark> e) {
            return e;
        }

        public static IEnumerable<PrivelegeAndBonus> ApplyContext(this IEnumerable<PrivelegeAndBonus> e,
                                                                  QPDataContext context) {
            foreach (PrivelegeAndBonus item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PrivelegeAndBonus> Published(this IQueryable<PrivelegeAndBonus> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PrivelegeAndBonus> ForFrontEnd(this IQueryable<PrivelegeAndBonus> e) {
            return e;
        }

        public static IEnumerable<FaqContent> ApplyContext(this IEnumerable<FaqContent> e, QPDataContext context) {
            foreach (FaqContent item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<FaqContent> Published(this IQueryable<FaqContent> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<FaqContent> ForFrontEnd(this IQueryable<FaqContent> e) {
            return e;
        }

        public static IEnumerable<FaqGroup> ApplyContext(this IEnumerable<FaqGroup> e, QPDataContext context) {
            foreach (FaqGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<FaqGroup> Published(this IQueryable<FaqGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<FaqGroup> ForFrontEnd(this IQueryable<FaqGroup> e) {
            return e;
        }

        public static IEnumerable<SiteText> ApplyContext(this IEnumerable<SiteText> e, QPDataContext context) {
            foreach (SiteText item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SiteText> Published(this IQueryable<SiteText> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SiteText> ForFrontEnd(this IQueryable<SiteText> e) {
            return e;
        }

        public static IEnumerable<TVChannelTheme> ApplyContext(this IEnumerable<TVChannelTheme> e, QPDataContext context) {
            foreach (TVChannelTheme item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TVChannelTheme> Published(this IQueryable<TVChannelTheme> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TVChannelTheme> ForFrontEnd(this IQueryable<TVChannelTheme> e) {
            return e;
        }

        public static IEnumerable<MarketingTVPackage> ApplyContext(this IEnumerable<MarketingTVPackage> e,
                                                                   QPDataContext context) {
            foreach (MarketingTVPackage item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MarketingTVPackage> Published(this IQueryable<MarketingTVPackage> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MarketingTVPackage> ForFrontEnd(this IQueryable<MarketingTVPackage> e) {
            return e;
        }

        public static IEnumerable<QuickLinksTitle> ApplyContext(this IEnumerable<QuickLinksTitle> e,
                                                                QPDataContext context) {
            foreach (QuickLinksTitle item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<QuickLinksTitle> Published(this IQueryable<QuickLinksTitle> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<QuickLinksTitle> ForFrontEnd(this IQueryable<QuickLinksTitle> e) {
            return e;
        }

        public static IEnumerable<QuickLinksGroup> ApplyContext(this IEnumerable<QuickLinksGroup> e,
                                                                QPDataContext context) {
            foreach (QuickLinksGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<QuickLinksGroup> Published(this IQueryable<QuickLinksGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<QuickLinksGroup> ForFrontEnd(this IQueryable<QuickLinksGroup> e) {
            return e;
        }

        public static IEnumerable<ProvodTariffParameterGroup> ApplyContext(
            this IEnumerable<ProvodTariffParameterGroup> e, QPDataContext context) {
            foreach (ProvodTariffParameterGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ProvodTariffParameterGroup> Published(this IQueryable<ProvodTariffParameterGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ProvodTariffParameterGroup> ForFrontEnd(this IQueryable<ProvodTariffParameterGroup> e) {
            return e;
        }

        public static IEnumerable<InternetTariffFamily> ApplyContext(this IEnumerable<InternetTariffFamily> e,
                                                                     QPDataContext context) {
            foreach (InternetTariffFamily item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<InternetTariffFamily> Published(this IQueryable<InternetTariffFamily> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<InternetTariffFamily> ForFrontEnd(this IQueryable<InternetTariffFamily> e) {
            return e;
        }

        public static IEnumerable<PhoneTariffFamily> ApplyContext(this IEnumerable<PhoneTariffFamily> e,
                                                                  QPDataContext context) {
            foreach (PhoneTariffFamily item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PhoneTariffFamily> Published(this IQueryable<PhoneTariffFamily> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PhoneTariffFamily> ForFrontEnd(this IQueryable<PhoneTariffFamily> e) {
            return e;
        }

        public static IEnumerable<ProvodServiceFamily> ApplyContext(this IEnumerable<ProvodServiceFamily> e,
                                                                    QPDataContext context) {
            foreach (ProvodServiceFamily item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ProvodServiceFamily> Published(this IQueryable<ProvodServiceFamily> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ProvodServiceFamily> ForFrontEnd(this IQueryable<ProvodServiceFamily> e) {
            return e;
        }

        public static IEnumerable<ProvodServiceParameterGroup> ApplyContext(
            this IEnumerable<ProvodServiceParameterGroup> e, QPDataContext context) {
            foreach (ProvodServiceParameterGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ProvodServiceParameterGroup> Published(this IQueryable<ProvodServiceParameterGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ProvodServiceParameterGroup> ForFrontEnd(this IQueryable<ProvodServiceParameterGroup> e) {
            return e;
        }

        public static IEnumerable<InternetTariffCategory> ApplyContext(this IEnumerable<InternetTariffCategory> e,
                                                                       QPDataContext context) {
            foreach (InternetTariffCategory item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<InternetTariffCategory> Published(this IQueryable<InternetTariffCategory> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<InternetTariffCategory> ForFrontEnd(this IQueryable<InternetTariffCategory> e) {
            return e;
        }

        public static IEnumerable<PhoneTariffCategory> ApplyContext(this IEnumerable<PhoneTariffCategory> e,
                                                                    QPDataContext context) {
            foreach (PhoneTariffCategory item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PhoneTariffCategory> Published(this IQueryable<PhoneTariffCategory> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PhoneTariffCategory> ForFrontEnd(this IQueryable<PhoneTariffCategory> e) {
            return e;
        }

        public static IEnumerable<ProvodServiceCategory> ApplyContext(this IEnumerable<ProvodServiceCategory> e,
                                                                      QPDataContext context) {
            foreach (ProvodServiceCategory item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ProvodServiceCategory> Published(this IQueryable<ProvodServiceCategory> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ProvodServiceCategory> ForFrontEnd(this IQueryable<ProvodServiceCategory> e) {
            return e;
        }

        public static IEnumerable<MarketingInternetTariff> ApplyContext(this IEnumerable<MarketingInternetTariff> e,
                                                                        QPDataContext context) {
            foreach (MarketingInternetTariff item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MarketingInternetTariff> Published(this IQueryable<MarketingInternetTariff> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MarketingInternetTariff> ForFrontEnd(this IQueryable<MarketingInternetTariff> e) {
            return e;
        }

        public static IEnumerable<MarketingPhoneTariff> ApplyContext(this IEnumerable<MarketingPhoneTariff> e,
                                                                     QPDataContext context) {
            foreach (MarketingPhoneTariff item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MarketingPhoneTariff> Published(this IQueryable<MarketingPhoneTariff> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MarketingPhoneTariff> ForFrontEnd(this IQueryable<MarketingPhoneTariff> e) {
            return e;
        }

        public static IEnumerable<MarketingProvodService> ApplyContext(this IEnumerable<MarketingProvodService> e,
                                                                       QPDataContext context) {
            foreach (MarketingProvodService item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MarketingProvodService> Published(this IQueryable<MarketingProvodService> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MarketingProvodService> ForFrontEnd(this IQueryable<MarketingProvodService> e) {
            return e;
        }

        public static IEnumerable<ServiceForIternetTariff> ApplyContext(this IEnumerable<ServiceForIternetTariff> e,
                                                                        QPDataContext context) {
            foreach (ServiceForIternetTariff item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ServiceForIternetTariff> Published(this IQueryable<ServiceForIternetTariff> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ServiceForIternetTariff> ForFrontEnd(this IQueryable<ServiceForIternetTariff> e) {
            return e;
        }

        public static IEnumerable<ServiceForPhoneTariff> ApplyContext(this IEnumerable<ServiceForPhoneTariff> e,
                                                                      QPDataContext context) {
            foreach (ServiceForPhoneTariff item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ServiceForPhoneTariff> Published(this IQueryable<ServiceForPhoneTariff> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ServiceForPhoneTariff> ForFrontEnd(this IQueryable<ServiceForPhoneTariff> e) {
            return e;
        }

        public static IEnumerable<ProvodKitFamily> ApplyContext(this IEnumerable<ProvodKitFamily> e,
                                                                QPDataContext context) {
            foreach (ProvodKitFamily item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ProvodKitFamily> Published(this IQueryable<ProvodKitFamily> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ProvodKitFamily> ForFrontEnd(this IQueryable<ProvodKitFamily> e) {
            return e;
        }

        public static IEnumerable<ProvodKitCategory> ApplyContext(this IEnumerable<ProvodKitCategory> e,
                                                                  QPDataContext context) {
            foreach (ProvodKitCategory item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ProvodKitCategory> Published(this IQueryable<ProvodKitCategory> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ProvodKitCategory> ForFrontEnd(this IQueryable<ProvodKitCategory> e) {
            return e;
        }

        public static IEnumerable<MarketingProvodKit> ApplyContext(this IEnumerable<MarketingProvodKit> e,
                                                                   QPDataContext context) {
            foreach (MarketingProvodKit item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MarketingProvodKit> Published(this IQueryable<MarketingProvodKit> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MarketingProvodKit> ForFrontEnd(this IQueryable<MarketingProvodKit> e) {
            return e;
        }

        public static IEnumerable<ProvodKit> ApplyContext(this IEnumerable<ProvodKit> e, QPDataContext context) {
            foreach (ProvodKit item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ProvodKit> Published(this IQueryable<ProvodKit> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ProvodKit> ForFrontEnd(this IQueryable<ProvodKit> e) {
            return e;
        }

        public static IEnumerable<LocalRoamingOperator> ApplyContext(this IEnumerable<LocalRoamingOperator> e,
                                                                     QPDataContext context) {
            foreach (LocalRoamingOperator item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<LocalRoamingOperator> Published(this IQueryable<LocalRoamingOperator> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<LocalRoamingOperator> ForFrontEnd(this IQueryable<LocalRoamingOperator> e) {
            return e;
        }

        public static IEnumerable<RoamingTariffParam> ApplyContext(this IEnumerable<RoamingTariffParam> e,
                                                                   QPDataContext context) {
            foreach (RoamingTariffParam item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RoamingTariffParam> Published(this IQueryable<RoamingTariffParam> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RoamingTariffParam> ForFrontEnd(this IQueryable<RoamingTariffParam> e) {
            return e;
        }

        public static IEnumerable<TVPackageFamily> ApplyContext(this IEnumerable<TVPackageFamily> e,
                                                                QPDataContext context) {
            foreach (TVPackageFamily item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TVPackageFamily> Published(this IQueryable<TVPackageFamily> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TVPackageFamily> ForFrontEnd(this IQueryable<TVPackageFamily> e) {
            return e;
        }

        public static IEnumerable<SocialNetwork> ApplyContext(this IEnumerable<SocialNetwork> e, QPDataContext context) {
            foreach (SocialNetwork item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SocialNetwork> Published(this IQueryable<SocialNetwork> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SocialNetwork> ForFrontEnd(this IQueryable<SocialNetwork> e) {
            return e;
        }

        public static IEnumerable<HelpDeviceType> ApplyContext(this IEnumerable<HelpDeviceType> e, QPDataContext context) {
            foreach (HelpDeviceType item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<HelpDeviceType> Published(this IQueryable<HelpDeviceType> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<HelpDeviceType> ForFrontEnd(this IQueryable<HelpDeviceType> e) {
            return e;
        }

        public static IEnumerable<HelpCenterParam> ApplyContext(this IEnumerable<HelpCenterParam> e,
                                                                QPDataContext context) {
            foreach (HelpCenterParam item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<HelpCenterParam> Published(this IQueryable<HelpCenterParam> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<HelpCenterParam> ForFrontEnd(this IQueryable<HelpCenterParam> e) {
            return e;
        }

        public static IEnumerable<TVChannelRegion> ApplyContext(this IEnumerable<TVChannelRegion> e,
                                                                QPDataContext context) {
            foreach (TVChannelRegion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TVChannelRegion> Published(this IQueryable<TVChannelRegion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TVChannelRegion> ForFrontEnd(this IQueryable<TVChannelRegion> e) {
            return e;
        }

        public static IEnumerable<PhoneAsModemTab> ApplyContext(this IEnumerable<PhoneAsModemTab> e,
                                                                QPDataContext context) {
            foreach (PhoneAsModemTab item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PhoneAsModemTab> Published(this IQueryable<PhoneAsModemTab> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PhoneAsModemTab> ForFrontEnd(this IQueryable<PhoneAsModemTab> e) {
            return e;
        }

        public static IEnumerable<PhoneAsModemInterface> ApplyContext(this IEnumerable<PhoneAsModemInterface> e,
                                                                      QPDataContext context) {
            foreach (PhoneAsModemInterface item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PhoneAsModemInterface> Published(this IQueryable<PhoneAsModemInterface> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PhoneAsModemInterface> ForFrontEnd(this IQueryable<PhoneAsModemInterface> e) {
            return e;
        }

        public static IEnumerable<OperatingSystem> ApplyContext(this IEnumerable<OperatingSystem> e,
                                                                QPDataContext context) {
            foreach (OperatingSystem item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<OperatingSystem> Published(this IQueryable<OperatingSystem> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<OperatingSystem> ForFrontEnd(this IQueryable<OperatingSystem> e) {
            return e;
        }

        public static IEnumerable<CpaPartner> ApplyContext(this IEnumerable<CpaPartner> e, QPDataContext context) {
            foreach (CpaPartner item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<CpaPartner> Published(this IQueryable<CpaPartner> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<CpaPartner> ForFrontEnd(this IQueryable<CpaPartner> e) {
            return e;
        }

        public static IEnumerable<PhoneAsModemInstruction> ApplyContext(this IEnumerable<PhoneAsModemInstruction> e,
                                                                        QPDataContext context) {
            foreach (PhoneAsModemInstruction item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PhoneAsModemInstruction> Published(this IQueryable<PhoneAsModemInstruction> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PhoneAsModemInstruction> ForFrontEnd(this IQueryable<PhoneAsModemInstruction> e) {
            return e;
        }

        public static IEnumerable<CpaShortNumber> ApplyContext(this IEnumerable<CpaShortNumber> e, QPDataContext context) {
            foreach (CpaShortNumber item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<CpaShortNumber> Published(this IQueryable<CpaShortNumber> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<CpaShortNumber> ForFrontEnd(this IQueryable<CpaShortNumber> e) {
            return e;
        }

        public static IEnumerable<ChangeNumberErrorText> ApplyContext(this IEnumerable<ChangeNumberErrorText> e,
                                                                      QPDataContext context) {
            foreach (ChangeNumberErrorText item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ChangeNumberErrorText> Published(this IQueryable<ChangeNumberErrorText> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ChangeNumberErrorText> ForFrontEnd(this IQueryable<ChangeNumberErrorText> e) {
            return e;
        }

        public static IEnumerable<EquipmentParamsTab> ApplyContext(this IEnumerable<EquipmentParamsTab> e,
                                                                   QPDataContext context) {
            foreach (EquipmentParamsTab item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<EquipmentParamsTab> Published(this IQueryable<EquipmentParamsTab> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<EquipmentParamsTab> ForFrontEnd(this IQueryable<EquipmentParamsTab> e) {
            return e;
        }

        public static IEnumerable<EquipmentTab> ApplyContext(this IEnumerable<EquipmentTab> e, QPDataContext context) {
            foreach (EquipmentTab item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<EquipmentTab> Published(this IQueryable<EquipmentTab> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<EquipmentTab> ForFrontEnd(this IQueryable<EquipmentTab> e) {
            return e;
        }

        public static IEnumerable<EquipmentType> ApplyContext(this IEnumerable<EquipmentType> e, QPDataContext context) {
            foreach (EquipmentType item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<EquipmentType> Published(this IQueryable<EquipmentType> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<EquipmentType> ForFrontEnd(this IQueryable<EquipmentType> e) {
            return e;
        }

        public static IEnumerable<EquipmentCategory> ApplyContext(this IEnumerable<EquipmentCategory> e,
                                                                  QPDataContext context) {
            foreach (EquipmentCategory item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<EquipmentCategory> Published(this IQueryable<EquipmentCategory> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<EquipmentCategory> ForFrontEnd(this IQueryable<EquipmentCategory> e) {
            return e;
        }

        public static IEnumerable<EquipmentParam> ApplyContext(this IEnumerable<EquipmentParam> e, QPDataContext context) {
            foreach (EquipmentParam item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<EquipmentParam> Published(this IQueryable<EquipmentParam> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<EquipmentParam> ForFrontEnd(this IQueryable<EquipmentParam> e) {
            return e;
        }

        public static IEnumerable<EquipmentParamsGroup> ApplyContext(this IEnumerable<EquipmentParamsGroup> e,
                                                                     QPDataContext context) {
            foreach (EquipmentParamsGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<EquipmentParamsGroup> Published(this IQueryable<EquipmentParamsGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<EquipmentParamsGroup> ForFrontEnd(this IQueryable<EquipmentParamsGroup> e) {
            return e;
        }

        public static IEnumerable<Equipment> ApplyContext(this IEnumerable<Equipment> e, QPDataContext context) {
            foreach (Equipment item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<Equipment> Published(this IQueryable<Equipment> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<Equipment> ForFrontEnd(this IQueryable<Equipment> e) {
            return e;
        }

        public static IEnumerable<MarketingEquipment> ApplyContext(this IEnumerable<MarketingEquipment> e,
                                                                   QPDataContext context) {
            foreach (MarketingEquipment item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MarketingEquipment> Published(this IQueryable<MarketingEquipment> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MarketingEquipment> ForFrontEnd(this IQueryable<MarketingEquipment> e) {
            return e;
        }

        public static IEnumerable<EquipmentImage> ApplyContext(this IEnumerable<EquipmentImage> e, QPDataContext context) {
            foreach (EquipmentImage item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<EquipmentImage> Published(this IQueryable<EquipmentImage> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<EquipmentImage> ForFrontEnd(this IQueryable<EquipmentImage> e) {
            return e;
        }

        public static IEnumerable<PaymentServiceFilter> ApplyContext(this IEnumerable<PaymentServiceFilter> e,
                                                                     QPDataContext context) {
            foreach (PaymentServiceFilter item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PaymentServiceFilter> Published(this IQueryable<PaymentServiceFilter> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PaymentServiceFilter> ForFrontEnd(this IQueryable<PaymentServiceFilter> e) {
            return e;
        }

        public static IEnumerable<INACParamType> ApplyContext(this IEnumerable<INACParamType> e, QPDataContext context) {
            foreach (INACParamType item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<INACParamType> Published(this IQueryable<INACParamType> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<INACParamType> ForFrontEnd(this IQueryable<INACParamType> e) {
            return e;
        }

        public static IEnumerable<InternetTariff> ApplyContext(this IEnumerable<InternetTariff> e, QPDataContext context) {
            foreach (InternetTariff item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<InternetTariff> Published(this IQueryable<InternetTariff> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<InternetTariff> ForFrontEnd(this IQueryable<InternetTariff> e) {
            return e;
        }

        public static IEnumerable<InternetTariffParam> ApplyContext(this IEnumerable<InternetTariffParam> e,
                                                                    QPDataContext context) {
            foreach (InternetTariffParam item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<InternetTariffParam> Published(this IQueryable<InternetTariffParam> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<InternetTariffParam> ForFrontEnd(this IQueryable<InternetTariffParam> e) {
            return e;
        }

        public static IEnumerable<PhoneTariff> ApplyContext(this IEnumerable<PhoneTariff> e, QPDataContext context) {
            foreach (PhoneTariff item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PhoneTariff> Published(this IQueryable<PhoneTariff> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PhoneTariff> ForFrontEnd(this IQueryable<PhoneTariff> e) {
            return e;
        }

        public static IEnumerable<PhoneTariffParam> ApplyContext(this IEnumerable<PhoneTariffParam> e,
                                                                 QPDataContext context) {
            foreach (PhoneTariffParam item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<PhoneTariffParam> Published(this IQueryable<PhoneTariffParam> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<PhoneTariffParam> ForFrontEnd(this IQueryable<PhoneTariffParam> e) {
            return e;
        }

        public static IEnumerable<DiagnoseItem> ApplyContext(this IEnumerable<DiagnoseItem> e, QPDataContext context) {
            foreach (DiagnoseItem item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<DiagnoseItem> Published(this IQueryable<DiagnoseItem> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<DiagnoseItem> ForFrontEnd(this IQueryable<DiagnoseItem> e) {
            return e;
        }

        public static IEnumerable<MutualTVPackageGroup> ApplyContext(this IEnumerable<MutualTVPackageGroup> e,
                                                                     QPDataContext context) {
            foreach (MutualTVPackageGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MutualTVPackageGroup> Published(this IQueryable<MutualTVPackageGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MutualTVPackageGroup> ForFrontEnd(this IQueryable<MutualTVPackageGroup> e) {
            return e;
        }

        public static IEnumerable<TVPackageCategory> ApplyContext(this IEnumerable<TVPackageCategory> e,
                                                                  QPDataContext context) {
            foreach (TVPackageCategory item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TVPackageCategory> Published(this IQueryable<TVPackageCategory> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TVPackageCategory> ForFrontEnd(this IQueryable<TVPackageCategory> e) {
            return e;
        }

        public static IEnumerable<ProvodServiceParamTab> ApplyContext(this IEnumerable<ProvodServiceParamTab> e,
                                                                      QPDataContext context) {
            foreach (ProvodServiceParamTab item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ProvodServiceParamTab> Published(this IQueryable<ProvodServiceParamTab> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ProvodServiceParamTab> ForFrontEnd(this IQueryable<ProvodServiceParamTab> e) {
            return e;
        }

        public static IEnumerable<ParamTabInProvodService> ApplyContext(this IEnumerable<ParamTabInProvodService> e,
                                                                        QPDataContext context) {
            foreach (ParamTabInProvodService item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ParamTabInProvodService> Published(this IQueryable<ParamTabInProvodService> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ParamTabInProvodService> ForFrontEnd(this IQueryable<ParamTabInProvodService> e) {
            return e;
        }

        public static IEnumerable<ProvodServiceParam> ApplyContext(this IEnumerable<ProvodServiceParam> e,
                                                                   QPDataContext context) {
            foreach (ProvodServiceParam item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ProvodServiceParam> Published(this IQueryable<ProvodServiceParam> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ProvodServiceParam> ForFrontEnd(this IQueryable<ProvodServiceParam> e) {
            return e;
        }

        public static IEnumerable<DiagnoseStatisticItem> ApplyContext(this IEnumerable<DiagnoseStatisticItem> e,
                                                                      QPDataContext context) {
            foreach (DiagnoseStatisticItem item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<DiagnoseStatisticItem> Published(this IQueryable<DiagnoseStatisticItem> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<DiagnoseStatisticItem> ForFrontEnd(this IQueryable<DiagnoseStatisticItem> e) {
            return e;
        }

        public static IEnumerable<RoamingCountryGroup> ApplyContext(this IEnumerable<RoamingCountryGroup> e,
                                                                    QPDataContext context) {
            foreach (RoamingCountryGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RoamingCountryGroup> Published(this IQueryable<RoamingCountryGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RoamingCountryGroup> ForFrontEnd(this IQueryable<RoamingCountryGroup> e) {
            return e;
        }

        public static IEnumerable<RoamingRegionFriendGroup> ApplyContext(this IEnumerable<RoamingRegionFriendGroup> e,
                                                                         QPDataContext context) {
            foreach (RoamingRegionFriendGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RoamingRegionFriendGroup> Published(this IQueryable<RoamingRegionFriendGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RoamingRegionFriendGroup> ForFrontEnd(this IQueryable<RoamingRegionFriendGroup> e) {
            return e;
        }

        public static IEnumerable<SukkRegionFriend> ApplyContext(this IEnumerable<SukkRegionFriend> e,
                                                                 QPDataContext context) {
            foreach (SukkRegionFriend item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SukkRegionFriend> Published(this IQueryable<SukkRegionFriend> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SukkRegionFriend> ForFrontEnd(this IQueryable<SukkRegionFriend> e) {
            return e;
        }

        public static IEnumerable<SukkCountryInGroup> ApplyContext(this IEnumerable<SukkCountryInGroup> e,
                                                                   QPDataContext context) {
            foreach (SukkCountryInGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SukkCountryInGroup> Published(this IQueryable<SukkCountryInGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SukkCountryInGroup> ForFrontEnd(this IQueryable<SukkCountryInGroup> e) {
            return e;
        }

        public static IEnumerable<SukkToMarketingRegion> ApplyContext(this IEnumerable<SukkToMarketingRegion> e,
                                                                      QPDataContext context) {
            foreach (SukkToMarketingRegion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SukkToMarketingRegion> Published(this IQueryable<SukkToMarketingRegion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SukkToMarketingRegion> ForFrontEnd(this IQueryable<SukkToMarketingRegion> e) {
            return e;
        }

        public static IEnumerable<SetupConnectionTab> ApplyContext(this IEnumerable<SetupConnectionTab> e,
                                                                   QPDataContext context) {
            foreach (SetupConnectionTab item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SetupConnectionTab> Published(this IQueryable<SetupConnectionTab> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SetupConnectionTab> ForFrontEnd(this IQueryable<SetupConnectionTab> e) {
            return e;
        }

        public static IEnumerable<SetupConnectionGoal> ApplyContext(this IEnumerable<SetupConnectionGoal> e,
                                                                    QPDataContext context) {
            foreach (SetupConnectionGoal item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SetupConnectionGoal> Published(this IQueryable<SetupConnectionGoal> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SetupConnectionGoal> ForFrontEnd(this IQueryable<SetupConnectionGoal> e) {
            return e;
        }

        public static IEnumerable<SetupConnectionInstruction> ApplyContext(
            this IEnumerable<SetupConnectionInstruction> e, QPDataContext context) {
            foreach (SetupConnectionInstruction item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SetupConnectionInstruction> Published(this IQueryable<SetupConnectionInstruction> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SetupConnectionInstruction> ForFrontEnd(this IQueryable<SetupConnectionInstruction> e) {
            return e;
        }

        public static IEnumerable<ProvodService> ApplyContext(this IEnumerable<ProvodService> e, QPDataContext context) {
            foreach (ProvodService item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ProvodService> Published(this IQueryable<ProvodService> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ProvodService> ForFrontEnd(this IQueryable<ProvodService> e) {
            return e;
        }

        public static IEnumerable<WordForm> ApplyContext(this IEnumerable<WordForm> e, QPDataContext context) {
            foreach (WordForm item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<WordForm> Published(this IQueryable<WordForm> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<WordForm> ForFrontEnd(this IQueryable<WordForm> e) {
            return e;
        }

        public static IEnumerable<ServiceForTVPackage> ApplyContext(this IEnumerable<ServiceForTVPackage> e,
                                                                    QPDataContext context) {
            foreach (ServiceForTVPackage item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ServiceForTVPackage> Published(this IQueryable<ServiceForTVPackage> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ServiceForTVPackage> ForFrontEnd(this IQueryable<ServiceForTVPackage> e) {
            return e;
        }

        public static IEnumerable<QP_TrusteePayment> ApplyContext(this IEnumerable<QP_TrusteePayment> e,
                                                                  QPDataContext context) {
            foreach (QP_TrusteePayment item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<QP_TrusteePayment> Published(this IQueryable<QP_TrusteePayment> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<QP_TrusteePayment> ForFrontEnd(this IQueryable<QP_TrusteePayment> e) {
            return e;
        }

        public static IEnumerable<QP_TrusteePaymentTab> ApplyContext(this IEnumerable<QP_TrusteePaymentTab> e,
                                                                     QPDataContext context) {
            foreach (QP_TrusteePaymentTab item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<QP_TrusteePaymentTab> Published(this IQueryable<QP_TrusteePaymentTab> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<QP_TrusteePaymentTab> ForFrontEnd(this IQueryable<QP_TrusteePaymentTab> e) {
            return e;
        }

        public static IEnumerable<SearchAnnouncement> ApplyContext(this IEnumerable<SearchAnnouncement> e,
                                                                   QPDataContext context) {
            foreach (SearchAnnouncement item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SearchAnnouncement> Published(this IQueryable<SearchAnnouncement> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SearchAnnouncement> ForFrontEnd(this IQueryable<SearchAnnouncement> e) {
            return e;
        }

        public static IEnumerable<WarrantyService> ApplyContext(this IEnumerable<WarrantyService> e,
                                                                QPDataContext context) {
            foreach (WarrantyService item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<WarrantyService> Published(this IQueryable<WarrantyService> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<WarrantyService> ForFrontEnd(this IQueryable<WarrantyService> e) {
            return e;
        }

        public static IEnumerable<SKADType> ApplyContext(this IEnumerable<SKADType> e, QPDataContext context) {
            foreach (SKADType item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SKADType> Published(this IQueryable<SKADType> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SKADType> ForFrontEnd(this IQueryable<SKADType> e) {
            return e;
        }

        public static IEnumerable<SKADService> ApplyContext(this IEnumerable<SKADService> e, QPDataContext context) {
            foreach (SKADService item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SKADService> Published(this IQueryable<SKADService> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SKADService> ForFrontEnd(this IQueryable<SKADService> e) {
            return e;
        }

        public static IEnumerable<SKADSpeciality> ApplyContext(this IEnumerable<SKADSpeciality> e, QPDataContext context) {
            foreach (SKADSpeciality item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SKADSpeciality> Published(this IQueryable<SKADSpeciality> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SKADSpeciality> ForFrontEnd(this IQueryable<SKADSpeciality> e) {
            return e;
        }

        public static IEnumerable<SalesPointParameter> ApplyContext(this IEnumerable<SalesPointParameter> e,
                                                                    QPDataContext context) {
            foreach (SalesPointParameter item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SalesPointParameter> Published(this IQueryable<SalesPointParameter> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SalesPointParameter> ForFrontEnd(this IQueryable<SalesPointParameter> e) {
            return e;
        }

        public static IEnumerable<SalesPointType> ApplyContext(this IEnumerable<SalesPointType> e, QPDataContext context) {
            foreach (SalesPointType item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SalesPointType> Published(this IQueryable<SalesPointType> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SalesPointType> ForFrontEnd(this IQueryable<SalesPointType> e) {
            return e;
        }

        public static IEnumerable<SalesPointSpeciality> ApplyContext(this IEnumerable<SalesPointSpeciality> e,
                                                                     QPDataContext context) {
            foreach (SalesPointSpeciality item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SalesPointSpeciality> Published(this IQueryable<SalesPointSpeciality> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SalesPointSpeciality> ForFrontEnd(this IQueryable<SalesPointSpeciality> e) {
            return e;
        }

        public static IEnumerable<SalesPointService> ApplyContext(this IEnumerable<SalesPointService> e,
                                                                  QPDataContext context) {
            foreach (SalesPointService item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SalesPointService> Published(this IQueryable<SalesPointService> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SalesPointService> ForFrontEnd(this IQueryable<SalesPointService> e) {
            return e;
        }

        public static IEnumerable<TargetUser> ApplyContext(this IEnumerable<TargetUser> e, QPDataContext context) {
            foreach (TargetUser item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<TargetUser> Published(this IQueryable<TargetUser> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<TargetUser> ForFrontEnd(this IQueryable<TargetUser> e) {
            return e;
        }

        public static IEnumerable<FeedbackSubthemeGroup> ApplyContext(this IEnumerable<FeedbackSubthemeGroup> e,
                                                                      QPDataContext context) {
            foreach (FeedbackSubthemeGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<FeedbackSubthemeGroup> Published(this IQueryable<FeedbackSubthemeGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<FeedbackSubthemeGroup> ForFrontEnd(this IQueryable<FeedbackSubthemeGroup> e) {
            return e;
        }

        public static IEnumerable<ArchiveTvTariff> ApplyContext(this IEnumerable<ArchiveTvTariff> e,
                                                                QPDataContext context) {
            foreach (ArchiveTvTariff item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ArchiveTvTariff> Published(this IQueryable<ArchiveTvTariff> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ArchiveTvTariff> ForFrontEnd(this IQueryable<ArchiveTvTariff> e) {
            return e;
        }

        public static IEnumerable<ObsoleteUrlRedirect> ApplyContext(this IEnumerable<ObsoleteUrlRedirect> e,
                                                                    QPDataContext context) {
            foreach (ObsoleteUrlRedirect item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ObsoleteUrlRedirect> Published(this IQueryable<ObsoleteUrlRedirect> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ObsoleteUrlRedirect> ForFrontEnd(this IQueryable<ObsoleteUrlRedirect> e) {
            return e;
        }

        public static IEnumerable<ArchiveViewTariff> ApplyContext(this IEnumerable<ArchiveViewTariff> e,
                                                                  QPDataContext context) {
            foreach (ArchiveViewTariff item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ArchiveViewTariff> Published(this IQueryable<ArchiveViewTariff> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ArchiveViewTariff> ForFrontEnd(this IQueryable<ArchiveViewTariff> e) {
            return e;
        }

        public static IEnumerable<MarketingRegionNoCallback> ApplyContext(this IEnumerable<MarketingRegionNoCallback> e,
                                                                          QPDataContext context) {
            foreach (MarketingRegionNoCallback item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MarketingRegionNoCallback> Published(this IQueryable<MarketingRegionNoCallback> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MarketingRegionNoCallback> ForFrontEnd(this IQueryable<MarketingRegionNoCallback> e) {
            return e;
        }

        public static IEnumerable<FeedbackCallbackTime> ApplyContext(this IEnumerable<FeedbackCallbackTime> e,
                                                                     QPDataContext context) {
            foreach (FeedbackCallbackTime item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<FeedbackCallbackTime> Published(this IQueryable<FeedbackCallbackTime> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<FeedbackCallbackTime> ForFrontEnd(this IQueryable<FeedbackCallbackTime> e) {
            return e;
        }

        public static IEnumerable<FeedbackSwindleProblem> ApplyContext(this IEnumerable<FeedbackSwindleProblem> e,
                                                                       QPDataContext context) {
            foreach (FeedbackSwindleProblem item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<FeedbackSwindleProblem> Published(this IQueryable<FeedbackSwindleProblem> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<FeedbackSwindleProblem> ForFrontEnd(this IQueryable<FeedbackSwindleProblem> e) {
            return e;
        }

        public static IEnumerable<Contact> ApplyContext(this IEnumerable<Contact> e, QPDataContext context) {
            foreach (Contact item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<Contact> Published(this IQueryable<Contact> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<Contact> ForFrontEnd(this IQueryable<Contact> e) {
            return e;
        }

        public static IEnumerable<ContactsTab> ApplyContext(this IEnumerable<ContactsTab> e, QPDataContext context) {
            foreach (ContactsTab item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ContactsTab> Published(this IQueryable<ContactsTab> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ContactsTab> ForFrontEnd(this IQueryable<ContactsTab> e) {
            return e;
        }

        public static IEnumerable<ContactsGroup> ApplyContext(this IEnumerable<ContactsGroup> e, QPDataContext context) {
            foreach (ContactsGroup item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ContactsGroup> Published(this IQueryable<ContactsGroup> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ContactsGroup> ForFrontEnd(this IQueryable<ContactsGroup> e) {
            return e;
        }

        public static IEnumerable<CampaignIdToRegion> ApplyContext(this IEnumerable<CampaignIdToRegion> e,
                                                                   QPDataContext context) {
            foreach (CampaignIdToRegion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<CampaignIdToRegion> Published(this IQueryable<CampaignIdToRegion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<CampaignIdToRegion> ForFrontEnd(this IQueryable<CampaignIdToRegion> e) {
            return e;
        }

        public static IEnumerable<SitemapXml> ApplyContext(this IEnumerable<SitemapXml> e, QPDataContext context) {
            foreach (SitemapXml item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SitemapXml> Published(this IQueryable<SitemapXml> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SitemapXml> ForFrontEnd(this IQueryable<SitemapXml> e) {
            return e;
        }

        public static IEnumerable<SiteConfig> ApplyContext(this IEnumerable<SiteConfig> e, QPDataContext context) {
            foreach (SiteConfig item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SiteConfig> Published(this IQueryable<SiteConfig> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SiteConfig> ForFrontEnd(this IQueryable<SiteConfig> e) {
            return e;
        }

        public static IEnumerable<RobotsTxt> ApplyContext(this IEnumerable<RobotsTxt> e, QPDataContext context) {
            foreach (RobotsTxt item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RobotsTxt> Published(this IQueryable<RobotsTxt> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RobotsTxt> ForFrontEnd(this IQueryable<RobotsTxt> e) {
            return e;
        }

        public static IEnumerable<MetroLine> ApplyContext(this IEnumerable<MetroLine> e, QPDataContext context) {
            foreach (MetroLine item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MetroLine> Published(this IQueryable<MetroLine> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MetroLine> ForFrontEnd(this IQueryable<MetroLine> e) {
            return e;
        }

        public static IEnumerable<AnnualContractSetting> ApplyContext(this IEnumerable<AnnualContractSetting> e,
                                                                      QPDataContext context) {
            foreach (AnnualContractSetting item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<AnnualContractSetting> Published(this IQueryable<AnnualContractSetting> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<AnnualContractSetting> ForFrontEnd(this IQueryable<AnnualContractSetting> e) {
            return e;
        }

        public static IEnumerable<MnpCrmSetting> ApplyContext(this IEnumerable<MnpCrmSetting> e, QPDataContext context) {
            foreach (MnpCrmSetting item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MnpCrmSetting> Published(this IQueryable<MnpCrmSetting> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MnpCrmSetting> ForFrontEnd(this IQueryable<MnpCrmSetting> e) {
            return e;
        }

        public static IEnumerable<MNPRequestOfficeInRegion> ApplyContext(this IEnumerable<MNPRequestOfficeInRegion> e,
                                                                         QPDataContext context) {
            foreach (MNPRequestOfficeInRegion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MNPRequestOfficeInRegion> Published(this IQueryable<MNPRequestOfficeInRegion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MNPRequestOfficeInRegion> ForFrontEnd(this IQueryable<MNPRequestOfficeInRegion> e) {
            return e;
        }

        public static IEnumerable<MNPRequesCourierInRegion> ApplyContext(this IEnumerable<MNPRequesCourierInRegion> e,
                                                                         QPDataContext context) {
            foreach (MNPRequesCourierInRegion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MNPRequesCourierInRegion> Published(this IQueryable<MNPRequesCourierInRegion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MNPRequesCourierInRegion> ForFrontEnd(this IQueryable<MNPRequesCourierInRegion> e) {
            return e;
        }

        public static IEnumerable<MNPRequestPageInRegion> ApplyContext(this IEnumerable<MNPRequestPageInRegion> e,
                                                                       QPDataContext context) {
            foreach (MNPRequestPageInRegion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MNPRequestPageInRegion> Published(this IQueryable<MNPRequestPageInRegion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MNPRequestPageInRegion> ForFrontEnd(this IQueryable<MNPRequestPageInRegion> e) {
            return e;
        }

        public static IEnumerable<SkadToFederalRegion> ApplyContext(this IEnumerable<SkadToFederalRegion> e,
                                                                    QPDataContext context) {
            foreach (SkadToFederalRegion item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<SkadToFederalRegion> Published(this IQueryable<SkadToFederalRegion> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<SkadToFederalRegion> ForFrontEnd(this IQueryable<SkadToFederalRegion> e) {
            return e;
        }

        public static IEnumerable<ServicePrefix> ApplyContext(this IEnumerable<ServicePrefix> e, QPDataContext context) {
            foreach (ServicePrefix item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<ServicePrefix> Published(this IQueryable<ServicePrefix> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<ServicePrefix> ForFrontEnd(this IQueryable<ServicePrefix> e) {
            return e;
        }

        public static IEnumerable<RegionalEmail> ApplyContext(this IEnumerable<RegionalEmail> e, QPDataContext context) {
            foreach (RegionalEmail item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RegionalEmail> Published(this IQueryable<RegionalEmail> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RegionalEmail> ForFrontEnd(this IQueryable<RegionalEmail> e) {
            return e;
        }

        public static IEnumerable<RoamingParameter> ApplyContext(this IEnumerable<RoamingParameter> e,
                                                                 QPDataContext context) {
            foreach (RoamingParameter item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RoamingParameter> Published(this IQueryable<RoamingParameter> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RoamingParameter> ForFrontEnd(this IQueryable<RoamingParameter> e) {
            return e;
        }

        public static IEnumerable<RoamingOptionCalculation> ApplyContext(this IEnumerable<RoamingOptionCalculation> e,
                                                                         QPDataContext context) {
            foreach (RoamingOptionCalculation item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<RoamingOptionCalculation> Published(this IQueryable<RoamingOptionCalculation> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<RoamingOptionCalculation> ForFrontEnd(this IQueryable<RoamingOptionCalculation> e) {
            return e;
        }

        public static IEnumerable<MobileDevice> ApplyContext(this IEnumerable<MobileDevice> e, QPDataContext context) {
            foreach (MobileDevice item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MobileDevice> Published(this IQueryable<MobileDevice> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MobileDevice> ForFrontEnd(this IQueryable<MobileDevice> e) {
            return e;
        }

        public static IEnumerable<MobileServiceUsageType> ApplyContext(this IEnumerable<MobileServiceUsageType> e,
                                                                       QPDataContext context) {
            foreach (MobileServiceUsageType item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MobileServiceUsageType> Published(this IQueryable<MobileServiceUsageType> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MobileServiceUsageType> ForFrontEnd(this IQueryable<MobileServiceUsageType> e) {
            return e;
        }

        public static IEnumerable<MobileServiceByRegionUsageType> ApplyContext(
            this IEnumerable<MobileServiceByRegionUsageType> e, QPDataContext context) {
            foreach (MobileServiceByRegionUsageType item in e)
                item.InternalDataContext = context;
            return e;
        }

        public static IQueryable<MobileServiceByRegionUsageType> Published(
            this IQueryable<MobileServiceByRegionUsageType> e) {
            return e.Where(n => n.StatusType.Name == "Published");
        }

        public static IQueryable<MobileServiceByRegionUsageType> ForFrontEnd(
            this IQueryable<MobileServiceByRegionUsageType> e) {
            return e;
        }
    }

    public class BindingListSelector<TSource, T> : ListSelector<TSource, T>, IBindingList {
        protected IBindingList sourceAsBindingList;

        public BindingListSelector(IBindingList source, Func<TSource, T> selector, Action<IList<TSource>, T> onAdd,
                                   Action<IList<TSource>, T> onRemove)
            : base(source as IList<TSource>, selector, onAdd, onRemove) {
            sourceAsBindingList = source;
        }

        #region IBindingList Members

        public void AddIndex(PropertyDescriptor property) {
            sourceAsBindingList.AddIndex(property);
        }

        public object AddNew() {
            return sourceAsBindingList.AddNew();
        }

        public bool AllowEdit {
            get { return sourceAsBindingList.AllowEdit; }
        }

        public bool AllowNew {
            get { return sourceAsBindingList.AllowNew; }
        }

        public bool AllowRemove {
            get { return sourceAsBindingList.AllowRemove; }
        }

        public void ApplySort(PropertyDescriptor property, ListSortDirection direction) {
            sourceAsBindingList.ApplySort(property, direction);
        }

        public int Find(PropertyDescriptor property, object key) {
            return sourceAsBindingList.Find(property, key);
        }

        public bool IsSorted {
            get { return sourceAsBindingList.IsSorted; }
        }

        public event ListChangedEventHandler ListChanged {
            add { sourceAsBindingList.ListChanged += value; }
            remove { sourceAsBindingList.ListChanged -= value; }
        }

        public void RemoveIndex(PropertyDescriptor property) {
            sourceAsBindingList.RemoveIndex(property);
        }

        public void RemoveSort() {
            sourceAsBindingList.RemoveSort();
        }

        public ListSortDirection SortDirection {
            get { return sourceAsBindingList.SortDirection; }
        }

        public PropertyDescriptor SortProperty {
            get { return sourceAsBindingList.SortProperty; }
        }

        public bool SupportsChangeNotification {
            get { return sourceAsBindingList.SupportsChangeNotification; }
        }

        public bool SupportsSearching {
            get { return sourceAsBindingList.SupportsSearching; }
        }

        public bool SupportsSorting {
            get { return sourceAsBindingList.SupportsSorting; }
        }

        #endregion
    }

    public static class ListSelectorExtensions {
        public static ListSelector<TSource, T> AsListSelector<TSource, T>(this IList<TSource> source,
                                                                          Func<TSource, T> selector,
                                                                          Action<IList<TSource>, T> onAdd,
                                                                          Action<IList<TSource>, T> onRemove) {
            return new ListSelector<TSource, T>(source, selector, onAdd, onRemove);
        }

        public static BindingListSelector<TSource, T> AsListSelector<TSource, T>(this IBindingList source,
                                                                                 Func<TSource, T> selector,
                                                                                 Action<IList<TSource>, T> onAdd,
                                                                                 Action<IList<TSource>, T> onRemove) {
            return new BindingListSelector<TSource, T>(source, selector, onAdd, onRemove);
        }
    }

    public class ListSelector<TSource, T> : IList<T>, IList {
        protected Action<IList<TSource>, T> onAdd;
        protected Action<IList<TSource>, T> onRemove;
        protected List<T> projection;
        protected Func<TSource, T> selector;
        protected IList<TSource> source;

        public ListSelector(IList<TSource> source, Func<TSource, T> selector, Action<IList<TSource>, T> onAdd,
                            Action<IList<TSource>, T> onRemove) {
            this.source = source;
            this.selector = selector;
            this.onAdd = onAdd;
            this.onRemove = onRemove;
            UpdateProjection();
        }

        #region IList Members

        int IList.Add(object value) {
            throw new Exception("The method or operation is not implemented.");
        }

        void IList.Clear() {
            throw new Exception("The method or operation is not implemented.");
        }

        bool IList.Contains(object value) {
            return Contains((T) value);
        }

        int IList.IndexOf(object value) {
            return IndexOf((T) value);
        }

        void IList.Insert(int index, object value) {
            throw new Exception("The method or operation is not implemented.");
        }

        bool IList.IsFixedSize {
            get { return true; }
        }

        bool IList.IsReadOnly {
            get { return true; }
        }

        void IList.Remove(object value) {
            throw new Exception("The method or operation is not implemented.");
        }

        void IList.RemoveAt(int index) {
            throw new Exception("The method or operation is not implemented.");
        }

        object IList.this[int index] {
            get { return this[index]; }
            set { }
        }

        void ICollection.CopyTo(Array array, int index) {
            var arrayOfT = array as T[];
            if (arrayOfT == null)
                throw new ArgumentException("Incorrect array type");
            CopyTo(arrayOfT, index);
        }

        int ICollection.Count {
            get { return Count; }
        }

        bool ICollection.IsSynchronized {
            get { return false; }
        }

        object ICollection.SyncRoot {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion

        #region IList<T> Members

        public int IndexOf(T item) {
            int i = 0;
            foreach (T t in projection) {
                if (t.Equals(item))
                    return i;
                i++;
            }
            return -1;
        }

        public void Insert(int index, T item) {
            throw new Exception("The method or operation is not implemented.");
        }

        public void RemoveAt(int index) {
            throw new Exception("The method or operation is not implemented.");
        }

        public T this[int index] {
            get { return projection[index]; }
            set { throw new Exception("The method or operation is not implemented."); }
        }

        public void Add(T item) {
            if (onAdd != null) {
                onAdd(source, item);
                UpdateProjection();
            }
        }

        public bool Remove(T item) {
            if (onRemove != null) {
                onRemove(source, item);
                UpdateProjection();
                return true;
            }
            else
                return false;
        }

        public void Clear() {
            foreach (T item in projection.ToList())
                Remove(item);
        }

        public bool Contains(T item) {
            return projection.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex) {
            projection.CopyTo(array, arrayIndex);
        }

        public int Count {
            get { return projection.Count(); }
        }

        public bool IsReadOnly {
            get { return true; }
        }

        public IEnumerator<T> GetEnumerator() {
            return projection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return projection.GetEnumerator();
        }

        #endregion

        private void UpdateProjection() {
            projection = source.Select(selector).Where(n => n != null).ToList();
        }

        public void Add(IEnumerable<T> items) {
            if (items != null) {
                foreach (T item in items)
                    Add(item);
            }
        }

        public void Remove(IEnumerable<T> items) {
            if (items != null) {
                foreach (T item in items.ToList())
                    Remove(item);
            }
        }
    }

    partial class GeoRegion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public GeoRegion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OldIdExact {
            get { return (OldId.HasValue) ? OldId.Value : default(Int32); }
        }

        public Int32 OldParentIdExact {
            get { return (OldParentId.HasValue) ? OldParentId.Value : default(Int32); }
        }

        public Int32 MniIdExact {
            get { return (MniId.HasValue) ? MniId.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MarketingRegion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MarketingRegion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IsPopularExact {
            get { return (IsPopular.HasValue) ? IsPopular.Value : default(Boolean); }
        }

        public Boolean IsMainExact {
            get { return (IsMain.HasValue) ? IsMain.Value : default(Boolean); }
        }

        public Boolean Has3GExact {
            get { return (Has3G.HasValue) ? Has3G.Value : default(Boolean); }
        }

        public Boolean IsB2BExact {
            get { return (IsB2B.HasValue) ? IsB2B.Value : default(Boolean); }
        }

        public Double LatExact {
            get { return (Lat.HasValue) ? Lat.Value : default(Double); }
        }

        public Double LonExact {
            get { return (Lon.HasValue) ? Lon.Value : default(Double); }
        }

        public Double Lon1Exact {
            get { return (Lon1.HasValue) ? Lon1.Value : default(Double); }
        }

        public Double Lat1Exact {
            get { return (Lat1.HasValue) ? Lat1.Value : default(Double); }
        }

        public Double Lon2Exact {
            get { return (Lon2.HasValue) ? Lon2.Value : default(Double); }
        }

        public Double Lat2Exact {
            get { return (Lat2.HasValue) ? Lat2.Value : default(Double); }
        }

        public Int32 GMTExact {
            get { return (GMT.HasValue) ? GMT.Value : default(Int32); }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Url = InternalDataContext.ReplacePlaceholders(_Url);
            _RegionAddress = InternalDataContext.ReplacePlaceholders(_RegionAddress);
            _RegionPhone = InternalDataContext.ReplacePlaceholders(_RegionPhone);
            _TitlePreposition = InternalDataContext.ReplacePlaceholders(_TitlePreposition);
            _TitlePrepositionEngl = InternalDataContext.ReplacePlaceholders(_TitlePrepositionEngl);
            _TitlePrepositionTat = InternalDataContext.ReplacePlaceholders(_TitlePrepositionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class GeoRegionsIPAddress : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public GeoRegionsIPAddress(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int64 IpFromExact {
            get { return (IpFrom.HasValue) ? IpFrom.Value : default(Int64); }
        }

        public Int64 IpToExact {
            get { return (IpTo.HasValue) ? IpTo.Value : default(Int64); }
        }

        public Int32 OldGniIdExact {
            get { return (OldGniId.HasValue) ? OldGniId.Value : default(Int32); }
        }

        public Int32 OldIdExact {
            get { return (OldId.HasValue) ? OldId.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SiteProduct : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SiteProduct(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SiteNumExact {
            get { return (SiteNum.HasValue) ? SiteNum.Value : default(Int32); }
        }

        public Int32 OldIdExact {
            get { return (OldId.HasValue) ? OldId.Value : default(Int32); }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Mask = InternalDataContext.ReplacePlaceholders(_Mask);
            _RegularExpression = InternalDataContext.ReplacePlaceholders(_RegularExpression);
            _StageMask = InternalDataContext.ReplacePlaceholders(_StageMask);
            _StageTitle = InternalDataContext.ReplacePlaceholders(_StageTitle);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _PrepositionalTitle = InternalDataContext.ReplacePlaceholders(_PrepositionalTitle);
            _PrepositionalTitleEngl = InternalDataContext.ReplacePlaceholders(_PrepositionalTitleEngl);
            _PrepositionalTitleTat = InternalDataContext.ReplacePlaceholders(_PrepositionalTitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class QPAbstractItem : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public QPAbstractItem(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IsVisibleExact {
            get { return (IsVisible.HasValue) ? IsVisible.Value : default(Boolean); }
        }

        public Boolean IsPageExact {
            get { return (IsPage.HasValue) ? IsPage.Value : default(Boolean); }
        }

        public Int32 ContentIdExact {
            get { return (ContentId.HasValue) ? ContentId.Value : default(Int32); }
        }

        public Boolean IsInSiteMapExact {
            get { return (IsInSiteMap.HasValue) ? IsInSiteMap.Value : default(Boolean); }
        }

        public Int32 IndexOrderExact {
            get { return (IndexOrder.HasValue) ? IndexOrder.Value : default(Int32); }
        }

        public Int32 ExtensionIdExact {
            get { return (ExtensionId.HasValue) ? ExtensionId.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Name = InternalDataContext.ReplacePlaceholders(_Name);
            _ZoneName = InternalDataContext.ReplacePlaceholders(_ZoneName);
            _AllowedUrlPatterns = InternalDataContext.ReplacePlaceholders(_AllowedUrlPatterns);
            _DeniedUrlPatterns = InternalDataContext.ReplacePlaceholders(_DeniedUrlPatterns);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _Keywords = InternalDataContext.ReplacePlaceholders(_Keywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
            _Tags = InternalDataContext.ReplacePlaceholders(_Tags);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class QPDiscriminator : IQPContent {
        private string _IconUrlUploadPath;
        private string _IconUrlUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public QPDiscriminator(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string IconUrlUrl {
            get {
                if (String.IsNullOrEmpty(IconUrl))
                    return String.Empty;
                else {
                    if (_IconUrlUrl == null) {
                        _IconUrlUrl = String.Format(@"{0}/{1}",
                                                    InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                        InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                            InternalDataContext.SiteId, "QPDiscriminator", "IconUrl"),
                                                        true, _InternalDataContext.ShouldRemoveSchema), IconUrl);
                    }
                    return _IconUrlUrl;
                }
            }
        }

        public string IconUrlUploadPath {
            get {
                if (_IconUrlUploadPath == null) {
                    _IconUrlUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "QPDiscriminator", "IconUrl"));
                }

                return (_IconUrlUploadPath);
            }
        }

        public Int32 PreferredContentIdExact {
            get { return (PreferredContentId.HasValue) ? PreferredContentId.Value : default(Int32); }
        }

        public Boolean IsPageExact {
            get { return (IsPage.HasValue) ? IsPage.Value : default(Boolean); }
        }

        public Boolean FilterPartByUrlExact {
            get { return (FilterPartByUrl.HasValue) ? FilterPartByUrl.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Name = InternalDataContext.ReplacePlaceholders(_Name);
            _CategoryName = InternalDataContext.ReplacePlaceholders(_CategoryName);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _AllowedZones = InternalDataContext.ReplacePlaceholders(_AllowedZones);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class QPCulture : IQPContent {
        private string _IconUploadPath;
        private string _IconUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public QPCulture(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string IconUrl {
            get {
                if (String.IsNullOrEmpty(Icon))
                    return String.Empty;
                else {
                    if (_IconUrl == null) {
                        _IconUrl = String.Format(@"{0}/{1}",
                                                 InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                     InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                         InternalDataContext.SiteId, "QPCulture", "Icon"), true,
                                                     _InternalDataContext.ShouldRemoveSchema), Icon);
                    }
                    return _IconUrl;
                }
            }
        }

        public string IconUploadPath {
            get {
                if (_IconUploadPath == null) {
                    _IconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "QPCulture",
                                                                             "Icon"));
                }

                return (_IconUploadPath);
            }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Name = InternalDataContext.ReplacePlaceholders(_Name);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ItemTitleFormat : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ItemTitleFormat(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Value = InternalDataContext.ReplacePlaceholders(_Value);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class QPRegion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public QPRegion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Double ParentIdExact {
            get { return (ParentId.HasValue) ? ParentId.Value : default(Double); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class Poll : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public Poll(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public DateTime StartDateExact {
            get { return (StartDate.HasValue) ? StartDate.Value : default(DateTime); }
        }

        public DateTime EndDateExact {
            get { return (EndDate.HasValue) ? EndDate.Value : default(DateTime); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _MarketCodes = InternalDataContext.ReplacePlaceholders(_MarketCodes);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PollQuestion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public PollQuestion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 NumberOfAnswersExact {
            get { return (NumberOfAnswers.HasValue) ? NumberOfAnswers.Value : default(Int32); }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PollAnswer : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public PollAnswer(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class UserPollAnswer : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public UserPollAnswer(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _User = InternalDataContext.ReplacePlaceholders(_User);
            _MarketCode = InternalDataContext.ReplacePlaceholders(_MarketCode);
            _AnswerCode = InternalDataContext.ReplacePlaceholders(_AnswerCode);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TrailedAbstractItem : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public TrailedAbstractItem(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Trail = InternalDataContext.ReplacePlaceholders(_Trail);
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Name = InternalDataContext.ReplacePlaceholders(_Name);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class QPObsoleteUrl : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public QPObsoleteUrl(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Url = InternalDataContext.ReplacePlaceholders(_Url);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RegionTag : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RegionTag(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RegionTagValue : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RegionTagValue(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Value = InternalDataContext.ReplacePlaceholders(_Value);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SearchSuggestion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SearchSuggestion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 PriorityExact {
            get { return (Priority.HasValue) ? Priority.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SearchResult : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SearchResult(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IsTopExact {
            get { return (IsTop.HasValue) ? IsTop.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Query = InternalDataContext.ReplacePlaceholders(_Query);
            _PageUrl = InternalDataContext.ReplacePlaceholders(_PageUrl);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SiteSection : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SiteSection(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class NewsCategory : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public NewsCategory(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class NewsArticle : IQPContent {
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;
        private string _TileImageUploadPath;
        private string _TileImageUrl;

        public NewsArticle(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "NewsArticle", "Image"), true,
                                                      _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "NewsArticle",
                                                                             "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string TileImageUrl {
            get {
                if (String.IsNullOrEmpty(TileImage))
                    return String.Empty;
                else {
                    if (_TileImageUrl == null) {
                        _TileImageUrl = String.Format(@"{0}/{1}",
                                                      InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                          InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                              InternalDataContext.SiteId, "NewsArticle", "TileImage"),
                                                          true, _InternalDataContext.ShouldRemoveSchema), TileImage);
                    }
                    return _TileImageUrl;
                }
            }
        }

        public string TileImageUploadPath {
            get {
                if (_TileImageUploadPath == null) {
                    _TileImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "NewsArticle",
                                                                             "TileImage"));
                }

                return (_TileImageUploadPath);
            }
        }

        public DateTime StartPublishDateExact {
            get { return (StartPublishDate.HasValue) ? StartPublishDate.Value : default(DateTime); }
        }

        public DateTime EndPublishDateExact {
            get { return (EndPublishDate.HasValue) ? EndPublishDate.Value : default(DateTime); }
        }

        public Boolean ShowOnWebExact {
            get { return (ShowOnWeb.HasValue) ? ShowOnWeb.Value : default(Boolean); }
        }

        public Boolean ShowInSelfServiceExact {
            get { return (ShowInSelfService.HasValue) ? ShowInSelfService.Value : default(Boolean); }
        }

        public Boolean ExcludeFromSubscriptionExact {
            get { return (ExcludeFromSubscription.HasValue) ? ExcludeFromSubscription.Value : default(Boolean); }
        }

        public Boolean IsUrgentExact {
            get { return (IsUrgent.HasValue) ? IsUrgent.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _PaymentSystem = InternalDataContext.ReplacePlaceholders(_PaymentSystem);
            _Currency = InternalDataContext.ReplacePlaceholders(_Currency);
            _BusinessSegment = InternalDataContext.ReplacePlaceholders(_BusinessSegment);
            _Preview = InternalDataContext.ReplacePlaceholders(_Preview);
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _Tariff = InternalDataContext.ReplacePlaceholders(_Tariff);
            _FileCTN = InternalDataContext.ReplacePlaceholders(_FileCTN);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _PreviewEngl = InternalDataContext.ReplacePlaceholders(_PreviewEngl);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _PreviewTat = InternalDataContext.ReplacePlaceholders(_PreviewTat);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
            _MetaKeywords = InternalDataContext.ReplacePlaceholders(_MetaKeywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class NotificationTemplate : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public NotificationTemplate(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TemplateCode = InternalDataContext.ReplacePlaceholders(_TemplateCode);
            _Subject = InternalDataContext.ReplacePlaceholders(_Subject);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RoamingTariffZone : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RoamingTariffZone(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        public Boolean HiddenByDefaultExact {
            get { return (HiddenByDefault.HasValue) ? HiddenByDefault.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RoamingCountryZone : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RoamingCountryZone(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class UserSubscription : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public UserSubscription(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IsActiveExact {
            get { return (IsActive.HasValue) ? IsActive.Value : default(Boolean); }
        }

        public Boolean IsDeletedExact {
            get { return (IsDeleted.HasValue) ? IsDeleted.Value : default(Boolean); }
        }

        public Boolean IsOldExact {
            get { return (IsOld.HasValue) ? IsOld.Value : default(Boolean); }
        }

        public Boolean IsHtmlExact {
            get { return (IsHtml.HasValue) ? IsHtml.Value : default(Boolean); }
        }

        public Int32 FailedDeliveriesExact {
            get { return (FailedDeliveries.HasValue) ? FailedDeliveries.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Email = InternalDataContext.ReplacePlaceholders(_Email);
            _Login = InternalDataContext.ReplacePlaceholders(_Login);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SubscriptionCategory : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SubscriptionCategory(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IsActiveExact {
            get { return (IsActive.HasValue) ? IsActive.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ConfirmationRequest : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ConfirmationRequest(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 ActionExact {
            get { return (Action.HasValue) ? Action.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Code = InternalDataContext.ReplacePlaceholders(_Code);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class FeedbackTheme : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public FeedbackTheme(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean ShowHasContractExact {
            get { return (ShowHasContract.HasValue) ? ShowHasContract.Value : default(Boolean); }
        }

        public Boolean ShowLoginExact {
            get { return (ShowLogin.HasValue) ? ShowLogin.Value : default(Boolean); }
        }

        public Boolean ShowBeelinePhoneExact {
            get { return (ShowBeelinePhone.HasValue) ? ShowBeelinePhone.Value : default(Boolean); }
        }

        public Boolean ShowAttachmentsForEmailExact {
            get { return (ShowAttachmentsForEmail.HasValue) ? ShowAttachmentsForEmail.Value : default(Boolean); }
        }

        public Boolean ShowAttachmentsForCallExact {
            get { return (ShowAttachmentsForCall.HasValue) ? ShowAttachmentsForCall.Value : default(Boolean); }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _SendToEmail = InternalDataContext.ReplacePlaceholders(_SendToEmail);
            _FilterThemeId = InternalDataContext.ReplacePlaceholders(_FilterThemeId);
            _Source = InternalDataContext.ReplacePlaceholders(_Source);
            _OnlineChatId = InternalDataContext.ReplacePlaceholders(_OnlineChatId);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class FeedbackType : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public FeedbackType(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _FilterTypeId = InternalDataContext.ReplacePlaceholders(_FilterTypeId);
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
            _SuccessTitle = InternalDataContext.ReplacePlaceholders(_SuccessTitle);
            _SuccessTitleEngl = InternalDataContext.ReplacePlaceholders(_SuccessTitleEngl);
            _SuccessTitleTat = InternalDataContext.ReplacePlaceholders(_SuccessTitleTat);
            _SuccessText = InternalDataContext.ReplacePlaceholders(_SuccessText);
            _SuccessTextEngl = InternalDataContext.ReplacePlaceholders(_SuccessTextEngl);
            _SuccessTextTat = InternalDataContext.ReplacePlaceholders(_SuccessTextTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class FeedbackSubtheme : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public FeedbackSubtheme(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _ContactType = InternalDataContext.ReplacePlaceholders(_ContactType);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class FeedbackQueue : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public FeedbackQueue(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _SourceID = InternalDataContext.ReplacePlaceholders(_SourceID);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TVChannel : IQPContent {
        private string _GrayLogoSmallUploadPath;
        private string _GrayLogoSmallUrl;
        private string _GrayLogoUploadPath;
        private string _GrayLogoUrl;
        private QPDataContext _InternalDataContext;
        private string _LogoImageUploadPath;

        private string _LogoImageUrl;
        private string _PromoVideoUploadPath;
        private string _PromoVideoUrl;
        private string _SmallLogoUploadPath;
        private string _SmallLogoUrl;
        private bool _StatusTypeChanged;
        private string _TinyLogoUploadPath;
        private string _TinyLogoUrl;
        private string _VideoPreviewImageUploadPath;
        private string _VideoPreviewImageUrl;

        public TVChannel(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string LogoImageUrl {
            get {
                if (String.IsNullOrEmpty(LogoImage))
                    return String.Empty;
                else {
                    if (_LogoImageUrl == null) {
                        _LogoImageUrl = String.Format(@"{0}/{1}",
                                                      InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                          InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                              InternalDataContext.SiteId, "TVChannel", "LogoImage"),
                                                          true, _InternalDataContext.ShouldRemoveSchema), LogoImage);
                    }
                    return _LogoImageUrl;
                }
            }
        }

        public string LogoImageUploadPath {
            get {
                if (_LogoImageUploadPath == null) {
                    _LogoImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVChannel",
                                                                             "LogoImage"));
                }

                return (_LogoImageUploadPath);
            }
        }

        public string GrayLogoUrl {
            get {
                if (String.IsNullOrEmpty(GrayLogo))
                    return String.Empty;
                else {
                    if (_GrayLogoUrl == null) {
                        _GrayLogoUrl = String.Format(@"{0}/{1}",
                                                     InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                         InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                             InternalDataContext.SiteId, "TVChannel", "GrayLogo"), true,
                                                         _InternalDataContext.ShouldRemoveSchema), GrayLogo);
                    }
                    return _GrayLogoUrl;
                }
            }
        }

        public string GrayLogoUploadPath {
            get {
                if (_GrayLogoUploadPath == null) {
                    _GrayLogoUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVChannel",
                                                                             "GrayLogo"));
                }

                return (_GrayLogoUploadPath);
            }
        }

        public string GrayLogoSmallUrl {
            get {
                if (String.IsNullOrEmpty(GrayLogoSmall))
                    return String.Empty;
                else {
                    if (_GrayLogoSmallUrl == null) {
                        _GrayLogoSmallUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "TVChannel",
                                                                  "GrayLogoSmall"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), GrayLogoSmall);
                    }
                    return _GrayLogoSmallUrl;
                }
            }
        }

        public string GrayLogoSmallUploadPath {
            get {
                if (_GrayLogoSmallUploadPath == null) {
                    _GrayLogoSmallUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVChannel",
                                                                             "GrayLogoSmall"));
                }

                return (_GrayLogoSmallUploadPath);
            }
        }

        public string SmallLogoUrl {
            get {
                if (String.IsNullOrEmpty(SmallLogo))
                    return String.Empty;
                else {
                    if (_SmallLogoUrl == null) {
                        _SmallLogoUrl = String.Format(@"{0}/{1}",
                                                      InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                          InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                              InternalDataContext.SiteId, "TVChannel", "SmallLogo"),
                                                          true, _InternalDataContext.ShouldRemoveSchema), SmallLogo);
                    }
                    return _SmallLogoUrl;
                }
            }
        }

        public string SmallLogoUploadPath {
            get {
                if (_SmallLogoUploadPath == null) {
                    _SmallLogoUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVChannel",
                                                                             "SmallLogo"));
                }

                return (_SmallLogoUploadPath);
            }
        }

        public string TinyLogoUrl {
            get {
                if (String.IsNullOrEmpty(TinyLogo))
                    return String.Empty;
                else {
                    if (_TinyLogoUrl == null) {
                        _TinyLogoUrl = String.Format(@"{0}/{1}",
                                                     InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                         InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                             InternalDataContext.SiteId, "TVChannel", "TinyLogo"), true,
                                                         _InternalDataContext.ShouldRemoveSchema), TinyLogo);
                    }
                    return _TinyLogoUrl;
                }
            }
        }

        public string TinyLogoUploadPath {
            get {
                if (_TinyLogoUploadPath == null) {
                    _TinyLogoUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVChannel",
                                                                             "TinyLogo"));
                }

                return (_TinyLogoUploadPath);
            }
        }

        public string VideoPreviewImageUrl {
            get {
                if (String.IsNullOrEmpty(VideoPreviewImage))
                    return String.Empty;
                else {
                    if (_VideoPreviewImageUrl == null) {
                        _VideoPreviewImageUrl = String.Format(@"{0}/{1}",
                                                              InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                  InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                      InternalDataContext.SiteId, "TVChannel",
                                                                      "VideoPreviewImage"), true,
                                                                  _InternalDataContext.ShouldRemoveSchema),
                                                              VideoPreviewImage);
                    }
                    return _VideoPreviewImageUrl;
                }
            }
        }

        public string VideoPreviewImageUploadPath {
            get {
                if (_VideoPreviewImageUploadPath == null) {
                    _VideoPreviewImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVChannel",
                                                                             "VideoPreviewImage"));
                }

                return (_VideoPreviewImageUploadPath);
            }
        }

        public string PromoVideoUrl {
            get {
                if (String.IsNullOrEmpty(PromoVideo))
                    return String.Empty;
                else {
                    if (_PromoVideoUrl == null) {
                        _PromoVideoUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "TVChannel", "PromoVideo"),
                                                           true, _InternalDataContext.ShouldRemoveSchema), PromoVideo);
                    }
                    return _PromoVideoUrl;
                }
            }
        }

        public string PromoVideoUploadPath {
            get {
                if (_PromoVideoUploadPath == null) {
                    _PromoVideoUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVChannel",
                                                                             "PromoVideo"));
                }

                return (_PromoVideoUploadPath);
            }
        }

        public Int32 TopOrderExact {
            get { return (TopOrder.HasValue) ? TopOrder.Value : default(Int32); }
        }

        public Boolean IsOnlineExact {
            get { return (IsOnline.HasValue) ? IsOnline.Value : default(Boolean); }
        }

        public Int32 MinAgeExact {
            get { return (MinAge.HasValue) ? MinAge.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _OnlineId = InternalDataContext.ReplacePlaceholders(_OnlineId);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _Languages = InternalDataContext.ReplacePlaceholders(_Languages);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RegionFeedbackGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RegionFeedbackGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Code = InternalDataContext.ReplacePlaceholders(_Code);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class DeviceType : IQPContent {
        private string _DefaultServiceTileIconUploadPath;
        private string _DefaultServiceTileIconUrl;
        private string _DefaultTariffTileIconUploadPath;
        private string _DefaultTariffTileIconUrl;
        private string _IconForTariffUploadPath;
        private string _IconForTariffUrl;
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public DeviceType(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "DeviceType", "Image"), true,
                                                      _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "DeviceType",
                                                                             "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string IconForTariffUrl {
            get {
                if (String.IsNullOrEmpty(IconForTariff))
                    return String.Empty;
                else {
                    if (_IconForTariffUrl == null) {
                        _IconForTariffUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "DeviceType",
                                                                  "IconForTariff"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), IconForTariff);
                    }
                    return _IconForTariffUrl;
                }
            }
        }

        public string IconForTariffUploadPath {
            get {
                if (_IconForTariffUploadPath == null) {
                    _IconForTariffUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "DeviceType",
                                                                             "IconForTariff"));
                }

                return (_IconForTariffUploadPath);
            }
        }

        public string DefaultTariffTileIconUrl {
            get {
                if (String.IsNullOrEmpty(DefaultTariffTileIcon))
                    return String.Empty;
                else {
                    if (_DefaultTariffTileIconUrl == null) {
                        _DefaultTariffTileIconUrl = String.Format(@"{0}/{1}",
                                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                          InternalDataContext.SiteId, "DeviceType",
                                                                          "DefaultTariffTileIcon"), true,
                                                                      _InternalDataContext.ShouldRemoveSchema),
                                                                  DefaultTariffTileIcon);
                    }
                    return _DefaultTariffTileIconUrl;
                }
            }
        }

        public string DefaultTariffTileIconUploadPath {
            get {
                if (_DefaultTariffTileIconUploadPath == null) {
                    _DefaultTariffTileIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "DeviceType",
                                                                             "DefaultTariffTileIcon"));
                }

                return (_DefaultTariffTileIconUploadPath);
            }
        }

        public string DefaultServiceTileIconUrl {
            get {
                if (String.IsNullOrEmpty(DefaultServiceTileIcon))
                    return String.Empty;
                else {
                    if (_DefaultServiceTileIconUrl == null) {
                        _DefaultServiceTileIconUrl = String.Format(@"{0}/{1}",
                                                                   InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                       InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                           InternalDataContext.SiteId, "DeviceType",
                                                                           "DefaultServiceTileIcon"), true,
                                                                       _InternalDataContext.ShouldRemoveSchema),
                                                                   DefaultServiceTileIcon);
                    }
                    return _DefaultServiceTileIconUrl;
                }
            }
        }

        public string DefaultServiceTileIconUploadPath {
            get {
                if (_DefaultServiceTileIconUploadPath == null) {
                    _DefaultServiceTileIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "DeviceType",
                                                                             "DefaultServiceTileIcon"));
                }

                return (_DefaultServiceTileIconUploadPath);
            }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _TitleForIcon = InternalDataContext.ReplacePlaceholders(_TitleForIcon);
            _TitleForIconEngl = InternalDataContext.ReplacePlaceholders(_TitleForIconEngl);
            _TitleForIconTat = InternalDataContext.ReplacePlaceholders(_TitleForIconTat);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class Action : IQPContent {
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;
        private string _TileImageUploadPath;
        private string _TileImageUrl;
        private string _UpSaleIconUploadPath;
        private string _UpSaleIconUrl;

        public Action(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "Action", "Image"), true,
                                                      _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "Action",
                                                                             "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string TileImageUrl {
            get {
                if (String.IsNullOrEmpty(TileImage))
                    return String.Empty;
                else {
                    if (_TileImageUrl == null) {
                        _TileImageUrl = String.Format(@"{0}/{1}",
                                                      InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                          InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                              InternalDataContext.SiteId, "Action", "TileImage"), true,
                                                          _InternalDataContext.ShouldRemoveSchema), TileImage);
                    }
                    return _TileImageUrl;
                }
            }
        }

        public string TileImageUploadPath {
            get {
                if (_TileImageUploadPath == null) {
                    _TileImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "Action",
                                                                             "TileImage"));
                }

                return (_TileImageUploadPath);
            }
        }

        public string UpSaleIconUrl {
            get {
                if (String.IsNullOrEmpty(UpSaleIcon))
                    return String.Empty;
                else {
                    if (_UpSaleIconUrl == null) {
                        _UpSaleIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "Action", "UpSaleIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), UpSaleIcon);
                    }
                    return _UpSaleIconUrl;
                }
            }
        }

        public string UpSaleIconUploadPath {
            get {
                if (_UpSaleIconUploadPath == null) {
                    _UpSaleIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "Action",
                                                                             "UpSaleIcon"));
                }

                return (_UpSaleIconUploadPath);
            }
        }

        public Int32 PriorityExact {
            get { return (Priority.HasValue) ? Priority.Value : default(Int32); }
        }

        public DateTime StartDateExact {
            get { return (StartDate.HasValue) ? StartDate.Value : default(DateTime); }
        }

        public DateTime EndDateExact {
            get { return (EndDate.HasValue) ? EndDate.Value : default(DateTime); }
        }

        public Boolean IsArchiveExact {
            get { return (IsArchive.HasValue) ? IsArchive.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _SpecialConditions = InternalDataContext.ReplacePlaceholders(_SpecialConditions);
            _Preview = InternalDataContext.ReplacePlaceholders(_Preview);
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _ProductLinks = InternalDataContext.ReplacePlaceholders(_ProductLinks);
            _MetaKeywords = InternalDataContext.ReplacePlaceholders(_MetaKeywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
            _TileType = InternalDataContext.ReplacePlaceholders(_TileType);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _PreviewEngl = InternalDataContext.ReplacePlaceholders(_PreviewEngl);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _PreviewTat = InternalDataContext.ReplacePlaceholders(_PreviewTat);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
            _SpecialConditionsEngl = InternalDataContext.ReplacePlaceholders(_SpecialConditionsEngl);
            _SpecialConditionsTat = InternalDataContext.ReplacePlaceholders(_SpecialConditionsTat);
            _ProductLinksEngl = InternalDataContext.ReplacePlaceholders(_ProductLinksEngl);
            _ProductLinksTat = InternalDataContext.ReplacePlaceholders(_ProductLinksTat);
            _MobilePreview = InternalDataContext.ReplacePlaceholders(_MobilePreview);
            _MobilePreviewEngl = InternalDataContext.ReplacePlaceholders(_MobilePreviewEngl);
            _MobilePreviewTat = InternalDataContext.ReplacePlaceholders(_MobilePreviewTat);
            _MobileText = InternalDataContext.ReplacePlaceholders(_MobileText);
            _MobileTextEngl = InternalDataContext.ReplacePlaceholders(_MobileTextEngl);
            _MobileTextTat = InternalDataContext.ReplacePlaceholders(_MobileTextTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TVPackage : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private QPDataContext _InternalDataContext;
        private string _PDFEnglUploadPath;
        private string _PDFEnglUrl;
        private string _PDFTatUploadPath;
        private string _PDFTatUrl;
        private string _PDFUploadPath;

        private string _PDFUrl;
        private bool _StatusTypeChanged;

        public TVPackage(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string PDFUrl {
            get {
                if (String.IsNullOrEmpty(PDF))
                    return String.Empty;
                else {
                    if (_PDFUrl == null) {
                        _PDFUrl = String.Format(@"{0}/{1}",
                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                        InternalDataContext.SiteId, "TVPackage", "PDF"), true,
                                                    _InternalDataContext.ShouldRemoveSchema), PDF);
                    }
                    return _PDFUrl;
                }
            }
        }

        public string PDFUploadPath {
            get {
                if (_PDFUploadPath == null) {
                    _PDFUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVPackage",
                                                                             "PDF"));
                }

                return (_PDFUploadPath);
            }
        }

        public string PDFEnglUrl {
            get {
                if (String.IsNullOrEmpty(PDFEngl))
                    return String.Empty;
                else {
                    if (_PDFEnglUrl == null) {
                        _PDFEnglUrl = String.Format(@"{0}/{1}",
                                                    InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                        InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                            InternalDataContext.SiteId, "TVPackage", "PDFEngl"), true,
                                                        _InternalDataContext.ShouldRemoveSchema), PDFEngl);
                    }
                    return _PDFEnglUrl;
                }
            }
        }

        public string PDFEnglUploadPath {
            get {
                if (_PDFEnglUploadPath == null) {
                    _PDFEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVPackage",
                                                                             "PDFEngl"));
                }

                return (_PDFEnglUploadPath);
            }
        }

        public string PDFTatUrl {
            get {
                if (String.IsNullOrEmpty(PDFTat))
                    return String.Empty;
                else {
                    if (_PDFTatUrl == null) {
                        _PDFTatUrl = String.Format(@"{0}/{1}",
                                                   InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                       InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                           InternalDataContext.SiteId, "TVPackage", "PDFTat"), true,
                                                       _InternalDataContext.ShouldRemoveSchema), PDFTat);
                    }
                    return _PDFTatUrl;
                }
            }
        }

        public string PDFTatUploadPath {
            get {
                if (_PDFTatUploadPath == null) {
                    _PDFTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVPackage",
                                                                             "PDFTat"));
                }

                return (_PDFTatUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "TVPackage", "FamilyIcon"),
                                                           true, _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVPackage",
                                                                             "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "TVPackage",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVPackage",
                                                                             "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "TVPackage",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVPackage",
                                                                             "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "TVPackage",
                                                                    "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVPackage",
                                                                             "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId, "TVPackage",
                                                                        "FamilyIconHoverEngl"), true,
                                                                    _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVPackage",
                                                                             "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId, "TVPackage",
                                                                       "FamilyIconHoverTat"), true,
                                                                   _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "TVPackage",
                                                                             "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public Double PriceExact {
            get { return (Price.HasValue) ? Price.Value : default(Double); }
        }

        public Double SubscriptionFeeExact {
            get { return (SubscriptionFee.HasValue) ? SubscriptionFee.Value : default(Double); }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        public Double TransferPriceExact {
            get { return (TransferPrice.HasValue) ? TransferPrice.Value : default(Double); }
        }

        public Boolean IsArchiveExact {
            get { return (IsArchive.HasValue) ? IsArchive.Value : default(Boolean); }
        }

        public Double MinPackagePaymentExact {
            get { return (MinPackagePayment.HasValue) ? MinPackagePayment.Value : default(Double); }
        }

        public Int32 MinTotalPackagesExact {
            get { return (MinTotalPackages.HasValue) ? MinTotalPackages.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _INAC = InternalDataContext.ReplacePlaceholders(_INAC);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _DiscountCondition = InternalDataContext.ReplacePlaceholders(_DiscountCondition);
            _EmptyPaymentReplacement = InternalDataContext.ReplacePlaceholders(_EmptyPaymentReplacement);
            _ChannelsCountReplacement = InternalDataContext.ReplacePlaceholders(_ChannelsCountReplacement);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ExternalRegionMapping : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ExternalRegionMapping(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _RegionCode = InternalDataContext.ReplacePlaceholders(_RegionCode);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ExternalRegionSystem : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ExternalRegionSystem(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SiteSetting : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SiteSetting(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TextValue = InternalDataContext.ReplacePlaceholders(_TextValue);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PhoneCode : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public PhoneCode(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 CodeExact {
            get { return (Code.HasValue) ? Code.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class QPItemDefinitionConstraint : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public QPItemDefinitionConstraint(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MarketingMobileTariff : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private string _IconUploadPath;
        private string _IconUrl;
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;
        private string _UpSaleIconUploadPath;
        private string _UpSaleIconUrl;

        public MarketingMobileTariff(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "MarketingMobileTariff", "Image"),
                                                      true, _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileTariff", "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string IconUrl {
            get {
                if (String.IsNullOrEmpty(Icon))
                    return String.Empty;
                else {
                    if (_IconUrl == null) {
                        _IconUrl = String.Format(@"{0}/{1}",
                                                 InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                     InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                         InternalDataContext.SiteId, "MarketingMobileTariff", "Icon"),
                                                     true, _InternalDataContext.ShouldRemoveSchema), Icon);
                    }
                    return _IconUrl;
                }
            }
        }

        public string IconUploadPath {
            get {
                if (_IconUploadPath == null) {
                    _IconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileTariff", "Icon"));
                }

                return (_IconUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingMobileTariff",
                                                               "FamilyIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileTariff", "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "MarketingMobileTariff",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileTariff", "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "MarketingMobileTariff",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileTariff", "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "MarketingMobileTariff",
                                                                    "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileTariff", "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId,
                                                                       "MarketingMobileTariff", "FamilyIconHoverTat"),
                                                                   true, _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileTariff",
                                                                             "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId,
                                                                        "MarketingMobileTariff", "FamilyIconHoverEngl"),
                                                                    true, _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileTariff",
                                                                             "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string UpSaleIconUrl {
            get {
                if (String.IsNullOrEmpty(UpSaleIcon))
                    return String.Empty;
                else {
                    if (_UpSaleIconUrl == null) {
                        _UpSaleIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingMobileTariff",
                                                               "UpSaleIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), UpSaleIcon);
                    }
                    return _UpSaleIconUrl;
                }
            }
        }

        public string UpSaleIconUploadPath {
            get {
                if (_UpSaleIconUploadPath == null) {
                    _UpSaleIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileTariff", "UpSaleIcon"));
                }

                return (_UpSaleIconUploadPath);
            }
        }

        public Boolean B2CExact {
            get { return (B2C.HasValue) ? B2C.Value : default(Boolean); }
        }

        public Boolean B2BExact {
            get { return (B2B.HasValue) ? B2B.Value : default(Boolean); }
        }

        public Boolean IsStandardInterCityTariffExact {
            get { return (IsStandardInterCityTariff.HasValue) ? IsStandardInterCityTariff.Value : default(Boolean); }
        }

        public Boolean MNPExact {
            get { return (MNP.HasValue) ? MNP.Value : default(Boolean); }
        }

        public Boolean IsShowNoteInsteadButtonExact {
            get { return (IsShowNoteInsteadButton.HasValue) ? IsShowNoteInsteadButton.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _HtmlTitle = InternalDataContext.ReplacePlaceholders(_HtmlTitle);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _HtmlTitleEngl = InternalDataContext.ReplacePlaceholders(_HtmlTitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _HtmlTitleTat = InternalDataContext.ReplacePlaceholders(_HtmlTitleTat);
            _PaymentSystem = InternalDataContext.ReplacePlaceholders(_PaymentSystem);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _Purpose = InternalDataContext.ReplacePlaceholders(_Purpose);
            _PurposeEngl = InternalDataContext.ReplacePlaceholders(_PurposeEngl);
            _PurposeTat = InternalDataContext.ReplacePlaceholders(_PurposeTat);
            _MetaKeywords = InternalDataContext.ReplacePlaceholders(_MetaKeywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
            _TitleForFamily = InternalDataContext.ReplacePlaceholders(_TitleForFamily);
            _TitleForFamilyEngl = InternalDataContext.ReplacePlaceholders(_TitleForFamilyEngl);
            _TitleForFamilyTat = InternalDataContext.ReplacePlaceholders(_TitleForFamilyTat);
            _CommentForFamily = InternalDataContext.ReplacePlaceholders(_CommentForFamily);
            _CommentForFamilyTat = InternalDataContext.ReplacePlaceholders(_CommentForFamilyTat);
            _CommentForFamilyEngl = InternalDataContext.ReplacePlaceholders(_CommentForFamilyEngl);
            _MobileDescription = InternalDataContext.ReplacePlaceholders(_MobileDescription);
            _MobileDescriptionEngl = InternalDataContext.ReplacePlaceholders(_MobileDescriptionEngl);
            _MobileDescriptionTat = InternalDataContext.ReplacePlaceholders(_MobileDescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TariffGuideQuestion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public TariffGuideQuestion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        public Boolean B2CExact {
            get { return (B2C.HasValue) ? B2C.Value : default(Boolean); }
        }

        public Boolean B2BExact {
            get { return (B2B.HasValue) ? B2B.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
            _ControlType = InternalDataContext.ReplacePlaceholders(_ControlType);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TariffGuideAnswer : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public TariffGuideAnswer(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        public Boolean IsDefaultExact {
            get { return (IsDefault.HasValue) ? IsDefault.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TariffGuideResult : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public TariffGuideResult(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _MarketingSegment = InternalDataContext.ReplacePlaceholders(_MarketingSegment);
            _Answers = InternalDataContext.ReplacePlaceholders(_Answers);
            _Tariffs = InternalDataContext.ReplacePlaceholders(_Tariffs);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MobileTariffParameterGroup : IQPContent {
        private string _IconUploadPath;
        private string _IconUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MobileTariffParameterGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string IconUrl {
            get {
                if (String.IsNullOrEmpty(Icon))
                    return String.Empty;
                else {
                    if (_IconUrl == null) {
                        _IconUrl = String.Format(@"{0}/{1}",
                                                 InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                     InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                         InternalDataContext.SiteId, "MobileTariffParameterGroup",
                                                         "Icon"), true, _InternalDataContext.ShouldRemoveSchema), Icon);
                    }
                    return _IconUrl;
                }
            }
        }

        public string IconUploadPath {
            get {
                if (_IconUploadPath == null) {
                    _IconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MobileTariffParameterGroup", "Icon"));
                }

                return (_IconUploadPath);
            }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        public Boolean IsComparableExact {
            get { return (IsComparable.HasValue) ? IsComparable.Value : default(Boolean); }
        }

        public Boolean PreOpenExact {
            get { return (PreOpen.HasValue) ? PreOpen.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _TitleDative = InternalDataContext.ReplacePlaceholders(_TitleDative);
            _TitleDativeEngl = InternalDataContext.ReplacePlaceholders(_TitleDativeEngl);
            _TitleDativeTat = InternalDataContext.ReplacePlaceholders(_TitleDativeTat);
            _GroupType = InternalDataContext.ReplacePlaceholders(_GroupType);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MarketingSign : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MarketingSign(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MarketingMobileService : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private string _IconUploadPath;
        private string _IconUrl;
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;
        private string _UpSaleIconUploadPath;
        private string _UpSaleIconUrl;

        public MarketingMobileService(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "MarketingMobileService", "Image"),
                                                      true, _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileService", "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string IconUrl {
            get {
                if (String.IsNullOrEmpty(Icon))
                    return String.Empty;
                else {
                    if (_IconUrl == null) {
                        _IconUrl = String.Format(@"{0}/{1}",
                                                 InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                     InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                         InternalDataContext.SiteId, "MarketingMobileService", "Icon"),
                                                     true, _InternalDataContext.ShouldRemoveSchema), Icon);
                    }
                    return _IconUrl;
                }
            }
        }

        public string IconUploadPath {
            get {
                if (_IconUploadPath == null) {
                    _IconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileService", "Icon"));
                }

                return (_IconUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingMobileService",
                                                               "FamilyIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileService", "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "MarketingMobileService",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileService", "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "MarketingMobileService",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileService", "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "MarketingMobileService",
                                                                    "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileService", "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId,
                                                                       "MarketingMobileService", "FamilyIconHoverTat"),
                                                                   true, _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileService",
                                                                             "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId,
                                                                        "MarketingMobileService", "FamilyIconHoverEngl"),
                                                                    true, _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileService",
                                                                             "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string UpSaleIconUrl {
            get {
                if (String.IsNullOrEmpty(UpSaleIcon))
                    return String.Empty;
                else {
                    if (_UpSaleIconUrl == null) {
                        _UpSaleIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingMobileService",
                                                               "UpSaleIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), UpSaleIcon);
                    }
                    return _UpSaleIconUrl;
                }
            }
        }

        public string UpSaleIconUploadPath {
            get {
                if (_UpSaleIconUploadPath == null) {
                    _UpSaleIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingMobileService", "UpSaleIcon"));
                }

                return (_UpSaleIconUploadPath);
            }
        }

        public Boolean B2CExact {
            get { return (B2C.HasValue) ? B2C.Value : default(Boolean); }
        }

        public Boolean B2BExact {
            get { return (B2B.HasValue) ? B2B.Value : default(Boolean); }
        }

        public Boolean TariffOptionExact {
            get { return (TariffOption.HasValue) ? TariffOption.Value : default(Boolean); }
        }

        public Double CallCenterPriceExact {
            get { return (CallCenterPrice.HasValue) ? CallCenterPrice.Value : default(Double); }
        }

        public Boolean ShowPriceInsteadFeeExact {
            get { return (ShowPriceInsteadFee.HasValue) ? ShowPriceInsteadFee.Value : default(Boolean); }
        }

        public Boolean OfferForIntercityCallsExact {
            get { return (OfferForIntercityCalls.HasValue) ? OfferForIntercityCalls.Value : default(Boolean); }
        }

        public Boolean PromoteForRoamingExact {
            get { return (PromoteForRoaming.HasValue) ? PromoteForRoaming.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _HtmlTitle = InternalDataContext.ReplacePlaceholders(_HtmlTitle);
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _HtmlTitleEngl = InternalDataContext.ReplacePlaceholders(_HtmlTitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _HtmlTitleTat = InternalDataContext.ReplacePlaceholders(_HtmlTitleTat);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _Purpose = InternalDataContext.ReplacePlaceholders(_Purpose);
            _PurposeEngl = InternalDataContext.ReplacePlaceholders(_PurposeEngl);
            _PurposeTat = InternalDataContext.ReplacePlaceholders(_PurposeTat);
            _Widget = InternalDataContext.ReplacePlaceholders(_Widget);
            _TitleForFamily = InternalDataContext.ReplacePlaceholders(_TitleForFamily);
            _TitleForFamilyEngl = InternalDataContext.ReplacePlaceholders(_TitleForFamilyEngl);
            _TitleForFamilyTat = InternalDataContext.ReplacePlaceholders(_TitleForFamilyTat);
            _MetaKeywords = InternalDataContext.ReplacePlaceholders(_MetaKeywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
            _ArchiveTariffs = InternalDataContext.ReplacePlaceholders(_ArchiveTariffs);
            _ArchiveTariffsEng = InternalDataContext.ReplacePlaceholders(_ArchiveTariffsEng);
            _ArchiveTariffsTat = InternalDataContext.ReplacePlaceholders(_ArchiveTariffsTat);
            _CommentForFamily = InternalDataContext.ReplacePlaceholders(_CommentForFamily);
            _CommentForFamilyEngl = InternalDataContext.ReplacePlaceholders(_CommentForFamilyEngl);
            _CommentForFamilyTat = InternalDataContext.ReplacePlaceholders(_CommentForFamilyTat);
            _MobileDescription = InternalDataContext.ReplacePlaceholders(_MobileDescription);
            _MobileDescriptionEngl = InternalDataContext.ReplacePlaceholders(_MobileDescriptionEngl);
            _MobileDescriptionTat = InternalDataContext.ReplacePlaceholders(_MobileDescriptionTat);
            _ShortBenefit = InternalDataContext.ReplacePlaceholders(_ShortBenefit);
            _Coverage = InternalDataContext.ReplacePlaceholders(_Coverage);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SubscriptionFeeType : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SubscriptionFeeType(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _TextForCheck = InternalDataContext.ReplacePlaceholders(_TextForCheck);
            _TextForCheckEngl = InternalDataContext.ReplacePlaceholders(_TextForCheckEngl);
            _TextForCheckTat = InternalDataContext.ReplacePlaceholders(_TextForCheckTat);
            _TextForBasket = InternalDataContext.ReplacePlaceholders(_TextForBasket);
            _TextForBasketEngl = InternalDataContext.ReplacePlaceholders(_TextForBasketEngl);
            _TextForBasketTat = InternalDataContext.ReplacePlaceholders(_TextForBasketTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MobileServiceParameterGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MobileServiceParameterGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        public Boolean PreOpenExact {
            get { return (PreOpen.HasValue) ? PreOpen.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _GroupType = InternalDataContext.ReplacePlaceholders(_GroupType);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MobileTariffFamily : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MobileTariffFamily(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _Signature = InternalDataContext.ReplacePlaceholders(_Signature);
            _SignatureEngl = InternalDataContext.ReplacePlaceholders(_SignatureEngl);
            _SignatureTat = InternalDataContext.ReplacePlaceholders(_SignatureTat);
            _TitleForTile = InternalDataContext.ReplacePlaceholders(_TitleForTile);
            _TitleForTileEngl = InternalDataContext.ReplacePlaceholders(_TitleForTileEngl);
            _TitleForTileTat = InternalDataContext.ReplacePlaceholders(_TitleForTileTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MutualMobileServiceGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MutualMobileServiceGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MobileServiceFamily : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MobileServiceFamily(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IsUseModernDesignExact {
            get { return (IsUseModernDesign.HasValue) ? IsUseModernDesign.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _Subtitle = InternalDataContext.ReplacePlaceholders(_Subtitle);
            _SubtitleEngl = InternalDataContext.ReplacePlaceholders(_SubtitleEngl);
            _SubtitleTat = InternalDataContext.ReplacePlaceholders(_SubtitleTat);
            _TitleForTile = InternalDataContext.ReplacePlaceholders(_TitleForTile);
            _TitleForTileEngl = InternalDataContext.ReplacePlaceholders(_TitleForTileEngl);
            _TitleForTileTat = InternalDataContext.ReplacePlaceholders(_TitleForTileTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MobileTariffCategory : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MobileTariffCategory(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        public Boolean IsMarkedExact {
            get { return (IsMarked.HasValue) ? IsMarked.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MobileServiceCategory : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MobileServiceCategory(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        public Boolean IsMarkedExact {
            get { return (IsMarked.HasValue) ? IsMarked.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ArchiveTariff : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ArchiveTariff(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _SOC = InternalDataContext.ReplacePlaceholders(_SOC);
            _Notes = InternalDataContext.ReplacePlaceholders(_Notes);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ArchiveTariffTab : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ArchiveTariffTab(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MobileServiceTab : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MobileServiceTab(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MobileParamsGroupTab : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MobileParamsGroupTab(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TariffParamGroupPriority : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public TariffParamGroupPriority(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ArchiveService : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ArchiveService(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IsActionExact {
            get { return (IsAction.HasValue) ? IsAction.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Note = InternalDataContext.ReplacePlaceholders(_Note);
            _Price = InternalDataContext.ReplacePlaceholders(_Price);
            _SOC = InternalDataContext.ReplacePlaceholders(_SOC);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ArchiveMobileServiceBookmark : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ArchiveMobileServiceBookmark(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _FullDescription = InternalDataContext.ReplacePlaceholders(_FullDescription);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PrivelegeAndBonus : IQPContent {
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;
        private string _TileImageUploadPath;
        private string _TileImageUrl;

        public PrivelegeAndBonus(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "PrivelegeAndBonus", "Image"),
                                                      true, _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "PrivelegeAndBonus", "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string TileImageUrl {
            get {
                if (String.IsNullOrEmpty(TileImage))
                    return String.Empty;
                else {
                    if (_TileImageUrl == null) {
                        _TileImageUrl = String.Format(@"{0}/{1}",
                                                      InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                          InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                              InternalDataContext.SiteId, "PrivelegeAndBonus",
                                                              "TileImage"), true,
                                                          _InternalDataContext.ShouldRemoveSchema), TileImage);
                    }
                    return _TileImageUrl;
                }
            }
        }

        public string TileImageUploadPath {
            get {
                if (_TileImageUploadPath == null) {
                    _TileImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "PrivelegeAndBonus", "TileImage"));
                }

                return (_TileImageUploadPath);
            }
        }

        public Int32 PriorityExact {
            get { return (Priority.HasValue) ? Priority.Value : default(Int32); }
        }

        public Boolean IsBlankExact {
            get { return (IsBlank.HasValue) ? IsBlank.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _Preview = InternalDataContext.ReplacePlaceholders(_Preview);
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _TileType = InternalDataContext.ReplacePlaceholders(_TileType);
            _SpecialConditions = InternalDataContext.ReplacePlaceholders(_SpecialConditions);
            _MetaKeywords = InternalDataContext.ReplacePlaceholders(_MetaKeywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
            _ProductLinks = InternalDataContext.ReplacePlaceholders(_ProductLinks);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _PreviewEngl = InternalDataContext.ReplacePlaceholders(_PreviewEngl);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _PreviewTat = InternalDataContext.ReplacePlaceholders(_PreviewTat);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
            _SpecialConditionsEngl = InternalDataContext.ReplacePlaceholders(_SpecialConditionsEngl);
            _SpecialConditionsTat = InternalDataContext.ReplacePlaceholders(_SpecialConditionsTat);
            _ProductLinksTat = InternalDataContext.ReplacePlaceholders(_ProductLinksTat);
            _ProductLinksEngl = InternalDataContext.ReplacePlaceholders(_ProductLinksEngl);
            _PageLayout = InternalDataContext.ReplacePlaceholders(_PageLayout);
            _ChildRelativePath = InternalDataContext.ReplacePlaceholders(_ChildRelativePath);
            _MobilePreview = InternalDataContext.ReplacePlaceholders(_MobilePreview);
            _MobilePreviewEngl = InternalDataContext.ReplacePlaceholders(_MobilePreviewEngl);
            _MobilePreviewTat = InternalDataContext.ReplacePlaceholders(_MobilePreviewTat);
            _MobileText = InternalDataContext.ReplacePlaceholders(_MobileText);
            _MobileTextEngl = InternalDataContext.ReplacePlaceholders(_MobileTextEngl);
            _MobileTextTat = InternalDataContext.ReplacePlaceholders(_MobileTextTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class FaqContent : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public FaqContent(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Question = InternalDataContext.ReplacePlaceholders(_Question);
            _Answer = InternalDataContext.ReplacePlaceholders(_Answer);
            _Urls = InternalDataContext.ReplacePlaceholders(_Urls);
            _FirstInUrls = InternalDataContext.ReplacePlaceholders(_FirstInUrls);
            _Notice = InternalDataContext.ReplacePlaceholders(_Notice);
            _QuestionEngl = InternalDataContext.ReplacePlaceholders(_QuestionEngl);
            _QuestionTat = InternalDataContext.ReplacePlaceholders(_QuestionTat);
            _AnswerEngl = InternalDataContext.ReplacePlaceholders(_AnswerEngl);
            _AnswerTat = InternalDataContext.ReplacePlaceholders(_AnswerTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class FaqGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public FaqGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SiteText : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SiteText(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TVChannelTheme : IQPContent {
        private string _IconUploadPath;
        private string _IconUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public TVChannelTheme(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string IconUrl {
            get {
                if (String.IsNullOrEmpty(Icon))
                    return String.Empty;
                else {
                    if (_IconUrl == null) {
                        _IconUrl = String.Format(@"{0}/{1}",
                                                 InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                     InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                         InternalDataContext.SiteId, "TVChannelTheme", "Icon"), true,
                                                     _InternalDataContext.ShouldRemoveSchema), Icon);
                    }
                    return _IconUrl;
                }
            }
        }

        public string IconUploadPath {
            get {
                if (_IconUploadPath == null) {
                    _IconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "TVChannelTheme", "Icon"));
                }

                return (_IconUploadPath);
            }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MarketingTVPackage : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;
        private string _TileIconUploadPath;
        private string _TileIconUrl;

        public MarketingTVPackage(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "MarketingTVPackage", "Image"),
                                                      true, _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingTVPackage", "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string TileIconUrl {
            get {
                if (String.IsNullOrEmpty(TileIcon))
                    return String.Empty;
                else {
                    if (_TileIconUrl == null) {
                        _TileIconUrl = String.Format(@"{0}/{1}",
                                                     InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                         InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                             InternalDataContext.SiteId, "MarketingTVPackage",
                                                             "TileIcon"), true, _InternalDataContext.ShouldRemoveSchema),
                                                     TileIcon);
                    }
                    return _TileIconUrl;
                }
            }
        }

        public string TileIconUploadPath {
            get {
                if (_TileIconUploadPath == null) {
                    _TileIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingTVPackage", "TileIcon"));
                }

                return (_TileIconUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingTVPackage",
                                                               "FamilyIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingTVPackage", "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "MarketingTVPackage",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingTVPackage", "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "MarketingTVPackage",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingTVPackage", "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "MarketingTVPackage",
                                                                    "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingTVPackage", "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId, "MarketingTVPackage",
                                                                        "FamilyIconHoverEngl"), true,
                                                                    _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingTVPackage", "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId, "MarketingTVPackage",
                                                                       "FamilyIconHoverTat"), true,
                                                                   _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingTVPackage", "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public Boolean IsArchiveExact {
            get { return (IsArchive.HasValue) ? IsArchive.Value : default(Boolean); }
        }

        public Boolean ShowInTileExact {
            get { return (ShowInTile.HasValue) ? ShowInTile.Value : default(Boolean); }
        }

        public Boolean IsTariffExact {
            get { return (IsTariff.HasValue) ? IsTariff.Value : default(Boolean); }
        }

        public Boolean IsPopularExact {
            get { return (IsPopular.HasValue) ? IsPopular.Value : default(Boolean); }
        }

        public Boolean HideInListExact {
            get { return (HideInList.HasValue) ? HideInList.Value : default(Boolean); }
        }

        public Boolean CollectableExact {
            get { return (Collectable.HasValue) ? Collectable.Value : default(Boolean); }
        }

        public Boolean DiscountedExact {
            get { return (Discounted.HasValue) ? Discounted.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _Purpose = InternalDataContext.ReplacePlaceholders(_Purpose);
            _PurposeEngl = InternalDataContext.ReplacePlaceholders(_PurposeEngl);
            _PurposeTat = InternalDataContext.ReplacePlaceholders(_PurposeTat);
            _MetaKeywords = InternalDataContext.ReplacePlaceholders(_MetaKeywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
            _MetaTags = InternalDataContext.ReplacePlaceholders(_MetaTags);
            _RemarksForFamily = InternalDataContext.ReplacePlaceholders(_RemarksForFamily);
            _RemarksForFamilyEngl = InternalDataContext.ReplacePlaceholders(_RemarksForFamilyEngl);
            _RemarksForFamilyTat = InternalDataContext.ReplacePlaceholders(_RemarksForFamilyTat);
            _TariffType = InternalDataContext.ReplacePlaceholders(_TariffType);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class QuickLinksTitle : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public QuickLinksTitle(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _PhraseText = InternalDataContext.ReplacePlaceholders(_PhraseText);
            _PhraseTextEngl = InternalDataContext.ReplacePlaceholders(_PhraseTextEngl);
            _PhraseTextTat = InternalDataContext.ReplacePlaceholders(_PhraseTextTat);
            _DefaultWord = InternalDataContext.ReplacePlaceholders(_DefaultWord);
            _DefaultWordEngl = InternalDataContext.ReplacePlaceholders(_DefaultWordEngl);
            _DefaultWordTat = InternalDataContext.ReplacePlaceholders(_DefaultWordTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class QuickLinksGroup : IQPContent {
        private string _IconUploadPath;
        private string _IconUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public QuickLinksGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string IconUrl {
            get {
                if (String.IsNullOrEmpty(Icon))
                    return String.Empty;
                else {
                    if (_IconUrl == null) {
                        _IconUrl = String.Format(@"{0}/{1}",
                                                 InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                     InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                         InternalDataContext.SiteId, "QuickLinksGroup", "Icon"), true,
                                                     _InternalDataContext.ShouldRemoveSchema), Icon);
                    }
                    return _IconUrl;
                }
            }
        }

        public string IconUploadPath {
            get {
                if (_IconUploadPath == null) {
                    _IconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "QuickLinksGroup", "Icon"));
                }

                return (_IconUploadPath);
            }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _UrlForTitle = InternalDataContext.ReplacePlaceholders(_UrlForTitle);
            _Keyword = InternalDataContext.ReplacePlaceholders(_Keyword);
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ProvodTariffParameterGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ProvodTariffParameterGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        public Boolean ExpandByDefaultExact {
            get { return (ExpandByDefault.HasValue) ? ExpandByDefault.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class InternetTariffFamily : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public InternetTariffFamily(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _TitleForTile = InternalDataContext.ReplacePlaceholders(_TitleForTile);
            _TitleForTileEngl = InternalDataContext.ReplacePlaceholders(_TitleForTileEngl);
            _TitleForTileTat = InternalDataContext.ReplacePlaceholders(_TitleForTileTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PhoneTariffFamily : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public PhoneTariffFamily(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _TitleForTile = InternalDataContext.ReplacePlaceholders(_TitleForTile);
            _TitleForTileEngl = InternalDataContext.ReplacePlaceholders(_TitleForTileEngl);
            _TitleForTileTat = InternalDataContext.ReplacePlaceholders(_TitleForTileTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ProvodServiceFamily : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ProvodServiceFamily(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _TextForSwitch = InternalDataContext.ReplacePlaceholders(_TextForSwitch);
            _TextForSwitchEngl = InternalDataContext.ReplacePlaceholders(_TextForSwitchEngl);
            _TextForSwitchTat = InternalDataContext.ReplacePlaceholders(_TextForSwitchTat);
            _TitleForTile = InternalDataContext.ReplacePlaceholders(_TitleForTile);
            _TitleForTileEngl = InternalDataContext.ReplacePlaceholders(_TitleForTileEngl);
            _TitleForTileTat = InternalDataContext.ReplacePlaceholders(_TitleForTileTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ProvodServiceParameterGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ProvodServiceParameterGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        public Boolean ExpandByDefaultExact {
            get { return (ExpandByDefault.HasValue) ? ExpandByDefault.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class InternetTariffCategory : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public InternetTariffCategory(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean CheckConnectionAvailableExact {
            get { return (CheckConnectionAvailable.HasValue) ? CheckConnectionAvailable.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PhoneTariffCategory : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public PhoneTariffCategory(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean CheckConnectionAvailableExact {
            get { return (CheckConnectionAvailable.HasValue) ? CheckConnectionAvailable.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ProvodServiceCategory : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ProvodServiceCategory(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean CheckConnectionAvailableExact {
            get { return (CheckConnectionAvailable.HasValue) ? CheckConnectionAvailable.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MarketingInternetTariff : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;
        private string _TileIconUploadPath;
        private string _TileIconUrl;
        private string _UpSaleIconUploadPath;
        private string _UpSaleIconUrl;

        public MarketingInternetTariff(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "MarketingInternetTariff", "Image"),
                                                      true, _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingInternetTariff", "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string TileIconUrl {
            get {
                if (String.IsNullOrEmpty(TileIcon))
                    return String.Empty;
                else {
                    if (_TileIconUrl == null) {
                        _TileIconUrl = String.Format(@"{0}/{1}",
                                                     InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                         InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                             InternalDataContext.SiteId, "MarketingInternetTariff",
                                                             "TileIcon"), true, _InternalDataContext.ShouldRemoveSchema),
                                                     TileIcon);
                    }
                    return _TileIconUrl;
                }
            }
        }

        public string TileIconUploadPath {
            get {
                if (_TileIconUploadPath == null) {
                    _TileIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingInternetTariff", "TileIcon"));
                }

                return (_TileIconUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingInternetTariff",
                                                               "FamilyIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingInternetTariff", "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "MarketingInternetTariff",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingInternetTariff", "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "MarketingInternetTariff",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingInternetTariff", "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId,
                                                                    "MarketingInternetTariff", "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingInternetTariff",
                                                                             "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId,
                                                                        "MarketingInternetTariff", "FamilyIconHoverEngl"),
                                                                    true, _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingInternetTariff",
                                                                             "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId,
                                                                       "MarketingInternetTariff", "FamilyIconHoverTat"),
                                                                   true, _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingInternetTariff",
                                                                             "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public string UpSaleIconUrl {
            get {
                if (String.IsNullOrEmpty(UpSaleIcon))
                    return String.Empty;
                else {
                    if (_UpSaleIconUrl == null) {
                        _UpSaleIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingInternetTariff",
                                                               "UpSaleIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), UpSaleIcon);
                    }
                    return _UpSaleIconUrl;
                }
            }
        }

        public string UpSaleIconUploadPath {
            get {
                if (_UpSaleIconUploadPath == null) {
                    _UpSaleIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingInternetTariff", "UpSaleIcon"));
                }

                return (_UpSaleIconUploadPath);
            }
        }

        public Boolean IsArchiveExact {
            get { return (IsArchive.HasValue) ? IsArchive.Value : default(Boolean); }
        }

        public Boolean ShowInTileExact {
            get { return (ShowInTile.HasValue) ? ShowInTile.Value : default(Boolean); }
        }

        public Boolean HideInListExact {
            get { return (HideInList.HasValue) ? HideInList.Value : default(Boolean); }
        }

        public Boolean AnnualContractExact {
            get { return (AnnualContract.HasValue) ? AnnualContract.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _Purpose = InternalDataContext.ReplacePlaceholders(_Purpose);
            _PurposeEngl = InternalDataContext.ReplacePlaceholders(_PurposeEngl);
            _PurposeTat = InternalDataContext.ReplacePlaceholders(_PurposeTat);
            _MetaKeywords = InternalDataContext.ReplacePlaceholders(_MetaKeywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
            _MetaTags = InternalDataContext.ReplacePlaceholders(_MetaTags);
            _RemarksForFamily = InternalDataContext.ReplacePlaceholders(_RemarksForFamily);
            _RemarksForFamilyEngl = InternalDataContext.ReplacePlaceholders(_RemarksForFamilyEngl);
            _RemarksForFamilyTat = InternalDataContext.ReplacePlaceholders(_RemarksForFamilyTat);
            _AnnualContractFeeType = InternalDataContext.ReplacePlaceholders(_AnnualContractFeeType);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MarketingPhoneTariff : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;
        private string _TileIconUploadPath;
        private string _TileIconUrl;

        public MarketingPhoneTariff(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "MarketingPhoneTariff", "Image"),
                                                      true, _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingPhoneTariff", "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string TileIconUrl {
            get {
                if (String.IsNullOrEmpty(TileIcon))
                    return String.Empty;
                else {
                    if (_TileIconUrl == null) {
                        _TileIconUrl = String.Format(@"{0}/{1}",
                                                     InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                         InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                             InternalDataContext.SiteId, "MarketingPhoneTariff",
                                                             "TileIcon"), true, _InternalDataContext.ShouldRemoveSchema),
                                                     TileIcon);
                    }
                    return _TileIconUrl;
                }
            }
        }

        public string TileIconUploadPath {
            get {
                if (_TileIconUploadPath == null) {
                    _TileIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingPhoneTariff", "TileIcon"));
                }

                return (_TileIconUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingPhoneTariff",
                                                               "FamilyIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingPhoneTariff", "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "MarketingPhoneTariff",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingPhoneTariff", "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "MarketingPhoneTariff",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingPhoneTariff", "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "MarketingPhoneTariff",
                                                                    "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingPhoneTariff", "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId,
                                                                        "MarketingPhoneTariff", "FamilyIconHoverEngl"),
                                                                    true, _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingPhoneTariff",
                                                                             "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId,
                                                                       "MarketingPhoneTariff", "FamilyIconHoverTat"),
                                                                   true, _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingPhoneTariff",
                                                                             "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public Boolean IsArchiveExact {
            get { return (IsArchive.HasValue) ? IsArchive.Value : default(Boolean); }
        }

        public Boolean ShowInTileExact {
            get { return (ShowInTile.HasValue) ? ShowInTile.Value : default(Boolean); }
        }

        public Boolean HideInListExact {
            get { return (HideInList.HasValue) ? HideInList.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _Purpose = InternalDataContext.ReplacePlaceholders(_Purpose);
            _PurposeEngl = InternalDataContext.ReplacePlaceholders(_PurposeEngl);
            _PurposeTat = InternalDataContext.ReplacePlaceholders(_PurposeTat);
            _MetaKeywords = InternalDataContext.ReplacePlaceholders(_MetaKeywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
            _MetaTags = InternalDataContext.ReplacePlaceholders(_MetaTags);
            _RemarksForFamily = InternalDataContext.ReplacePlaceholders(_RemarksForFamily);
            _RemarksForFamilyEngl = InternalDataContext.ReplacePlaceholders(_RemarksForFamilyEngl);
            _RemarksForFamilyTat = InternalDataContext.ReplacePlaceholders(_RemarksForFamilyTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MarketingProvodService : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;
        private string _TileIconUploadPath;
        private string _TileIconUrl;

        public MarketingProvodService(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "MarketingProvodService", "Image"),
                                                      true, _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodService", "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string TileIconUrl {
            get {
                if (String.IsNullOrEmpty(TileIcon))
                    return String.Empty;
                else {
                    if (_TileIconUrl == null) {
                        _TileIconUrl = String.Format(@"{0}/{1}",
                                                     InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                         InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                             InternalDataContext.SiteId, "MarketingProvodService",
                                                             "TileIcon"), true, _InternalDataContext.ShouldRemoveSchema),
                                                     TileIcon);
                    }
                    return _TileIconUrl;
                }
            }
        }

        public string TileIconUploadPath {
            get {
                if (_TileIconUploadPath == null) {
                    _TileIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodService", "TileIcon"));
                }

                return (_TileIconUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingProvodService",
                                                               "FamilyIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodService", "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "MarketingProvodService",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodService", "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "MarketingProvodService",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodService", "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "MarketingProvodService",
                                                                    "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodService", "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId,
                                                                        "MarketingProvodService", "FamilyIconHoverEngl"),
                                                                    true, _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodService",
                                                                             "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId,
                                                                       "MarketingProvodService", "FamilyIconHoverTat"),
                                                                   true, _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodService",
                                                                             "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public Boolean IsArchiveExact {
            get { return (IsArchive.HasValue) ? IsArchive.Value : default(Boolean); }
        }

        public Boolean ShowInTileExact {
            get { return (ShowInTile.HasValue) ? ShowInTile.Value : default(Boolean); }
        }

        public Boolean OfferInCartExact {
            get { return (OfferInCart.HasValue) ? OfferInCart.Value : default(Boolean); }
        }

        public Boolean OfferInCartForOneTvExact {
            get { return (OfferInCartForOneTv.HasValue) ? OfferInCartForOneTv.Value : default(Boolean); }
        }

        public Boolean OfferInCartForOneTvInKitsExact {
            get { return (OfferInCartForOneTvInKits.HasValue) ? OfferInCartForOneTvInKits.Value : default(Boolean); }
        }

        public Boolean OfferInCartForSeveralTvExact {
            get { return (OfferInCartForSeveralTv.HasValue) ? OfferInCartForSeveralTv.Value : default(Boolean); }
        }

        public Boolean OfferInCartForSeveralTvInKitsExact {
            get { return (OfferInCartForSeveralTvInKits.HasValue) ? OfferInCartForSeveralTvInKits.Value : default(Boolean); }
        }

        public Boolean IncludeInCartForSeveralTvExact {
            get { return (IncludeInCartForSeveralTv.HasValue) ? IncludeInCartForSeveralTv.Value : default(Boolean); }
        }

        public Boolean IncludeInCartForSeveralTvInKitsExact {
            get {
                return (IncludeInCartForSeveralTvInKits.HasValue)
                           ? IncludeInCartForSeveralTvInKits.Value
                           : default(Boolean);
            }
        }

        public Boolean IncludeInCartForTvTariffsWithoutInternetExact {
            get {
                return (IncludeInCartForTvTariffsWithoutInternet.HasValue)
                           ? IncludeInCartForTvTariffsWithoutInternet.Value
                           : default(Boolean);
            }
        }

        public Boolean IncludeInCartForTvTariffsWithoutInternetInKitsExact {
            get {
                return (IncludeInCartForTvTariffsWithoutInternetInKits.HasValue)
                           ? IncludeInCartForTvTariffsWithoutInternetInKits.Value
                           : default(Boolean);
            }
        }

        public Boolean OfferInCartForInternetTariffsExact {
            get { return (OfferInCartForInternetTariffs.HasValue) ? OfferInCartForInternetTariffs.Value : default(Boolean); }
        }

        public Boolean HideInListExact {
            get { return (HideInList.HasValue) ? HideInList.Value : default(Boolean); }
        }

        public Boolean AvailableInSelfServiceExact {
            get { return (AvailableInSelfService.HasValue) ? AvailableInSelfService.Value : default(Boolean); }
        }

        public Boolean DiscountsExpandExact {
            get { return (DiscountsExpand.HasValue) ? DiscountsExpand.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _Purpose = InternalDataContext.ReplacePlaceholders(_Purpose);
            _PurposeEngl = InternalDataContext.ReplacePlaceholders(_PurposeEngl);
            _PurposeTat = InternalDataContext.ReplacePlaceholders(_PurposeTat);
            _MetaKeywords = InternalDataContext.ReplacePlaceholders(_MetaKeywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
            _MetaTags = InternalDataContext.ReplacePlaceholders(_MetaTags);
            _CommentsForFamily = InternalDataContext.ReplacePlaceholders(_CommentsForFamily);
            _CommentsForFamilyEngl = InternalDataContext.ReplacePlaceholders(_CommentsForFamilyEngl);
            _CommentsForFamilyTat = InternalDataContext.ReplacePlaceholders(_CommentsForFamilyTat);
            _DiscountArray = InternalDataContext.ReplacePlaceholders(_DiscountArray);
            _ServiceType = InternalDataContext.ReplacePlaceholders(_ServiceType);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ServiceForIternetTariff : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ServiceForIternetTariff(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IncludedByDefaultExact {
            get { return (IncludedByDefault.HasValue) ? IncludedByDefault.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ServiceForPhoneTariff : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ServiceForPhoneTariff(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IncludedByDefaultExact {
            get { return (IncludedByDefault.HasValue) ? IncludedByDefault.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ProvodKitFamily : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ProvodKitFamily(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _TitleForTile = InternalDataContext.ReplacePlaceholders(_TitleForTile);
            _TitleForTileEngl = InternalDataContext.ReplacePlaceholders(_TitleForTileEngl);
            _TitleForTileTat = InternalDataContext.ReplacePlaceholders(_TitleForTileTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ProvodKitCategory : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ProvodKitCategory(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean CheckConnectionAvailableExact {
            get { return (CheckConnectionAvailable.HasValue) ? CheckConnectionAvailable.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MarketingProvodKit : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;
        private string _TileIconUploadPath;
        private string _TileIconUrl;

        public MarketingProvodKit(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "MarketingProvodKit", "Image"),
                                                      true, _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodKit", "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string TileIconUrl {
            get {
                if (String.IsNullOrEmpty(TileIcon))
                    return String.Empty;
                else {
                    if (_TileIconUrl == null) {
                        _TileIconUrl = String.Format(@"{0}/{1}",
                                                     InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                         InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                             InternalDataContext.SiteId, "MarketingProvodKit",
                                                             "TileIcon"), true, _InternalDataContext.ShouldRemoveSchema),
                                                     TileIcon);
                    }
                    return _TileIconUrl;
                }
            }
        }

        public string TileIconUploadPath {
            get {
                if (_TileIconUploadPath == null) {
                    _TileIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodKit", "TileIcon"));
                }

                return (_TileIconUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingProvodKit",
                                                               "FamilyIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodKit", "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "MarketingProvodKit",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodKit", "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "MarketingProvodKit",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodKit", "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "MarketingProvodKit",
                                                                    "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodKit", "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId, "MarketingProvodKit",
                                                                        "FamilyIconHoverEngl"), true,
                                                                    _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodKit", "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId, "MarketingProvodKit",
                                                                       "FamilyIconHoverTat"), true,
                                                                   _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingProvodKit", "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public Boolean IsArchiveExact {
            get { return (IsArchive.HasValue) ? IsArchive.Value : default(Boolean); }
        }

        public Boolean ShowInTileExact {
            get { return (ShowInTile.HasValue) ? ShowInTile.Value : default(Boolean); }
        }

        public Boolean HideMainTariffParametersExact {
            get { return (HideMainTariffParameters.HasValue) ? HideMainTariffParameters.Value : default(Boolean); }
        }

        public Boolean HideInListExact {
            get { return (HideInList.HasValue) ? HideInList.Value : default(Boolean); }
        }

        public Boolean AnnualContractExact {
            get { return (AnnualContract.HasValue) ? AnnualContract.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _Purpose = InternalDataContext.ReplacePlaceholders(_Purpose);
            _PurposeEngl = InternalDataContext.ReplacePlaceholders(_PurposeEngl);
            _PurposeTat = InternalDataContext.ReplacePlaceholders(_PurposeTat);
            _MetaKeywords = InternalDataContext.ReplacePlaceholders(_MetaKeywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
            _MetaTags = InternalDataContext.ReplacePlaceholders(_MetaTags);
            _CommentsForFamily = InternalDataContext.ReplacePlaceholders(_CommentsForFamily);
            _CommentsForFamilyEngl = InternalDataContext.ReplacePlaceholders(_CommentsForFamilyEngl);
            _CommentsForFamilyTat = InternalDataContext.ReplacePlaceholders(_CommentsForFamilyTat);
            _AnnualContractFeeType = InternalDataContext.ReplacePlaceholders(_AnnualContractFeeType);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ProvodKit : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private QPDataContext _InternalDataContext;
        private string _PDFEnglUploadPath;
        private string _PDFEnglUrl;
        private string _PDFTatUploadPath;
        private string _PDFTatUrl;
        private string _PDFUploadPath;

        private string _PDFUrl;
        private bool _StatusTypeChanged;

        public ProvodKit(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string PDFUrl {
            get {
                if (String.IsNullOrEmpty(PDF))
                    return String.Empty;
                else {
                    if (_PDFUrl == null) {
                        _PDFUrl = String.Format(@"{0}/{1}",
                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                        InternalDataContext.SiteId, "ProvodKit", "PDF"), true,
                                                    _InternalDataContext.ShouldRemoveSchema), PDF);
                    }
                    return _PDFUrl;
                }
            }
        }

        public string PDFUploadPath {
            get {
                if (_PDFUploadPath == null) {
                    _PDFUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodKit",
                                                                             "PDF"));
                }

                return (_PDFUploadPath);
            }
        }

        public string PDFEnglUrl {
            get {
                if (String.IsNullOrEmpty(PDFEngl))
                    return String.Empty;
                else {
                    if (_PDFEnglUrl == null) {
                        _PDFEnglUrl = String.Format(@"{0}/{1}",
                                                    InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                        InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                            InternalDataContext.SiteId, "ProvodKit", "PDFEngl"), true,
                                                        _InternalDataContext.ShouldRemoveSchema), PDFEngl);
                    }
                    return _PDFEnglUrl;
                }
            }
        }

        public string PDFEnglUploadPath {
            get {
                if (_PDFEnglUploadPath == null) {
                    _PDFEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodKit",
                                                                             "PDFEngl"));
                }

                return (_PDFEnglUploadPath);
            }
        }

        public string PDFTatUrl {
            get {
                if (String.IsNullOrEmpty(PDFTat))
                    return String.Empty;
                else {
                    if (_PDFTatUrl == null) {
                        _PDFTatUrl = String.Format(@"{0}/{1}",
                                                   InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                       InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                           InternalDataContext.SiteId, "ProvodKit", "PDFTat"), true,
                                                       _InternalDataContext.ShouldRemoveSchema), PDFTat);
                    }
                    return _PDFTatUrl;
                }
            }
        }

        public string PDFTatUploadPath {
            get {
                if (_PDFTatUploadPath == null) {
                    _PDFTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodKit",
                                                                             "PDFTat"));
                }

                return (_PDFTatUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "ProvodKit", "FamilyIcon"),
                                                           true, _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodKit",
                                                                             "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "ProvodKit",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodKit",
                                                                             "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "ProvodKit",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodKit",
                                                                             "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "ProvodKit",
                                                                    "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodKit",
                                                                             "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId, "ProvodKit",
                                                                        "FamilyIconHoverEngl"), true,
                                                                    _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodKit",
                                                                             "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId, "ProvodKit",
                                                                       "FamilyIconHoverTat"), true,
                                                                   _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodKit",
                                                                             "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        public Double PriceExact {
            get { return (Price.HasValue) ? Price.Value : default(Double); }
        }

        public Double SubscriptionFeeExact {
            get { return (SubscriptionFee.HasValue) ? SubscriptionFee.Value : default(Double); }
        }

        public Boolean IsArchiveExact {
            get { return (IsArchive.HasValue) ? IsArchive.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _MainParameterText = InternalDataContext.ReplacePlaceholders(_MainParameterText);
            _MainParameterValue = InternalDataContext.ReplacePlaceholders(_MainParameterValue);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _MainParameterTextEngl = InternalDataContext.ReplacePlaceholders(_MainParameterTextEngl);
            _MainParameterValueEngl = InternalDataContext.ReplacePlaceholders(_MainParameterValueEngl);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _MainParameterTextTat = InternalDataContext.ReplacePlaceholders(_MainParameterTextTat);
            _MainParameterValueTat = InternalDataContext.ReplacePlaceholders(_MainParameterValueTat);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _INACID = InternalDataContext.ReplacePlaceholders(_INACID);
            _InacIdArray = InternalDataContext.ReplacePlaceholders(_InacIdArray);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class LocalRoamingOperator : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public LocalRoamingOperator(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RoamingTariffParam : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RoamingTariffParam(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Group = InternalDataContext.ReplacePlaceholders(_Group);
            _Value = InternalDataContext.ReplacePlaceholders(_Value);
            _ValueEngl = InternalDataContext.ReplacePlaceholders(_ValueEngl);
            _ValueTat = InternalDataContext.ReplacePlaceholders(_ValueTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TVPackageFamily : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public TVPackageFamily(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _TitleForTile = InternalDataContext.ReplacePlaceholders(_TitleForTile);
            _TitleForTileEngl = InternalDataContext.ReplacePlaceholders(_TitleForTileEngl);
            _TitleForTileTat = InternalDataContext.ReplacePlaceholders(_TitleForTileTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SocialNetwork : IQPContent {
        private string _IconHoverUploadPath;
        private string _IconHoverUrl;
        private string _IconUploadPath;
        private string _IconUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SocialNetwork(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string IconUrl {
            get {
                if (String.IsNullOrEmpty(Icon))
                    return String.Empty;
                else {
                    if (_IconUrl == null) {
                        _IconUrl = String.Format(@"{0}/{1}",
                                                 InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                     InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                         InternalDataContext.SiteId, "SocialNetwork", "Icon"), true,
                                                     _InternalDataContext.ShouldRemoveSchema), Icon);
                    }
                    return _IconUrl;
                }
            }
        }

        public string IconUploadPath {
            get {
                if (_IconUploadPath == null) {
                    _IconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "SocialNetwork",
                                                                             "Icon"));
                }

                return (_IconUploadPath);
            }
        }

        public string IconHoverUrl {
            get {
                if (String.IsNullOrEmpty(IconHover))
                    return String.Empty;
                else {
                    if (_IconHoverUrl == null) {
                        _IconHoverUrl = String.Format(@"{0}/{1}",
                                                      InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                          InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                              InternalDataContext.SiteId, "SocialNetwork", "IconHover"),
                                                          true, _InternalDataContext.ShouldRemoveSchema), IconHover);
                    }
                    return _IconHoverUrl;
                }
            }
        }

        public string IconHoverUploadPath {
            get {
                if (_IconHoverUploadPath == null) {
                    _IconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "SocialNetwork",
                                                                             "IconHover"));
                }

                return (_IconHoverUploadPath);
            }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Url = InternalDataContext.ReplacePlaceholders(_Url);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class HelpDeviceType : IQPContent {
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public HelpDeviceType(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "HelpDeviceType", "Image"), true,
                                                      _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "HelpDeviceType", "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class HelpCenterParam : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public HelpCenterParam(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _ShortNumber = InternalDataContext.ReplacePlaceholders(_ShortNumber);
            _CityNumber = InternalDataContext.ReplacePlaceholders(_CityNumber);
            _Email = InternalDataContext.ReplacePlaceholders(_Email);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TVChannelRegion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public TVChannelRegion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PhoneAsModemTab : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public PhoneAsModemTab(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PhoneAsModemInterface : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public PhoneAsModemInterface(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 IndexExact {
            get { return (Index.HasValue) ? Index.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class OperatingSystem : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public OperatingSystem(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 IndexExact {
            get { return (Index.HasValue) ? Index.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class CpaPartner : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public CpaPartner(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEng = InternalDataContext.ReplacePlaceholders(_TitleEng);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Phones = InternalDataContext.ReplacePlaceholders(_Phones);
            _Email = InternalDataContext.ReplacePlaceholders(_Email);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PhoneAsModemInstruction : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public PhoneAsModemInstruction(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 IndexExact {
            get { return (Index.HasValue) ? Index.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class CpaShortNumber : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public CpaShortNumber(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Number = InternalDataContext.ReplacePlaceholders(_Number);
            _Type = InternalDataContext.ReplacePlaceholders(_Type);
            _Tariff = InternalDataContext.ReplacePlaceholders(_Tariff);
            _TariffEng = InternalDataContext.ReplacePlaceholders(_TariffEng);
            _TariffTat = InternalDataContext.ReplacePlaceholders(_TariffTat);
            _Disconnect = InternalDataContext.ReplacePlaceholders(_Disconnect);
            _DisconnectEng = InternalDataContext.ReplacePlaceholders(_DisconnectEng);
            _DisconnectTat = InternalDataContext.ReplacePlaceholders(_DisconnectTat);
            _Service = InternalDataContext.ReplacePlaceholders(_Service);
            _ServiceEng = InternalDataContext.ReplacePlaceholders(_ServiceEng);
            _ServiceTat = InternalDataContext.ReplacePlaceholders(_ServiceTat);
            _AreaOfService = InternalDataContext.ReplacePlaceholders(_AreaOfService);
            _AreaOfServiceEng = InternalDataContext.ReplacePlaceholders(_AreaOfServiceEng);
            _AreaOfServiceTat = InternalDataContext.ReplacePlaceholders(_AreaOfServiceTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ChangeNumberErrorText : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ChangeNumberErrorText(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 ServiceErrorCodeExact {
            get { return (ServiceErrorCode.HasValue) ? ServiceErrorCode.Value : default(Int32); }
        }

        public Boolean ServiceUnavailableExact {
            get { return (ServiceUnavailable.HasValue) ? ServiceUnavailable.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _ErrorText = InternalDataContext.ReplacePlaceholders(_ErrorText);
            _ErrorTextEngl = InternalDataContext.ReplacePlaceholders(_ErrorTextEngl);
            _ErrorTextTat = InternalDataContext.ReplacePlaceholders(_ErrorTextTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class EquipmentParamsTab : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public EquipmentParamsTab(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class EquipmentTab : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public EquipmentTab(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class EquipmentType : IQPContent {
        private string _DefaultImageUploadPath;
        private string _DefaultImageUrl;
        private string _ImageUploadPath;
        private string _ImageUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public EquipmentType(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageUrl {
            get {
                if (String.IsNullOrEmpty(Image))
                    return String.Empty;
                else {
                    if (_ImageUrl == null) {
                        _ImageUrl = String.Format(@"{0}/{1}",
                                                  InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                      InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                          InternalDataContext.SiteId, "EquipmentType", "Image"), true,
                                                      _InternalDataContext.ShouldRemoveSchema), Image);
                    }
                    return _ImageUrl;
                }
            }
        }

        public string ImageUploadPath {
            get {
                if (_ImageUploadPath == null) {
                    _ImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "EquipmentType",
                                                                             "Image"));
                }

                return (_ImageUploadPath);
            }
        }

        public string DefaultImageUrl {
            get {
                if (String.IsNullOrEmpty(DefaultImage))
                    return String.Empty;
                else {
                    if (_DefaultImageUrl == null) {
                        _DefaultImageUrl = String.Format(@"{0}/{1}",
                                                         InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                             InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                 InternalDataContext.SiteId, "EquipmentType",
                                                                 "DefaultImage"), true,
                                                             _InternalDataContext.ShouldRemoveSchema), DefaultImage);
                    }
                    return _DefaultImageUrl;
                }
            }
        }

        public string DefaultImageUploadPath {
            get {
                if (_DefaultImageUploadPath == null) {
                    _DefaultImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "EquipmentType",
                                                                             "DefaultImage"));
                }

                return (_DefaultImageUploadPath);
            }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        public Boolean MaySortListExact {
            get { return (MaySortList.HasValue) ? MaySortList.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _StillTitle1 = InternalDataContext.ReplacePlaceholders(_StillTitle1);
            _StillTitle2 = InternalDataContext.ReplacePlaceholders(_StillTitle2);
            _StillTitle3 = InternalDataContext.ReplacePlaceholders(_StillTitle3);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class EquipmentCategory : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public EquipmentCategory(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        public Boolean MarkedExact {
            get { return (Marked.HasValue) ? Marked.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class EquipmentParam : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public EquipmentParam(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean ShowInTilesExact {
            get { return (ShowInTiles.HasValue) ? ShowInTiles.Value : default(Boolean); }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Value = InternalDataContext.ReplacePlaceholders(_Value);
            _ValueEngl = InternalDataContext.ReplacePlaceholders(_ValueEngl);
            _ValueTat = InternalDataContext.ReplacePlaceholders(_ValueTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class EquipmentParamsGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public EquipmentParamsGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        public Boolean IsPriceGroupExact {
            get { return (IsPriceGroup.HasValue) ? IsPriceGroup.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class Equipment : IQPContent {
        private QPDataContext _InternalDataContext;
        private string _PdfFileEnglUploadPath;
        private string _PdfFileEnglUrl;
        private string _PdfFileTatUploadPath;
        private string _PdfFileTatUrl;
        private string _PdfFileUploadPath;

        private string _PdfFileUrl;
        private bool _StatusTypeChanged;

        public Equipment(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string PdfFileUrl {
            get {
                if (String.IsNullOrEmpty(PdfFile))
                    return String.Empty;
                else {
                    if (_PdfFileUrl == null) {
                        _PdfFileUrl = String.Format(@"{0}/{1}",
                                                    InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                        InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                            InternalDataContext.SiteId, "Equipment", "PdfFile"), true,
                                                        _InternalDataContext.ShouldRemoveSchema), PdfFile);
                    }
                    return _PdfFileUrl;
                }
            }
        }

        public string PdfFileUploadPath {
            get {
                if (_PdfFileUploadPath == null) {
                    _PdfFileUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "Equipment",
                                                                             "PdfFile"));
                }

                return (_PdfFileUploadPath);
            }
        }

        public string PdfFileEnglUrl {
            get {
                if (String.IsNullOrEmpty(PdfFileEngl))
                    return String.Empty;
                else {
                    if (_PdfFileEnglUrl == null) {
                        _PdfFileEnglUrl = String.Format(@"{0}/{1}",
                                                        InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                            InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                InternalDataContext.SiteId, "Equipment", "PdfFileEngl"),
                                                            true, _InternalDataContext.ShouldRemoveSchema), PdfFileEngl);
                    }
                    return _PdfFileEnglUrl;
                }
            }
        }

        public string PdfFileEnglUploadPath {
            get {
                if (_PdfFileEnglUploadPath == null) {
                    _PdfFileEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "Equipment",
                                                                             "PdfFileEngl"));
                }

                return (_PdfFileEnglUploadPath);
            }
        }

        public string PdfFileTatUrl {
            get {
                if (String.IsNullOrEmpty(PdfFileTat))
                    return String.Empty;
                else {
                    if (_PdfFileTatUrl == null) {
                        _PdfFileTatUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "Equipment", "PdfFileTat"),
                                                           true, _InternalDataContext.ShouldRemoveSchema), PdfFileTat);
                    }
                    return _PdfFileTatUrl;
                }
            }
        }

        public string PdfFileTatUploadPath {
            get {
                if (_PdfFileTatUploadPath == null) {
                    _PdfFileTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "Equipment",
                                                                             "PdfFileTat"));
                }

                return (_PdfFileTatUploadPath);
            }
        }

        public Int32 PriceExact {
            get { return (Price.HasValue) ? Price.Value : default(Int32); }
        }

        public Boolean CreditEnabledExact {
            get { return (CreditEnabled.HasValue) ? CreditEnabled.Value : default(Boolean); }
        }

        public Boolean IsArchivedExact {
            get { return (IsArchived.HasValue) ? IsArchived.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MarketingEquipment : IQPContent {
        private string _ApplicationIconUploadPath;
        private string _ApplicationIconUrl;
        private string _FileToDownloadUploadPath;
        private string _FileToDownloadUrl;
        private QPDataContext _InternalDataContext;
        private string _PdfFileEnglUploadPath;
        private string _PdfFileEnglUrl;
        private string _PdfFileTatUploadPath;
        private string _PdfFileTatUrl;
        private string _PdfFileUploadPath;
        private string _PdfFileUrl;
        private bool _StatusTypeChanged;
        private string _TileImageUploadPath;

        private string _TileImageUrl;
        private string _UpSaleIconUploadPath;
        private string _UpSaleIconUrl;

        public MarketingEquipment(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string TileImageUrl {
            get {
                if (String.IsNullOrEmpty(TileImage))
                    return String.Empty;
                else {
                    if (_TileImageUrl == null) {
                        _TileImageUrl = String.Format(@"{0}/{1}",
                                                      InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                          InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                              InternalDataContext.SiteId, "MarketingEquipment",
                                                              "TileImage"), true,
                                                          _InternalDataContext.ShouldRemoveSchema), TileImage);
                    }
                    return _TileImageUrl;
                }
            }
        }

        public string TileImageUploadPath {
            get {
                if (_TileImageUploadPath == null) {
                    _TileImageUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingEquipment", "TileImage"));
                }

                return (_TileImageUploadPath);
            }
        }

        public string ApplicationIconUrl {
            get {
                if (String.IsNullOrEmpty(ApplicationIcon))
                    return String.Empty;
                else {
                    if (_ApplicationIconUrl == null) {
                        _ApplicationIconUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "MarketingEquipment",
                                                                    "ApplicationIcon"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            ApplicationIcon);
                    }
                    return _ApplicationIconUrl;
                }
            }
        }

        public string ApplicationIconUploadPath {
            get {
                if (_ApplicationIconUploadPath == null) {
                    _ApplicationIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingEquipment", "ApplicationIcon"));
                }

                return (_ApplicationIconUploadPath);
            }
        }

        public string FileToDownloadUrl {
            get {
                if (String.IsNullOrEmpty(FileToDownload))
                    return String.Empty;
                else {
                    if (_FileToDownloadUrl == null) {
                        _FileToDownloadUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "MarketingEquipment",
                                                                   "FileToDownload"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FileToDownload);
                    }
                    return _FileToDownloadUrl;
                }
            }
        }

        public string FileToDownloadUploadPath {
            get {
                if (_FileToDownloadUploadPath == null) {
                    _FileToDownloadUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingEquipment", "FileToDownload"));
                }

                return (_FileToDownloadUploadPath);
            }
        }

        public string PdfFileUrl {
            get {
                if (String.IsNullOrEmpty(PdfFile))
                    return String.Empty;
                else {
                    if (_PdfFileUrl == null) {
                        _PdfFileUrl = String.Format(@"{0}/{1}",
                                                    InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                        InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                            InternalDataContext.SiteId, "MarketingEquipment", "PdfFile"),
                                                        true, _InternalDataContext.ShouldRemoveSchema), PdfFile);
                    }
                    return _PdfFileUrl;
                }
            }
        }

        public string PdfFileUploadPath {
            get {
                if (_PdfFileUploadPath == null) {
                    _PdfFileUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingEquipment", "PdfFile"));
                }

                return (_PdfFileUploadPath);
            }
        }

        public string PdfFileEnglUrl {
            get {
                if (String.IsNullOrEmpty(PdfFileEngl))
                    return String.Empty;
                else {
                    if (_PdfFileEnglUrl == null) {
                        _PdfFileEnglUrl = String.Format(@"{0}/{1}",
                                                        InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                            InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                InternalDataContext.SiteId, "MarketingEquipment",
                                                                "PdfFileEngl"), true,
                                                            _InternalDataContext.ShouldRemoveSchema), PdfFileEngl);
                    }
                    return _PdfFileEnglUrl;
                }
            }
        }

        public string PdfFileEnglUploadPath {
            get {
                if (_PdfFileEnglUploadPath == null) {
                    _PdfFileEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingEquipment", "PdfFileEngl"));
                }

                return (_PdfFileEnglUploadPath);
            }
        }

        public string PdfFileTatUrl {
            get {
                if (String.IsNullOrEmpty(PdfFileTat))
                    return String.Empty;
                else {
                    if (_PdfFileTatUrl == null) {
                        _PdfFileTatUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingEquipment",
                                                               "PdfFileTat"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), PdfFileTat);
                    }
                    return _PdfFileTatUrl;
                }
            }
        }

        public string PdfFileTatUploadPath {
            get {
                if (_PdfFileTatUploadPath == null) {
                    _PdfFileTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingEquipment", "PdfFileTat"));
                }

                return (_PdfFileTatUploadPath);
            }
        }

        public string UpSaleIconUrl {
            get {
                if (String.IsNullOrEmpty(UpSaleIcon))
                    return String.Empty;
                else {
                    if (_UpSaleIconUrl == null) {
                        _UpSaleIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "MarketingEquipment",
                                                               "UpSaleIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), UpSaleIcon);
                    }
                    return _UpSaleIconUrl;
                }
            }
        }

        public string UpSaleIconUploadPath {
            get {
                if (_UpSaleIconUploadPath == null) {
                    _UpSaleIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MarketingEquipment", "UpSaleIcon"));
                }

                return (_UpSaleIconUploadPath);
            }
        }

        public Int32 PriceExact {
            get { return (Price.HasValue) ? Price.Value : default(Int32); }
        }

        public Boolean IsApplicationExact {
            get { return (IsApplication.HasValue) ? IsApplication.Value : default(Boolean); }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        public Boolean IsArchivedExact {
            get { return (IsArchived.HasValue) ? IsArchived.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _AppStoreLink = InternalDataContext.ReplacePlaceholders(_AppStoreLink);
            _GooglePlayLink = InternalDataContext.ReplacePlaceholders(_GooglePlayLink);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _Destination = InternalDataContext.ReplacePlaceholders(_Destination);
            _DestinationEngl = InternalDataContext.ReplacePlaceholders(_DestinationEngl);
            _DestinationTat = InternalDataContext.ReplacePlaceholders(_DestinationTat);
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
            _MetaKeywords = InternalDataContext.ReplacePlaceholders(_MetaKeywords);
            _MetaDescription = InternalDataContext.ReplacePlaceholders(_MetaDescription);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class EquipmentImage : IQPContent {
        private string _ImageFileUploadPath;
        private string _ImageFileUrl;
        private QPDataContext _InternalDataContext;
        private string _PreviewFileUploadPath;
        private string _PreviewFileUrl;
        private bool _StatusTypeChanged;

        public EquipmentImage(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string ImageFileUrl {
            get {
                if (String.IsNullOrEmpty(ImageFile))
                    return String.Empty;
                else {
                    if (_ImageFileUrl == null) {
                        _ImageFileUrl = String.Format(@"{0}/{1}",
                                                      InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                          InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                              InternalDataContext.SiteId, "EquipmentImage", "ImageFile"),
                                                          true, _InternalDataContext.ShouldRemoveSchema), ImageFile);
                    }
                    return _ImageFileUrl;
                }
            }
        }

        public string ImageFileUploadPath {
            get {
                if (_ImageFileUploadPath == null) {
                    _ImageFileUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "EquipmentImage", "ImageFile"));
                }

                return (_ImageFileUploadPath);
            }
        }

        public string PreviewFileUrl {
            get {
                if (String.IsNullOrEmpty(PreviewFile))
                    return String.Empty;
                else {
                    if (_PreviewFileUrl == null) {
                        _PreviewFileUrl = String.Format(@"{0}/{1}",
                                                        InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                            InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                InternalDataContext.SiteId, "EquipmentImage",
                                                                "PreviewFile"), true,
                                                            _InternalDataContext.ShouldRemoveSchema), PreviewFile);
                    }
                    return _PreviewFileUrl;
                }
            }
        }

        public string PreviewFileUploadPath {
            get {
                if (_PreviewFileUploadPath == null) {
                    _PreviewFileUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "EquipmentImage", "PreviewFile"));
                }

                return (_PreviewFileUploadPath);
            }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PaymentServiceFilter : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public PaymentServiceFilter(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean HasPaymentFormExact {
            get { return (HasPaymentForm.HasValue) ? HasPaymentForm.Value : default(Boolean); }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _UrlTitle = InternalDataContext.ReplacePlaceholders(_UrlTitle);
            _ServiceBlockText = InternalDataContext.ReplacePlaceholders(_ServiceBlockText);
            _ServiceBlockTextEngl = InternalDataContext.ReplacePlaceholders(_ServiceBlockTextEngl);
            _ServiceBlockTextTat = InternalDataContext.ReplacePlaceholders(_ServiceBlockTextTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class INACParamType : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public INACParamType(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class InternetTariff : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private QPDataContext _InternalDataContext;
        private string _PDFEnglUploadPath;
        private string _PDFEnglUrl;
        private string _PDFTatUploadPath;
        private string _PDFTatUrl;
        private string _PDFUploadPath;

        private string _PDFUrl;
        private bool _StatusTypeChanged;

        public InternetTariff(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string PDFUrl {
            get {
                if (String.IsNullOrEmpty(PDF))
                    return String.Empty;
                else {
                    if (_PDFUrl == null) {
                        _PDFUrl = String.Format(@"{0}/{1}",
                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                        InternalDataContext.SiteId, "InternetTariff", "PDF"), true,
                                                    _InternalDataContext.ShouldRemoveSchema), PDF);
                    }
                    return _PDFUrl;
                }
            }
        }

        public string PDFUploadPath {
            get {
                if (_PDFUploadPath == null) {
                    _PDFUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "InternetTariff", "PDF"));
                }

                return (_PDFUploadPath);
            }
        }

        public string PDFEnglUrl {
            get {
                if (String.IsNullOrEmpty(PDFEngl))
                    return String.Empty;
                else {
                    if (_PDFEnglUrl == null) {
                        _PDFEnglUrl = String.Format(@"{0}/{1}",
                                                    InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                        InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                            InternalDataContext.SiteId, "InternetTariff", "PDFEngl"),
                                                        true, _InternalDataContext.ShouldRemoveSchema), PDFEngl);
                    }
                    return _PDFEnglUrl;
                }
            }
        }

        public string PDFEnglUploadPath {
            get {
                if (_PDFEnglUploadPath == null) {
                    _PDFEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "InternetTariff", "PDFEngl"));
                }

                return (_PDFEnglUploadPath);
            }
        }

        public string PDFTatUrl {
            get {
                if (String.IsNullOrEmpty(PDFTat))
                    return String.Empty;
                else {
                    if (_PDFTatUrl == null) {
                        _PDFTatUrl = String.Format(@"{0}/{1}",
                                                   InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                       InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                           InternalDataContext.SiteId, "InternetTariff", "PDFTat"), true,
                                                       _InternalDataContext.ShouldRemoveSchema), PDFTat);
                    }
                    return _PDFTatUrl;
                }
            }
        }

        public string PDFTatUploadPath {
            get {
                if (_PDFTatUploadPath == null) {
                    _PDFTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "InternetTariff", "PDFTat"));
                }

                return (_PDFTatUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "InternetTariff",
                                                               "FamilyIcon"), true,
                                                           _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "InternetTariff", "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "InternetTariff",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "InternetTariff", "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "InternetTariff",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "InternetTariff", "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "InternetTariff",
                                                                    "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "InternetTariff", "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId, "InternetTariff",
                                                                        "FamilyIconHoverEngl"), true,
                                                                    _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "InternetTariff", "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId, "InternetTariff",
                                                                       "FamilyIconHoverTat"), true,
                                                                   _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "InternetTariff", "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public Double PriceExact {
            get { return (Price.HasValue) ? Price.Value : default(Double); }
        }

        public Double TransferPriceExact {
            get { return (TransferPrice.HasValue) ? TransferPrice.Value : default(Double); }
        }

        public Double SubscriptionFeeExact {
            get { return (SubscriptionFee.HasValue) ? SubscriptionFee.Value : default(Double); }
        }

        public Boolean IsArchiveExact {
            get { return (IsArchive.HasValue) ? IsArchive.Value : default(Boolean); }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _INACID = InternalDataContext.ReplacePlaceholders(_INACID);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class InternetTariffParam : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public InternetTariffParam(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean MainInCardExact {
            get { return (MainInCard.HasValue) ? MainInCard.Value : default(Boolean); }
        }

        public Boolean ShowInTileExact {
            get { return (ShowInTile.HasValue) ? ShowInTile.Value : default(Boolean); }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _Value = InternalDataContext.ReplacePlaceholders(_Value);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
            _ValueEngl = InternalDataContext.ReplacePlaceholders(_ValueEngl);
            _ValueTat = InternalDataContext.ReplacePlaceholders(_ValueTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PhoneTariff : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private QPDataContext _InternalDataContext;
        private string _PDFEnglUploadPath;
        private string _PDFEnglUrl;
        private string _PDFTatUploadPath;
        private string _PDFTatUrl;
        private string _PDFUploadPath;

        private string _PDFUrl;
        private bool _StatusTypeChanged;

        public PhoneTariff(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string PDFUrl {
            get {
                if (String.IsNullOrEmpty(PDF))
                    return String.Empty;
                else {
                    if (_PDFUrl == null) {
                        _PDFUrl = String.Format(@"{0}/{1}",
                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                        InternalDataContext.SiteId, "PhoneTariff", "PDF"), true,
                                                    _InternalDataContext.ShouldRemoveSchema), PDF);
                    }
                    return _PDFUrl;
                }
            }
        }

        public string PDFUploadPath {
            get {
                if (_PDFUploadPath == null) {
                    _PDFUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "PhoneTariff",
                                                                             "PDF"));
                }

                return (_PDFUploadPath);
            }
        }

        public string PDFEnglUrl {
            get {
                if (String.IsNullOrEmpty(PDFEngl))
                    return String.Empty;
                else {
                    if (_PDFEnglUrl == null) {
                        _PDFEnglUrl = String.Format(@"{0}/{1}",
                                                    InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                        InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                            InternalDataContext.SiteId, "PhoneTariff", "PDFEngl"), true,
                                                        _InternalDataContext.ShouldRemoveSchema), PDFEngl);
                    }
                    return _PDFEnglUrl;
                }
            }
        }

        public string PDFEnglUploadPath {
            get {
                if (_PDFEnglUploadPath == null) {
                    _PDFEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "PhoneTariff",
                                                                             "PDFEngl"));
                }

                return (_PDFEnglUploadPath);
            }
        }

        public string PDFTatUrl {
            get {
                if (String.IsNullOrEmpty(PDFTat))
                    return String.Empty;
                else {
                    if (_PDFTatUrl == null) {
                        _PDFTatUrl = String.Format(@"{0}/{1}",
                                                   InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                       InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                           InternalDataContext.SiteId, "PhoneTariff", "PDFTat"), true,
                                                       _InternalDataContext.ShouldRemoveSchema), PDFTat);
                    }
                    return _PDFTatUrl;
                }
            }
        }

        public string PDFTatUploadPath {
            get {
                if (_PDFTatUploadPath == null) {
                    _PDFTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "PhoneTariff",
                                                                             "PDFTat"));
                }

                return (_PDFTatUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "PhoneTariff", "FamilyIcon"),
                                                           true, _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "PhoneTariff",
                                                                             "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "PhoneTariff",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "PhoneTariff",
                                                                             "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "PhoneTariff",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "PhoneTariff",
                                                                             "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "PhoneTariff",
                                                                    "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "PhoneTariff",
                                                                             "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId, "PhoneTariff",
                                                                        "FamilyIconHoverEngl"), true,
                                                                    _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "PhoneTariff",
                                                                             "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId, "PhoneTariff",
                                                                       "FamilyIconHoverTat"), true,
                                                                   _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "PhoneTariff",
                                                                             "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public Double PriceExact {
            get { return (Price.HasValue) ? Price.Value : default(Double); }
        }

        public Double TransferPriceExact {
            get { return (TransferPrice.HasValue) ? TransferPrice.Value : default(Double); }
        }

        public Double SubscriptionFeeExact {
            get { return (SubscriptionFee.HasValue) ? SubscriptionFee.Value : default(Double); }
        }

        public Boolean IsArchiveExact {
            get { return (IsArchive.HasValue) ? IsArchive.Value : default(Boolean); }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _INACID = InternalDataContext.ReplacePlaceholders(_INACID);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class PhoneTariffParam : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public PhoneTariffParam(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean MainInCardExact {
            get { return (MainInCard.HasValue) ? MainInCard.Value : default(Boolean); }
        }

        public Boolean ShowInTileExact {
            get { return (ShowInTile.HasValue) ? ShowInTile.Value : default(Boolean); }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
            _Value = InternalDataContext.ReplacePlaceholders(_Value);
            _ValueEngl = InternalDataContext.ReplacePlaceholders(_ValueEngl);
            _ValueTat = InternalDataContext.ReplacePlaceholders(_ValueTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class DiagnoseItem : IQPContent {
        private QPDataContext _InternalDataContext;
        private string _PdfInstructionEngUploadPath;
        private string _PdfInstructionEngUrl;
        private string _PdfInstructionTatUploadPath;
        private string _PdfInstructionTatUrl;
        private string _PdfInstructionUploadPath;

        private string _PdfInstructionUrl;
        private bool _StatusTypeChanged;

        public DiagnoseItem(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string PdfInstructionUrl {
            get {
                if (String.IsNullOrEmpty(PdfInstruction))
                    return String.Empty;
                else {
                    if (_PdfInstructionUrl == null) {
                        _PdfInstructionUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "DiagnoseItem",
                                                                   "PdfInstruction"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), PdfInstruction);
                    }
                    return _PdfInstructionUrl;
                }
            }
        }

        public string PdfInstructionUploadPath {
            get {
                if (_PdfInstructionUploadPath == null) {
                    _PdfInstructionUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "DiagnoseItem",
                                                                             "PdfInstruction"));
                }

                return (_PdfInstructionUploadPath);
            }
        }

        public string PdfInstructionEngUrl {
            get {
                if (String.IsNullOrEmpty(PdfInstructionEng))
                    return String.Empty;
                else {
                    if (_PdfInstructionEngUrl == null) {
                        _PdfInstructionEngUrl = String.Format(@"{0}/{1}",
                                                              InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                  InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                      InternalDataContext.SiteId, "DiagnoseItem",
                                                                      "PdfInstructionEng"), true,
                                                                  _InternalDataContext.ShouldRemoveSchema),
                                                              PdfInstructionEng);
                    }
                    return _PdfInstructionEngUrl;
                }
            }
        }

        public string PdfInstructionEngUploadPath {
            get {
                if (_PdfInstructionEngUploadPath == null) {
                    _PdfInstructionEngUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "DiagnoseItem",
                                                                             "PdfInstructionEng"));
                }

                return (_PdfInstructionEngUploadPath);
            }
        }

        public string PdfInstructionTatUrl {
            get {
                if (String.IsNullOrEmpty(PdfInstructionTat))
                    return String.Empty;
                else {
                    if (_PdfInstructionTatUrl == null) {
                        _PdfInstructionTatUrl = String.Format(@"{0}/{1}",
                                                              InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                  InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                      InternalDataContext.SiteId, "DiagnoseItem",
                                                                      "PdfInstructionTat"), true,
                                                                  _InternalDataContext.ShouldRemoveSchema),
                                                              PdfInstructionTat);
                    }
                    return _PdfInstructionTatUrl;
                }
            }
        }

        public string PdfInstructionTatUploadPath {
            get {
                if (_PdfInstructionTatUploadPath == null) {
                    _PdfInstructionTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "DiagnoseItem",
                                                                             "PdfInstructionTat"));
                }

                return (_PdfInstructionTatUploadPath);
            }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        public Boolean ShowStandardTextExact {
            get { return (ShowStandardText.HasValue) ? ShowStandardText.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _ItemType = InternalDataContext.ReplacePlaceholders(_ItemType);
            _Instruction = InternalDataContext.ReplacePlaceholders(_Instruction);
            _TitleEng = InternalDataContext.ReplacePlaceholders(_TitleEng);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _InstructionEng = InternalDataContext.ReplacePlaceholders(_InstructionEng);
            _InstructionTat = InternalDataContext.ReplacePlaceholders(_InstructionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MutualTVPackageGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MutualTVPackageGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TVPackageCategory : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public TVPackageCategory(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean CheckConnectionAvailableExact {
            get { return (CheckConnectionAvailable.HasValue) ? CheckConnectionAvailable.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ProvodServiceParamTab : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ProvodServiceParamTab(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ParamTabInProvodService : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ParamTabInProvodService(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ProvodServiceParam : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ProvodServiceParam(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
            _Value = InternalDataContext.ReplacePlaceholders(_Value);
            _ValueEngl = InternalDataContext.ReplacePlaceholders(_ValueEngl);
            _ValueTat = InternalDataContext.ReplacePlaceholders(_ValueTat);
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class DiagnoseStatisticItem : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public DiagnoseStatisticItem(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int64 HelpedExact {
            get { return (Helped.HasValue) ? Helped.Value : default(Int64); }
        }

        public Int64 NotHelpedExact {
            get { return (NotHelped.HasValue) ? NotHelped.Value : default(Int64); }
        }

        public Int32 DifferenceExact {
            get { return (Difference.HasValue) ? Difference.Value : default(Int32); }
        }

        public Int32 AmountExact {
            get { return (Amount.HasValue) ? Amount.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Version = InternalDataContext.ReplacePlaceholders(_Version);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RoamingCountryGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RoamingCountryGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RoamingRegionFriendGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RoamingRegionFriendGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SukkRegionFriend : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SukkRegionFriend(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SukkCountryInGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SukkCountryInGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SukkToMarketingRegion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SukkToMarketingRegion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SetupConnectionTab : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SetupConnectionTab(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 IndexExact {
            get { return (Index.HasValue) ? Index.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SetupConnectionGoal : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SetupConnectionGoal(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 IndexExact {
            get { return (Index.HasValue) ? Index.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SetupConnectionInstruction : IQPContent {
        private string _FileEnglUploadPath;
        private string _FileEnglUrl;
        private string _FileTatUploadPath;
        private string _FileTatUrl;
        private string _FileUploadPath;
        private string _FileUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;
        private string _WizardUploadPath;
        private string _WizardUrl;

        public SetupConnectionInstruction(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string FileUrl {
            get {
                if (String.IsNullOrEmpty(File))
                    return String.Empty;
                else {
                    if (_FileUrl == null) {
                        _FileUrl = String.Format(@"{0}/{1}",
                                                 InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                     InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                         InternalDataContext.SiteId, "SetupConnectionInstruction",
                                                         "File"), true, _InternalDataContext.ShouldRemoveSchema), File);
                    }
                    return _FileUrl;
                }
            }
        }

        public string FileUploadPath {
            get {
                if (_FileUploadPath == null) {
                    _FileUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "SetupConnectionInstruction", "File"));
                }

                return (_FileUploadPath);
            }
        }

        public string WizardUrl {
            get {
                if (String.IsNullOrEmpty(Wizard))
                    return String.Empty;
                else {
                    if (_WizardUrl == null) {
                        _WizardUrl = String.Format(@"{0}/{1}",
                                                   InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                       InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                           InternalDataContext.SiteId, "SetupConnectionInstruction",
                                                           "Wizard"), true, _InternalDataContext.ShouldRemoveSchema),
                                                   Wizard);
                    }
                    return _WizardUrl;
                }
            }
        }

        public string WizardUploadPath {
            get {
                if (_WizardUploadPath == null) {
                    _WizardUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "SetupConnectionInstruction", "Wizard"));
                }

                return (_WizardUploadPath);
            }
        }

        public string FileEnglUrl {
            get {
                if (String.IsNullOrEmpty(FileEngl))
                    return String.Empty;
                else {
                    if (_FileEnglUrl == null) {
                        _FileEnglUrl = String.Format(@"{0}/{1}",
                                                     InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                         InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                             InternalDataContext.SiteId, "SetupConnectionInstruction",
                                                             "FileEngl"), true, _InternalDataContext.ShouldRemoveSchema),
                                                     FileEngl);
                    }
                    return _FileEnglUrl;
                }
            }
        }

        public string FileEnglUploadPath {
            get {
                if (_FileEnglUploadPath == null) {
                    _FileEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "SetupConnectionInstruction", "FileEngl"));
                }

                return (_FileEnglUploadPath);
            }
        }

        public string FileTatUrl {
            get {
                if (String.IsNullOrEmpty(FileTat))
                    return String.Empty;
                else {
                    if (_FileTatUrl == null) {
                        _FileTatUrl = String.Format(@"{0}/{1}",
                                                    InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                        InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                            InternalDataContext.SiteId, "SetupConnectionInstruction",
                                                            "FileTat"), true, _InternalDataContext.ShouldRemoveSchema),
                                                    FileTat);
                    }
                    return _FileTatUrl;
                }
            }
        }

        public string FileTatUploadPath {
            get {
                if (_FileTatUploadPath == null) {
                    _FileTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "SetupConnectionInstruction", "FileTat"));
                }

                return (_FileTatUploadPath);
            }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Content = InternalDataContext.ReplacePlaceholders(_Content);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ProvodService : IQPContent {
        private string _FamilyIconEnglUploadPath;
        private string _FamilyIconEnglUrl;
        private string _FamilyIconHoverEnglUploadPath;
        private string _FamilyIconHoverEnglUrl;
        private string _FamilyIconHoverTatUploadPath;
        private string _FamilyIconHoverTatUrl;
        private string _FamilyIconHoverUploadPath;
        private string _FamilyIconHoverUrl;
        private string _FamilyIconTatUploadPath;
        private string _FamilyIconTatUrl;
        private string _FamilyIconUploadPath;
        private string _FamilyIconUrl;
        private QPDataContext _InternalDataContext;
        private string _PDFEnglUploadPath;
        private string _PDFEnglUrl;
        private string _PDFTatUploadPath;
        private string _PDFTatUrl;
        private string _PDFUploadPath;

        private string _PDFUrl;
        private bool _StatusTypeChanged;

        public ProvodService(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string PDFUrl {
            get {
                if (String.IsNullOrEmpty(PDF))
                    return String.Empty;
                else {
                    if (_PDFUrl == null) {
                        _PDFUrl = String.Format(@"{0}/{1}",
                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                        InternalDataContext.SiteId, "ProvodService", "PDF"), true,
                                                    _InternalDataContext.ShouldRemoveSchema), PDF);
                    }
                    return _PDFUrl;
                }
            }
        }

        public string PDFUploadPath {
            get {
                if (_PDFUploadPath == null) {
                    _PDFUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodService",
                                                                             "PDF"));
                }

                return (_PDFUploadPath);
            }
        }

        public string PDFEnglUrl {
            get {
                if (String.IsNullOrEmpty(PDFEngl))
                    return String.Empty;
                else {
                    if (_PDFEnglUrl == null) {
                        _PDFEnglUrl = String.Format(@"{0}/{1}",
                                                    InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                        InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                            InternalDataContext.SiteId, "ProvodService", "PDFEngl"),
                                                        true, _InternalDataContext.ShouldRemoveSchema), PDFEngl);
                    }
                    return _PDFEnglUrl;
                }
            }
        }

        public string PDFEnglUploadPath {
            get {
                if (_PDFEnglUploadPath == null) {
                    _PDFEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodService",
                                                                             "PDFEngl"));
                }

                return (_PDFEnglUploadPath);
            }
        }

        public string PDFTatUrl {
            get {
                if (String.IsNullOrEmpty(PDFTat))
                    return String.Empty;
                else {
                    if (_PDFTatUrl == null) {
                        _PDFTatUrl = String.Format(@"{0}/{1}",
                                                   InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                       InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                           InternalDataContext.SiteId, "ProvodService", "PDFTat"), true,
                                                       _InternalDataContext.ShouldRemoveSchema), PDFTat);
                    }
                    return _PDFTatUrl;
                }
            }
        }

        public string PDFTatUploadPath {
            get {
                if (_PDFTatUploadPath == null) {
                    _PDFTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodService",
                                                                             "PDFTat"));
                }

                return (_PDFTatUploadPath);
            }
        }

        public string FamilyIconUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIcon))
                    return String.Empty;
                else {
                    if (_FamilyIconUrl == null) {
                        _FamilyIconUrl = String.Format(@"{0}/{1}",
                                                       InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                           InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                               InternalDataContext.SiteId, "ProvodService", "FamilyIcon"),
                                                           true, _InternalDataContext.ShouldRemoveSchema), FamilyIcon);
                    }
                    return _FamilyIconUrl;
                }
            }
        }

        public string FamilyIconUploadPath {
            get {
                if (_FamilyIconUploadPath == null) {
                    _FamilyIconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodService",
                                                                             "FamilyIcon"));
                }

                return (_FamilyIconUploadPath);
            }
        }

        public string FamilyIconEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconEnglUrl == null) {
                        _FamilyIconEnglUrl = String.Format(@"{0}/{1}",
                                                           InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                               InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                   InternalDataContext.SiteId, "ProvodService",
                                                                   "FamilyIconEngl"), true,
                                                               _InternalDataContext.ShouldRemoveSchema), FamilyIconEngl);
                    }
                    return _FamilyIconEnglUrl;
                }
            }
        }

        public string FamilyIconEnglUploadPath {
            get {
                if (_FamilyIconEnglUploadPath == null) {
                    _FamilyIconEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodService",
                                                                             "FamilyIconEngl"));
                }

                return (_FamilyIconEnglUploadPath);
            }
        }

        public string FamilyIconTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconTat))
                    return String.Empty;
                else {
                    if (_FamilyIconTatUrl == null) {
                        _FamilyIconTatUrl = String.Format(@"{0}/{1}",
                                                          InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                              InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                  InternalDataContext.SiteId, "ProvodService",
                                                                  "FamilyIconTat"), true,
                                                              _InternalDataContext.ShouldRemoveSchema), FamilyIconTat);
                    }
                    return _FamilyIconTatUrl;
                }
            }
        }

        public string FamilyIconTatUploadPath {
            get {
                if (_FamilyIconTatUploadPath == null) {
                    _FamilyIconTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodService",
                                                                             "FamilyIconTat"));
                }

                return (_FamilyIconTatUploadPath);
            }
        }

        public string FamilyIconHoverUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHover))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverUrl == null) {
                        _FamilyIconHoverUrl = String.Format(@"{0}/{1}",
                                                            InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                    InternalDataContext.SiteId, "ProvodService",
                                                                    "FamilyIconHover"), true,
                                                                _InternalDataContext.ShouldRemoveSchema),
                                                            FamilyIconHover);
                    }
                    return _FamilyIconHoverUrl;
                }
            }
        }

        public string FamilyIconHoverUploadPath {
            get {
                if (_FamilyIconHoverUploadPath == null) {
                    _FamilyIconHoverUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodService",
                                                                             "FamilyIconHover"));
                }

                return (_FamilyIconHoverUploadPath);
            }
        }

        public string FamilyIconHoverEnglUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverEngl))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverEnglUrl == null) {
                        _FamilyIconHoverEnglUrl = String.Format(@"{0}/{1}",
                                                                InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                    InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                        InternalDataContext.SiteId, "ProvodService",
                                                                        "FamilyIconHoverEngl"), true,
                                                                    _InternalDataContext.ShouldRemoveSchema),
                                                                FamilyIconHoverEngl);
                    }
                    return _FamilyIconHoverEnglUrl;
                }
            }
        }

        public string FamilyIconHoverEnglUploadPath {
            get {
                if (_FamilyIconHoverEnglUploadPath == null) {
                    _FamilyIconHoverEnglUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodService",
                                                                             "FamilyIconHoverEngl"));
                }

                return (_FamilyIconHoverEnglUploadPath);
            }
        }

        public string FamilyIconHoverTatUrl {
            get {
                if (String.IsNullOrEmpty(FamilyIconHoverTat))
                    return String.Empty;
                else {
                    if (_FamilyIconHoverTatUrl == null) {
                        _FamilyIconHoverTatUrl = String.Format(@"{0}/{1}",
                                                               InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                                   InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                                       InternalDataContext.SiteId, "ProvodService",
                                                                       "FamilyIconHoverTat"), true,
                                                                   _InternalDataContext.ShouldRemoveSchema),
                                                               FamilyIconHoverTat);
                    }
                    return _FamilyIconHoverTatUrl;
                }
            }
        }

        public string FamilyIconHoverTatUploadPath {
            get {
                if (_FamilyIconHoverTatUploadPath == null) {
                    _FamilyIconHoverTatUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "ProvodService",
                                                                             "FamilyIconHoverTat"));
                }

                return (_FamilyIconHoverTatUploadPath);
            }
        }

        public Double PriceExact {
            get { return (Price.HasValue) ? Price.Value : default(Double); }
        }

        public Double SubscriptionFeeExact {
            get { return (SubscriptionFee.HasValue) ? SubscriptionFee.Value : default(Double); }
        }

        public Boolean IsArchiveExact {
            get { return (IsArchive.HasValue) ? IsArchive.Value : default(Boolean); }
        }

        public Int32 SortOrderExact {
            get { return (SortOrder.HasValue) ? SortOrder.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Legal = InternalDataContext.ReplacePlaceholders(_Legal);
            _LegalEngl = InternalDataContext.ReplacePlaceholders(_LegalEngl);
            _LegalTat = InternalDataContext.ReplacePlaceholders(_LegalTat);
            _INACID = InternalDataContext.ReplacePlaceholders(_INACID);
            _Benefit = InternalDataContext.ReplacePlaceholders(_Benefit);
            _BenefitEngl = InternalDataContext.ReplacePlaceholders(_BenefitEngl);
            _BenefitTat = InternalDataContext.ReplacePlaceholders(_BenefitTat);
            _DiscountArray = InternalDataContext.ReplacePlaceholders(_DiscountArray);
            _DiscountsByMonths = InternalDataContext.ReplacePlaceholders(_DiscountsByMonths);
            _DiscountsText1 = InternalDataContext.ReplacePlaceholders(_DiscountsText1);
            _DiscountsValue1 = InternalDataContext.ReplacePlaceholders(_DiscountsValue1);
            _DiscountsText2 = InternalDataContext.ReplacePlaceholders(_DiscountsText2);
            _DiscountsValue2 = InternalDataContext.ReplacePlaceholders(_DiscountsValue2);
            _DiscountsText3 = InternalDataContext.ReplacePlaceholders(_DiscountsText3);
            _DiscountsValue3 = InternalDataContext.ReplacePlaceholders(_DiscountsValue3);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class WordForm : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public WordForm(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Key = InternalDataContext.ReplacePlaceholders(_Key);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _EngPlural = InternalDataContext.ReplacePlaceholders(_EngPlural);
            _EngSingular = InternalDataContext.ReplacePlaceholders(_EngSingular);
            _TatSingular = InternalDataContext.ReplacePlaceholders(_TatSingular);
            _RusFirstPlural = InternalDataContext.ReplacePlaceholders(_RusFirstPlural);
            _RusSecondPlural = InternalDataContext.ReplacePlaceholders(_RusSecondPlural);
            _RusThirdPlural = InternalDataContext.ReplacePlaceholders(_RusThirdPlural);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ServiceForTVPackage : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ServiceForTVPackage(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IncludedByDefaultExact {
            get { return (IncludedByDefault.HasValue) ? IncludedByDefault.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class QP_TrusteePayment : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public QP_TrusteePayment(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Range = InternalDataContext.ReplacePlaceholders(_Range);
            _Sum = InternalDataContext.ReplacePlaceholders(_Sum);
            _MinBalance = InternalDataContext.ReplacePlaceholders(_MinBalance);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class QP_TrusteePaymentTab : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public QP_TrusteePaymentTab(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IsDefaultExact {
            get { return (IsDefault.HasValue) ? IsDefault.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Name = InternalDataContext.ReplacePlaceholders(_Name);
            _MinBalance = InternalDataContext.ReplacePlaceholders(_MinBalance);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SearchAnnouncement : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SearchAnnouncement(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean ShowAlwaysExact {
            get { return (ShowAlways.HasValue) ? ShowAlways.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Url = InternalDataContext.ReplacePlaceholders(_Url);
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _Keywords = InternalDataContext.ReplacePlaceholders(_Keywords);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class WarrantyService : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public WarrantyService(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Address = InternalDataContext.ReplacePlaceholders(_Address);
            _Schedule = InternalDataContext.ReplacePlaceholders(_Schedule);
            _Phone = InternalDataContext.ReplacePlaceholders(_Phone);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SKADType : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SKADType(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SKADService : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SKADService(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SKADSpeciality : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SKADSpeciality(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SalesPointParameter : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SalesPointParameter(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Services = InternalDataContext.ReplacePlaceholders(_Services);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SalesPointType : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SalesPointType(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SalesPointSpeciality : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SalesPointSpeciality(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SalesPointService : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SalesPointService(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class TargetUser : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public TargetUser(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class FeedbackSubthemeGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public FeedbackSubthemeGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ArchiveTvTariff : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ArchiveTvTariff(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Boolean IsTariffExact {
            get { return (IsTariff.HasValue) ? IsTariff.Value : default(Boolean); }
        }

        public Double MonthPriceExact {
            get { return (MonthPrice.HasValue) ? MonthPrice.Value : default(Double); }
        }

        public Int32 IdINACExact {
            get { return (IdINAC.HasValue) ? IdINAC.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ObsoleteUrlRedirect : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ObsoleteUrlRedirect(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        public Boolean InOnMainSiteExact {
            get { return (InOnMainSite.HasValue) ? InOnMainSite.Value : default(Boolean); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _OldUrl = InternalDataContext.ReplacePlaceholders(_OldUrl);
            _UrlToRedirect = InternalDataContext.ReplacePlaceholders(_UrlToRedirect);
            _RedirectType = InternalDataContext.ReplacePlaceholders(_RedirectType);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ArchiveViewTariff : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ArchiveViewTariff(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Double IsTvExact {
            get { return (IsTv.HasValue) ? IsTv.Value : default(Double); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MarketingRegionNoCallback : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MarketingRegionNoCallback(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class FeedbackCallbackTime : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public FeedbackCallbackTime(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 CampaignIdExact {
            get { return (CampaignId.HasValue) ? CampaignId.Value : default(Int32); }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class FeedbackSwindleProblem : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public FeedbackSwindleProblem(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class Contact : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public Contact(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Description = InternalDataContext.ReplacePlaceholders(_Description);
            _Email = InternalDataContext.ReplacePlaceholders(_Email);
            _Phone = InternalDataContext.ReplacePlaceholders(_Phone);
            _Details = InternalDataContext.ReplacePlaceholders(_Details);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _DescriptionEngl = InternalDataContext.ReplacePlaceholders(_DescriptionEngl);
            _DescriptionTat = InternalDataContext.ReplacePlaceholders(_DescriptionTat);
            _DetailsEngl = InternalDataContext.ReplacePlaceholders(_DetailsEngl);
            _DetailsTat = InternalDataContext.ReplacePlaceholders(_DetailsTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ContactsTab : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ContactsTab(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ContactsGroup : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ContactsGroup(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 OrderExact {
            get { return (Order.HasValue) ? Order.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
            _TextEngl = InternalDataContext.ReplacePlaceholders(_TextEngl);
            _TextTat = InternalDataContext.ReplacePlaceholders(_TextTat);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class CampaignIdToRegion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public CampaignIdToRegion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 CampaignIdExact {
            get { return (CampaignId.HasValue) ? CampaignId.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SitemapXml : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SitemapXml(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Generated = InternalDataContext.ReplacePlaceholders(_Generated);
            _Current = InternalDataContext.ReplacePlaceholders(_Current);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SiteConfig : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SiteConfig(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Alias = InternalDataContext.ReplacePlaceholders(_Alias);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RobotsTxt : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RobotsTxt(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Text = InternalDataContext.ReplacePlaceholders(_Text);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MetroLine : IQPContent {
        private string _IconUploadPath;
        private string _IconUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MetroLine(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string IconUrl {
            get {
                if (String.IsNullOrEmpty(Icon))
                    return String.Empty;
                else {
                    if (_IconUrl == null) {
                        _IconUrl = String.Format(@"{0}/{1}",
                                                 InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                     InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                         InternalDataContext.SiteId, "MetroLine", "Icon"), true,
                                                     _InternalDataContext.ShouldRemoveSchema), Icon);
                    }
                    return _IconUrl;
                }
            }
        }

        public string IconUploadPath {
            get {
                if (_IconUploadPath == null) {
                    _IconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "MetroLine",
                                                                             "Icon"));
                }

                return (_IconUploadPath);
            }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _ColorName = InternalDataContext.ReplacePlaceholders(_ColorName);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class AnnualContractSetting : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public AnnualContractSetting(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public Int32 FeeLimitExact {
            get { return (FeeLimit.HasValue) ? FeeLimit.Value : default(Int32); }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _FamilyText = InternalDataContext.ReplacePlaceholders(_FamilyText);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MnpCrmSetting : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MnpCrmSetting(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _QueueName = InternalDataContext.ReplacePlaceholders(_QueueName);
            _CaseType0 = InternalDataContext.ReplacePlaceholders(_CaseType0);
            _CaseType1 = InternalDataContext.ReplacePlaceholders(_CaseType1);
            _CaseType2 = InternalDataContext.ReplacePlaceholders(_CaseType2);
            _CaseType3 = InternalDataContext.ReplacePlaceholders(_CaseType3);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MNPRequestOfficeInRegion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MNPRequestOfficeInRegion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MNPRequesCourierInRegion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MNPRequesCourierInRegion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MNPRequestPageInRegion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MNPRequestPageInRegion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class SkadToFederalRegion : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public SkadToFederalRegion(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _FederalCode = InternalDataContext.ReplacePlaceholders(_FederalCode);
            _SkadCode = InternalDataContext.ReplacePlaceholders(_SkadCode);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class ServicePrefix : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public ServicePrefix(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RegionalEmail : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RegionalEmail(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Email = InternalDataContext.ReplacePlaceholders(_Email);
            _Type = InternalDataContext.ReplacePlaceholders(_Type);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RoamingParameter : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RoamingParameter(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class RoamingOptionCalculation : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public RoamingOptionCalculation(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _ParamValue = InternalDataContext.ReplacePlaceholders(_ParamValue);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MobileDevice : IQPContent {
        private string _IconUploadPath;
        private string _IconUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MobileDevice(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string IconUrl {
            get {
                if (String.IsNullOrEmpty(Icon))
                    return String.Empty;
                else {
                    if (_IconUrl == null) {
                        _IconUrl = String.Format(@"{0}/{1}",
                                                 InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                     InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                         InternalDataContext.SiteId, "MobileDevice", "Icon"), true,
                                                     _InternalDataContext.ShouldRemoveSchema), Icon);
                    }
                    return _IconUrl;
                }
            }
        }

        public string IconUploadPath {
            get {
                if (_IconUploadPath == null) {
                    _IconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId, "MobileDevice",
                                                                             "Icon"));
                }

                return (_IconUploadPath);
            }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MobileServiceUsageType : IQPContent {
        private string _IconUploadPath;
        private string _IconUrl;
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MobileServiceUsageType(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        public string IconUrl {
            get {
                if (String.IsNullOrEmpty(Icon))
                    return String.Empty;
                else {
                    if (_IconUrl == null) {
                        _IconUrl = String.Format(@"{0}/{1}",
                                                 InternalDataContext.Cnn.GetUrlForFileAttribute(
                                                     InternalDataContext.Cnn.GetAttributeIdByNetNames(
                                                         InternalDataContext.SiteId, "MobileServiceUsageType", "Icon"),
                                                     true, _InternalDataContext.ShouldRemoveSchema), Icon);
                    }
                    return _IconUrl;
                }
            }
        }

        public string IconUploadPath {
            get {
                if (_IconUploadPath == null) {
                    _IconUploadPath =
                        InternalDataContext.Cnn.GetDirectoryForFileAttribute(
                            InternalDataContext.Cnn.GetAttributeIdByNetNames(InternalDataContext.SiteId,
                                                                             "MobileServiceUsageType", "Icon"));
                }

                return (_IconUploadPath);
            }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
            _Title = InternalDataContext.ReplacePlaceholders(_Title);
            _TitleEngl = InternalDataContext.ReplacePlaceholders(_TitleEngl);
            _TitleTat = InternalDataContext.ReplacePlaceholders(_TitleTat);
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }

    partial class MobileServiceByRegionUsageType : IQPContent {
        private QPDataContext _InternalDataContext;
        private bool _StatusTypeChanged;

        public MobileServiceByRegionUsageType(QPDataContext context) {
            _InternalDataContext = context;
            OnCreated();
        }

        public QPDataContext InternalDataContext {
            get { return _InternalDataContext; }
            set { _InternalDataContext = value; }
        }

        #region IQPContent Members

        public bool StatusTypeChanged {
            get { return _StatusTypeChanged; }
            set { _StatusTypeChanged = value; }
        }

        #endregion

        partial void OnLoaded() {
        }

        partial void OnCreated() {
            if (_InternalDataContext == null)
                _InternalDataContext = LinqHelper.Context;
            _Visible = true;
            _Archive = false;
            _StatusTypeId = _InternalDataContext.PublishedId;
            _StatusTypeChanged = false;
            PropertyChanged += HandlePropertyChangedEvent;
        }

        private void HandlePropertyChangedEvent(object sender, PropertyChangedEventArgs a) {
            if (a.PropertyName == "StatusType")
                _StatusTypeChanged = true;
        }

        public void LoadStatusType() {
            _StatusType.Entity = InternalDataContext.StatusTypes.Single(n => n.Id == _StatusTypeId);
        }
    }
}
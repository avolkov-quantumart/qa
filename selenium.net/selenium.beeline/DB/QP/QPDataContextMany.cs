﻿
using System.Data.Linq;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System;
using System.Collections;
using System.ComponentModel;

    
namespace QA.Beeline.DAL 
{

public partial class MarketingRegion
{
	

	private ListSelector<SiteProductToMarketingRegion, SiteProduct> _SiteProducts = null;

	public ListSelector<SiteProductToMarketingRegion, SiteProduct> SiteProducts
	{
		get
		{
			if (_SiteProducts == null)
			{
				_SiteProducts = this.SiteProductToMarketingRegions.GetNewBindingList()
				.AsListSelector<SiteProductToMarketingRegion, SiteProduct>
				(
					od => od.SiteProduct,
					delegate(IList<SiteProductToMarketingRegion> ods, SiteProduct p)
					{
						var items = ods.Where(od => od.SiteProduct_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new SiteProductToMarketingRegion 
								{
									MarketingRegion_ID = this.Id,
									SiteProduct = p
								}
							);
						}
					},
					delegate(IList<SiteProductToMarketingRegion> ods, SiteProduct p)
					{
						var items = ods.Where(od => od.SiteProduct_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.SiteProductToMarketingRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _SiteProducts;
		}
	}
			
	public string[] SiteProductsIDs
	{
		get
		{
			return SiteProducts.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string SiteProductsString
	{
		get
		{
			return String.Join(",", SiteProductsIDs.ToArray());
		}
	}
	

	private ListSelector<ExternalRegionMappingToMarketingRegion, ExternalRegionMapping> _ExternalRegionMapping = null;

	public ListSelector<ExternalRegionMappingToMarketingRegion, ExternalRegionMapping> ExternalRegionMapping
	{
		get
		{
			if (_ExternalRegionMapping == null)
			{
				_ExternalRegionMapping = this.ExternalRegionMappingsToMarketingRegions.GetNewBindingList()
				.AsListSelector<ExternalRegionMappingToMarketingRegion, ExternalRegionMapping>
				(
					od => od.ExternalRegionMapping,
					delegate(IList<ExternalRegionMappingToMarketingRegion> ods, ExternalRegionMapping p)
					{
						var items = ods.Where(od => od.ExternalRegionMapping_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ExternalRegionMappingToMarketingRegion 
								{
									MarketingRegion_ID = this.Id,
									ExternalRegionMapping = p
								}
							);
						}
					},
					delegate(IList<ExternalRegionMappingToMarketingRegion> ods, ExternalRegionMapping p)
					{
						var items = ods.Where(od => od.ExternalRegionMapping_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ExternalRegionMappingsToMarketingRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _ExternalRegionMapping;
		}
	}
			
	public string[] ExternalRegionMappingIDs
	{
		get
		{
			return ExternalRegionMapping.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ExternalRegionMappingString
	{
		get
		{
			return String.Join(",", ExternalRegionMappingIDs.ToArray());
		}
	}
	
}

public partial class SiteProduct
{
	

	private ListSelector<SiteProductToMarketingRegion, MarketingRegion> _MarketingRegions = null;

	public ListSelector<SiteProductToMarketingRegion, MarketingRegion> MarketingRegions
	{
		get
		{
			if (_MarketingRegions == null)
			{
				_MarketingRegions = this.SiteProductToMarketingRegions.GetNewBindingList()
				.AsListSelector<SiteProductToMarketingRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<SiteProductToMarketingRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new SiteProductToMarketingRegion 
								{
									SiteProduct_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<SiteProductToMarketingRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.SiteProductToMarketingRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketingRegions;
		}
	}
			
	public string[] MarketingRegionsIDs
	{
		get
		{
			return MarketingRegions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketingRegionsString
	{
		get
		{
			return String.Join(",", MarketingRegionsIDs.ToArray());
		}
	}
	

	private ListSelector<ProduktovoenapravlenieProduktyArticle, DeviceType> _DeviceTypes = null;

	public ListSelector<ProduktovoenapravlenieProduktyArticle, DeviceType> DeviceTypes
	{
		get
		{
			if (_DeviceTypes == null)
			{
				_DeviceTypes = this.ProduktovoenapravlenieProduktyArticles.GetNewBindingList()
				.AsListSelector<ProduktovoenapravlenieProduktyArticle, DeviceType>
				(
					od => od.DeviceType,
					delegate(IList<ProduktovoenapravlenieProduktyArticle> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ProduktovoenapravlenieProduktyArticle 
								{
									SiteProduct_ID = this.Id,
									DeviceType = p
								}
							);
						}
					},
					delegate(IList<ProduktovoenapravlenieProduktyArticle> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ProduktovoenapravlenieProduktyArticles.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _DeviceTypes;
		}
	}
			
	public string[] DeviceTypesIDs
	{
		get
		{
			return DeviceTypes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string DeviceTypesString
	{
		get
		{
			return String.Join(",", DeviceTypesIDs.ToArray());
		}
	}
	
}

public partial class QPAbstractItem
{
	

	private ListSelector<AbstractItemAbtractItemRegionArticle, QPRegion> _Regions = null;

	public ListSelector<AbstractItemAbtractItemRegionArticle, QPRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.AbstractItemAbtractItemRegionArticles.GetNewBindingList()
				.AsListSelector<AbstractItemAbtractItemRegionArticle, QPRegion>
				(
					od => od.QPRegion,
					delegate(IList<AbstractItemAbtractItemRegionArticle> ods, QPRegion p)
					{
						var items = ods.Where(od => od.QPRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new AbstractItemAbtractItemRegionArticle 
								{
									QPAbstractItem_ID = this.Id,
									QPRegion = p
								}
							);
						}
					},
					delegate(IList<AbstractItemAbtractItemRegionArticle> ods, QPRegion p)
					{
						var items = ods.Where(od => od.QPRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.AbstractItemAbtractItemRegionArticles.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class QPDiscriminator
{
	

	private ListSelector<ItemDefinitionItemDefinitionArticle, QPDiscriminator> _AllowedItemDefinitions1 = null;

	public ListSelector<ItemDefinitionItemDefinitionArticle, QPDiscriminator> AllowedItemDefinitions1
	{
		get
		{
			if (_AllowedItemDefinitions1 == null)
			{
				_AllowedItemDefinitions1 = this.ItemDefinitionItemDefinitionArticles.GetNewBindingList()
				.AsListSelector<ItemDefinitionItemDefinitionArticle, QPDiscriminator>
				(
					od => od.QPDiscriminator2,
					delegate(IList<ItemDefinitionItemDefinitionArticle> ods, QPDiscriminator p)
					{
						var items = ods.Where(od => od.QPDiscriminator_ID2 == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ItemDefinitionItemDefinitionArticle 
								{
									QPDiscriminator_ID = this.Id,
									QPDiscriminator2 = p
								}
							);
						}
					},
					delegate(IList<ItemDefinitionItemDefinitionArticle> ods, QPDiscriminator p)
					{
						var items = ods.Where(od => od.QPDiscriminator_ID2 == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ItemDefinitionItemDefinitionArticles.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _AllowedItemDefinitions1;
		}
	}
			
	public string[] AllowedItemDefinitions1IDs
	{
		get
		{
			return AllowedItemDefinitions1.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string AllowedItemDefinitions1String
	{
		get
		{
			return String.Join(",", AllowedItemDefinitions1IDs.ToArray());
		}
	}
	
}

public partial class RegionTagValue
{
	

	private ListSelector<TagInRegion, MarketingRegion> _Regions = null;

	public ListSelector<TagInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.TagInRegions.GetNewBindingList()
				.AsListSelector<TagInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<TagInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new TagInRegion 
								{
									RegionTagValue_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<TagInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.TagInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class SearchSuggestion
{
	

	private ListSelector<SearchSuggestInRegion, MarketingRegion> _Regions = null;

	public ListSelector<SearchSuggestInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.SearchSuggestInRegions.GetNewBindingList()
				.AsListSelector<SearchSuggestInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<SearchSuggestInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new SearchSuggestInRegion 
								{
									SearchSuggestion_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<SearchSuggestInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.SearchSuggestInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class NewsArticle
{
	

	private ListSelector<NewsInRegion, MarketingRegion> _MarketRegions = null;

	public ListSelector<NewsInRegion, MarketingRegion> MarketRegions
	{
		get
		{
			if (_MarketRegions == null)
			{
				_MarketRegions = this.NewsInRegions.GetNewBindingList()
				.AsListSelector<NewsInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<NewsInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new NewsInRegion 
								{
									NewsArticle_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<NewsInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.NewsInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketRegions;
		}
	}
			
	public string[] MarketRegionsIDs
	{
		get
		{
			return MarketRegions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketRegionsString
	{
		get
		{
			return String.Join(",", MarketRegionsIDs.ToArray());
		}
	}
	
}

public partial class RoamingTariffZone
{
	

	private ListSelector<MarketingMobileServiceInRoamingTariff, MarketingMobileService> _RecomendedServices = null;

	public ListSelector<MarketingMobileServiceInRoamingTariff, MarketingMobileService> RecomendedServices
	{
		get
		{
			if (_RecomendedServices == null)
			{
				_RecomendedServices = this.MarketingMobileServiceInRoamingTariffs.GetNewBindingList()
				.AsListSelector<MarketingMobileServiceInRoamingTariff, MarketingMobileService>
				(
					od => od.MarketingMobileService,
					delegate(IList<MarketingMobileServiceInRoamingTariff> ods, MarketingMobileService p)
					{
						var items = ods.Where(od => od.MarketingMobileService_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingMobileServiceInRoamingTariff 
								{
									RoamingTariffZone_ID = this.Id,
									MarketingMobileService = p
								}
							);
						}
					},
					delegate(IList<MarketingMobileServiceInRoamingTariff> ods, MarketingMobileService p)
					{
						var items = ods.Where(od => od.MarketingMobileService_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingMobileServiceInRoamingTariffs.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _RecomendedServices;
		}
	}
			
	public string[] RecomendedServicesIDs
	{
		get
		{
			return RecomendedServices.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RecomendedServicesString
	{
		get
		{
			return String.Join(",", RecomendedServicesIDs.ToArray());
		}
	}
	
}

public partial class FeedbackTheme
{
	

	private ListSelector<FeedbackThemeToFaqContent, FaqContent> _FaqContents = null;

	public ListSelector<FeedbackThemeToFaqContent, FaqContent> FaqContents
	{
		get
		{
			if (_FaqContents == null)
			{
				_FaqContents = this.FeedbackThemesToFaqContents.GetNewBindingList()
				.AsListSelector<FeedbackThemeToFaqContent, FaqContent>
				(
					od => od.FaqContent,
					delegate(IList<FeedbackThemeToFaqContent> ods, FaqContent p)
					{
						var items = ods.Where(od => od.FaqContent_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new FeedbackThemeToFaqContent 
								{
									FeedbackTheme_ID = this.Id,
									FaqContent = p
								}
							);
						}
					},
					delegate(IList<FeedbackThemeToFaqContent> ods, FaqContent p)
					{
						var items = ods.Where(od => od.FaqContent_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.FeedbackThemesToFaqContents.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _FaqContents;
		}
	}
			
	public string[] FaqContentsIDs
	{
		get
		{
			return FaqContents.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string FaqContentsString
	{
		get
		{
			return String.Join(",", FaqContentsIDs.ToArray());
		}
	}
	
}

public partial class FeedbackSubtheme
{
	

	private ListSelector<FeedbackSubThemeToFaqContent, FaqContent> _FaqContents = null;

	public ListSelector<FeedbackSubThemeToFaqContent, FaqContent> FaqContents
	{
		get
		{
			if (_FaqContents == null)
			{
				_FaqContents = this.FeedbackSubThemesToFaqContents.GetNewBindingList()
				.AsListSelector<FeedbackSubThemeToFaqContent, FaqContent>
				(
					od => od.FaqContent,
					delegate(IList<FeedbackSubThemeToFaqContent> ods, FaqContent p)
					{
						var items = ods.Where(od => od.FaqContent_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new FeedbackSubThemeToFaqContent 
								{
									FeedbackSubtheme_ID = this.Id,
									FaqContent = p
								}
							);
						}
					},
					delegate(IList<FeedbackSubThemeToFaqContent> ods, FaqContent p)
					{
						var items = ods.Where(od => od.FaqContent_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.FeedbackSubThemesToFaqContents.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _FaqContents;
		}
	}
			
	public string[] FaqContentsIDs
	{
		get
		{
			return FaqContents.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string FaqContentsString
	{
		get
		{
			return String.Join(",", FaqContentsIDs.ToArray());
		}
	}
	
}

public partial class FeedbackQueue
{
	

	private ListSelector<FeedbackQueueInRegion, MarketingRegion> _Regions = null;

	public ListSelector<FeedbackQueueInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.FeedbackQueueInRegions.GetNewBindingList()
				.AsListSelector<FeedbackQueueInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<FeedbackQueueInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new FeedbackQueueInRegion 
								{
									FeedbackQueue_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<FeedbackQueueInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.FeedbackQueueInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class TVChannel
{
	

	private ListSelector<TVChannelsInRegion, MarketingRegion> _Regions = null;

	public ListSelector<TVChannelsInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.TVChannelsInRegions.GetNewBindingList()
				.AsListSelector<TVChannelsInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<TVChannelsInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new TVChannelsInRegion 
								{
									TVChannel_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<TVChannelsInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.TVChannelsInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	

	private ListSelector<TVChannelsInTheme, TVChannelTheme> _Themes = null;

	public ListSelector<TVChannelsInTheme, TVChannelTheme> Themes
	{
		get
		{
			if (_Themes == null)
			{
				_Themes = this.TVChannelsInThemes.GetNewBindingList()
				.AsListSelector<TVChannelsInTheme, TVChannelTheme>
				(
					od => od.TVChannelTheme,
					delegate(IList<TVChannelsInTheme> ods, TVChannelTheme p)
					{
						var items = ods.Where(od => od.TVChannelTheme_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new TVChannelsInTheme 
								{
									TVChannel_ID = this.Id,
									TVChannelTheme = p
								}
							);
						}
					},
					delegate(IList<TVChannelsInTheme> ods, TVChannelTheme p)
					{
						var items = ods.Where(od => od.TVChannelTheme_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.TVChannelsInThemes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Themes;
		}
	}
			
	public string[] ThemesIDs
	{
		get
		{
			return Themes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ThemesString
	{
		get
		{
			return String.Join(",", ThemesIDs.ToArray());
		}
	}
	

	private ListSelector<TVChannelsInMarketingPackage, MarketingTVPackage> _MarketingTVPackages = null;

	public ListSelector<TVChannelsInMarketingPackage, MarketingTVPackage> MarketingTVPackages
	{
		get
		{
			if (_MarketingTVPackages == null)
			{
				_MarketingTVPackages = this.TVChannelsInMarketingPackages.GetNewBindingList()
				.AsListSelector<TVChannelsInMarketingPackage, MarketingTVPackage>
				(
					od => od.MarketingTVPackage,
					delegate(IList<TVChannelsInMarketingPackage> ods, MarketingTVPackage p)
					{
						var items = ods.Where(od => od.MarketingTVPackage_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new TVChannelsInMarketingPackage 
								{
									TVChannel_ID = this.Id,
									MarketingTVPackage = p
								}
							);
						}
					},
					delegate(IList<TVChannelsInMarketingPackage> ods, MarketingTVPackage p)
					{
						var items = ods.Where(od => od.MarketingTVPackage_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.TVChannelsInMarketingPackages.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketingTVPackages;
		}
	}
			
	public string[] MarketingTVPackagesIDs
	{
		get
		{
			return MarketingTVPackages.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketingTVPackagesString
	{
		get
		{
			return String.Join(",", MarketingTVPackagesIDs.ToArray());
		}
	}
	
}

public partial class RegionFeedbackGroup
{
	

	private ListSelector<FeedbackGroupInRegion, MarketingRegion> _Regions = null;

	public ListSelector<FeedbackGroupInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.FeedbackGroupInRegions.GetNewBindingList()
				.AsListSelector<FeedbackGroupInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<FeedbackGroupInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new FeedbackGroupInRegion 
								{
									RegionFeedbackGroup_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<FeedbackGroupInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.FeedbackGroupInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class DeviceType
{
	

	private ListSelector<ProduktovoenapravlenieProduktyArticle, SiteProduct> _SiteProducts = null;

	public ListSelector<ProduktovoenapravlenieProduktyArticle, SiteProduct> SiteProducts
	{
		get
		{
			if (_SiteProducts == null)
			{
				_SiteProducts = this.ProduktovoenapravlenieProduktyArticles.GetNewBindingList()
				.AsListSelector<ProduktovoenapravlenieProduktyArticle, SiteProduct>
				(
					od => od.SiteProduct,
					delegate(IList<ProduktovoenapravlenieProduktyArticle> ods, SiteProduct p)
					{
						var items = ods.Where(od => od.SiteProduct_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ProduktovoenapravlenieProduktyArticle 
								{
									DeviceType_ID = this.Id,
									SiteProduct = p
								}
							);
						}
					},
					delegate(IList<ProduktovoenapravlenieProduktyArticle> ods, SiteProduct p)
					{
						var items = ods.Where(od => od.SiteProduct_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ProduktovoenapravlenieProduktyArticles.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _SiteProducts;
		}
	}
			
	public string[] SiteProductsIDs
	{
		get
		{
			return SiteProducts.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string SiteProductsString
	{
		get
		{
			return String.Join(",", SiteProductsIDs.ToArray());
		}
	}
	
}

public partial class Action
{
	

	private ListSelector<ActionProduct, SiteProduct> _Products = null;

	public ListSelector<ActionProduct, SiteProduct> Products
	{
		get
		{
			if (_Products == null)
			{
				_Products = this.ActionProducts.GetNewBindingList()
				.AsListSelector<ActionProduct, SiteProduct>
				(
					od => od.SiteProduct,
					delegate(IList<ActionProduct> ods, SiteProduct p)
					{
						var items = ods.Where(od => od.SiteProduct_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ActionProduct 
								{
									Action_ID = this.Id,
									SiteProduct = p
								}
							);
						}
					},
					delegate(IList<ActionProduct> ods, SiteProduct p)
					{
						var items = ods.Where(od => od.SiteProduct_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ActionProducts.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Products;
		}
	}
			
	public string[] ProductsIDs
	{
		get
		{
			return Products.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ProductsString
	{
		get
		{
			return String.Join(",", ProductsIDs.ToArray());
		}
	}
	

	private ListSelector<ActionDeviceType, DeviceType> _DeviceTypes = null;

	public ListSelector<ActionDeviceType, DeviceType> DeviceTypes
	{
		get
		{
			if (_DeviceTypes == null)
			{
				_DeviceTypes = this.ActionDeviceTypes.GetNewBindingList()
				.AsListSelector<ActionDeviceType, DeviceType>
				(
					od => od.DeviceType,
					delegate(IList<ActionDeviceType> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ActionDeviceType 
								{
									Action_ID = this.Id,
									DeviceType = p
								}
							);
						}
					},
					delegate(IList<ActionDeviceType> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ActionDeviceTypes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _DeviceTypes;
		}
	}
			
	public string[] DeviceTypesIDs
	{
		get
		{
			return DeviceTypes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string DeviceTypesString
	{
		get
		{
			return String.Join(",", DeviceTypesIDs.ToArray());
		}
	}
	

	private ListSelector<ActionInRegion, MarketingRegion> _Regions = null;

	public ListSelector<ActionInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.ActionInRegions.GetNewBindingList()
				.AsListSelector<ActionInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<ActionInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ActionInRegion 
								{
									Action_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<ActionInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ActionInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class TVPackage
{
	

	private ListSelector<TVPackageInRegion, MarketingRegion> _Regions = null;

	public ListSelector<TVPackageInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.TVPackageInRegions.GetNewBindingList()
				.AsListSelector<TVPackageInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<TVPackageInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new TVPackageInRegion 
								{
									TVPackage_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<TVPackageInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.TVPackageInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	

	private ListSelector<IncludedDiscountTvPackagesInMarketingPackage, MarketingTVPackage> _IncludedDiscountTvPackages = null;

	public ListSelector<IncludedDiscountTvPackagesInMarketingPackage, MarketingTVPackage> IncludedDiscountTvPackages
	{
		get
		{
			if (_IncludedDiscountTvPackages == null)
			{
				_IncludedDiscountTvPackages = this.IncludedDiscountTvPackagesInMarketingPackages.GetNewBindingList()
				.AsListSelector<IncludedDiscountTvPackagesInMarketingPackage, MarketingTVPackage>
				(
					od => od.MarketingTVPackage,
					delegate(IList<IncludedDiscountTvPackagesInMarketingPackage> ods, MarketingTVPackage p)
					{
						var items = ods.Where(od => od.MarketingTVPackage_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new IncludedDiscountTvPackagesInMarketingPackage 
								{
									TVPackage_ID = this.Id,
									MarketingTVPackage = p
								}
							);
						}
					},
					delegate(IList<IncludedDiscountTvPackagesInMarketingPackage> ods, MarketingTVPackage p)
					{
						var items = ods.Where(od => od.MarketingTVPackage_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.IncludedDiscountTvPackagesInMarketingPackages.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _IncludedDiscountTvPackages;
		}
	}
			
	public string[] IncludedDiscountTvPackagesIDs
	{
		get
		{
			return IncludedDiscountTvPackages.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string IncludedDiscountTvPackagesString
	{
		get
		{
			return String.Join(",", IncludedDiscountTvPackagesIDs.ToArray());
		}
	}
	

	private ListSelector<ActivatedDiscountTvPackagesInMarketingPackage, MarketingTVPackage> _ActivatedDiscountTvPackages = null;

	public ListSelector<ActivatedDiscountTvPackagesInMarketingPackage, MarketingTVPackage> ActivatedDiscountTvPackages
	{
		get
		{
			if (_ActivatedDiscountTvPackages == null)
			{
				_ActivatedDiscountTvPackages = this.ActivatedDiscountTvPackagesInMarketingPackages.GetNewBindingList()
				.AsListSelector<ActivatedDiscountTvPackagesInMarketingPackage, MarketingTVPackage>
				(
					od => od.MarketingTVPackage,
					delegate(IList<ActivatedDiscountTvPackagesInMarketingPackage> ods, MarketingTVPackage p)
					{
						var items = ods.Where(od => od.MarketingTVPackage_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ActivatedDiscountTvPackagesInMarketingPackage 
								{
									TVPackage_ID = this.Id,
									MarketingTVPackage = p
								}
							);
						}
					},
					delegate(IList<ActivatedDiscountTvPackagesInMarketingPackage> ods, MarketingTVPackage p)
					{
						var items = ods.Where(od => od.MarketingTVPackage_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ActivatedDiscountTvPackagesInMarketingPackages.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _ActivatedDiscountTvPackages;
		}
	}
			
	public string[] ActivatedDiscountTvPackagesIDs
	{
		get
		{
			return ActivatedDiscountTvPackages.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ActivatedDiscountTvPackagesString
	{
		get
		{
			return String.Join(",", ActivatedDiscountTvPackagesIDs.ToArray());
		}
	}
	
}

public partial class ExternalRegionMapping
{
	

	private ListSelector<ExternalRegionMappingToMarketingRegion, MarketingRegion> _MarketingRegion = null;

	public ListSelector<ExternalRegionMappingToMarketingRegion, MarketingRegion> MarketingRegion
	{
		get
		{
			if (_MarketingRegion == null)
			{
				_MarketingRegion = this.ExternalRegionMappingsToMarketingRegions.GetNewBindingList()
				.AsListSelector<ExternalRegionMappingToMarketingRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<ExternalRegionMappingToMarketingRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ExternalRegionMappingToMarketingRegion 
								{
									ExternalRegionMapping_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<ExternalRegionMappingToMarketingRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ExternalRegionMappingsToMarketingRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketingRegion;
		}
	}
			
	public string[] MarketingRegionIDs
	{
		get
		{
			return MarketingRegion.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketingRegionString
	{
		get
		{
			return String.Join(",", MarketingRegionIDs.ToArray());
		}
	}
	
}

public partial class MarketingMobileTariff
{
	

	private ListSelector<MarketingTariffDeviceType, DeviceType> _DeviceTypes = null;

	public ListSelector<MarketingTariffDeviceType, DeviceType> DeviceTypes
	{
		get
		{
			if (_DeviceTypes == null)
			{
				_DeviceTypes = this.MarketingTariffDeviceTypes.GetNewBindingList()
				.AsListSelector<MarketingTariffDeviceType, DeviceType>
				(
					od => od.DeviceType,
					delegate(IList<MarketingTariffDeviceType> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingTariffDeviceType 
								{
									MarketingMobileTariff_ID = this.Id,
									DeviceType = p
								}
							);
						}
					},
					delegate(IList<MarketingTariffDeviceType> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingTariffDeviceTypes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _DeviceTypes;
		}
	}
			
	public string[] DeviceTypesIDs
	{
		get
		{
			return DeviceTypes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string DeviceTypesString
	{
		get
		{
			return String.Join(",", DeviceTypesIDs.ToArray());
		}
	}
	

	private ListSelector<MobileTariffInCategory, MobileTariffCategory> _Categories = null;

	public ListSelector<MobileTariffInCategory, MobileTariffCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.MobileTariffInCategories.GetNewBindingList()
				.AsListSelector<MobileTariffInCategory, MobileTariffCategory>
				(
					od => od.MobileTariffCategory,
					delegate(IList<MobileTariffInCategory> ods, MobileTariffCategory p)
					{
						var items = ods.Where(od => od.MobileTariffCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MobileTariffInCategory 
								{
									MarketingMobileTariff_ID = this.Id,
									MobileTariffCategory = p
								}
							);
						}
					},
					delegate(IList<MobileTariffInCategory> ods, MobileTariffCategory p)
					{
						var items = ods.Where(od => od.MobileTariffCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MobileTariffInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingMobileTariffInParamGroup, MobileTariffParameterGroup> _PreOpenParamGroups = null;

	public ListSelector<MarketingMobileTariffInParamGroup, MobileTariffParameterGroup> PreOpenParamGroups
	{
		get
		{
			if (_PreOpenParamGroups == null)
			{
				_PreOpenParamGroups = this.MarketingMobileTariffInParamGroups.GetNewBindingList()
				.AsListSelector<MarketingMobileTariffInParamGroup, MobileTariffParameterGroup>
				(
					od => od.MobileTariffParameterGroup,
					delegate(IList<MarketingMobileTariffInParamGroup> ods, MobileTariffParameterGroup p)
					{
						var items = ods.Where(od => od.MobileTariffParameterGroup_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingMobileTariffInParamGroup 
								{
									MarketingMobileTariff_ID = this.Id,
									MobileTariffParameterGroup = p
								}
							);
						}
					},
					delegate(IList<MarketingMobileTariffInParamGroup> ods, MobileTariffParameterGroup p)
					{
						var items = ods.Where(od => od.MobileTariffParameterGroup_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingMobileTariffInParamGroups.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _PreOpenParamGroups;
		}
	}
			
	public string[] PreOpenParamGroupsIDs
	{
		get
		{
			return PreOpenParamGroups.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string PreOpenParamGroupsString
	{
		get
		{
			return String.Join(",", PreOpenParamGroupsIDs.ToArray());
		}
	}
	
}

public partial class TariffGuideQuestion
{
	

	private ListSelector<GuideQuestionDeviceType, DeviceType> _DeviceTypes = null;

	public ListSelector<GuideQuestionDeviceType, DeviceType> DeviceTypes
	{
		get
		{
			if (_DeviceTypes == null)
			{
				_DeviceTypes = this.GuideQuestionDeviceTypes.GetNewBindingList()
				.AsListSelector<GuideQuestionDeviceType, DeviceType>
				(
					od => od.DeviceType,
					delegate(IList<GuideQuestionDeviceType> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new GuideQuestionDeviceType 
								{
									TariffGuideQuestion_ID = this.Id,
									DeviceType = p
								}
							);
						}
					},
					delegate(IList<GuideQuestionDeviceType> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.GuideQuestionDeviceTypes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _DeviceTypes;
		}
	}
			
	public string[] DeviceTypesIDs
	{
		get
		{
			return DeviceTypes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string DeviceTypesString
	{
		get
		{
			return String.Join(",", DeviceTypesIDs.ToArray());
		}
	}
	
}

public partial class MarketingMobileService
{
	

	private ListSelector<MobileServiceInCategory, MobileServiceCategory> _Categories = null;

	public ListSelector<MobileServiceInCategory, MobileServiceCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.MobileServiceInCategories.GetNewBindingList()
				.AsListSelector<MobileServiceInCategory, MobileServiceCategory>
				(
					od => od.MobileServiceCategory,
					delegate(IList<MobileServiceInCategory> ods, MobileServiceCategory p)
					{
						var items = ods.Where(od => od.MobileServiceCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MobileServiceInCategory 
								{
									MarketingMobileService_ID = this.Id,
									MobileServiceCategory = p
								}
							);
						}
					},
					delegate(IList<MobileServiceInCategory> ods, MobileServiceCategory p)
					{
						var items = ods.Where(od => od.MobileServiceCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MobileServiceInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingMobileServiceDeviceType, DeviceType> _DeviceTypes = null;

	public ListSelector<MarketingMobileServiceDeviceType, DeviceType> DeviceTypes
	{
		get
		{
			if (_DeviceTypes == null)
			{
				_DeviceTypes = this.MarketingMobileServiceDeviceTypes.GetNewBindingList()
				.AsListSelector<MarketingMobileServiceDeviceType, DeviceType>
				(
					od => od.DeviceType,
					delegate(IList<MarketingMobileServiceDeviceType> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingMobileServiceDeviceType 
								{
									MarketingMobileService_ID = this.Id,
									DeviceType = p
								}
							);
						}
					},
					delegate(IList<MarketingMobileServiceDeviceType> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingMobileServiceDeviceTypes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _DeviceTypes;
		}
	}
			
	public string[] DeviceTypesIDs
	{
		get
		{
			return DeviceTypes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string DeviceTypesString
	{
		get
		{
			return String.Join(",", DeviceTypesIDs.ToArray());
		}
	}
	

	private ListSelector<ServiceInTariffParameterGroup, MobileTariffParameterGroup> _TariffParameterGroups = null;

	public ListSelector<ServiceInTariffParameterGroup, MobileTariffParameterGroup> TariffParameterGroups
	{
		get
		{
			if (_TariffParameterGroups == null)
			{
				_TariffParameterGroups = this.ServiceInTariffParameterGroups.GetNewBindingList()
				.AsListSelector<ServiceInTariffParameterGroup, MobileTariffParameterGroup>
				(
					od => od.MobileTariffParameterGroup,
					delegate(IList<ServiceInTariffParameterGroup> ods, MobileTariffParameterGroup p)
					{
						var items = ods.Where(od => od.MobileTariffParameterGroup_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ServiceInTariffParameterGroup 
								{
									MarketingMobileService_ID = this.Id,
									MobileTariffParameterGroup = p
								}
							);
						}
					},
					delegate(IList<ServiceInTariffParameterGroup> ods, MobileTariffParameterGroup p)
					{
						var items = ods.Where(od => od.MobileTariffParameterGroup_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ServiceInTariffParameterGroups.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _TariffParameterGroups;
		}
	}
			
	public string[] TariffParameterGroupsIDs
	{
		get
		{
			return TariffParameterGroups.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string TariffParameterGroupsString
	{
		get
		{
			return String.Join(",", TariffParameterGroupsIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingMobileServiceInParamGroup, MobileServiceParameterGroup> _PreOpenParamGroups = null;

	public ListSelector<MarketingMobileServiceInParamGroup, MobileServiceParameterGroup> PreOpenParamGroups
	{
		get
		{
			if (_PreOpenParamGroups == null)
			{
				_PreOpenParamGroups = this.MarketingMobileServiceInParamGroups.GetNewBindingList()
				.AsListSelector<MarketingMobileServiceInParamGroup, MobileServiceParameterGroup>
				(
					od => od.MobileServiceParameterGroup,
					delegate(IList<MarketingMobileServiceInParamGroup> ods, MobileServiceParameterGroup p)
					{
						var items = ods.Where(od => od.MobileServiceParameterGroup_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingMobileServiceInParamGroup 
								{
									MarketingMobileService_ID = this.Id,
									MobileServiceParameterGroup = p
								}
							);
						}
					},
					delegate(IList<MarketingMobileServiceInParamGroup> ods, MobileServiceParameterGroup p)
					{
						var items = ods.Where(od => od.MobileServiceParameterGroup_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingMobileServiceInParamGroups.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _PreOpenParamGroups;
		}
	}
			
	public string[] PreOpenParamGroupsIDs
	{
		get
		{
			return PreOpenParamGroups.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string PreOpenParamGroupsString
	{
		get
		{
			return String.Join(",", PreOpenParamGroupsIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingMobileServiceMobileDeviceType, MobileDevice> _MobileDevices = null;

	public ListSelector<MarketingMobileServiceMobileDeviceType, MobileDevice> MobileDevices
	{
		get
		{
			if (_MobileDevices == null)
			{
				_MobileDevices = this.MarketingMobileServiceMobileDeviceTypes.GetNewBindingList()
				.AsListSelector<MarketingMobileServiceMobileDeviceType, MobileDevice>
				(
					od => od.MobileDevice,
					delegate(IList<MarketingMobileServiceMobileDeviceType> ods, MobileDevice p)
					{
						var items = ods.Where(od => od.MobileDevice_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingMobileServiceMobileDeviceType 
								{
									MarketingMobileService_ID = this.Id,
									MobileDevice = p
								}
							);
						}
					},
					delegate(IList<MarketingMobileServiceMobileDeviceType> ods, MobileDevice p)
					{
						var items = ods.Where(od => od.MobileDevice_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingMobileServiceMobileDeviceTypes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MobileDevices;
		}
	}
			
	public string[] MobileDevicesIDs
	{
		get
		{
			return MobileDevices.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MobileDevicesString
	{
		get
		{
			return String.Join(",", MobileDevicesIDs.ToArray());
		}
	}
	
}

public partial class MobileTariffFamily
{
	

	private ListSelector<MobileTariffFamilyInCategory, MobileTariffCategory> _Categories = null;

	public ListSelector<MobileTariffFamilyInCategory, MobileTariffCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.MobileTariffFamilyInCategories.GetNewBindingList()
				.AsListSelector<MobileTariffFamilyInCategory, MobileTariffCategory>
				(
					od => od.MobileTariffCategory,
					delegate(IList<MobileTariffFamilyInCategory> ods, MobileTariffCategory p)
					{
						var items = ods.Where(od => od.MobileTariffCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MobileTariffFamilyInCategory 
								{
									MobileTariffFamily_ID = this.Id,
									MobileTariffCategory = p
								}
							);
						}
					},
					delegate(IList<MobileTariffFamilyInCategory> ods, MobileTariffCategory p)
					{
						var items = ods.Where(od => od.MobileTariffCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MobileTariffFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	
}

public partial class MutualMobileServiceGroup
{
	

	private ListSelector<MutualMobileService, MarketingMobileService> _MobileServices = null;

	public ListSelector<MutualMobileService, MarketingMobileService> MobileServices
	{
		get
		{
			if (_MobileServices == null)
			{
				_MobileServices = this.MutualMobileServices.GetNewBindingList()
				.AsListSelector<MutualMobileService, MarketingMobileService>
				(
					od => od.MarketingMobileService,
					delegate(IList<MutualMobileService> ods, MarketingMobileService p)
					{
						var items = ods.Where(od => od.MarketingMobileService_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MutualMobileService 
								{
									MutualMobileServiceGroup_ID = this.Id,
									MarketingMobileService = p
								}
							);
						}
					},
					delegate(IList<MutualMobileService> ods, MarketingMobileService p)
					{
						var items = ods.Where(od => od.MarketingMobileService_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MutualMobileServices.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MobileServices;
		}
	}
			
	public string[] MobileServicesIDs
	{
		get
		{
			return MobileServices.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MobileServicesString
	{
		get
		{
			return String.Join(",", MobileServicesIDs.ToArray());
		}
	}
	
}

public partial class MobileServiceFamily
{
	

	private ListSelector<MobileServiceFamilyInCategory, MobileServiceCategory> _Categories = null;

	public ListSelector<MobileServiceFamilyInCategory, MobileServiceCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.MobileServiceFamilyInCategories.GetNewBindingList()
				.AsListSelector<MobileServiceFamilyInCategory, MobileServiceCategory>
				(
					od => od.MobileServiceCategory,
					delegate(IList<MobileServiceFamilyInCategory> ods, MobileServiceCategory p)
					{
						var items = ods.Where(od => od.MobileServiceCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MobileServiceFamilyInCategory 
								{
									MobileServiceFamily_ID = this.Id,
									MobileServiceCategory = p
								}
							);
						}
					},
					delegate(IList<MobileServiceFamilyInCategory> ods, MobileServiceCategory p)
					{
						var items = ods.Where(od => od.MobileServiceCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MobileServiceFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	
}

public partial class MobileTariffCategory
{
	

	private ListSelector<MobileTariffInCategory, MarketingMobileTariff> _MarketingTariffs = null;

	public ListSelector<MobileTariffInCategory, MarketingMobileTariff> MarketingTariffs
	{
		get
		{
			if (_MarketingTariffs == null)
			{
				_MarketingTariffs = this.MobileTariffInCategories.GetNewBindingList()
				.AsListSelector<MobileTariffInCategory, MarketingMobileTariff>
				(
					od => od.MarketingMobileTariff,
					delegate(IList<MobileTariffInCategory> ods, MarketingMobileTariff p)
					{
						var items = ods.Where(od => od.MarketingMobileTariff_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MobileTariffInCategory 
								{
									MobileTariffCategory_ID = this.Id,
									MarketingMobileTariff = p
								}
							);
						}
					},
					delegate(IList<MobileTariffInCategory> ods, MarketingMobileTariff p)
					{
						var items = ods.Where(od => od.MarketingMobileTariff_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MobileTariffInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketingTariffs;
		}
	}
			
	public string[] MarketingTariffsIDs
	{
		get
		{
			return MarketingTariffs.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketingTariffsString
	{
		get
		{
			return String.Join(",", MarketingTariffsIDs.ToArray());
		}
	}
	

	private ListSelector<MobileTariffFamilyInCategory, MobileTariffFamily> _TariffFamilies = null;

	public ListSelector<MobileTariffFamilyInCategory, MobileTariffFamily> TariffFamilies
	{
		get
		{
			if (_TariffFamilies == null)
			{
				_TariffFamilies = this.MobileTariffFamilyInCategories.GetNewBindingList()
				.AsListSelector<MobileTariffFamilyInCategory, MobileTariffFamily>
				(
					od => od.MobileTariffFamily,
					delegate(IList<MobileTariffFamilyInCategory> ods, MobileTariffFamily p)
					{
						var items = ods.Where(od => od.MobileTariffFamily_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MobileTariffFamilyInCategory 
								{
									MobileTariffCategory_ID = this.Id,
									MobileTariffFamily = p
								}
							);
						}
					},
					delegate(IList<MobileTariffFamilyInCategory> ods, MobileTariffFamily p)
					{
						var items = ods.Where(od => od.MobileTariffFamily_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MobileTariffFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _TariffFamilies;
		}
	}
			
	public string[] TariffFamiliesIDs
	{
		get
		{
			return TariffFamilies.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string TariffFamiliesString
	{
		get
		{
			return String.Join(",", TariffFamiliesIDs.ToArray());
		}
	}
	
}

public partial class MobileServiceCategory
{
	

	private ListSelector<MobileServiceInCategory, MarketingMobileService> _MobileServices = null;

	public ListSelector<MobileServiceInCategory, MarketingMobileService> MobileServices
	{
		get
		{
			if (_MobileServices == null)
			{
				_MobileServices = this.MobileServiceInCategories.GetNewBindingList()
				.AsListSelector<MobileServiceInCategory, MarketingMobileService>
				(
					od => od.MarketingMobileService,
					delegate(IList<MobileServiceInCategory> ods, MarketingMobileService p)
					{
						var items = ods.Where(od => od.MarketingMobileService_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MobileServiceInCategory 
								{
									MobileServiceCategory_ID = this.Id,
									MarketingMobileService = p
								}
							);
						}
					},
					delegate(IList<MobileServiceInCategory> ods, MarketingMobileService p)
					{
						var items = ods.Where(od => od.MarketingMobileService_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MobileServiceInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MobileServices;
		}
	}
			
	public string[] MobileServicesIDs
	{
		get
		{
			return MobileServices.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MobileServicesString
	{
		get
		{
			return String.Join(",", MobileServicesIDs.ToArray());
		}
	}
	

	private ListSelector<MobileServiceFamilyInCategory, MobileServiceFamily> _ServiceFamilies = null;

	public ListSelector<MobileServiceFamilyInCategory, MobileServiceFamily> ServiceFamilies
	{
		get
		{
			if (_ServiceFamilies == null)
			{
				_ServiceFamilies = this.MobileServiceFamilyInCategories.GetNewBindingList()
				.AsListSelector<MobileServiceFamilyInCategory, MobileServiceFamily>
				(
					od => od.MobileServiceFamily,
					delegate(IList<MobileServiceFamilyInCategory> ods, MobileServiceFamily p)
					{
						var items = ods.Where(od => od.MobileServiceFamily_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MobileServiceFamilyInCategory 
								{
									MobileServiceCategory_ID = this.Id,
									MobileServiceFamily = p
								}
							);
						}
					},
					delegate(IList<MobileServiceFamilyInCategory> ods, MobileServiceFamily p)
					{
						var items = ods.Where(od => od.MobileServiceFamily_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MobileServiceFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _ServiceFamilies;
		}
	}
			
	public string[] ServiceFamiliesIDs
	{
		get
		{
			return ServiceFamilies.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ServiceFamiliesString
	{
		get
		{
			return String.Join(",", ServiceFamiliesIDs.ToArray());
		}
	}
	
}

public partial class MobileParamsGroupTab
{
	

	private ListSelector<MobileParamGroupInTab, MobileServiceParameterGroup> _Groups = null;

	public ListSelector<MobileParamGroupInTab, MobileServiceParameterGroup> Groups
	{
		get
		{
			if (_Groups == null)
			{
				_Groups = this.MobileParamGroupInTabs.GetNewBindingList()
				.AsListSelector<MobileParamGroupInTab, MobileServiceParameterGroup>
				(
					od => od.MobileServiceParameterGroup,
					delegate(IList<MobileParamGroupInTab> ods, MobileServiceParameterGroup p)
					{
						var items = ods.Where(od => od.MobileServiceParameterGroup_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MobileParamGroupInTab 
								{
									MobileParamsGroupTab_ID = this.Id,
									MobileServiceParameterGroup = p
								}
							);
						}
					},
					delegate(IList<MobileParamGroupInTab> ods, MobileServiceParameterGroup p)
					{
						var items = ods.Where(od => od.MobileServiceParameterGroup_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MobileParamGroupInTabs.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Groups;
		}
	}
			
	public string[] GroupsIDs
	{
		get
		{
			return Groups.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string GroupsString
	{
		get
		{
			return String.Join(",", GroupsIDs.ToArray());
		}
	}
	
}

public partial class PrivelegeAndBonus
{
	

	private ListSelector<PrivelegeAndBonusProduct, SiteProduct> _Products = null;

	public ListSelector<PrivelegeAndBonusProduct, SiteProduct> Products
	{
		get
		{
			if (_Products == null)
			{
				_Products = this.PrivelegeAndBonusProducts.GetNewBindingList()
				.AsListSelector<PrivelegeAndBonusProduct, SiteProduct>
				(
					od => od.SiteProduct,
					delegate(IList<PrivelegeAndBonusProduct> ods, SiteProduct p)
					{
						var items = ods.Where(od => od.SiteProduct_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new PrivelegeAndBonusProduct 
								{
									PrivelegeAndBonus_ID = this.Id,
									SiteProduct = p
								}
							);
						}
					},
					delegate(IList<PrivelegeAndBonusProduct> ods, SiteProduct p)
					{
						var items = ods.Where(od => od.SiteProduct_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.PrivelegeAndBonusProducts.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Products;
		}
	}
			
	public string[] ProductsIDs
	{
		get
		{
			return Products.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ProductsString
	{
		get
		{
			return String.Join(",", ProductsIDs.ToArray());
		}
	}
	

	private ListSelector<PrivelegeAndBonusDeviceType, DeviceType> _DeviceTypes = null;

	public ListSelector<PrivelegeAndBonusDeviceType, DeviceType> DeviceTypes
	{
		get
		{
			if (_DeviceTypes == null)
			{
				_DeviceTypes = this.PrivelegeAndBonusDeviceTypes.GetNewBindingList()
				.AsListSelector<PrivelegeAndBonusDeviceType, DeviceType>
				(
					od => od.DeviceType,
					delegate(IList<PrivelegeAndBonusDeviceType> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new PrivelegeAndBonusDeviceType 
								{
									PrivelegeAndBonus_ID = this.Id,
									DeviceType = p
								}
							);
						}
					},
					delegate(IList<PrivelegeAndBonusDeviceType> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.PrivelegeAndBonusDeviceTypes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _DeviceTypes;
		}
	}
			
	public string[] DeviceTypesIDs
	{
		get
		{
			return DeviceTypes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string DeviceTypesString
	{
		get
		{
			return String.Join(",", DeviceTypesIDs.ToArray());
		}
	}
	

	private ListSelector<PrivelegeAndBonusInRegion, MarketingRegion> _Regions = null;

	public ListSelector<PrivelegeAndBonusInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.PrivelegeAndBonusInRegions.GetNewBindingList()
				.AsListSelector<PrivelegeAndBonusInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<PrivelegeAndBonusInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new PrivelegeAndBonusInRegion 
								{
									PrivelegeAndBonus_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<PrivelegeAndBonusInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.PrivelegeAndBonusInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class FaqContent
{
	

	private ListSelector<FaqContentToRegion, MarketingRegion> _Regions = null;

	public ListSelector<FaqContentToRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.FaqContentToRegions.GetNewBindingList()
				.AsListSelector<FaqContentToRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<FaqContentToRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new FaqContentToRegion 
								{
									FaqContent_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<FaqContentToRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.FaqContentToRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	

	private ListSelector<FeedbackSubThemeToFaqContent, FeedbackSubtheme> _FeedbackSubThemes = null;

	public ListSelector<FeedbackSubThemeToFaqContent, FeedbackSubtheme> FeedbackSubThemes
	{
		get
		{
			if (_FeedbackSubThemes == null)
			{
				_FeedbackSubThemes = this.FeedbackSubThemesToFaqContents.GetNewBindingList()
				.AsListSelector<FeedbackSubThemeToFaqContent, FeedbackSubtheme>
				(
					od => od.FeedbackSubtheme,
					delegate(IList<FeedbackSubThemeToFaqContent> ods, FeedbackSubtheme p)
					{
						var items = ods.Where(od => od.FeedbackSubtheme_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new FeedbackSubThemeToFaqContent 
								{
									FaqContent_ID = this.Id,
									FeedbackSubtheme = p
								}
							);
						}
					},
					delegate(IList<FeedbackSubThemeToFaqContent> ods, FeedbackSubtheme p)
					{
						var items = ods.Where(od => od.FeedbackSubtheme_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.FeedbackSubThemesToFaqContents.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _FeedbackSubThemes;
		}
	}
			
	public string[] FeedbackSubThemesIDs
	{
		get
		{
			return FeedbackSubThemes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string FeedbackSubThemesString
	{
		get
		{
			return String.Join(",", FeedbackSubThemesIDs.ToArray());
		}
	}
	

	private ListSelector<FeedbackThemeToFaqContent, FeedbackTheme> _FeedbackThemes = null;

	public ListSelector<FeedbackThemeToFaqContent, FeedbackTheme> FeedbackThemes
	{
		get
		{
			if (_FeedbackThemes == null)
			{
				_FeedbackThemes = this.FeedbackThemesToFaqContents.GetNewBindingList()
				.AsListSelector<FeedbackThemeToFaqContent, FeedbackTheme>
				(
					od => od.FeedbackTheme,
					delegate(IList<FeedbackThemeToFaqContent> ods, FeedbackTheme p)
					{
						var items = ods.Where(od => od.FeedbackTheme_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new FeedbackThemeToFaqContent 
								{
									FaqContent_ID = this.Id,
									FeedbackTheme = p
								}
							);
						}
					},
					delegate(IList<FeedbackThemeToFaqContent> ods, FeedbackTheme p)
					{
						var items = ods.Where(od => od.FeedbackTheme_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.FeedbackThemesToFaqContents.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _FeedbackThemes;
		}
	}
			
	public string[] FeedbackThemesIDs
	{
		get
		{
			return FeedbackThemes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string FeedbackThemesString
	{
		get
		{
			return String.Join(",", FeedbackThemesIDs.ToArray());
		}
	}
	
}

public partial class TVChannelTheme
{
	

	private ListSelector<TVChannelsInTheme, TVChannel> _Channels = null;

	public ListSelector<TVChannelsInTheme, TVChannel> Channels
	{
		get
		{
			if (_Channels == null)
			{
				_Channels = this.TVChannelsInThemes.GetNewBindingList()
				.AsListSelector<TVChannelsInTheme, TVChannel>
				(
					od => od.TVChannel,
					delegate(IList<TVChannelsInTheme> ods, TVChannel p)
					{
						var items = ods.Where(od => od.TVChannel_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new TVChannelsInTheme 
								{
									TVChannelTheme_ID = this.Id,
									TVChannel = p
								}
							);
						}
					},
					delegate(IList<TVChannelsInTheme> ods, TVChannel p)
					{
						var items = ods.Where(od => od.TVChannel_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.TVChannelsInThemes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Channels;
		}
	}
			
	public string[] ChannelsIDs
	{
		get
		{
			return Channels.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ChannelsString
	{
		get
		{
			return String.Join(",", ChannelsIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingTVPackageInTheme, MarketingTVPackage> _MarketingTVPackages = null;

	public ListSelector<MarketingTVPackageInTheme, MarketingTVPackage> MarketingTVPackages
	{
		get
		{
			if (_MarketingTVPackages == null)
			{
				_MarketingTVPackages = this.MarketingTVPackageInThemes.GetNewBindingList()
				.AsListSelector<MarketingTVPackageInTheme, MarketingTVPackage>
				(
					od => od.MarketingTVPackage,
					delegate(IList<MarketingTVPackageInTheme> ods, MarketingTVPackage p)
					{
						var items = ods.Where(od => od.MarketingTVPackage_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingTVPackageInTheme 
								{
									TVChannelTheme_ID = this.Id,
									MarketingTVPackage = p
								}
							);
						}
					},
					delegate(IList<MarketingTVPackageInTheme> ods, MarketingTVPackage p)
					{
						var items = ods.Where(od => od.MarketingTVPackage_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingTVPackageInThemes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketingTVPackages;
		}
	}
			
	public string[] MarketingTVPackagesIDs
	{
		get
		{
			return MarketingTVPackages.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketingTVPackagesString
	{
		get
		{
			return String.Join(",", MarketingTVPackagesIDs.ToArray());
		}
	}
	
}

public partial class MarketingTVPackage
{
	

	private ListSelector<MarketingTVPackageInTheme, TVChannelTheme> _Themes = null;

	public ListSelector<MarketingTVPackageInTheme, TVChannelTheme> Themes
	{
		get
		{
			if (_Themes == null)
			{
				_Themes = this.MarketingTVPackageInThemes.GetNewBindingList()
				.AsListSelector<MarketingTVPackageInTheme, TVChannelTheme>
				(
					od => od.TVChannelTheme,
					delegate(IList<MarketingTVPackageInTheme> ods, TVChannelTheme p)
					{
						var items = ods.Where(od => od.TVChannelTheme_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingTVPackageInTheme 
								{
									MarketingTVPackage_ID = this.Id,
									TVChannelTheme = p
								}
							);
						}
					},
					delegate(IList<MarketingTVPackageInTheme> ods, TVChannelTheme p)
					{
						var items = ods.Where(od => od.TVChannelTheme_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingTVPackageInThemes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Themes;
		}
	}
			
	public string[] ThemesIDs
	{
		get
		{
			return Themes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ThemesString
	{
		get
		{
			return String.Join(",", ThemesIDs.ToArray());
		}
	}
	

	private ListSelector<TVChannelsInMarketingPackage, TVChannel> _Channels = null;

	public ListSelector<TVChannelsInMarketingPackage, TVChannel> Channels
	{
		get
		{
			if (_Channels == null)
			{
				_Channels = this.TVChannelsInMarketingPackages.GetNewBindingList()
				.AsListSelector<TVChannelsInMarketingPackage, TVChannel>
				(
					od => od.TVChannel,
					delegate(IList<TVChannelsInMarketingPackage> ods, TVChannel p)
					{
						var items = ods.Where(od => od.TVChannel_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new TVChannelsInMarketingPackage 
								{
									MarketingTVPackage_ID = this.Id,
									TVChannel = p
								}
							);
						}
					},
					delegate(IList<TVChannelsInMarketingPackage> ods, TVChannel p)
					{
						var items = ods.Where(od => od.TVChannel_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.TVChannelsInMarketingPackages.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Channels;
		}
	}
			
	public string[] ChannelsIDs
	{
		get
		{
			return Channels.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ChannelsString
	{
		get
		{
			return String.Join(",", ChannelsIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingTVPackageInMutualGroup, MutualTVPackageGroup> _MutualGroups = null;

	public ListSelector<MarketingTVPackageInMutualGroup, MutualTVPackageGroup> MutualGroups
	{
		get
		{
			if (_MutualGroups == null)
			{
				_MutualGroups = this.MarketingTVPackageInMutualGroups.GetNewBindingList()
				.AsListSelector<MarketingTVPackageInMutualGroup, MutualTVPackageGroup>
				(
					od => od.MutualTVPackageGroup,
					delegate(IList<MarketingTVPackageInMutualGroup> ods, MutualTVPackageGroup p)
					{
						var items = ods.Where(od => od.MutualTVPackageGroup_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingTVPackageInMutualGroup 
								{
									MarketingTVPackage_ID = this.Id,
									MutualTVPackageGroup = p
								}
							);
						}
					},
					delegate(IList<MarketingTVPackageInMutualGroup> ods, MutualTVPackageGroup p)
					{
						var items = ods.Where(od => od.MutualTVPackageGroup_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingTVPackageInMutualGroups.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MutualGroups;
		}
	}
			
	public string[] MutualGroupsIDs
	{
		get
		{
			return MutualGroups.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MutualGroupsString
	{
		get
		{
			return String.Join(",", MutualGroupsIDs.ToArray());
		}
	}
	

	private ListSelector<UnavailableServiceInMarketingTVPackage, MarketingProvodService> _UnavailableServices = null;

	public ListSelector<UnavailableServiceInMarketingTVPackage, MarketingProvodService> UnavailableServices
	{
		get
		{
			if (_UnavailableServices == null)
			{
				_UnavailableServices = this.UnavailableServiceInMarketingTVPackages.GetNewBindingList()
				.AsListSelector<UnavailableServiceInMarketingTVPackage, MarketingProvodService>
				(
					od => od.MarketingProvodService,
					delegate(IList<UnavailableServiceInMarketingTVPackage> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new UnavailableServiceInMarketingTVPackage 
								{
									MarketingTVPackage_ID = this.Id,
									MarketingProvodService = p
								}
							);
						}
					},
					delegate(IList<UnavailableServiceInMarketingTVPackage> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.UnavailableServiceInMarketingTVPackages.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _UnavailableServices;
		}
	}
			
	public string[] UnavailableServicesIDs
	{
		get
		{
			return UnavailableServices.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string UnavailableServicesString
	{
		get
		{
			return String.Join(",", UnavailableServicesIDs.ToArray());
		}
	}
	
}

public partial class QuickLinksGroup
{
	

	private ListSelector<QuickLinksGroupInRegion, MarketingRegion> _Regions = null;

	public ListSelector<QuickLinksGroupInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.QuickLinksGroupInRegions.GetNewBindingList()
				.AsListSelector<QuickLinksGroupInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<QuickLinksGroupInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new QuickLinksGroupInRegion 
								{
									QuickLinksGroup_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<QuickLinksGroupInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.QuickLinksGroupInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class ProvodTariffParameterGroup
{
	

	private ListSelector<ProvodTariffParameterGroupInProduct, DeviceType> _Product = null;

	public ListSelector<ProvodTariffParameterGroupInProduct, DeviceType> Product
	{
		get
		{
			if (_Product == null)
			{
				_Product = this.ProvodTariffParameterGroupInProducts.GetNewBindingList()
				.AsListSelector<ProvodTariffParameterGroupInProduct, DeviceType>
				(
					od => od.DeviceType,
					delegate(IList<ProvodTariffParameterGroupInProduct> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ProvodTariffParameterGroupInProduct 
								{
									ProvodTariffParameterGroup_ID = this.Id,
									DeviceType = p
								}
							);
						}
					},
					delegate(IList<ProvodTariffParameterGroupInProduct> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ProvodTariffParameterGroupInProducts.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Product;
		}
	}
			
	public string[] ProductIDs
	{
		get
		{
			return Product.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ProductString
	{
		get
		{
			return String.Join(",", ProductIDs.ToArray());
		}
	}
	
}

public partial class InternetTariffFamily
{
	

	private ListSelector<InternetTariffFamilyInCategory, InternetTariffCategory> _Categories = null;

	public ListSelector<InternetTariffFamilyInCategory, InternetTariffCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.InternetTariffFamilyInCategories.GetNewBindingList()
				.AsListSelector<InternetTariffFamilyInCategory, InternetTariffCategory>
				(
					od => od.InternetTariffCategory,
					delegate(IList<InternetTariffFamilyInCategory> ods, InternetTariffCategory p)
					{
						var items = ods.Where(od => od.InternetTariffCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new InternetTariffFamilyInCategory 
								{
									InternetTariffFamily_ID = this.Id,
									InternetTariffCategory = p
								}
							);
						}
					},
					delegate(IList<InternetTariffFamilyInCategory> ods, InternetTariffCategory p)
					{
						var items = ods.Where(od => od.InternetTariffCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.InternetTariffFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	
}

public partial class PhoneTariffFamily
{
	

	private ListSelector<PhoneTariffFamilyInCategory, PhoneTariffCategory> _Categories = null;

	public ListSelector<PhoneTariffFamilyInCategory, PhoneTariffCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.PhoneTariffFamilyInCategories.GetNewBindingList()
				.AsListSelector<PhoneTariffFamilyInCategory, PhoneTariffCategory>
				(
					od => od.PhoneTariffCategory,
					delegate(IList<PhoneTariffFamilyInCategory> ods, PhoneTariffCategory p)
					{
						var items = ods.Where(od => od.PhoneTariffCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new PhoneTariffFamilyInCategory 
								{
									PhoneTariffFamily_ID = this.Id,
									PhoneTariffCategory = p
								}
							);
						}
					},
					delegate(IList<PhoneTariffFamilyInCategory> ods, PhoneTariffCategory p)
					{
						var items = ods.Where(od => od.PhoneTariffCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.PhoneTariffFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	
}

public partial class ProvodServiceFamily
{
	

	private ListSelector<ProvodServiceFamilyInCategory, ProvodServiceCategory> _Categories = null;

	public ListSelector<ProvodServiceFamilyInCategory, ProvodServiceCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.ProvodServiceFamilyInCategories.GetNewBindingList()
				.AsListSelector<ProvodServiceFamilyInCategory, ProvodServiceCategory>
				(
					od => od.ProvodServiceCategory,
					delegate(IList<ProvodServiceFamilyInCategory> ods, ProvodServiceCategory p)
					{
						var items = ods.Where(od => od.ProvodServiceCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ProvodServiceFamilyInCategory 
								{
									ProvodServiceFamily_ID = this.Id,
									ProvodServiceCategory = p
								}
							);
						}
					},
					delegate(IList<ProvodServiceFamilyInCategory> ods, ProvodServiceCategory p)
					{
						var items = ods.Where(od => od.ProvodServiceCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ProvodServiceFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	
}

public partial class InternetTariffCategory
{
	

	private ListSelector<InternetTariffFamilyInCategory, InternetTariffFamily> _Families = null;

	public ListSelector<InternetTariffFamilyInCategory, InternetTariffFamily> Families
	{
		get
		{
			if (_Families == null)
			{
				_Families = this.InternetTariffFamilyInCategories.GetNewBindingList()
				.AsListSelector<InternetTariffFamilyInCategory, InternetTariffFamily>
				(
					od => od.InternetTariffFamily,
					delegate(IList<InternetTariffFamilyInCategory> ods, InternetTariffFamily p)
					{
						var items = ods.Where(od => od.InternetTariffFamily_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new InternetTariffFamilyInCategory 
								{
									InternetTariffCategory_ID = this.Id,
									InternetTariffFamily = p
								}
							);
						}
					},
					delegate(IList<InternetTariffFamilyInCategory> ods, InternetTariffFamily p)
					{
						var items = ods.Where(od => od.InternetTariffFamily_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.InternetTariffFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Families;
		}
	}
			
	public string[] FamiliesIDs
	{
		get
		{
			return Families.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string FamiliesString
	{
		get
		{
			return String.Join(",", FamiliesIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingInternetTariffInCategory, MarketingInternetTariff> _MarketingTariffs = null;

	public ListSelector<MarketingInternetTariffInCategory, MarketingInternetTariff> MarketingTariffs
	{
		get
		{
			if (_MarketingTariffs == null)
			{
				_MarketingTariffs = this.MarketingInternetTariffInCategories.GetNewBindingList()
				.AsListSelector<MarketingInternetTariffInCategory, MarketingInternetTariff>
				(
					od => od.MarketingInternetTariff,
					delegate(IList<MarketingInternetTariffInCategory> ods, MarketingInternetTariff p)
					{
						var items = ods.Where(od => od.MarketingInternetTariff_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingInternetTariffInCategory 
								{
									InternetTariffCategory_ID = this.Id,
									MarketingInternetTariff = p
								}
							);
						}
					},
					delegate(IList<MarketingInternetTariffInCategory> ods, MarketingInternetTariff p)
					{
						var items = ods.Where(od => od.MarketingInternetTariff_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingInternetTariffInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketingTariffs;
		}
	}
			
	public string[] MarketingTariffsIDs
	{
		get
		{
			return MarketingTariffs.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketingTariffsString
	{
		get
		{
			return String.Join(",", MarketingTariffsIDs.ToArray());
		}
	}
	
}

public partial class PhoneTariffCategory
{
	

	private ListSelector<PhoneTariffFamilyInCategory, PhoneTariffFamily> _Families = null;

	public ListSelector<PhoneTariffFamilyInCategory, PhoneTariffFamily> Families
	{
		get
		{
			if (_Families == null)
			{
				_Families = this.PhoneTariffFamilyInCategories.GetNewBindingList()
				.AsListSelector<PhoneTariffFamilyInCategory, PhoneTariffFamily>
				(
					od => od.PhoneTariffFamily,
					delegate(IList<PhoneTariffFamilyInCategory> ods, PhoneTariffFamily p)
					{
						var items = ods.Where(od => od.PhoneTariffFamily_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new PhoneTariffFamilyInCategory 
								{
									PhoneTariffCategory_ID = this.Id,
									PhoneTariffFamily = p
								}
							);
						}
					},
					delegate(IList<PhoneTariffFamilyInCategory> ods, PhoneTariffFamily p)
					{
						var items = ods.Where(od => od.PhoneTariffFamily_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.PhoneTariffFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Families;
		}
	}
			
	public string[] FamiliesIDs
	{
		get
		{
			return Families.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string FamiliesString
	{
		get
		{
			return String.Join(",", FamiliesIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingPhoneTariffInCategory, MarketingPhoneTariff> _MarketingPhoneTariffs = null;

	public ListSelector<MarketingPhoneTariffInCategory, MarketingPhoneTariff> MarketingPhoneTariffs
	{
		get
		{
			if (_MarketingPhoneTariffs == null)
			{
				_MarketingPhoneTariffs = this.MarketingPhoneTariffInCategories.GetNewBindingList()
				.AsListSelector<MarketingPhoneTariffInCategory, MarketingPhoneTariff>
				(
					od => od.MarketingPhoneTariff,
					delegate(IList<MarketingPhoneTariffInCategory> ods, MarketingPhoneTariff p)
					{
						var items = ods.Where(od => od.MarketingPhoneTariff_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingPhoneTariffInCategory 
								{
									PhoneTariffCategory_ID = this.Id,
									MarketingPhoneTariff = p
								}
							);
						}
					},
					delegate(IList<MarketingPhoneTariffInCategory> ods, MarketingPhoneTariff p)
					{
						var items = ods.Where(od => od.MarketingPhoneTariff_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingPhoneTariffInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketingPhoneTariffs;
		}
	}
			
	public string[] MarketingPhoneTariffsIDs
	{
		get
		{
			return MarketingPhoneTariffs.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketingPhoneTariffsString
	{
		get
		{
			return String.Join(",", MarketingPhoneTariffsIDs.ToArray());
		}
	}
	
}

public partial class ProvodServiceCategory
{
	

	private ListSelector<MarketingProvodServiceInCategory, MarketingProvodService> _MarketingProvodServices = null;

	public ListSelector<MarketingProvodServiceInCategory, MarketingProvodService> MarketingProvodServices
	{
		get
		{
			if (_MarketingProvodServices == null)
			{
				_MarketingProvodServices = this.MarketingProvodServiceInCategories.GetNewBindingList()
				.AsListSelector<MarketingProvodServiceInCategory, MarketingProvodService>
				(
					od => od.MarketingProvodService,
					delegate(IList<MarketingProvodServiceInCategory> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingProvodServiceInCategory 
								{
									ProvodServiceCategory_ID = this.Id,
									MarketingProvodService = p
								}
							);
						}
					},
					delegate(IList<MarketingProvodServiceInCategory> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingProvodServiceInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketingProvodServices;
		}
	}
			
	public string[] MarketingProvodServicesIDs
	{
		get
		{
			return MarketingProvodServices.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketingProvodServicesString
	{
		get
		{
			return String.Join(",", MarketingProvodServicesIDs.ToArray());
		}
	}
	

	private ListSelector<ProvodServiceFamilyInCategory, ProvodServiceFamily> _ProvodServiceFamilies = null;

	public ListSelector<ProvodServiceFamilyInCategory, ProvodServiceFamily> ProvodServiceFamilies
	{
		get
		{
			if (_ProvodServiceFamilies == null)
			{
				_ProvodServiceFamilies = this.ProvodServiceFamilyInCategories.GetNewBindingList()
				.AsListSelector<ProvodServiceFamilyInCategory, ProvodServiceFamily>
				(
					od => od.ProvodServiceFamily,
					delegate(IList<ProvodServiceFamilyInCategory> ods, ProvodServiceFamily p)
					{
						var items = ods.Where(od => od.ProvodServiceFamily_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ProvodServiceFamilyInCategory 
								{
									ProvodServiceCategory_ID = this.Id,
									ProvodServiceFamily = p
								}
							);
						}
					},
					delegate(IList<ProvodServiceFamilyInCategory> ods, ProvodServiceFamily p)
					{
						var items = ods.Where(od => od.ProvodServiceFamily_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ProvodServiceFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _ProvodServiceFamilies;
		}
	}
			
	public string[] ProvodServiceFamiliesIDs
	{
		get
		{
			return ProvodServiceFamilies.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ProvodServiceFamiliesString
	{
		get
		{
			return String.Join(",", ProvodServiceFamiliesIDs.ToArray());
		}
	}
	
}

public partial class MarketingInternetTariff
{
	

	private ListSelector<MarketingInternetTariffInCategory, InternetTariffCategory> _Categories = null;

	public ListSelector<MarketingInternetTariffInCategory, InternetTariffCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.MarketingInternetTariffInCategories.GetNewBindingList()
				.AsListSelector<MarketingInternetTariffInCategory, InternetTariffCategory>
				(
					od => od.InternetTariffCategory,
					delegate(IList<MarketingInternetTariffInCategory> ods, InternetTariffCategory p)
					{
						var items = ods.Where(od => od.InternetTariffCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingInternetTariffInCategory 
								{
									MarketingInternetTariff_ID = this.Id,
									InternetTariffCategory = p
								}
							);
						}
					},
					delegate(IList<MarketingInternetTariffInCategory> ods, InternetTariffCategory p)
					{
						var items = ods.Where(od => od.InternetTariffCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingInternetTariffInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	

	private ListSelector<UnavailableServicesInInternetTariff, MarketingProvodService> _UnavailableServices = null;

	public ListSelector<UnavailableServicesInInternetTariff, MarketingProvodService> UnavailableServices
	{
		get
		{
			if (_UnavailableServices == null)
			{
				_UnavailableServices = this.UnavailableServicesInInternetTariffs.GetNewBindingList()
				.AsListSelector<UnavailableServicesInInternetTariff, MarketingProvodService>
				(
					od => od.MarketingProvodService,
					delegate(IList<UnavailableServicesInInternetTariff> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new UnavailableServicesInInternetTariff 
								{
									MarketingInternetTariff_ID = this.Id,
									MarketingProvodService = p
								}
							);
						}
					},
					delegate(IList<UnavailableServicesInInternetTariff> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.UnavailableServicesInInternetTariffs.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _UnavailableServices;
		}
	}
			
	public string[] UnavailableServicesIDs
	{
		get
		{
			return UnavailableServices.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string UnavailableServicesString
	{
		get
		{
			return String.Join(",", UnavailableServicesIDs.ToArray());
		}
	}
	
}

public partial class MarketingPhoneTariff
{
	

	private ListSelector<MarketingPhoneTariffInCategory, PhoneTariffCategory> _Categories = null;

	public ListSelector<MarketingPhoneTariffInCategory, PhoneTariffCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.MarketingPhoneTariffInCategories.GetNewBindingList()
				.AsListSelector<MarketingPhoneTariffInCategory, PhoneTariffCategory>
				(
					od => od.PhoneTariffCategory,
					delegate(IList<MarketingPhoneTariffInCategory> ods, PhoneTariffCategory p)
					{
						var items = ods.Where(od => od.PhoneTariffCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingPhoneTariffInCategory 
								{
									MarketingPhoneTariff_ID = this.Id,
									PhoneTariffCategory = p
								}
							);
						}
					},
					delegate(IList<MarketingPhoneTariffInCategory> ods, PhoneTariffCategory p)
					{
						var items = ods.Where(od => od.PhoneTariffCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingPhoneTariffInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	

	private ListSelector<UnavailableServiceInMarketingPhoneTariff, MarketingProvodService> _UnavailableServices = null;

	public ListSelector<UnavailableServiceInMarketingPhoneTariff, MarketingProvodService> UnavailableServices
	{
		get
		{
			if (_UnavailableServices == null)
			{
				_UnavailableServices = this.UnavailableServiceInMarketingPhoneTariffs.GetNewBindingList()
				.AsListSelector<UnavailableServiceInMarketingPhoneTariff, MarketingProvodService>
				(
					od => od.MarketingProvodService,
					delegate(IList<UnavailableServiceInMarketingPhoneTariff> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new UnavailableServiceInMarketingPhoneTariff 
								{
									MarketingPhoneTariff_ID = this.Id,
									MarketingProvodService = p
								}
							);
						}
					},
					delegate(IList<UnavailableServiceInMarketingPhoneTariff> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.UnavailableServiceInMarketingPhoneTariffs.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _UnavailableServices;
		}
	}
			
	public string[] UnavailableServicesIDs
	{
		get
		{
			return UnavailableServices.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string UnavailableServicesString
	{
		get
		{
			return String.Join(",", UnavailableServicesIDs.ToArray());
		}
	}
	
}

public partial class MarketingProvodService
{
	

	private ListSelector<MarketingProvodServiceInProduct, DeviceType> _Products = null;

	public ListSelector<MarketingProvodServiceInProduct, DeviceType> Products
	{
		get
		{
			if (_Products == null)
			{
				_Products = this.MarketingProvodServiceInProducts.GetNewBindingList()
				.AsListSelector<MarketingProvodServiceInProduct, DeviceType>
				(
					od => od.DeviceType,
					delegate(IList<MarketingProvodServiceInProduct> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingProvodServiceInProduct 
								{
									MarketingProvodService_ID = this.Id,
									DeviceType = p
								}
							);
						}
					},
					delegate(IList<MarketingProvodServiceInProduct> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingProvodServiceInProducts.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Products;
		}
	}
			
	public string[] ProductsIDs
	{
		get
		{
			return Products.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ProductsString
	{
		get
		{
			return String.Join(",", ProductsIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingProvodServiceInCategory, ProvodServiceCategory> _Categories = null;

	public ListSelector<MarketingProvodServiceInCategory, ProvodServiceCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.MarketingProvodServiceInCategories.GetNewBindingList()
				.AsListSelector<MarketingProvodServiceInCategory, ProvodServiceCategory>
				(
					od => od.ProvodServiceCategory,
					delegate(IList<MarketingProvodServiceInCategory> ods, ProvodServiceCategory p)
					{
						var items = ods.Where(od => od.ProvodServiceCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingProvodServiceInCategory 
								{
									MarketingProvodService_ID = this.Id,
									ProvodServiceCategory = p
								}
							);
						}
					},
					delegate(IList<MarketingProvodServiceInCategory> ods, ProvodServiceCategory p)
					{
						var items = ods.Where(od => od.ProvodServiceCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingProvodServiceInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	
}

public partial class ProvodKitFamily
{
	

	private ListSelector<ProvodKitFamilyInCategory, ProvodKitCategory> _Categories = null;

	public ListSelector<ProvodKitFamilyInCategory, ProvodKitCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.ProvodKitFamilyInCategories.GetNewBindingList()
				.AsListSelector<ProvodKitFamilyInCategory, ProvodKitCategory>
				(
					od => od.ProvodKitCategory,
					delegate(IList<ProvodKitFamilyInCategory> ods, ProvodKitCategory p)
					{
						var items = ods.Where(od => od.ProvodKitCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ProvodKitFamilyInCategory 
								{
									ProvodKitFamily_ID = this.Id,
									ProvodKitCategory = p
								}
							);
						}
					},
					delegate(IList<ProvodKitFamilyInCategory> ods, ProvodKitCategory p)
					{
						var items = ods.Where(od => od.ProvodKitCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ProvodKitFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	
}

public partial class ProvodKitCategory
{
	

	private ListSelector<ProvodKitFamilyInCategory, ProvodKitFamily> _Families = null;

	public ListSelector<ProvodKitFamilyInCategory, ProvodKitFamily> Families
	{
		get
		{
			if (_Families == null)
			{
				_Families = this.ProvodKitFamilyInCategories.GetNewBindingList()
				.AsListSelector<ProvodKitFamilyInCategory, ProvodKitFamily>
				(
					od => od.ProvodKitFamily,
					delegate(IList<ProvodKitFamilyInCategory> ods, ProvodKitFamily p)
					{
						var items = ods.Where(od => od.ProvodKitFamily_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ProvodKitFamilyInCategory 
								{
									ProvodKitCategory_ID = this.Id,
									ProvodKitFamily = p
								}
							);
						}
					},
					delegate(IList<ProvodKitFamilyInCategory> ods, ProvodKitFamily p)
					{
						var items = ods.Where(od => od.ProvodKitFamily_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ProvodKitFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Families;
		}
	}
			
	public string[] FamiliesIDs
	{
		get
		{
			return Families.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string FamiliesString
	{
		get
		{
			return String.Join(",", FamiliesIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingProvodKitInCategory, MarketingProvodKit> _MarketingProvodKits = null;

	public ListSelector<MarketingProvodKitInCategory, MarketingProvodKit> MarketingProvodKits
	{
		get
		{
			if (_MarketingProvodKits == null)
			{
				_MarketingProvodKits = this.MarketingProvodKitInCategories.GetNewBindingList()
				.AsListSelector<MarketingProvodKitInCategory, MarketingProvodKit>
				(
					od => od.MarketingProvodKit,
					delegate(IList<MarketingProvodKitInCategory> ods, MarketingProvodKit p)
					{
						var items = ods.Where(od => od.MarketingProvodKit_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingProvodKitInCategory 
								{
									ProvodKitCategory_ID = this.Id,
									MarketingProvodKit = p
								}
							);
						}
					},
					delegate(IList<MarketingProvodKitInCategory> ods, MarketingProvodKit p)
					{
						var items = ods.Where(od => od.MarketingProvodKit_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingProvodKitInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketingProvodKits;
		}
	}
			
	public string[] MarketingProvodKitsIDs
	{
		get
		{
			return MarketingProvodKits.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketingProvodKitsString
	{
		get
		{
			return String.Join(",", MarketingProvodKitsIDs.ToArray());
		}
	}
	
}

public partial class MarketingProvodKit
{
	

	private ListSelector<MarketingProvodKitInCategory, ProvodKitCategory> _Categories = null;

	public ListSelector<MarketingProvodKitInCategory, ProvodKitCategory> Categories
	{
		get
		{
			if (_Categories == null)
			{
				_Categories = this.MarketingProvodKitInCategories.GetNewBindingList()
				.AsListSelector<MarketingProvodKitInCategory, ProvodKitCategory>
				(
					od => od.ProvodKitCategory,
					delegate(IList<MarketingProvodKitInCategory> ods, ProvodKitCategory p)
					{
						var items = ods.Where(od => od.ProvodKitCategory_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingProvodKitInCategory 
								{
									MarketingProvodKit_ID = this.Id,
									ProvodKitCategory = p
								}
							);
						}
					},
					delegate(IList<MarketingProvodKitInCategory> ods, ProvodKitCategory p)
					{
						var items = ods.Where(od => od.ProvodKitCategory_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingProvodKitInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Categories;
		}
	}
			
	public string[] CategoriesIDs
	{
		get
		{
			return Categories.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CategoriesString
	{
		get
		{
			return String.Join(",", CategoriesIDs.ToArray());
		}
	}
	

	private ListSelector<TVPackagesInProvodKit, MarketingTVPackage> _TVPackages = null;

	public ListSelector<TVPackagesInProvodKit, MarketingTVPackage> TVPackages
	{
		get
		{
			if (_TVPackages == null)
			{
				_TVPackages = this.TVPackagesInProvodKits.GetNewBindingList()
				.AsListSelector<TVPackagesInProvodKit, MarketingTVPackage>
				(
					od => od.MarketingTVPackage,
					delegate(IList<TVPackagesInProvodKit> ods, MarketingTVPackage p)
					{
						var items = ods.Where(od => od.MarketingTVPackage_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new TVPackagesInProvodKit 
								{
									MarketingProvodKit_ID = this.Id,
									MarketingTVPackage = p
								}
							);
						}
					},
					delegate(IList<TVPackagesInProvodKit> ods, MarketingTVPackage p)
					{
						var items = ods.Where(od => od.MarketingTVPackage_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.TVPackagesInProvodKits.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _TVPackages;
		}
	}
			
	public string[] TVPackagesIDs
	{
		get
		{
			return TVPackages.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string TVPackagesString
	{
		get
		{
			return String.Join(",", TVPackagesIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingProvodServiceInKit, MarketingProvodService> _MarketingProvodServices = null;

	public ListSelector<MarketingProvodServiceInKit, MarketingProvodService> MarketingProvodServices
	{
		get
		{
			if (_MarketingProvodServices == null)
			{
				_MarketingProvodServices = this.MarketingProvodServiceInKits.GetNewBindingList()
				.AsListSelector<MarketingProvodServiceInKit, MarketingProvodService>
				(
					od => od.MarketingProvodService,
					delegate(IList<MarketingProvodServiceInKit> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingProvodServiceInKit 
								{
									MarketingProvodKit_ID = this.Id,
									MarketingProvodService = p
								}
							);
						}
					},
					delegate(IList<MarketingProvodServiceInKit> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingProvodServiceInKits.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketingProvodServices;
		}
	}
			
	public string[] MarketingProvodServicesIDs
	{
		get
		{
			return MarketingProvodServices.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketingProvodServicesString
	{
		get
		{
			return String.Join(",", MarketingProvodServicesIDs.ToArray());
		}
	}
	

	private ListSelector<UnavailableServiceInMarketingProvodKit, MarketingProvodService> _UnavailableServices = null;

	public ListSelector<UnavailableServiceInMarketingProvodKit, MarketingProvodService> UnavailableServices
	{
		get
		{
			if (_UnavailableServices == null)
			{
				_UnavailableServices = this.UnavailableServiceInMarketingProvodKits.GetNewBindingList()
				.AsListSelector<UnavailableServiceInMarketingProvodKit, MarketingProvodService>
				(
					od => od.MarketingProvodService,
					delegate(IList<UnavailableServiceInMarketingProvodKit> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new UnavailableServiceInMarketingProvodKit 
								{
									MarketingProvodKit_ID = this.Id,
									MarketingProvodService = p
								}
							);
						}
					},
					delegate(IList<UnavailableServiceInMarketingProvodKit> ods, MarketingProvodService p)
					{
						var items = ods.Where(od => od.MarketingProvodService_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.UnavailableServiceInMarketingProvodKits.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _UnavailableServices;
		}
	}
			
	public string[] UnavailableServicesIDs
	{
		get
		{
			return UnavailableServices.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string UnavailableServicesString
	{
		get
		{
			return String.Join(",", UnavailableServicesIDs.ToArray());
		}
	}
	
}

public partial class ProvodKit
{
	

	private ListSelector<ProvodKitInRegion, MarketingRegion> _Regions = null;

	public ListSelector<ProvodKitInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.ProvodKitInRegions.GetNewBindingList()
				.AsListSelector<ProvodKitInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<ProvodKitInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ProvodKitInRegion 
								{
									ProvodKit_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<ProvodKitInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ProvodKitInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class RoamingTariffParam
{
	

	private ListSelector<RoamingParamsInCountryGroup, RoamingCountryGroup> _CountryGroups = null;

	public ListSelector<RoamingParamsInCountryGroup, RoamingCountryGroup> CountryGroups
	{
		get
		{
			if (_CountryGroups == null)
			{
				_CountryGroups = this.RoamingParamsInCountryGroups.GetNewBindingList()
				.AsListSelector<RoamingParamsInCountryGroup, RoamingCountryGroup>
				(
					od => od.RoamingCountryGroup,
					delegate(IList<RoamingParamsInCountryGroup> ods, RoamingCountryGroup p)
					{
						var items = ods.Where(od => od.RoamingCountryGroup_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new RoamingParamsInCountryGroup 
								{
									RoamingTariffParam_ID = this.Id,
									RoamingCountryGroup = p
								}
							);
						}
					},
					delegate(IList<RoamingParamsInCountryGroup> ods, RoamingCountryGroup p)
					{
						var items = ods.Where(od => od.RoamingCountryGroup_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.RoamingParamsInCountryGroups.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _CountryGroups;
		}
	}
			
	public string[] CountryGroupsIDs
	{
		get
		{
			return CountryGroups.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string CountryGroupsString
	{
		get
		{
			return String.Join(",", CountryGroupsIDs.ToArray());
		}
	}
	
}

public partial class HelpDeviceType
{
	

	private ListSelector<HelpDeviceInRegion, MarketingRegion> _Regions = null;

	public ListSelector<HelpDeviceInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.HelpDeviceInRegions.GetNewBindingList()
				.AsListSelector<HelpDeviceInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<HelpDeviceInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new HelpDeviceInRegion 
								{
									HelpDeviceType_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<HelpDeviceInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.HelpDeviceInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class HelpCenterParam
{
	

	private ListSelector<HelpCenterParamInDeviceType, HelpDeviceType> _DeviceTypes = null;

	public ListSelector<HelpCenterParamInDeviceType, HelpDeviceType> DeviceTypes
	{
		get
		{
			if (_DeviceTypes == null)
			{
				_DeviceTypes = this.HelpCenterParamInDeviceTypes.GetNewBindingList()
				.AsListSelector<HelpCenterParamInDeviceType, HelpDeviceType>
				(
					od => od.HelpDeviceType,
					delegate(IList<HelpCenterParamInDeviceType> ods, HelpDeviceType p)
					{
						var items = ods.Where(od => od.HelpDeviceType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new HelpCenterParamInDeviceType 
								{
									HelpCenterParam_ID = this.Id,
									HelpDeviceType = p
								}
							);
						}
					},
					delegate(IList<HelpCenterParamInDeviceType> ods, HelpDeviceType p)
					{
						var items = ods.Where(od => od.HelpDeviceType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.HelpCenterParamInDeviceTypes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _DeviceTypes;
		}
	}
			
	public string[] DeviceTypesIDs
	{
		get
		{
			return DeviceTypes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string DeviceTypesString
	{
		get
		{
			return String.Join(",", DeviceTypesIDs.ToArray());
		}
	}
	
}

public partial class TVChannelRegion
{
	

	private ListSelector<TVProgramChannelsInRegion, MarketingRegion> _Regions = null;

	public ListSelector<TVProgramChannelsInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.TVProgramChannelsInRegions.GetNewBindingList()
				.AsListSelector<TVProgramChannelsInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<TVProgramChannelsInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new TVProgramChannelsInRegion 
								{
									TVChannelRegion_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<TVProgramChannelsInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.TVProgramChannelsInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class EquipmentTab
{
	

	private ListSelector<EquipmentTabToParamsGroup, EquipmentParamsGroup> _ParamsGroups = null;

	public ListSelector<EquipmentTabToParamsGroup, EquipmentParamsGroup> ParamsGroups
	{
		get
		{
			if (_ParamsGroups == null)
			{
				_ParamsGroups = this.EquipmentTabsToParamsGroups.GetNewBindingList()
				.AsListSelector<EquipmentTabToParamsGroup, EquipmentParamsGroup>
				(
					od => od.EquipmentParamsGroup,
					delegate(IList<EquipmentTabToParamsGroup> ods, EquipmentParamsGroup p)
					{
						var items = ods.Where(od => od.EquipmentParamsGroup_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new EquipmentTabToParamsGroup 
								{
									EquipmentTab_ID = this.Id,
									EquipmentParamsGroup = p
								}
							);
						}
					},
					delegate(IList<EquipmentTabToParamsGroup> ods, EquipmentParamsGroup p)
					{
						var items = ods.Where(od => od.EquipmentParamsGroup_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.EquipmentTabsToParamsGroups.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _ParamsGroups;
		}
	}
			
	public string[] ParamsGroupsIDs
	{
		get
		{
			return ParamsGroups.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ParamsGroupsString
	{
		get
		{
			return String.Join(",", ParamsGroupsIDs.ToArray());
		}
	}
	
}

public partial class Equipment
{
	

	private ListSelector<EquipmentToRegion, MarketingRegion> _Regions = null;

	public ListSelector<EquipmentToRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.EquipmentsToRegions.GetNewBindingList()
				.AsListSelector<EquipmentToRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<EquipmentToRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new EquipmentToRegion 
								{
									Equipment_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<EquipmentToRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.EquipmentsToRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class MarketingEquipment
{
	

	private ListSelector<MarketingEquipmentToEquipmentImage, EquipmentImage> _Images = null;

	public ListSelector<MarketingEquipmentToEquipmentImage, EquipmentImage> Images
	{
		get
		{
			if (_Images == null)
			{
				_Images = this.MarketingEquipmentsToEquipmentImages.GetNewBindingList()
				.AsListSelector<MarketingEquipmentToEquipmentImage, EquipmentImage>
				(
					od => od.EquipmentImage,
					delegate(IList<MarketingEquipmentToEquipmentImage> ods, EquipmentImage p)
					{
						var items = ods.Where(od => od.EquipmentImage_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingEquipmentToEquipmentImage 
								{
									MarketingEquipment_ID = this.Id,
									EquipmentImage = p
								}
							);
						}
					},
					delegate(IList<MarketingEquipmentToEquipmentImage> ods, EquipmentImage p)
					{
						var items = ods.Where(od => od.EquipmentImage_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingEquipmentsToEquipmentImages.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Images;
		}
	}
			
	public string[] ImagesIDs
	{
		get
		{
			return Images.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ImagesString
	{
		get
		{
			return String.Join(",", ImagesIDs.ToArray());
		}
	}
	

	private ListSelector<MarketingEquipmentsToMarketingEquipments, MarketingEquipment> _InstalledApplications = null;

	public ListSelector<MarketingEquipmentsToMarketingEquipments, MarketingEquipment> InstalledApplications
	{
		get
		{
			if (_InstalledApplications == null)
			{
				_InstalledApplications = this.MarketingEquipmentToMarketingEquipment.GetNewBindingList()
				.AsListSelector<MarketingEquipmentsToMarketingEquipments, MarketingEquipment>
				(
					od => od.MarketingEquipment2,
					delegate(IList<MarketingEquipmentsToMarketingEquipments> ods, MarketingEquipment p)
					{
						var items = ods.Where(od => od.MarketingEquipment_ID2 == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MarketingEquipmentsToMarketingEquipments 
								{
									MarketingEquipment_ID = this.Id,
									MarketingEquipment2 = p
								}
							);
						}
					},
					delegate(IList<MarketingEquipmentsToMarketingEquipments> ods, MarketingEquipment p)
					{
						var items = ods.Where(od => od.MarketingEquipment_ID2 == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MarketingEquipmentToMarketingEquipment.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _InstalledApplications;
		}
	}
			
	public string[] InstalledApplicationsIDs
	{
		get
		{
			return InstalledApplications.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string InstalledApplicationsString
	{
		get
		{
			return String.Join(",", InstalledApplicationsIDs.ToArray());
		}
	}
	
}

public partial class InternetTariff
{
	

	private ListSelector<InternetTariffInRegion, MarketingRegion> _Regions = null;

	public ListSelector<InternetTariffInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.InternetTariffInRegions.GetNewBindingList()
				.AsListSelector<InternetTariffInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<InternetTariffInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new InternetTariffInRegion 
								{
									InternetTariff_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<InternetTariffInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.InternetTariffInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class PhoneTariff
{
	

	private ListSelector<PhoneTariffInRegion, MarketingRegion> _Regions = null;

	public ListSelector<PhoneTariffInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.PhoneTariffInRegions.GetNewBindingList()
				.AsListSelector<PhoneTariffInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<PhoneTariffInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new PhoneTariffInRegion 
								{
									PhoneTariff_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<PhoneTariffInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.PhoneTariffInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class TVPackageCategory
{
	

	private ListSelector<TVPackageFamilyInCategory, TVPackageFamily> _Families = null;

	public ListSelector<TVPackageFamilyInCategory, TVPackageFamily> Families
	{
		get
		{
			if (_Families == null)
			{
				_Families = this.TVPackageFamilyInCategories.GetNewBindingList()
				.AsListSelector<TVPackageFamilyInCategory, TVPackageFamily>
				(
					od => od.TVPackageFamily,
					delegate(IList<TVPackageFamilyInCategory> ods, TVPackageFamily p)
					{
						var items = ods.Where(od => od.TVPackageFamily_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new TVPackageFamilyInCategory 
								{
									TVPackageCategory_ID = this.Id,
									TVPackageFamily = p
								}
							);
						}
					},
					delegate(IList<TVPackageFamilyInCategory> ods, TVPackageFamily p)
					{
						var items = ods.Where(od => od.TVPackageFamily_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.TVPackageFamilyInCategories.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Families;
		}
	}
			
	public string[] FamiliesIDs
	{
		get
		{
			return Families.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string FamiliesString
	{
		get
		{
			return String.Join(",", FamiliesIDs.ToArray());
		}
	}
	
}

public partial class ProvodServiceParam
{
	

	private ListSelector<ProvodServiceParamInRegion, MarketingRegion> _Regions = null;

	public ListSelector<ProvodServiceParamInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.ProvodServiceParamInRegions.GetNewBindingList()
				.AsListSelector<ProvodServiceParamInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<ProvodServiceParamInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ProvodServiceParamInRegion 
								{
									ProvodServiceParam_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<ProvodServiceParamInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ProvodServiceParamInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class ProvodService
{
	

	private ListSelector<ProvodServiceInRegion, MarketingRegion> _Regions = null;

	public ListSelector<ProvodServiceInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.ProvodServiceInRegions.GetNewBindingList()
				.AsListSelector<ProvodServiceInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<ProvodServiceInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ProvodServiceInRegion 
								{
									ProvodService_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<ProvodServiceInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ProvodServiceInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class SearchAnnouncement
{
	

	private ListSelector<SearchAnnouncementInRegion, MarketingRegion> _Regions = null;

	public ListSelector<SearchAnnouncementInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.SearchAnnouncementInRegions.GetNewBindingList()
				.AsListSelector<SearchAnnouncementInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<SearchAnnouncementInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new SearchAnnouncementInRegion 
								{
									SearchAnnouncement_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<SearchAnnouncementInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.SearchAnnouncementInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class WarrantyService
{
	

	private ListSelector<AvtorizovannyeservisnyetsentryProduktyArticle, DeviceType> _Products = null;

	public ListSelector<AvtorizovannyeservisnyetsentryProduktyArticle, DeviceType> Products
	{
		get
		{
			if (_Products == null)
			{
				_Products = this.AvtorizovannyeservisnyetsentryProduktyArticles.GetNewBindingList()
				.AsListSelector<AvtorizovannyeservisnyetsentryProduktyArticle, DeviceType>
				(
					od => od.DeviceType,
					delegate(IList<AvtorizovannyeservisnyetsentryProduktyArticle> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new AvtorizovannyeservisnyetsentryProduktyArticle 
								{
									WarrantyService_ID = this.Id,
									DeviceType = p
								}
							);
						}
					},
					delegate(IList<AvtorizovannyeservisnyetsentryProduktyArticle> ods, DeviceType p)
					{
						var items = ods.Where(od => od.DeviceType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.AvtorizovannyeservisnyetsentryProduktyArticles.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Products;
		}
	}
			
	public string[] ProductsIDs
	{
		get
		{
			return Products.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ProductsString
	{
		get
		{
			return String.Join(",", ProductsIDs.ToArray());
		}
	}
	

	private ListSelector<AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle, MarketingRegion> _Regions = null;

	public ListSelector<AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.AvtorizovannyeservisnyetsentryMarketingovyeregionyArticles.GetNewBindingList()
				.AsListSelector<AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle 
								{
									WarrantyService_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.AvtorizovannyeservisnyetsentryMarketingovyeregionyArticles.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class SalesPointParameter
{
	

	private ListSelector<SalesPointTypeToSKADType, SalesPointType> _SalesPointType = null;

	public ListSelector<SalesPointTypeToSKADType, SalesPointType> SalesPointType
	{
		get
		{
			if (_SalesPointType == null)
			{
				_SalesPointType = this.SalesPointTypeToSKADTypes.GetNewBindingList()
				.AsListSelector<SalesPointTypeToSKADType, SalesPointType>
				(
					od => od.SalesPointType,
					delegate(IList<SalesPointTypeToSKADType> ods, SalesPointType p)
					{
						var items = ods.Where(od => od.SalesPointType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new SalesPointTypeToSKADType 
								{
									SalesPointParameter_ID = this.Id,
									SalesPointType = p
								}
							);
						}
					},
					delegate(IList<SalesPointTypeToSKADType> ods, SalesPointType p)
					{
						var items = ods.Where(od => od.SalesPointType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.SalesPointTypeToSKADTypes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _SalesPointType;
		}
	}
			
	public string[] SalesPointTypeIDs
	{
		get
		{
			return SalesPointType.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string SalesPointTypeString
	{
		get
		{
			return String.Join(",", SalesPointTypeIDs.ToArray());
		}
	}
	

	private ListSelector<SalesPointSpecialityToSKADSpeciality, SalesPointSpeciality> _SalesPointSpeciality = null;

	public ListSelector<SalesPointSpecialityToSKADSpeciality, SalesPointSpeciality> SalesPointSpeciality
	{
		get
		{
			if (_SalesPointSpeciality == null)
			{
				_SalesPointSpeciality = this.SalesPointSpecialityToSKADSpecialities.GetNewBindingList()
				.AsListSelector<SalesPointSpecialityToSKADSpeciality, SalesPointSpeciality>
				(
					od => od.SalesPointSpeciality,
					delegate(IList<SalesPointSpecialityToSKADSpeciality> ods, SalesPointSpeciality p)
					{
						var items = ods.Where(od => od.SalesPointSpeciality_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new SalesPointSpecialityToSKADSpeciality 
								{
									SalesPointParameter_ID = this.Id,
									SalesPointSpeciality = p
								}
							);
						}
					},
					delegate(IList<SalesPointSpecialityToSKADSpeciality> ods, SalesPointSpeciality p)
					{
						var items = ods.Where(od => od.SalesPointSpeciality_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.SalesPointSpecialityToSKADSpecialities.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _SalesPointSpeciality;
		}
	}
			
	public string[] SalesPointSpecialityIDs
	{
		get
		{
			return SalesPointSpeciality.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string SalesPointSpecialityString
	{
		get
		{
			return String.Join(",", SalesPointSpecialityIDs.ToArray());
		}
	}
	

	private ListSelector<SalesPointServiceToSKADService, SalesPointService> _SalesPointServices = null;

	public ListSelector<SalesPointServiceToSKADService, SalesPointService> SalesPointServices
	{
		get
		{
			if (_SalesPointServices == null)
			{
				_SalesPointServices = this.SalesPointServiceToSKADServices.GetNewBindingList()
				.AsListSelector<SalesPointServiceToSKADService, SalesPointService>
				(
					od => od.SalesPointService,
					delegate(IList<SalesPointServiceToSKADService> ods, SalesPointService p)
					{
						var items = ods.Where(od => od.SalesPointService_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new SalesPointServiceToSKADService 
								{
									SalesPointParameter_ID = this.Id,
									SalesPointService = p
								}
							);
						}
					},
					delegate(IList<SalesPointServiceToSKADService> ods, SalesPointService p)
					{
						var items = ods.Where(od => od.SalesPointService_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.SalesPointServiceToSKADServices.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _SalesPointServices;
		}
	}
			
	public string[] SalesPointServicesIDs
	{
		get
		{
			return SalesPointServices.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string SalesPointServicesString
	{
		get
		{
			return String.Join(",", SalesPointServicesIDs.ToArray());
		}
	}
	
}

public partial class ArchiveTvTariff
{
	

	private ListSelector<ChannelOnTariff, TVChannel> _Channels = null;

	public ListSelector<ChannelOnTariff, TVChannel> Channels
	{
		get
		{
			if (_Channels == null)
			{
				_Channels = this.ChannelsOnTariffs.GetNewBindingList()
				.AsListSelector<ChannelOnTariff, TVChannel>
				(
					od => od.TVChannel,
					delegate(IList<ChannelOnTariff> ods, TVChannel p)
					{
						var items = ods.Where(od => od.TVChannel_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ChannelOnTariff 
								{
									ArchiveTvTariff_ID = this.Id,
									TVChannel = p
								}
							);
						}
					},
					delegate(IList<ChannelOnTariff> ods, TVChannel p)
					{
						var items = ods.Where(od => od.TVChannel_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ChannelsOnTariffs.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Channels;
		}
	}
			
	public string[] ChannelsIDs
	{
		get
		{
			return Channels.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ChannelsString
	{
		get
		{
			return String.Join(",", ChannelsIDs.ToArray());
		}
	}
	

	private ListSelector<ArchiveTvTariffInRegion, MarketingRegion> _Regions = null;

	public ListSelector<ArchiveTvTariffInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.ArchiveTvTariffInRegions.GetNewBindingList()
				.AsListSelector<ArchiveTvTariffInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<ArchiveTvTariffInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ArchiveTvTariffInRegion 
								{
									ArchiveTvTariff_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<ArchiveTvTariffInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ArchiveTvTariffInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class Contact
{
	

	private ListSelector<ContactToMarketingRegion, MarketingRegion> _MarketingRegions = null;

	public ListSelector<ContactToMarketingRegion, MarketingRegion> MarketingRegions
	{
		get
		{
			if (_MarketingRegions == null)
			{
				_MarketingRegions = this.ContactsToMarketingRegions.GetNewBindingList()
				.AsListSelector<ContactToMarketingRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<ContactToMarketingRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ContactToMarketingRegion 
								{
									Contact_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<ContactToMarketingRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ContactsToMarketingRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MarketingRegions;
		}
	}
			
	public string[] MarketingRegionsIDs
	{
		get
		{
			return MarketingRegions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MarketingRegionsString
	{
		get
		{
			return String.Join(",", MarketingRegionsIDs.ToArray());
		}
	}
	

	private ListSelector<ContactToContactsGroup, ContactsGroup> _ContactsGroups = null;

	public ListSelector<ContactToContactsGroup, ContactsGroup> ContactsGroups
	{
		get
		{
			if (_ContactsGroups == null)
			{
				_ContactsGroups = this.ContactsToContactsGroups.GetNewBindingList()
				.AsListSelector<ContactToContactsGroup, ContactsGroup>
				(
					od => od.ContactsGroup,
					delegate(IList<ContactToContactsGroup> ods, ContactsGroup p)
					{
						var items = ods.Where(od => od.ContactsGroup_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ContactToContactsGroup 
								{
									Contact_ID = this.Id,
									ContactsGroup = p
								}
							);
						}
					},
					delegate(IList<ContactToContactsGroup> ods, ContactsGroup p)
					{
						var items = ods.Where(od => od.ContactsGroup_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ContactsToContactsGroups.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _ContactsGroups;
		}
	}
			
	public string[] ContactsGroupsIDs
	{
		get
		{
			return ContactsGroups.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string ContactsGroupsString
	{
		get
		{
			return String.Join(",", ContactsGroupsIDs.ToArray());
		}
	}
	
}

public partial class AnnualContractSetting
{
	

	private ListSelector<AnnualContractSettingInRegion, MarketingRegion> _Regions = null;

	public ListSelector<AnnualContractSettingInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.AnnualContractSettingInRegions.GetNewBindingList()
				.AsListSelector<AnnualContractSettingInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<AnnualContractSettingInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new AnnualContractSettingInRegion 
								{
									AnnualContractSetting_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<AnnualContractSettingInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.AnnualContractSettingInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class MnpCrmSetting
{
	

	private ListSelector<MnpCrmSettingInRegion, MarketingRegion> _Regions = null;

	public ListSelector<MnpCrmSettingInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.MnpCrmSettingInRegions.GetNewBindingList()
				.AsListSelector<MnpCrmSettingInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<MnpCrmSettingInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new MnpCrmSettingInRegion 
								{
									MnpCrmSetting_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<MnpCrmSettingInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.MnpCrmSettingInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class RegionalEmail
{
	

	private ListSelector<RegionToEmail, MarketingRegion> _Regions = null;

	public ListSelector<RegionToEmail, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.RegionsToEmails.GetNewBindingList()
				.AsListSelector<RegionToEmail, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<RegionToEmail> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new RegionToEmail 
								{
									RegionalEmail_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<RegionToEmail> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.RegionsToEmails.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class RoamingOptionCalculation
{
	

	private ListSelector<RoamingTariffsInOption, RoamingTariffZone> _RoamingTariffs = null;

	public ListSelector<RoamingTariffsInOption, RoamingTariffZone> RoamingTariffs
	{
		get
		{
			if (_RoamingTariffs == null)
			{
				_RoamingTariffs = this.RoamingTariffsInOptions.GetNewBindingList()
				.AsListSelector<RoamingTariffsInOption, RoamingTariffZone>
				(
					od => od.RoamingTariffZone,
					delegate(IList<RoamingTariffsInOption> ods, RoamingTariffZone p)
					{
						var items = ods.Where(od => od.RoamingTariffZone_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new RoamingTariffsInOption 
								{
									RoamingOptionCalculation_ID = this.Id,
									RoamingTariffZone = p
								}
							);
						}
					},
					delegate(IList<RoamingTariffsInOption> ods, RoamingTariffZone p)
					{
						var items = ods.Where(od => od.RoamingTariffZone_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.RoamingTariffsInOptions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _RoamingTariffs;
		}
	}
			
	public string[] RoamingTariffsIDs
	{
		get
		{
			return RoamingTariffs.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RoamingTariffsString
	{
		get
		{
			return String.Join(",", RoamingTariffsIDs.ToArray());
		}
	}
	

	private ListSelector<RoamingOptionInRegion, MarketingRegion> _Regions = null;

	public ListSelector<RoamingOptionInRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.RoamingOptionInRegions.GetNewBindingList()
				.AsListSelector<RoamingOptionInRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<RoamingOptionInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new RoamingOptionInRegion 
								{
									RoamingOptionCalculation_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<RoamingOptionInRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.RoamingOptionInRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	
}

public partial class MobileServiceByRegionUsageType
{
	

	private ListSelector<ByRegionMobileServiceRegion, MarketingRegion> _Regions = null;

	public ListSelector<ByRegionMobileServiceRegion, MarketingRegion> Regions
	{
		get
		{
			if (_Regions == null)
			{
				_Regions = this.ByRegionMobileServiceRegions.GetNewBindingList()
				.AsListSelector<ByRegionMobileServiceRegion, MarketingRegion>
				(
					od => od.MarketingRegion,
					delegate(IList<ByRegionMobileServiceRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ByRegionMobileServiceRegion 
								{
									MobileServiceByRegionUsageType_ID = this.Id,
									MarketingRegion = p
								}
							);
						}
					},
					delegate(IList<ByRegionMobileServiceRegion> ods, MarketingRegion p)
					{
						var items = ods.Where(od => od.MarketingRegion_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ByRegionMobileServiceRegions.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _Regions;
		}
	}
			
	public string[] RegionsIDs
	{
		get
		{
			return Regions.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string RegionsString
	{
		get
		{
			return String.Join(",", RegionsIDs.ToArray());
		}
	}
	

	private ListSelector<ByRegionMobileServiceUsageType, MobileServiceUsageType> _MobileServiceUsageTypes = null;

	public ListSelector<ByRegionMobileServiceUsageType, MobileServiceUsageType> MobileServiceUsageTypes
	{
		get
		{
			if (_MobileServiceUsageTypes == null)
			{
				_MobileServiceUsageTypes = this.ByRegionMobileServiceUsageTypes.GetNewBindingList()
				.AsListSelector<ByRegionMobileServiceUsageType, MobileServiceUsageType>
				(
					od => od.MobileServiceUsageType,
					delegate(IList<ByRegionMobileServiceUsageType> ods, MobileServiceUsageType p)
					{
						var items = ods.Where(od => od.MobileServiceUsageType_ID == p.Id);
						if (items.Count() == 0)
						{
							this.Modified = System.DateTime.Now;
							ods.Add(
								new ByRegionMobileServiceUsageType 
								{
									MobileServiceByRegionUsageType_ID = this.Id,
									MobileServiceUsageType = p
								}
							);
						}
					},
					delegate(IList<ByRegionMobileServiceUsageType> ods, MobileServiceUsageType p)
					{
						var items = ods.Where(od => od.MobileServiceUsageType_ID == p.Id);
						if (items.Count() > 0)
						{
							this.Modified = System.DateTime.Now;
							var item = items.Single();
							item.InternalDataContext = InternalDataContext;
							InternalDataContext.ByRegionMobileServiceUsageTypes.DeleteOnSubmit(item);
							item.SaveRemovingInstruction();
							ods.Remove(item);
						}
					}
				);
			}
			return _MobileServiceUsageTypes;
		}
	}
			
	public string[] MobileServiceUsageTypesIDs
	{
		get
		{
			return MobileServiceUsageTypes.Select(n => n.Id.ToString()).ToArray();
		}
	}
	
	public string MobileServiceUsageTypesString
	{
		get
		{
			return String.Join(",", MobileServiceUsageTypesIDs.ToArray());
		}
	}
	
}

	
public partial class SiteProductToMarketingRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "SiteProductToMarketingRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SiteProductToMarketingRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class AbstractItemAbtractItemRegionArticle
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "AbstractItemAbtractItemRegionArticle");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "AbstractItemAbtractItemRegionArticle", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class TagInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "TagInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TagInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class SearchSuggestInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "SearchSuggestInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SearchSuggestInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class NewsInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "NewsInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "NewsInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingMobileServiceInRoamingTariff
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingMobileServiceInRoamingTariff");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingMobileServiceInRoamingTariff", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class FeedbackQueueInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "FeedbackQueueInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "FeedbackQueueInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class TVChannelsInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "TVChannelsInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVChannelsInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class TVChannelsInTheme
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "TVChannelsInTheme");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVChannelsInTheme", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class FeedbackGroupInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "FeedbackGroupInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "FeedbackGroupInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingMobileServiceDeviceType
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingMobileServiceDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingMobileServiceDeviceType", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ActionDeviceType
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ActionDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ActionDeviceType", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ActionInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ActionInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ActionInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ActionProduct
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ActionProduct");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ActionProduct", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class TVPackageInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "TVPackageInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVPackageInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class IncludedDiscountTvPackagesInMarketingPackage
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "IncludedDiscountTvPackagesInMarketingPackage");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "IncludedDiscountTvPackagesInMarketingPackage", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ActivatedDiscountTvPackagesInMarketingPackage
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ActivatedDiscountTvPackagesInMarketingPackage");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ActivatedDiscountTvPackagesInMarketingPackage", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ExternalRegionMappingToMarketingRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ExternalRegionMappingToMarketingRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ExternalRegionMappingToMarketingRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingTariffDeviceType
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingTariffDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingTariffDeviceType", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MobileTariffInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MobileTariffInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MobileTariffInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingMobileTariffInParamGroup
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingMobileTariffInParamGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingMobileTariffInParamGroup", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class GuideQuestionDeviceType
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "GuideQuestionDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "GuideQuestionDeviceType", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ServiceInTariffParameterGroup
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ServiceInTariffParameterGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ServiceInTariffParameterGroup", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingMobileServiceInParamGroup
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingMobileServiceInParamGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingMobileServiceInParamGroup", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MobileServiceInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MobileServiceInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MobileServiceInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingMobileServiceMobileDeviceType
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingMobileServiceMobileDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingMobileServiceMobileDeviceType", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MobileTariffFamilyInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MobileTariffFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MobileTariffFamilyInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MutualMobileService
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MutualMobileService");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MutualMobileService", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MobileServiceFamilyInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MobileServiceFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MobileServiceFamilyInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MobileParamGroupInTab
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MobileParamGroupInTab");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MobileParamGroupInTab", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class PrivelegeAndBonusDeviceType
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "PrivelegeAndBonusDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "PrivelegeAndBonusDeviceType", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class PrivelegeAndBonusInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "PrivelegeAndBonusInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "PrivelegeAndBonusInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class PrivelegeAndBonusProduct
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "PrivelegeAndBonusProduct");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "PrivelegeAndBonusProduct", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class FeedbackThemeToFaqContent
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "FeedbackThemeToFaqContent");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "FeedbackThemeToFaqContent", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class FeedbackSubThemeToFaqContent
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "FeedbackSubThemeToFaqContent");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "FeedbackSubThemeToFaqContent", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class FaqContentToRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "FaqContentToRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "FaqContentToRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingTVPackageInTheme
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingTVPackageInTheme");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingTVPackageInTheme", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class UnavailableServiceInMarketingTVPackage
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "UnavailableServiceInMarketingTVPackage");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "UnavailableServiceInMarketingTVPackage", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingTVPackageInMutualGroup
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingTVPackageInMutualGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingTVPackageInMutualGroup", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class TVChannelsInMarketingPackage
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "TVChannelsInMarketingPackage");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVChannelsInMarketingPackage", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class QuickLinksGroupInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "QuickLinksGroupInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "QuickLinksGroupInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ProvodTariffParameterGroupInProduct
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ProvodTariffParameterGroupInProduct");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodTariffParameterGroupInProduct", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class InternetTariffFamilyInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "InternetTariffFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "InternetTariffFamilyInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class PhoneTariffFamilyInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "PhoneTariffFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "PhoneTariffFamilyInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ProvodServiceFamilyInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ProvodServiceFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodServiceFamilyInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingInternetTariffInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingInternetTariffInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingInternetTariffInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class UnavailableServicesInInternetTariff
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "UnavailableServicesInInternetTariff");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "UnavailableServicesInInternetTariff", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingPhoneTariffInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingPhoneTariffInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingPhoneTariffInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class UnavailableServiceInMarketingPhoneTariff
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "UnavailableServiceInMarketingPhoneTariff");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "UnavailableServiceInMarketingPhoneTariff", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingProvodServiceInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingProvodServiceInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingProvodServiceInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingProvodServiceInProduct
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingProvodServiceInProduct");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingProvodServiceInProduct", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ProvodKitFamilyInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ProvodKitFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodKitFamilyInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingProvodKitInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingProvodKitInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingProvodKitInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class UnavailableServiceInMarketingProvodKit
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "UnavailableServiceInMarketingProvodKit");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "UnavailableServiceInMarketingProvodKit", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingProvodServiceInKit
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingProvodServiceInKit");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingProvodServiceInKit", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class TVPackagesInProvodKit
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "TVPackagesInProvodKit");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVPackagesInProvodKit", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ProvodKitInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ProvodKitInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodKitInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class RoamingParamsInCountryGroup
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "RoamingParamsInCountryGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "RoamingParamsInCountryGroup", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class TVPackageFamilyInCategory
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "TVPackageFamilyInCategory");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVPackageFamilyInCategory", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class HelpDeviceInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "HelpDeviceInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "HelpDeviceInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class HelpCenterParamInDeviceType
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "HelpCenterParamInDeviceType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "HelpCenterParamInDeviceType", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class TVProgramChannelsInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "TVProgramChannelsInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "TVProgramChannelsInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class EquipmentTabToParamsGroup
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "EquipmentTabToParamsGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "EquipmentTabToParamsGroup", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class EquipmentToRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "EquipmentToRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "EquipmentToRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingEquipmentsToMarketingEquipments
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingEquipmentsToMarketingEquipments");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingEquipmentsToMarketingEquipments", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MarketingEquipmentToEquipmentImage
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MarketingEquipmentToEquipmentImage");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MarketingEquipmentToEquipmentImage", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class InternetTariffInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "InternetTariffInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "InternetTariffInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class PhoneTariffInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "PhoneTariffInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "PhoneTariffInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ProvodServiceParamInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ProvodServiceParamInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodServiceParamInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ProvodServiceInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ProvodServiceInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProvodServiceInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class SearchAnnouncementInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "SearchAnnouncementInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SearchAnnouncementInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class SalesPointTypeToSKADType
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "SalesPointTypeToSKADType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SalesPointTypeToSKADType", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class SalesPointSpecialityToSKADSpeciality
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "SalesPointSpecialityToSKADSpeciality");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SalesPointSpecialityToSKADSpeciality", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class SalesPointServiceToSKADService
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "SalesPointServiceToSKADService");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "SalesPointServiceToSKADService", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ArchiveTvTariffInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ArchiveTvTariffInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ArchiveTvTariffInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ChannelOnTariff
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ChannelOnTariff");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ChannelOnTariff", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ContactToMarketingRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ContactToMarketingRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ContactToMarketingRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ContactToContactsGroup
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ContactToContactsGroup");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ContactToContactsGroup", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class AnnualContractSettingInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "AnnualContractSettingInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "AnnualContractSettingInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class MnpCrmSettingInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "MnpCrmSettingInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "MnpCrmSettingInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class RegionToEmail
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "RegionToEmail");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "RegionToEmail", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class RoamingOptionInRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "RoamingOptionInRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "RoamingOptionInRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class RoamingTariffsInOption
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "RoamingTariffsInOption");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "RoamingTariffsInOption", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ByRegionMobileServiceRegion
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ByRegionMobileServiceRegion");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ByRegionMobileServiceRegion", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ByRegionMobileServiceUsageType
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ByRegionMobileServiceUsageType");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ByRegionMobileServiceUsageType", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ProduktovoenapravlenieProduktyArticle
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ProduktovoenapravlenieProduktyArticle");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ProduktovoenapravlenieProduktyArticle", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class ItemDefinitionItemDefinitionArticle
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "ItemDefinitionItemDefinitionArticle");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "ItemDefinitionItemDefinitionArticle", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class AvtorizovannyeservisnyetsentryProduktyArticle
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "AvtorizovannyeservisnyetsentryProduktyArticle");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "AvtorizovannyeservisnyetsentryProduktyArticle", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

	
public partial class AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle
{
	private QPDataContext _InternalDataContext = LinqHelper.Context;
	
	public QPDataContext InternalDataContext
	{
		get { return _InternalDataContext; }
		set { _InternalDataContext = value; }
	}
        
	public decimal ITEM_ID
	{
		get
		{
			return _ITEM_ID;
		}
	}
	
	public decimal LINKED_ITEM_ID
	{
		get
		{
			return _LINKED_ITEM_ID;
		}
	}
	
	private string _removingInstruction;
	public string RemovingInstruction
	{
		get
		{
			if (String.IsNullOrEmpty(_removingInstruction))
				SaveRemovingInstruction();
			return _removingInstruction;
		}
	}
	
	public void SaveRemovingInstruction()
	{
		int linkId = InternalDataContext.Cnn.GetLinkIdByNetName(InternalDataContext.SiteId, "AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle");
		
		if (linkId == 0)
			throw new Exception(String.Format("Junction class '{0}' is not found on the site (ID = {1})", "AvtorizovannyeservisnyetsentryMarketingovyeregionyArticle", InternalDataContext.SiteId));
			
		_removingInstruction = String.Format("EXEC sp_executesql N'delete from item_to_item where link_id = @linkId and ((l_item_id = @itemId  and r_item_id = @linkedItemId) or (l_item_id = @linkedItemId  and r_item_id = @itemId))', N'@linkId NUMERIC, @itemId NUMERIC, @linkedItemId NUMERIC', @linkId = {0}, @itemId = {1}, @linkedItemId = {2}", linkId, this.ITEM_ID, this.LINKED_ITEM_ID);
	}
}

}

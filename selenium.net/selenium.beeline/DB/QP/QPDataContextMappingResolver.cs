﻿using System.Configuration;
using QA.Core.Data.Resolvers;

namespace QA.Beeline.DAL.QPDataContextClasses.Mapping
{
    /// <summary>
    /// Поставщик маппонга для контекста QPDataContext
    /// </summary>
    public class QPDataContextMappingResolver : ManifestXmlMappingResolver<QPDataContext>
    {
        protected const string StageManifestKey = "QA.Beeline.DAL.QPDataContextClasses.Mapping.QPDataContext_Stage.map";
        protected const string LiveManifestKey = "QA.Beeline.DAL.QPDataContextClasses.Mapping.QPDataContext_Live.map";

        /// <summary>
        /// Возращает путь к манифесту
        /// </summary>
        /// <param name="isStage">Режим</param>
        /// <returns></returns>
        protected override string GetManifestResourcePath(bool isStage)
        {
             return isStage ? StageManifestKey : LiveManifestKey;
        }

        /// <summary>
        /// Возвращает режим
        /// </summary>
        protected override bool IsStageMode
        {
            get {
                return true;// DataConfiguration.IsStageMode;
            }
        }
    }
}

using NUnit.Framework;

namespace selenium.beeline
{
    [SetUpFixture]
    public class NamespaceSetup {
        
        [SetUp]
        public void AssemblyInit() {
            BeelineSeleniumContext.Inst.Init();
        }

        [TearDown]
        public static void SuiteTearDown() {
            BeelineSeleniumContext.Inst.Destroy();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Products.Home;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products {
    internal class BasketPage : BeelinePageBase {

        [SimpleWebComponent(Css = ".active>div>span")] 
        public WebText TabName;

        [WebComponent] 
        public BasketList BasketList;

        [WebComponent("//*[@class='order-button']/span")] 
        public BeelineButton ContactDetailsButton;

        [WebComponent("//*[@class='order-button']/span")]
        public BeelineButton ConfirmButton;

        [WebComponent("//*[@class='order-button']/span")]
        public BeelineButton MakeOrderButton;

        [WebComponent("//*[@class='contact-information']/div[1]/div")]
        public BeelineInput UserNameInput;

        [WebComponent("//*[@class='contact-information']/div[2]/div")]
        public BeelineInput EmailAddressInput;

        [WebComponent("//*[@class='contact-information']/div[3]/div")]
        public BeelineInput PhoneNumberInput;

        [WebComponent("//*[@class='address-form']/div/div[4]/div")] 
        public BeelineInput ApartmentInput;

        [SimpleWebComponent(Css = ".connect-form-tip.valid.ng-scope")] 
        public WebText WrongApartmentWebText;

        [SimpleWebComponent(Css = ".content-block p:nth-of-type(1)")] 
        public WebText UserDetailsWebText;

        [SimpleWebComponent(Css = ".content-block p:nth-of-type(2)")] 
        public WebText AddressWebText;

        [WebComponent(".form-select .select")] 
        public BeelineDropList TvCountDropList;

        [SimpleWebComponent(Css = ".radio")] 
        public WebRadioButton TvRentRadioButton;

        [WebComponent] 
        public HomeRequestResultForm HomeRequestResultForm;

        public override string AbsolutePath {
            get { return "customers/products/home/basket/"; }
        }

        public void AssertTabIsVisible(string tabName) {
            Assert.AreEqual(tabName, TabName.Text);
        }

        // -- !
        public void AssertTariffIsCorrect() {
            Assert.AreEqual(FD.tariff, BasketList.GetTariffName());
        }

        public void AssertServiceIsCorrect() {
            List<BasketItem> items = BasketList.GetItems();
            string serviceName = " ";
            if (TabName.Text == "Список покупок") {
                foreach (BasketItem item in items) {
                    if (item.IsChecked) {
                        serviceName = item.ID;
                        break;
                    }
                }
            }
            else {
                items.Remove(items.First());
                serviceName = items.First().ID;
            }
            Assert.AreEqual(FD.service, serviceName);
        }

        public void ClickByRadioButton() {
            if (!TvRentRadioButton.IsVisible()) {
                TvCountDropList.SelectItem("2");
            }
            TvRentRadioButton.Select();
        }

        public void AssertUserDetailsIs(string userName, string userEmail, string userPhone) {
            var regex = new Regex("(?<name>.+)(\r\n?|\n)(?<phone>\\d+),(?<email>.+)");
            Match match = regex.Match(UserDetailsWebText.Text);
            Assert.AreEqual(userName, match.Groups["name"].Value.Trim());
            Assert.AreEqual(userEmail, match.Groups["email"].Value.Trim());
            Assert.AreEqual(userPhone, match.Groups["phone"].Value);
        }

        public void AssertAddressIs(string streetName, string houseNumber, string apartmentNubmer) {
            var regex = new Regex("(?<city>.+),(?<street>.+),(?<house>.+),(?<apartment>.+)");
            Match match = regex.Match(AddressWebText.Text);
            char[] charsToRemove = { 'д', 'к', 'в', '.', ' ' };
            Assert.AreEqual(streetName, match.Groups["street"].Value.Trim());
            Assert.AreEqual(houseNumber, match.Groups["house"].Value.Trim(charsToRemove));
            Assert.AreEqual(apartmentNubmer, match.Groups["apartment"].Value.Trim(charsToRemove));
        }
    }
}

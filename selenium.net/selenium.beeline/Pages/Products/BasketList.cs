﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;
using core;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products {
    public class BasketList : ListBase<BasketItem> {
        public BasketList(IPage parent)
            : base(parent, ".//*[contains(@class,'orders-list-wrap')][2]") {
        }

        public override string ItemIdScss {
            get { return ".orders-list span.ng-binding"; }
        }

        public override List<string> GetIds() {
            List<string> texts = base.GetIds();
            var ids = new List<string>();
            foreach (var text in texts) {
                var regex = new Regex("[\\w+] «(?<id>.*)»");
                Match match = regex.Match(text);
                ids.Add(match.Success ? match.Groups["id"].Value : text);
            }
            return ids;
        }

        // -- !
        public string GetTariffName() {
            return GetItems().Single(item => IsTariffLine(item)).ID;
        }
        // -- !
        private bool IsTariffLine(BasketItem item) {
            return RegexHelper.IsMatch(item.GetTitle(), "(Тариф|Тариф ТВ|Пакет) «(?:.*)»");
        }
    }

    public class BasketItem : ItemBase {
        public BasketItem(IContainer container, string id)
            : base(container, id) {
        }

        public override string ItemScss {
            get {
                string relativeXpath =
                    string.Format(
                        "tr[descendant::span[contains(@class,'ng-binding')][descendant-or-self::*[text()='{0}']]]", ID);
                return ContainerInnerScss(relativeXpath);
            }
        }

        public bool IsChecked {
            get { return Is.HasClass(InnerScss(".checkbox-slide"), "checked"); }
        }

        // -- !
        public string GetTitle() {
            return Get.Text(InnerScss(".black.ng-binding")); // "span.ng-binding"
        }
    }
}

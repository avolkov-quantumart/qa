﻿using selenium.beeline.Pages.Base;

namespace selenium.beeline.Pages.Products.Home {
    public class HomePage : BeelinePageBase {
        public override string AbsolutePath {
            get { return "customers/products/home/"; }
        }
    }
}

using OpenQA.Selenium;
using selenium.beeline.Pages.Base;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home {
    public class ProvodAboutPage : BeelinePageBase {
        [WebComponent]
        public TariffsList Tariffs;

        public override string AbsolutePath {
            get { return "customers/products/home/provod/about/"; }
        }
    }

    public class TariffsList:ListBase<ProvodTariff> {
        public TariffsList(IPage parent)
            : base(parent) {
        }

        public override string ItemIdScss {
            get { return InnerScss("div.content>div.content-wrap h2.header-special"); }
        }
    }

    public class ProvodTariff : ItemBase {
        public readonly WebButton ConnectButton;

        public ProvodTariff(IContainer container, string id)
            : base(container, id) {
            ConnectButton = ParentPage.RegisterComponent<WebButton>("����������",
                                                                    InnerSelector("span.button"));
        }

        public override bool IsVisible() {
            return Is.Visible(ItemScss);
        }

        public override string ItemScss {
            get {
                return ContainerInnerScss("div[contains(@class,'content')]/div[contains(@class,'content-wrap')]" +
                                           "[descendant::h2[contains(@class,'header-special')]/descendant-or-self::*[text()='{0}']]",
                                           ID);
            }
        }

        /// <summary>
        /// �������� �������� ������
        /// </summary>
        public string GetTariffName() {
            return Get.Text(InnerSelector("div.tarif-price h3>a"));
        }
    }
}
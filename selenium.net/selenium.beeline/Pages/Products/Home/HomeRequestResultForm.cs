﻿using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home {
    public class HomeRequestResultForm : RequestResultForm {

        [SimpleWebComponent(Css = ".acces-request .special-info-block>h4>strong")]
        public WebText ApplicationNumber;

        public HomeRequestResultForm(IPage parent) 
            : base(parent) {
        }

        public override bool IsVisible() {
            return Title.IsVisible() &&
                   GirlWithBubbles.IsVisible() &&
                   ApplicationNumber.IsVisible();
        }
    }
}

using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.Tariffs {
    /// <summary>
    /// ������ ������ � ������� ����������� � ������
    /// </summary>
    public class TariffBillWidget : ContainerBase {
        public readonly WebLink CompareTariffsLink;

        [WebComponent("root:.HomeBillSubmit", ComponentName = "�������� � �������")]
        public BeelineButton ToBuyButton;

        public TariffBillWidget(IPage parent)
            : base(parent, ".bill-content") {
            CompareTariffsLink = ParentPage.RegisterComponent<WebLink>("�������� ������",
                                                                       InnerSelector("span['�������� ������']"));
        }
    }
}
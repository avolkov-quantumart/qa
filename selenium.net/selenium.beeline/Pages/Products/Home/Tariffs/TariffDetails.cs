using System;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using core.Extensions;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.Tariffs {
    public class TariffDetails : ContainerBase {
        [WebComponent("root:.HomeBillSubmit",ComponentName = "� �������")]
        public BeelineButton AddToBusketButton;
        
        public TariffDetails(IPage parent)
            : base(parent, ".tariff-description.content-block") {
        }

        public string Name {
            get {
                string text = Get.Text(By.XPath("//*[contains(@class,'tariff-description')]/descendant::h1[contains(text(),' ')]"));
                return new Regex("�(?<name>[^�]*)�").Match(text).Groups["name"].Value;
            }
        }

        /// <summary>
        /// ������ �� ������ ����� � ��������� �������
        /// </summary>
        public bool HasBrothers() {
            return Is.Visible(By.ClassName("sub-nav"));
        }

        /// <summary>
        /// ������� �� ���� �� ����������� �������
        /// </summary>
        public void GoToRandomBrother() {
            IWebElement random = Find.Elements(By.CssSelector(".sub-nav li:not(.active) a")).RandomItem();
            Log.Action("������� �� ����� {0}", random.Text);
            Action.Click(random);
        }

        public override bool IsVisible() {
            return Is.Visible(RootScss);
        }
    }
}
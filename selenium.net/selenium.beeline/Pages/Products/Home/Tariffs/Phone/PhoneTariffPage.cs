using selenium.beeline.Pages.Base;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Phone {
    public class PhoneTariffPage : TariffPageBase {
        public override string AbsolutePath {
            get { return "customers/products/home/home-tariffs/phone/{tariff_url}/"; }
        }
    }
}
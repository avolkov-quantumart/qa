using selenium.beeline.Pages.Base.Tariffs;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Internet {
    public class InternetTariff : TariffBase {
        public InternetTariff(IContainer container, string id)
            : base(container, id) {
        }
    }
}
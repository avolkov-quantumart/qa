using selenium.beeline.Pages.Base.Tariffs;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Internet {
    public class InternetTariffsGroup : TariffsGroup<GroupInternetTariff> {
        public InternetTariffsGroup(IContainer container, string id)
            : base(container, id) {
        }

//        public override string ItemIdScss {
//            get { return ItemScss + "/descendant::ul/li/h5/strong/a"; }
//        }
//
//        public override string ItemScss {
//            get {
//                return string.Format("//div[contains(@class,'tariffs-group')]" +
//                                     "[descendant::div[contains(@class,'group-header')]/h3[text()='{0}']]", ID);
//            }
//        }
    }
}
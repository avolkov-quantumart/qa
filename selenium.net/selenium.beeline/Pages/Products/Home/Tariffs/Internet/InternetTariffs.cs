using selenium.beeline.Pages.Base.Tariffs;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Internet {
    public class InternetTariffs : CardsBase<InternetTariff, InternetTariffsGroup> {
        public InternetTariffs(IPage parent)
            : base(parent) {
        }
    }
}
﻿using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Base.Tariffs;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Common.Alerts;
using selenium.beeline.Pages.Common.ServiceWidgets;
using selenium.core.Exceptions;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Internet {
    public class InternetTariffPage : TariffPageBase {

        [WebComponent]
        public CheckConnectionAlert CheckConnectionAlert;

        [WebComponent]
        private ConnectHouseApplicationAlert ConnectHouseApplicationAlert;

        [WebComponent]
        public CompareTariffsAlert CompareTariffsAlert;

        [WebComponent]
        public TariffDetails TariffDetails;

        [WebComponent]
        public TariffAccessibilityWidget AccessibilityWidget;

        [WebComponent]
        public TariffBillWidget BillWidget;

        [WebComponent]
        public BasketWidget BasketWidget;

        [WebComponent] 
        public Basket Basket;

        public override string AbsolutePath {
            get { return "customers/products/home/home-tariffs/internet/{tariff_url}/"; }
        }

        /// <summary>
        /// Положить тариф в корзину
        /// </summary>
        /// <remarks>
        /// Выполнить клик по кнопке "К покупке"
        /// Если отобразился алерт проверки адреса - ввести улицу и дом и кликнуть по кнопке "Продолжить"
        /// </remarks>
        /// <param name="street">название улицы</param>
        /// <param name="house">номер дома</param>
        public void AddTariffToBusket(string street, string house) {
            OpenCheckConnectionAlert();
            var alert = State.HtmlAlertAs<CheckConnectionAlert>();
            alert.SetValidAddress(street, house);
        }

        public void OpenCheckConnectionAlert() {
            CheckConnectionAlert.Open(() => BillWidget.ToBuyButton.ClickAndWaitWhileAjax());
        }

        /// <summary>
        /// Перейти на другой тарифф того же типа
        /// </summary>
        /// <example>
        /// Если находимся на странице тарифа домашнего интернета - перейти на любой другой тариф домашнего интернета
        /// </example>
        public InternetTariffPage GoToOtherTariffOfSameType() {
            if (TariffDetails.HasBrothers()) {
                TariffDetails.GoToRandomBrother();
                return State.PageAs<InternetTariffPage>();
            }
            string tariffName = TariffDetails.Name;
            BreadCrumbs.StepBack();
            var tariffsPage = State.PageAs<TariffsPage>();
            ITariff anotherTariff = tariffsPage.GetAnotherTariff(tariffName);
            anotherTariff.ClickByDetailsButton();
            return State.PageAs<InternetTariffPage>();
        }

        /// <summary>
        /// Открыть окно сравнения тарифов
        /// </summary>
        public CompareTariffsAlert OpenCompareTariffsAlert() {
            BillWidget.CompareTariffsLink.ClickAndWaitWhileAjax();
            var alert = State.HtmlAlertAs<CompareTariffsAlert>();
            if (alert == null)
                throw Throw.FrameworkException("Не отобразилось окно сравнения тарифов");
            return alert;
        }
    }
}
﻿using System.Collections.Generic;
using selenium.beeline.Pages.Base.Tariffs;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Internet {
    public class InternetTariffsPage : TariffsPage {

        [WebComponent] 
        public CheckConnectionAlert CheckConnectionAlert;

        [WebComponent] 
        public TariffAccessibilityWidget TariffAccessibilityWidget;

        [WebComponent]
        public InternetTariffs Tariffs;

        public override string AbsolutePath {
            get { return "customers/products/home/home-tariffs/index/internet/"; }
        }

        protected override List<ICard> GetCards() {
            return Tariffs.GetCards();
        }
    }
}

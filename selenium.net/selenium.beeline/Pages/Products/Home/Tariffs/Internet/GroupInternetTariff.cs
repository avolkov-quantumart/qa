using selenium.beeline.Pages.Base.Tariffs;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Internet {
    public class GroupInternetTariff : GroupTariffBase {
        public GroupInternetTariff(ITariffsGroup container, string id)
            : base(container, id) {
        }
    }
}
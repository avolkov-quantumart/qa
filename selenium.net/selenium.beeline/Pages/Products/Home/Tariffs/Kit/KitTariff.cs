﻿using selenium.beeline.Pages.Base.Tariffs;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Kit {
    public class KitTariff : TariffBase {
        public KitTariff(IContainer container, string id)
            : base(container, id) {
        }
    }
}

﻿using selenium.beeline.Pages.Base.Tariffs;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Kit {
    public class GroupKitTariff : GroupTariffBase {
        public GroupKitTariff(IContainer container, string id) 
            : base((ITariffsGroup) container, id) {
        }
    }
}

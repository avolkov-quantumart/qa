﻿using System.Collections.Generic;
using selenium.beeline.Pages.Base.Tariffs;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Kit {
    public class KitTariffsPage : TariffsPage {

        [WebComponent]
        public CheckConnectionAlert CheckConnectionAlert;

        [WebComponent]
        public TariffAccessibilityWidget TariffAccessibilityWidget;

        [WebComponent] 
        public KitTariffs Tariffs;

        public override string AbsolutePath {
            get { return "customers/products/home/home-tariffs/index/pakety-uslug/"; }
        }

        protected override List<ICard> GetCards() {
            return Tariffs.GetCards();
        }    
    }
}

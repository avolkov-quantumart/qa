﻿using System.Collections.Generic;
using System.Threading;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Base.Tariffs;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Common.ServiceWidgets;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Kit {
    public class KitTariffPage : TariffPageBase {

        [WebComponent] 
        public CheckConnectionAlert CheckConnectionAlert;

        [WebComponent] 
        public TariffDetails TariffDetails;

        [WebComponent] 
        public TariffAccessibilityWidget AccessibilityWidget;

        [WebComponent] 
        public TariffBillWidget BillWidget;

        [WebComponent]
        public BasketWidget BasketWidget;

        public override string AbsolutePath {
            get { return "customers/products/home/home-tariffs/kit/{tariff_url}/"; }
        }

        public void ClickByOrderButton(List<string> excluded) {
            do {
                var page = State.PageAs<KitTariffPage>();
                if (page.BasketWidget.OrderButton.IsVisible()) {
                    FD.tariff = page.TariffDetails.Name;
                    page.BasketWidget.OrderButton.ClickAndWaitWhileAjax(true);
                    return;
                }
                excluded.Add(page.TariffDetails.Name);
                Go.Back();
                State.PageAs<KitTariffsPage>().Tariffs.GoToRandomTariffDetails(excluded);
                State.PageAs<KitTariffPage>().BillWidget.ToBuyButton.ClickAndWaitWhileAjax(true);
            } while (true);
        }

    }
}
﻿using selenium.beeline.Pages.Base.Tariffs;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Kit {
    public class KitTariffs : CardsBase<KitTariff, KitTariffsGroup> {
        public KitTariffs(IPage parent)
            : base(parent) {
        }
    }
}

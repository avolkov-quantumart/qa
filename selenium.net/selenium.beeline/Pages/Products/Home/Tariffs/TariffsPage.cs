using System.Collections.Generic;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Base.Tariffs;

namespace selenium.beeline.Pages.Products.Home.Tariffs {
    public abstract class TariffsPage : BeelinePageBase {
        protected abstract List<ICard> GetCards();

        /// <summary>
        /// ������� ������������ �����, �� ����������� ����������
        /// </summary>
        public ITariff GetAnotherTariff(string tariffName) {
            List<ICard> cards = GetCards();
            foreach (ICard card in cards) {
                if (card is ITariff) {
                    var tariff = card as ITariff;
                    if (!tariffName.Equals(tariff.Name))
                        return tariff;
                }
                else
                    return (card as TariffsGroup<ITariff>).GetAnotherTariff(tariffName);
            }
            return null;
        }
    }
}
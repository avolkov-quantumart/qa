using selenium.beeline.Pages.Base.Tariffs;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Tv {
    public class TvTariffsGroup:TariffsGroup<GroupTvTariff>{
        public TvTariffsGroup(IContainer container, string id) : base(container, id) {
        }
    }
}
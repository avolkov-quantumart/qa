using System.Collections.Generic;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Common.ServiceWidgets;
using selenium.beeline.Pages.Products.Home.TvSchedule;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Tv {
    public class TvTariffPage : TariffPageBase, IHasTvSchedule {
        [WebComponent("li[span['��������� �������']]", "active",ComponentName = "��������� �������")]
        public WebToggleButton TvScheduleButton;

        [WebComponent] 
        public CheckConnectionAlert CheckConnectionAlert;

        [WebComponent]
        public TariffDetails TariffDetails;

        [WebComponent] 
        public TariffAccessibilityWidget AccessibilityWidget;

        [WebComponent] 
        public TariffBillWidget BillWidget;

        [WebComponent] 
        public BasketWidget BasketWidget;

        public override string AbsolutePath {
            get { return "customers/products/home/home-tariffs/tv/{tariff_url}/"; }
        }

        public void ClickByToBuyButton(List<string> excluded) {
            do {
                var page = State.PageAs<TvTariffPage>();
                if (page.BillWidget.ToBuyButton.IsVisible() && page.BillWidget.ToBuyButton.IsEnabled()) {
                    page.BillWidget.ToBuyButton.ClickAndWaitWhileAjax(true);
                    return;
                }
                excluded.Add(page.TariffDetails.Name);
                Go.Back();
                State.PageAs<TvTariffsPage>().Tariffs.GoToRandomTariffDetails(excluded);
            } while (true);
        }

        public void ClickByOrderButton(List<string> excluded) {
            do {
                var page = State.PageAs<TvTariffPage>();
                if (page.BasketWidget.OrderButton.IsVisible()) {
                    FD.tariff = page.TariffDetails.Name;
                    page.BasketWidget.OrderButton.ClickAndWaitWhileAjax(true);
                    return;
                }
                ClickByToBuyButton(excluded);
            } while (true);
        }

        #region IHasTvSchedule Members

        [WebComponent]
        public TvScheduleFilter Filter { get; private set; }

        [WebComponent]
        public TvScheduleComponent Schedule { get; private set; }

        #endregion

        /// <summary>
        /// ������� ��������� �������
        /// </summary>
        public void OpenTvSchedule() {
            Schedule.Open(() => TvScheduleButton.ClickAndWaitWhileAjax());
        }

    }
}
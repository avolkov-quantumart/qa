using selenium.beeline.Pages.Base.Tariffs;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Tv {
    public class TvTariffs : CardsBase<TvTariff, TvTariffsGroup> {
        public TvTariffs(IPage parent)
            : base(parent) {
        }
    }
}
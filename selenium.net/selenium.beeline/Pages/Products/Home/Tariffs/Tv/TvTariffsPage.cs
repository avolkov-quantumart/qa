using System.Collections.Generic;
using selenium.beeline.Pages.Base.Tariffs;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.Tariffs.Tv {
    public class TvTariffsPage : TariffsPage {

        [WebComponent] 
        public CheckConnectionAlert CheckConnectionAlert;

        [WebComponent] 
        private TariffAccessibilityWidget TariffAccessibilityWidget;

        [WebComponent]
        public TvTariffs Tariffs;

        public override string AbsolutePath {
            get { return "customers/products/home/home-tariffs/index/tv/"; }
        }

        protected override List<ICard> GetCards() {
            return Tariffs.GetCards();
        }
    }
}
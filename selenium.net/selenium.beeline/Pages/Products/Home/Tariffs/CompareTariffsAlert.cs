using System;
using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.Tariffs {
    public class CompareTariffsAlert : AlertBase {
        [WebComponent(1)]
        public Tariff LeftTariff;

        [WebComponent(1)]
        public Tariff RightTariff;

        public CompareTariffsAlert(IPage parent)
            : base(parent) {
        }

        public override bool IsVisible() {
            return Is.Visible(By.ClassName("basket-popup"));
        }

        public override void Dismiss() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// �������� �� ������ "�������" ��� ������, ��� �������� �� ����� ����������
        /// </summary>
        public void SelectAnotherTariff(string tariffName) {
            if (LeftTariff.Name.Equals(tariffName))
                RightTariff.ClickBySelectButton();
            else
                LeftTariff.ClickBySelectButton();
        }

        #region Nested type: Tariff

        public class Tariff : ComponentBase {
            private readonly By _nameSelector;
            private readonly By _panelSelector;
            private readonly By _selectButtonSelector;
            private readonly int _side;

            public Tariff(IPage parent, int side)
                : base(parent) {
                _side = side;
                string rootCss = string.Format(".selected-tariffs>div:nth-child({0})", side);
                _panelSelector = By.CssSelector(rootCss);
                _selectButtonSelector = By.CssSelector(rootCss + " .submit .button");
                _nameSelector = By.CssSelector(rootCss + " h3 strong a");
            }

            public bool IsSelected {
                get { return Is.HasClass(_selectButtonSelector, "disabled"); }
            }

            public string Name {
                get { return Get.Text(_nameSelector); }
            }

            public override bool IsVisible() {
                throw new NotImplementedException();
            }

            private string SideName() {
                return _side == 0 ? "left" : "right";
            }

            /// <summary>
            /// �������� �� ������ "�������"
            /// </summary>
            public void ClickBySelectButton() {
                SelectTariff();
                Log.Action("���� �� ������ '�������'({0})", SideName());
                Action.ClickAndWaitWhileAjax(_selectButtonSelector);
            }

            public void SelectTariff() {
                if (!IsSelected) {
                    Log.Action("���� �� ������ ����� ������������ ������ '�������'({0})", SideName());
                    Action.Click(_panelSelector);
                }
            }
        }

        #endregion
    }
}
using System;
using OpenQA.Selenium;
using selenium.beeline.Pages.Common;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home {
    public class TariffAccessibilityWidget : ComponentBase {
        private readonly By _changeAddressSelector;
        private readonly By _selectAddressSelector;
        [WebLoaderArgs(Css = "#CheckConnectBillOverlay .load")]
        public WebLoader Loader;

        public TariffAccessibilityWidget(IPage parent)
            : base(parent) {
            _changeAddressSelector = By.Id("changeAddress");
            _selectAddressSelector = By.Id("address");
        }

        /// <summary>
        /// ������� ���� �������� ����������� ������� ��� �� ������
        /// </summary>
        public CheckConnectionAlert OpenCheckTariffAccessibilityAlert() {
            Log.Action("������� ���� �������� ����������� ������� �� ������");
            Action.ClickAndWaitForAlert(IsAddressSelected() ? _changeAddressSelector : _selectAddressSelector);
            var alert = State.HtmlAlertAs<CheckConnectionAlert>();
            if (alert == null)
                throw Throw.FrameworkException("�� ������������ ���� �������� ����������� ������� �� ������");
            alert.WaitWhileLoading();
            return alert;
        }

        /// <summary>
        /// ���������, ������ �� ����� � �������
        /// </summary>
        public bool IsAddressSelected() {
            return Is.Visible(_changeAddressSelector);
        }

        public override bool IsVisible() {
            throw new NotImplementedException();
        }
    }
}
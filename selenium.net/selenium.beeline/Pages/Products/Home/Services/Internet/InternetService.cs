﻿using selenium.beeline.Pages.Base.Tariffs;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Products.Home.Services.Internet {
    public class InternetService : TariffBase {
        public InternetService(IContainer container, string id)
            : base(container, id) {
        }
    }
}

﻿using selenium.beeline.Pages.Base.Tariffs;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Products.Home.Services.Internet {
    public class InternetServices : CardsBase<InternetService, InternetServicesGroup> {
        public InternetServices(IPage parent) 
            : base(parent) {
        }
    }
}

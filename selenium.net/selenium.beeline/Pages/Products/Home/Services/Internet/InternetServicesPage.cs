﻿using System.Collections.Generic;
using selenium.beeline.Pages.Base.Tariffs;
using selenium.beeline.Pages.Products.Home.Tariffs;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.Services.Internet {
    public class InternetServicesPage : TariffsPage {

        [WebComponent] 
        public InternetServices Services;

        public override string AbsolutePath {
            get { return "customers/products/home/home-services/index/internet/"; }
        }

        protected override List<ICard> GetCards() {
            return Services.GetCards();
        }
    }
}

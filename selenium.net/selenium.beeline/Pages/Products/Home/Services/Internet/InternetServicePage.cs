﻿using System.Collections.Generic;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common.ServiceWidgets;
using selenium.beeline.Pages.Products.Home.Tariffs;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.Services.Internet {
    public class InternetServicePage : TariffPageBase {

        [WebComponent] 
        public TariffBillWidget BillWidget;

        [WebComponent] 
        public BasketWidget BasketWidget;

        [WebComponent] 
        public TariffDetails ServiceDetails;

        public override string AbsolutePath {
            get { return "customers/products/home/home-services/service/{tariff_url}/"; }               
        }

        public void ClickByToBuyButton() {
            List<string> excluded = new List<string>();
            do {
                var page = State.PageAs<InternetServicePage>();
                if (page.BillWidget.ToBuyButton.IsVisible()) {
                    page.BillWidget.ToBuyButton.ClickAndWaitWhileAjax(true);
                    return;
                }
                excluded.Add(page.ServiceDetails.Name);
                Go.Back();
                State.PageAs<InternetServicesPage>().Services.GoToRandomTariffDetails(excluded);
            } while (true);
        }
    }
}

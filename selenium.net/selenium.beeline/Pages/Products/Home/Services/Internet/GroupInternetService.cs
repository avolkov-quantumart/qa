﻿using selenium.beeline.Pages.Base.Tariffs;

namespace selenium.beeline.Pages.Products.Home.Services.Internet {
    public class GroupInternetService : GroupTariffBase {
        public GroupInternetService(ITariffsGroup container, string id) 
            : base(container, id) {
        }
    }
}

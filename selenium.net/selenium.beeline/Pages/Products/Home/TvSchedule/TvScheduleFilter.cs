using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.TvSchedule {
    public class TvScheduleFilter : ContainerBase {
        [WebComponent]
        public TvChannelGenres Genres;

        [WebComponent]
        public TvScheduleDate Date;

        [WebComponent]
        public TvDayTime DayTime;

        [WebComponent(".custom-search-bar")]
        public TvChannelsSearch Search;

        public TvScheduleFilter(IPage parent)
            : base(parent) {
        }

        public void TypeInSearchQuery(string query) {
            Open();
            Search.TypeInAndSubmit(query);
        }

        private void Open() {
            Search.Open(() => Action.Click(By.CssSelector(".search-button")));
        }
    }
}
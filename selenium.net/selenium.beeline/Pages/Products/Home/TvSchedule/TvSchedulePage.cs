﻿using selenium.beeline.Pages.Base;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.TvSchedule {
    public class TvSchedulePage : BeelinePageBase, IHasTvSchedule {
        public override string AbsolutePath {
            get { return "customers/products/home/tv-schedule/"; }
        }

        [WebComponent]
        public TvScheduleFilter Filter { get; private set; }

        [WebComponent]
        public TvScheduleComponent Schedule { get; private set; }
    }
}
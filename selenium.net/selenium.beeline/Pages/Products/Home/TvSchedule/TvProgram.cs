using System;

namespace selenium.beeline.Pages.Products.Home.TvSchedule {
    public class TvProgram {
        public DateTime Time { get; set; }
        public string Name { get; set; }

        public TvProgram(DateTime time, string name) {
            Time = time;
            Name = name;
        }
    }
}
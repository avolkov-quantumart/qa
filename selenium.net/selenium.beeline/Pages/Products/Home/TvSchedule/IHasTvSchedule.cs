namespace selenium.beeline.Pages.Products.Home.TvSchedule {
    public interface IHasTvSchedule {
        TvScheduleFilter Filter { get; }
        TvScheduleComponent Schedule { get; }
    }
}
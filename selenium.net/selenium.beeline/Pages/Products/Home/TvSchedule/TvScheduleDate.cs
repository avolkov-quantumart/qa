using System;
using System.Globalization;
using System.Linq;
using OpenQA.Selenium;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.TvSchedule {
    public class TvScheduleDate : ContainerBase {
        public TvScheduleDate(IPage parent)
            : base(parent) {
        }

        protected override string RootScss {
            get { return ".//*[@id='dateFilter']"; }
        }

        public DateTime SelectRandom(params DateTime[] exclude) {
            DateTime randomDate;
            var excludeList = exclude.ToList();
            do {
                randomDate = DateTime.Today.AddDays(new Random().Next(7));
            } while (excludeList.Contains(randomDate));
            SelectDate(randomDate);
            return randomDate;
        }

        private void SelectDate(DateTime date) {
            if (date.Date == DateTime.Today)
                Action.ClickAndWaitWhileAjax(InnerSelector("ul>li['�������']"));
            else if (date.Date == DateTime.Today.AddDays(1))
                Action.ClickAndWaitWhileAjax(InnerSelector("ul>li['������']"));
            else {
                OpenDatesList();
                var dateSelector = GetDateSelector(date.ToString("d MMMM", CultureInfo.GetCultureInfo("Ru-ru")));
                Action.ClickAndWaitWhileAjax(dateSelector);
            }
        }

        private By GetDateSelector(string date) {
            return InnerSelector(".toggle-drop li[span['{0}']]", date);
        }

        private bool IsDatesListVisible() {
            return Is.Visible(InnerSelector("div.toggle-drop"));
        }

        private void OpenDatesList() {
            if (!IsDatesListVisible())
                Action.Click(InnerSelector(".toggle-select-mark"), 2000);
            if (!IsDatesListVisible())
                throw Throw.FrameworkException("���������� ������ � ������ �� ��������");
        }

        public void SelectToday() {
            SelectDate(DateTime.Today);
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using core.Extensions;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.TvSchedule {
    public class TvChannelGenres : ContainerBase {
        public TvChannelGenres(IPage parent)
            : base(parent) {
        }

        protected override string RootScss {
            get { return "#showThemesPanel,.genre-filter"; }
        }

        public void Open() {
            Open(() => Action.Click(By.CssSelector("#showThemesLink, #showThemesButton")));
        }

        public string SelectRandom(string exclude=null) {
            Open();
            DeselectAll();
            List<string> genres = GetGenres();
            string randomGenre;
            do {
                randomGenre = genres.RandomItem();
            } while (randomGenre==exclude);
            SelectGenres(randomGenre);
            return randomGenre;
        }

        public void DeselectAll() {
            var scss = GetSelectedGenresScss();
            int prevCount = 0;
            do {
                var elements = Find.Elements(scss);
                if (elements.Count == 0)
                    return;
                if (elements.Count == prevCount)
                    throw Throw.FrameworkException("�� ������� ����� ��������� � ���������");
                prevCount = elements.Count;
                Action.ClickAndWaitWhileAjax(elements.First());
            } while (true);
        }

        private By GetSelectedGenresScss() {
            return InnerSelector("span.checkbox.checked");
        }

        private By GetGenreSelector(string genre) {
            return InnerSelector("span[contains(@class,'checkbox')][following-sibling::label[text()='{0}']]", genre);
        }

        private By GenreNameXpath {
            get { return InnerSelector("span[contains(@class,'checkbox')]/following-sibling::label"); }
        }

        public bool IsGenreSelected(string genre) {
            return Is.HasClass(GetGenreSelector(genre), "checked");
        }

        private void SelectGenres(params string[] genres) {
            foreach (var genre in genres)
                Action.ClickAndWaitWhileAjax(GetGenreSelector(genre));
        }

        public List<string> GetGenres() {
            return Get.Texts(GenreNameXpath);
        }
    }
}
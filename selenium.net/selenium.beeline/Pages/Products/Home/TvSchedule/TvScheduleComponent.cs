using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using selenium.beeline.Pages.Customers;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.TvSchedule {
    public class TvScheduleComponent : ListBase<TvChannel> {
        public TvScheduleComponent(IPage parent)
            : base(parent) {
        }

        protected override string RootScss {
            get { return "#tvProgramContent"; }
        }

        public override List<string> GetIds() {
            return Get.Attrs(ItemIdScss, "alt");
        }

        public override string ItemIdScss {
            get { return InnerScss(".channel-icon>img"); }
        }

        private bool ChannelContainsProgramLike(TvChannel tvChannel, string searchQuery) {
            searchQuery = searchQuery.ToLower();
            var tvPrograms = tvChannel.GetPrograms();
            return tvPrograms.Any(program => program.Name.ToLower().Contains(searchQuery));
        }

        public Dictionary<string, List<TvProgram>> GetData() {
            return GetItems().ToDictionary(tvChannel => tvChannel.ID, tvChannel => tvChannel.GetPrograms());
        }

        public void AssertDayTimeIs(TvDayTime.EDayTime dayTime) {
            int start;
            int finish;
            TvDayTime.GetHours(dayTime, out start, out finish);
            var tvChannels = GetItems();
            foreach (var channel in tvChannels) {
                foreach (var program in channel.GetPrograms()) {
                    if (finish > start)
                        Assert.That(program.Time.Hour, NUnit.Framework.Is.InRange(start, finish),
                                    "�� �������� ������ �� ������� ���.");
                    else
                        Assert.True((program.Time.Hour >= start && program.Time.Hour <= 24) ||
                                    (program.Time.Hour >= 0 && program.Time.Hour <= finish),
                                    "�� �������� ������ �� ������� ���.");
                }
            }
        }

        public void AssertProgramsFilteredBy(string searchQuery) {
            var tvChannels = GetItems();
            foreach (var tvChannel in tvChannels)
                Assert.True(ChannelContainsProgramLike(tvChannel, searchQuery),
                            "��� ������ '{0}' �� ������� �� ����� ��������� ��������������� ������� '{1}'", tvChannel.ID,
                            searchQuery);
        }
    }
}
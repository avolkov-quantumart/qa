using System;
using System.Linq;
using OpenQA.Selenium;
using core.Extensions;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.TvSchedule {
    public class TvDayTime:ContainerBase {
        private const string TOGGLE_DROP_XPATH = "div[@class='toggle-drop']";

        public TvDayTime(IPage parent)
            : base(parent) {
        }

        protected override string RootScss {
            get { return "#timeFilter"; }
        }

        public override string ComponentName {
            get { return "����� ���"; }
        }

        /// <summary>
        /// �������� ����, ������� ������������� ��������� ����� �����
        /// </summary>
        public static void GetHours(EDayTime dayTime, out int startHour, out int endHour) {
            switch (dayTime) {
                case EDayTime.AllDay:
                    startHour = 0;
                    endHour = 24;
                    break;
                case EDayTime.Morning:
                    startHour = 3;
                    endHour = 12;
                    break;
                case EDayTime.Day:
                    startHour = 11;
                    endHour = 18;
                    break;
                case EDayTime.Evening:
                    startHour = 17;
                    endHour = 3;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public EDayTime SelectRandom(EDayTime? exclude = null) {
            EDayTime random;
            do {
                random = Enum.GetValues(typeof (EDayTime)).Cast<EDayTime>().ToList().RandomItem();
            } while (random == exclude);
            Select(random);
            return random;
        }

        private void OpenDropList() {
            if (!IsDropListVisible())
                Action.Click(InnerSelector(".toggle-select-mark"), 2000);
            if (!IsDropListVisible())
                throw Throw.FrameworkException("���������� ������ � ������ �� ��������");
        }

        private bool IsDropListVisible() {
            return Is.Visible(InnerSelector(TOGGLE_DROP_XPATH));
        }

        private bool IsDropListExists() {
            return Is.Exists(InnerSelector(TOGGLE_DROP_XPATH));
        }

        private void Select(EDayTime dayTime) {
            By dayTimeSelector;
            if (IsDropListExists() && dayTime!= EDayTime.AllDay) {
                dayTimeSelector = GetDLDayTimeSelector(dayTime);
                OpenDropList();
            }
            else
                dayTimeSelector = GetDayTimeSelector(dayTime);
            Action.ClickAndWaitWhileAjax(dayTimeSelector);
        }

        private By GetDayTimeSelector(EDayTime dayTime) {
            return InnerSelector("li['{0}']", dayTime.StringValue());
        }

        /// <summary>
        /// �������� �������� ���� � ���������� ������
        /// </summary>
        private By GetDLDayTimeSelector(EDayTime dayTime) {
            return InnerSelector(">div.toggle-drop>ul>li>span['{0}']", dayTime.StringValue());
        }

        public enum EDayTime {
            [StringValue("���� ����")]
            AllDay,
            [StringValue("�����")]
            Morning,
            [StringValue("����")]
            Day,
            [StringValue("�������")]
            Evening
        }
    }
}
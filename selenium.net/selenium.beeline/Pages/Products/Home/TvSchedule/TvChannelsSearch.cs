using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.TvSchedule {
    public class TvChannelsSearch : ContainerBase {
        [WebComponent("root:input",ComponentName = "����� �������")]
        public WebInput Input;

        public TvChannelsSearch(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public void TypeInAndSubmit(string query) {
            Input.TypeInAndSubmit(query);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using OpenQA.Selenium;
using core.Extensions;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home.TvSchedule {
    public class TvChannel : ItemBase {
        public TvChannel(IContainer container, string id)
            : base(container, id) {
        }

        public override string ItemScss {
            get {
                return ContainerInnerScss("*[@class='single-channel' or " +
                                           "@class='channel-programm'][descendant::img[@alt='{0}']]", ID);
            }
        }

        public List<TvProgram> GetPrograms() {
            List<DateTime> timeList = Get.Texts(GetTimeSelector()).TrimAll().Select(
                t => DateTime.ParseExact(t, "HH:mm", CultureInfo.InvariantCulture)).ToList();
            List<string> nameList = Get.Texts(GetNameSelector()).TrimAll();
            if (timeList.Count != nameList.Count)
                throw Throw.FrameworkException("timeList.Count!=nameList.Count");
            return timeList.Select((t, i) => new TvProgram(t, nameList[i])).ToList();
        }

        private By GetNameSelector() {
            return InnerSelector(".tv-show-title>span");
        }

        private By GetTimeSelector() {
            return InnerSelector(".tv-show-time");
        }
    }
}
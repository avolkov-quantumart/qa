﻿using System.Text.RegularExpressions;
using NUnit.Framework;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Home {

    class BasketPage : BeelinePageBase {

        [WebComponent(".order-button>span")]
        public BeelineButton ContactDetailsButton;

        [WebComponent(".order-button>span")]
        public BeelineButton ConfirmButton;

        [WebComponent(".order-button>span")]
        public BeelineButton MakeOrderButton;

        [WebComponent(".contact-information>div[1]>div")]
        public BeelineInput UserNameInput;

        [WebComponent(".contact-information>div[2]>div")] 
        public BeelineInput EmailAddressInput;

        [WebComponent(".contact-information>div[3]>div")]
        public BeelineInput PhoneNumberInput;

        [WebComponent(".address-form>div>div[4]>div")] 
        public BeelineInput ApartmentInput;

        [SimpleWebComponent(Css = ".content-block p:nth-of-type(1)")] 
        public WebText UserDetails;

        [SimpleWebComponent(Css = ".content-block p:nth-of-type(2)")]
        public WebText AddressWebText;

        public HomeRequestResultForm RequestResultForm;

        public override string AbsolutePath {
            get { return "customers/products/home/basket/"; }
        }

        public void AssertUserDetailsIs(string userName, string userEmail, string userPhone) {
            var regex = new Regex("(?<name>.+)(\r\n?|\n)(?<phone>\\d+),(?<email>.+)");
            Match match = regex.Match(UserDetails.Text);
            Assert.AreEqual(userName, match.Groups["name"].Value.Trim());
            Assert.AreEqual(userEmail, match.Groups["email"].Value.Trim());
            Assert.AreEqual(userPhone, match.Groups["phone"].Value);
        }

        public void AssertAddressIs(string streetName, string houseNumber, string apartmentNubmer) {
            var regex = new Regex("(?<city>.+),(?<street>.+),(?<house>.+),(?<apartment>.+)");
            Match match = regex.Match(AddressWebText.Text);
            char[] charsToRemove = { 'д', 'к', 'в', '.', ' ' };
            Assert.AreEqual(streetName, match.Groups["street"].Value.Trim());
            Assert.AreEqual(houseNumber, match.Groups["house"].Value.Trim(charsToRemove));
            Assert.AreEqual(apartmentNubmer, match.Groups["apartment"].Value.Trim(charsToRemove));
        }
    }

}

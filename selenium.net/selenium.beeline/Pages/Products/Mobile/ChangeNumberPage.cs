using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using core.Extensions;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Help.Mobile;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Products.Mobile {
    public class ChangeNumberPage : BeelinePageBase {

        public override string AbsolutePath {
            get { return "customers/products/mobile/services/details/nomer-na-vybor/"; }
        }

        [SimpleWebComponent(ComponentName = "�������� ���", Css = "#ChangeNumber_Random_ShowMore .button>label")]
        public WebButton ShowMoreButton;

        [WebComponent()]
        public SummaryWidget SummaryWidget;

        [SimpleWebComponent(ComponentName = "����� ������������", Css = "#SelectNumberForm p span")]
        public WebText UserNumber;

        [WebComponent("table#ChangeNumber_Random_table", ComponentName = "������� �� ���������� ��������")]
        public SuggestedPhonesGrid PhonesGrid;

        [WebComponent()]
        public SourceData SourceData;

        [WebComponent("div#ChangeNumberButtons>ul")]
        public ChangeMethodTabs Tabs;

        [WebComponent()]
        public GetConfirmationCodeAlert GetConfirmationCodeAlert;

        /// <summary>
        /// ������� ������� �� ���������� ��������
        /// </summary>
        public SuggestedPhonesGrid OpenTableWithSuggestedNumbers()
        {
            if(SourceData.IsVisible()) 
                SourceData.SetRandomBeelineNumber();
            if (!Tabs.IsTabSelected(ChangeMethodTabs.RANDOM_NUMBERS))
                Tabs.SelectTab(ChangeMethodTabs.RANDOM_NUMBERS);
            return PhonesGrid;
        }
    }

    public class SummaryWidget : ComponentBase {
        public SummaryWidget(IPage parent)
            : base(parent) {
        }

        public override string ComponentName {
            get { return "��������� ���������"; }
        }

        [WebComponent("#billSlideContainer .button", ComponentName = "����� � ����� ������")]
        public BeelineButton ChangeNumberButton;

        public override bool IsVisible() {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// �������� ��������� ������������� ����� ����� ��������
        /// </summary>
        public string GetNewNumber() {
            return Get.Text(By.CssSelector("#billSlideContainer small em span"));
        }

        public void AssertNewNumberIs(string expected) {
            Assert.AreEqual(expected, GetNewNumber(), "����� �� ������������� ���������� �������������");
        }

        public void AssertPriceIs(int expected) {
            Assert.AreEqual(expected, GetPrice(), "���� �� ������������� ��������� ���������� ������������� ������");
        }

        private int GetPrice() {
            return Get.Int(By.CssSelector("#billSlideContainer .price"));
        }
    }

    public class ChangeMethodTabs : BeelineTabs {
        public const string RANDOM_NUMBERS = "��������� ������";

        public ChangeMethodTabs(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public override string GetTabScss(string tabName) {
            return string.Format("{0}>li[span['{1}']]", RootScss, tabName);
        }
    }

    public class SuggestedPhonesGrid : ComponentBase {
        private readonly string _rootScss;

        public SuggestedPhonesColumn Column1;
        public SuggestedPhonesColumn Column2;
        public SuggestedPhonesColumn Column3;
        public SuggestedPhonesColumn Column4;

        public SuggestedPhonesGrid(IPage parent, string rootScss)
            : base(parent) {
            _rootScss = rootScss;
            Column1 = ParentPage.RegisterComponent<SuggestedPhonesColumn>(
                string.Format("������� {0} � ������� ��������� �������", 1),
                1, rootScss);
            Column2 = ParentPage.RegisterComponent<SuggestedPhonesColumn>(
                string.Format("������� {0} � ������� ��������� �������", 2),
                2, rootScss);
            Column3 = ParentPage.RegisterComponent<SuggestedPhonesColumn>(
                string.Format("������� {0} � ������� ��������� �������", 3),
                3, rootScss);
            Column4 = ParentPage.RegisterComponent<SuggestedPhonesColumn>(
                string.Format("������� {0} � ������� ��������� �������", 4),
                4, rootScss);
        }

        public override bool IsVisible() {
            return Is.Visible(_rootScss);
        }

        public void AssertIsNotEmpty() {
            Column1.AssertRowsCountMoreThan(1);
            Column2.AssertRowsCountMoreThan(1);
            Column3.AssertRowsCountMoreThan(1);
            Column4.AssertRowsCountMoreThan(1);
        }

        /// <summary>
        /// ������� ������������ ����� �� ������� ��������� �������
        /// </summary>
        public void SelectRandomNumber() {
            SuggestedPhonesColumn randomColumn = new List<SuggestedPhonesColumn> {Column1, Column2, Column3, Column4}.RandomItem();
            randomColumn.SelectRandomPhone();
        }

        /// <summary>
        /// �������� ���������� �����
        /// </summary>
        public string GetSelectedNumber() {
            return Get.TextS(GetActiveItemSelector());
        }

        private By GetActiveItemSelector() {
            return Scss.GetBy(_rootScss, "tr[2]>td li.active");
        }

        /// <summary>
        /// �������� ��������� ���������� ������ ������
        /// </summary>
        public int GetPriceOfSelectedNumber() {
            string code = Get.Attr(GetActiveItemSelector(), "data-featurecode");
            int columnIndex;
            switch (code) {
                case "ECNB1":
                    columnIndex = 1;
                    break;
                case "ECNB2":
                    columnIndex = 2;
                    break;
                case "ECNB3":
                    columnIndex = 3;
                    break;
                case "ECNA5":
                    columnIndex = 4;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("code");
            }
            return Get.Int(string.Format("th[{0}] .cost", columnIndex), true);
        }

        public List<string> GetPhones() {
            var phones = new List<string>();
            phones.AddRange(Column1.GetPhones());
            phones.AddRange(Column2.GetPhones());
            phones.AddRange(Column3.GetPhones());
            phones.AddRange(Column4.GetPhones());
            return phones;
        }

        public bool HasSelectedNumber() {
            return !string.IsNullOrEmpty(GetSelectedNumber());
        }
    }

    public class SuggestedPhonesColumn : ComponentBase {
        private readonly int _columnIndex;
        private readonly string _rootScss;

        public SuggestedPhonesColumn(IPage parent, int columnIndex, string rootScss)
            : base(parent) {
            _columnIndex = columnIndex;
            _rootScss = rootScss;
        }

        public override string ComponentName {
            get { return "�������" + _columnIndex; }
        }

        public string ItemsScss {
            get { return string.Format("{0} tr[2]>td[{1}] li", _rootScss, _columnIndex); }
        }

        public string GetItemScss(string phone) {
            return string.Format("{0}[label>span['{1}']]", ItemsScss, phone);
        }

        public override bool IsVisible() {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// �������� ������ ������� �������
        /// </summary>
        public List<string> GetPhones() {
            string phoneScss = ItemsScss + ">label>span";
            return Get.Texts(phoneScss);
        }

        public void AssertRowsCountMoreThan(int count) {
            Assert.IsTrue(GetPhones().Count > count, "'{0}' �������� ������ {1} ��������", ComponentName, count);
        }

        public void SelectRandomPhone() {
            List<string> phones = GetPhones();
            SelectPhone(phones.RandomItem());
        }

        /// <summary>
        /// �������� ��������� ����� ��������
        /// </summary>
        private void SelectPhone(string phone) {
            Action.Click(GetItemScss(phone));
            Log.WriteValue("selectedphone", phone);
        }
    }

    /// <summary>
    /// ����� � �������� ������� ��� ����� ������
    /// </summary>
    public class SourceData : ComponentBase {
        [WebComponent(".//*[@id='ChangeNumberContentContainer']", "OldNumber.PhoneCode", "OldNumber.Phone")]
        public BeelinePhoneNumber PhoneNumber;

        [WebComponent("#ChangeNumberContentContainer .ButtonSelectNumber", ComponentName = "�������� ������")]
        public BeelineButton ShowNumbersButton;

        public SourceData(IPage parent)
            : base(parent) {
        }

        public override bool IsVisible() {
            return Is.Visible(By.ClassName("change-number-old-number-form-marker"));
        }

        /// <summary>
        /// ���������� � �������� �������� ������ ������������ ����� Beeline
        /// </summary>
        public void SetRandomBeelineNumber() {
            PhoneNumber.FillWithRandomBeelineNumber();
            ShowNumbersButton.ClickAndWaitWhileAjax();
        }
    }
}
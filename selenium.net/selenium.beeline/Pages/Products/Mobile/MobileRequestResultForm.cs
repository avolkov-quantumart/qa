using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile {
    public class MobileRequestResultForm : RequestResultForm {

        [SimpleWebComponent(Css = ".acces-request .special-info-block>h3>strong")]
        public WebText ApplicationNumber;

        public MobileRequestResultForm(IPage parent)
            : base(parent) {
        }

        public override bool IsVisible() {
            return Title.IsVisible() &&
                   GirlWithBubbles.IsVisible() &&
                   ApplicationNumber.IsVisible();
        }
    }
}
using selenium.beeline.Pages.Base;

namespace selenium.beeline.Pages.Products.Mobile {
    public class ForGuestsPage : BeelinePageBase {
        public override string AbsolutePath {
            get { return "customers/products/mobile/roaming/for-guests/general-information/"; }
        }
    }
}
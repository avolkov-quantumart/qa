﻿using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common.ServiceWidgets;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.Services.Details {
    public class SendSmsPage : BeelinePageBase {
        [WebComponent(".sms-send-form")]
        public SendSmsForm SendSmsComponent;

        public override string AbsolutePath {
            get { return "customers/products/mobile/services/details/otpravka-sms/"; }
        }
    }
}

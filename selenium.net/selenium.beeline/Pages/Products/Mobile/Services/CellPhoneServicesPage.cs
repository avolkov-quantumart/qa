﻿using selenium.beeline.Pages.Base;

namespace selenium.beeline.Pages.Products.Mobile.Services {
    public class CellPhoneServicesPage : BeelinePageBase {
        public override string AbsolutePath {
            get { return "customers/products/mobile/services/index/cellphone/"; }
        }
    }
}
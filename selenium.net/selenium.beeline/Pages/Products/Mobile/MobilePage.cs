using selenium.beeline.Pages.Base;

namespace selenium.beeline.Pages.Products.Mobile
{
    public class MobilePage : BeelinePageBase {
        public override string AbsolutePath {
            get { return "customers/products/mobile/"; }
        }
    }
}

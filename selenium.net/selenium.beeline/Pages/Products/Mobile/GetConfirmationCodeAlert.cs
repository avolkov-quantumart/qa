using NUnit.Framework;
using OpenQA.Selenium;
using core;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Products.Mobile {
    public class GetConfirmationCodeAlert :AlertBase{
        public GetConfirmationCodeAlert(IPage parent) : base(parent) {
        }

        public override bool IsVisible() {
            return Is.Visible(By.ClassName("tariff-change"));
        }

        public override void Dismiss() {
            throw new System.NotImplementedException();
        }

        public void AssertNewNumberIs(string expected) {
            Assert.AreEqual(expected, GetNewNumber(),
                            "����� ����� �� ��������� � ��������� �������������");
        }

        private string GetCurrentNumber() {
            return Get.Text(By.CssSelector("#ConfirmationForm h4>span"));
        }

        private string GetNewNumber() {
            return Get.Text(By.CssSelector(".tariff-change h3 strong>span:nth-child(1)"));
        }

        public void AssertPriceIs(int expected) {
            Assert.AreEqual(expected, GetPrice(),
                            "��������� � ���� ��������� ���� ������������� �� ��������� �� ���������� ���������� ������������� ������");
        }

        private int GetPrice() {
            string text = Get.Text(By.CssSelector(".tariff-change h3>strong"));
            return RegexHelper.GetInt(text, "��������� (\\d+)");
        }

        public void AssertCurrentNumberIs(string expected) {
            Assert.AreEqual(expected, GetCurrentNumber(),
                            "������� ����� �� ��������� � ��������� �������������");
        }
    }
}
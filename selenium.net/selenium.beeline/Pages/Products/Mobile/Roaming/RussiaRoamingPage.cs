﻿using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.Roaming
{
    public class RussiaRoamingPage : RoamingPageBase {
        public override string AbsolutePath {
            get { return "customers/products/mobile/roaming/roaming-with-options-russia/"; }
        }

        [WebComponent(".roaming-services-wrap")]
        public RussiaRoamingPrices Prices;
    }

    public class RussiaRoamingPrices : ContainerBase {
        public RussiaRoamingPrices(IPage parent, string rootScss = null)
            : base(parent, rootScss) {
        }
    }
}

﻿using NUnit.Framework;
using OpenQA.Selenium;
using core;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.Roaming
{
    public class GlobalCallsPage : RoamingPageBase ,IHasRecommendedServices{
        public override string AbsolutePath {
            get { return "customers/products/mobile/roaming/mgmn-roaming-with-options/"; }
        }

        [WebComponent(".roaming-services-wrap")]
        public GlobalCallsPrices Prices;

        [WebComponent]
        public RecommendedServices RecommendedServices { get; private set; }
    }

    public class GlobalCallsPrices:ContainerBase {
        private readonly By _titleSelector;
        public GlobalCallsPrices(IPage parent, string rootScss = null) : base(parent, rootScss) {
            _titleSelector = InnerSelector("div.content-block>h3");
        }

        public void AssertTariffNameIs(string tariff) {
            Assert.AreEqual(tariff,GetTariff());
        }

        private string GetTariff() {
            return RegexHelper.GetString(Get.Text(_titleSelector), "Расценки связи на тариф «(.*)»");
        }
    }
}

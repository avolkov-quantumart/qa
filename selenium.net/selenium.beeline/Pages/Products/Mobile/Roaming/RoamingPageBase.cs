using selenium.beeline.Pages.Base;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.Roaming {
    public abstract class RoamingPageBase:BeelinePageBase {
        [WebComponent]
        public RoamingParams Params;        
    }
}
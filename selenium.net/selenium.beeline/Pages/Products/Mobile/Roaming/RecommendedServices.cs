﻿using System;
using NUnit.Framework;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.Roaming {
    public class RecommendedServices : ListBase<RecommendedService> {
        public RecommendedServices(IPage parent)
            : base(parent, ".good-buy-wrap") {
        }

        public RecommendedServices(IPage parent, string rootScss) : base(parent, rootScss) {
        }

        public void SelectService(string serviceName) {
            RecommendedService recommendedService = GetService(serviceName);
            recommendedService.Select();
        }

        private RecommendedService GetService(string serviceName) {
            RecommendedService service = FindSingle(s => s.ID == serviceName);
            if (service == null)
                throw new Exception(string.Format("Услуга '{0}' отсутвсвует в блоке рекомендованных услуг", serviceName));
            return service;
        }

        public void AssertServiceIsDisabled(string serviceName) {
            RecommendedService recommendedService = GetService(serviceName);
            recommendedService.AssertIsDisabled();
        }

        public override string ItemIdScss {
            get { return InnerScss(".checkblock a"); }
        }
    }

    public class RecommendedService : ItemBase {
        [WebComponent("root:.checkbox", "child::input", "")]
        public BeelineCheckbox Checkbox;

        public RecommendedService(IContainer container, string id) : base(container, id) {
        }

        public override string ItemScss {
            get { return ContainerInnerScss(".checkblock[a['{0}']]", ID); }
        }

        public void Select() {
            Checkbox.SelectAndWaitWhileAjax();
        }

        public void AssertIsDisabled() {
            Assert.True(Is.HasClass(InnerScss(".check-label"), "disabled"), "Название услуги '{0}' активно", ID);
            Assert.True(Is.HasClass(InnerScss(".check-label>span[1]"), "disable"), "Название услуги '{0}' активно", ID);
            Checkbox.AssertIsDisabled();
        }
    }
}

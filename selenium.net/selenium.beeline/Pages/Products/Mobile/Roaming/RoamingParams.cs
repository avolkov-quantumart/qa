using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Help.Mobile;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.Roaming {
    public class RoamingParams:ComponentBase {
        [WebComponent(".CalculateButton",ComponentName = "���������� ���������")]
        public BeelineButton CalculateButton;

        [WebComponent(".region.search-bar","li>span>strong>em",ComponentName = "���� �� �����")]
        public BeelineInputWithList Destination;

        [WebComponent("div[>label['��� �����']]",ComponentName = "��� �����")]
        public BeelineDropList Tariff;


        public RoamingParams(IPage parent) : base(parent) {
        }

        public override bool IsVisible() {
            throw new System.NotImplementedException();
        }

        public string SelectRandomTariff() {
            string random;
            do {
                random = Tariff.SelectRandom();
            } while (random == "������ ������");
            return random;
        }
    }
}
﻿using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Help.Mobile;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.Roaming
{
    public class GlobalRoamingPage : RoamingPageBase,IHasRecommendedServices{
        public override string AbsolutePath {
            get { return "customers/products/mobile/roaming/roaming-with-options/"; }
        }

        [WebComponent(".roaming-services-wrap")]
        public GlobalRoamingPrices Prices;

        [WebComponent]
        public RecommendedServices RecommendedServices { get; private set; }
    }

    public class GlobalRoamingPrices : ContainerBase {
        public GlobalRoamingPrices(IPage parent, string rootScss = null)
            : base(parent, rootScss) {
        }
    }
}

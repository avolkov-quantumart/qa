﻿namespace selenium.beeline.Pages.Products.Mobile.Roaming {
    internal interface IHasRecommendedServices {
        RecommendedServices RecommendedServices { get; }
    }
}

using OpenQA.Selenium;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.MNP {
    public class MNPApplicationForm:ContainerBase {
        [WebComponent("span.ButtonSend",ComponentName = "������� � ������ ������")]
        public BeelineButton SelectOfficeButton;
        [WebComponent(".captcha")]
        public BeelineCaptcha Captcha;
        [WebComponent("root:.form-phone-box")]
        public BeelinePhoneNumber PhoneNumber;
        [SimpleWebComponent(ID = "ContactPhoneCode",ComponentName = "��� ����������� ��������")]
        public WebInput ContactPhoneCode;
        [SimpleWebComponent(ID = "ContactPhone",ComponentName = "����� ����������� ��������")]
        public WebInput ContactPhone;
        [SimpleWebComponent(ID = "FirstName",ComponentName = "���")]
        public WebInput FirstName;
        [SimpleWebComponent(ID = "LastName",ComponentName = "�������")]
        public WebInput LastName;
        [SimpleWebComponent(ID = "MiddleName",ComponentName = "��������")]
        public WebInput PatronimicName;

        public MNPApplicationForm(IPage parent) : base(parent) {
        }

        [SimpleWebComponent(ID = "Address",ComponentName = "�����")]
        public WebInput City;

        [SimpleWebComponent(ID = "Street",ComponentName = "�����")]
        public WebInput Street;

        [SimpleWebComponent(ID = "Building",ComponentName = "����� ����")]
        public WebInput Building;

        [SimpleWebComponent(ID = "Apartment",ComponentName = "����� ��������")]
        public WebInput Apartment;

        public override bool IsVisible() {
            return Is.Visible(By.ClassName("portation-request"));
        }

        /// <summary>
        /// ����������� ����� �� ���� "����� ��� �������� � ���� ��������" � ���� "���������� �������"
        /// </summary>
        public void UseSamePhoneForContact() {
            ContactPhoneCode.TypeIn(PhoneNumber.Code.Text);
            ContactPhone.TypeIn(PhoneNumber.Phone.Text);
        }

        public void SelectCourierDelivary() {
            Log.Action("������� ����� '���������� �������� � ��������'");
            Browser.Action.Click("label[strong['���������� �������� � ��������']]");
        }
    }
}
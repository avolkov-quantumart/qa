using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.MNP {
    public class MobileTariffsList : ListBase<MobileTariff> {

        public MobileTariffsList(IPage parent)
            : this(parent, ".tariffs[div.card]") {
        }

        public MobileTariffsList(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public override string ItemIdScss {
            get { return InnerScss("div.summary>h3>strong>a"); }
        }
    }
}
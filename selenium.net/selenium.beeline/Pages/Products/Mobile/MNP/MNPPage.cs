using OpenQA.Selenium;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Customers.BeelineOnMap;
using selenium.beeline.Pages.Help.Mobile;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.MNP
{
    public class MNPPage : BeelinePageBase {
        public override string AbsolutePath {
            get { return "customers/products/mobile/mnp/"; }
        }

        [WebComponent]
        public MobileRequestResultForm RequestResultForm;

        [WebComponent]
        public SelectPortationTariff SelectTariff;

        [WebComponent("div.maps-wrap")]
        public OfficesTab OfficesTab;

        [WebComponent]
        public MNPApplicationForm ApplicationForm;
    }

    public class SendMnpApplicationBallon : BeelineBallonBase {
        [WebComponent("span[>#btnSelectOffice]",ComponentName = "������ ������")]
        public BeelineButton Submit;

        public SendMnpApplicationBallon(IPage parent, string rootScss = null)
            : base(parent, rootScss) {
        }
    }

}

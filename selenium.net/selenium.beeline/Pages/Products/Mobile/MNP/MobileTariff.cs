using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Help.Mobile;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.MNP {
    public class MobileTariff : ItemBase {
        public WebLink TitleLink;
        public BeelineButton SubmitButton;

        public MobileTariff(IContainer container, string id) : base(container, id) {
            TitleLink = container.ParentPage.RegisterComponent<WebLink>("�������� ������", InnerSelector("h3>strong>a"));
            SubmitButton = container.ParentPage.RegisterComponent<BeelineButton>(
                "������ ������ ������",
                InnerScss(".button"));
        }

        public override string ItemScss {
            get {
                return ContainerInnerScss("div[contains(@class,'card')][descendant::*[contains(@class,'summary')]" +
                                           "/h3/strong/a[text()='{0}']]", ID);
            }
        }
    }
}
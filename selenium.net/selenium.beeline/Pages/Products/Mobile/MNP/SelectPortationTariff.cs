using OpenQA.Selenium;
using core.Extensions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.MNP {
    public class SelectPortationTariff:ComponentBase {
        [WebComponent]
        public MobileTariffsList TariffsList;

        public SelectPortationTariff(IPage parent) : base(parent) {
        }

        public override bool IsVisible() {
            return Is.Visible(By.ClassName("portation-tariffs"));
        }

        public void SelectRandom() {
            TariffsList.GetItems().RandomItem().SubmitButton.ClickAndWaitWhileAjax();
        }
    }
}
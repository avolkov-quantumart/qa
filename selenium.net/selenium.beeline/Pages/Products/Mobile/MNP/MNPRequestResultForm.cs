using NUnit.Framework;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products.Mobile.MNP {
    public class MNPRequestResultForm : ComponentBase {
        private const string REQUEST_ACCEPTED_TITLE = "���� ������ �������";

        [SimpleWebComponent(Css = ".acces-request .special-info-block>h3>strong",ComponentName = "����� ������")]
        public WebText ApplicationNumber;

        [SimpleWebComponent(Css = ".acces-request>div>img",ComponentName = "������� � ����������")]
        public WebImage GirlWithBubbles;

        [SimpleWebComponent(Css = ".acces-request h1",ComponentName = "���������")]
        public WebText Title;

        public MNPRequestResultForm(IPage parent)
            : base(parent) {
        }

        public override bool IsVisible() {
            return Title.IsVisible() &&
                   GirlWithBubbles.IsVisible() &&
                   ApplicationNumber.IsVisible();
        }

        public void AssertApplicationAccepted() {
            Assert.AreEqual(REQUEST_ACCEPTED_TITLE, Title.Text, "������ �� �������");
        }
    }
}
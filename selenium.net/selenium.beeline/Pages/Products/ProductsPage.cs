﻿using OpenQA.Selenium;
using selenium.beeline.Pages.Base;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Products {
    public class ProductsPage : BeelinePageBase {
        [WebComponent(ComponentName = "Рекламные слайды")]
        public Carousel Carousel;

        public override string AbsolutePath {
            get { return "customers/products/"; }
        }
    }

    /// <summary>
    /// Компонент с рекламными слайдами
    /// </summary>
    public class Carousel : ComponentBase {
        public Carousel(IPage parent) : base(parent) {
        }

        public override bool IsVisible() {
            return Is.Visible(By.ClassName("carousel"));
        }
    }
}
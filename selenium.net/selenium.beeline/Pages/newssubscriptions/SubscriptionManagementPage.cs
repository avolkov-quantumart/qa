﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Customers.press;
using selenium.beeline.Pages.Help.Mobile;
using selenium.core.Framework.PageElements;
using selenium.core.Framework.Service;

namespace selenium.beeline.Pages.newssubscriptions {
    public class SubscriptionManagementPage : BeelinePageBase {
        [WebComponent]
        public SubscribeManagementComponent Component;

        public SubscriptionManagementPage() {
            Params = new StringDictionary {{"type", "AddSubscription"}};
        }

        public override string AbsolutePath {
            get { return "newssubscriptions/subscribeaction/"; }
        }

        public BeelineInput Email {
            get { return Component.Email; }
        }

        public BeelineCaptcha SubscribeCaptcha {
            get { return Component.SubscribeCaptcha; }
        }

        [WebComponent(".SubscribeBtnArea", ComponentName = "Подписаться")]
        public BeelineButton SubscribeButton;

        public override RequestData GetRequest(BaseUrlInfo defaultBaseUrlInfo) {
            // TODO: реализовать формирование Url с учетом параметров
            BaseUrlInfo baseUrlInfo = defaultBaseUrlInfo.ApplyActual(BaseUrlInfo);
            String url = String.Format("http://{0}{1}", baseUrlInfo.GetBaseUrl(), AbsolutePath);
            return new RequestData(url);
        }

        /// <summary>
        /// Получить список отмеченных тематик подписки
        /// </summary>
        /// <returns></returns>
        public List<string> GetSelectedThemes() {
            return Component.GetSelectedThemes();
        }

        public List<string> SelectRandomThemes(int number) {
            return Component.SelectRandomThemes(number);
        }

        public void AssertCheckMailForActivationMessage() {
            Component.AssertCheckMailForActivationMessage();
        }
    }
}
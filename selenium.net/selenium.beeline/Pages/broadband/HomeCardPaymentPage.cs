﻿using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.broadband {
    public class HomeCardPaymentPage : BeelinePageBase {
        [SimpleWebComponent(ID = "AccontNumber",ComponentName = "Номер счета")]
        public WebInput AccountNumber;

        [SimpleWebComponent(ID = "Sum",ComponentName = "Сумма")]
        public WebInput Sum;

        public override string AbsolutePath {
            get { return "broadband/"; }
        }
    }

    public class MobileCardPaymentPage : BeelinePageBase {
        [WebComponent(".//*[@id='Payments_0__CtnPrefix']", ".//*[@id='Payments_0__Ctn']",ComponentName = "Номер телефона")]
        public PhoneNumber Phone;

        [SimpleWebComponent(ID = "Payments_0__Sum",ComponentName = "Сумма")]
        public WebInput Sum;

        public override string AbsolutePath {
            get { return "mobile/"; }
        }
    }
}
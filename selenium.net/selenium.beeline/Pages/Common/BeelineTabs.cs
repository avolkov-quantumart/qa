using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using core;
using core.Extensions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Common {
    public class BeelineTabs : ContainerBase {
        private readonly string _innerTabNameScss;

        public BeelineTabs(IPage parent, string rootScss)
            : this(parent, rootScss, "span") {
        }

        public BeelineTabs(IPage parent, string rootScss, string innerTabNameScss)
            : base(parent, rootScss) {
            _innerTabNameScss = innerTabNameScss;
        }

        public List<string> GetTabNames() {
            return Get.Texts(TabNameScss);
        }

        public virtual string Selected {
            get {
                Scss scss = ScssBuilder.Concat("li.active", _innerTabNameScss);
                return Get.Text(InnerSelector(scss));
            }
        }

        public virtual string TabNameScss {
            get { return InnerScss(XPathBuilder.Concat("li", _innerTabNameScss)); }
        }

        public virtual string GetTabScss(string tabName) {
            string xpath = string.IsNullOrEmpty(_innerTabNameScss)
                               ? string.Format("'{0}'", tabName)
                               : string.Format("{0}['{1}']", _innerTabNameScss, tabName);
            return InnerScss("li[{0}]", xpath);
        }

        public bool IsTabVisible(string tabName) {
            return Is.Visible(GetTabScss(tabName));
        }

        public void AssertTabIsVisible(string tabName) {
            Assert.IsTrue(IsTabVisible(tabName), "������� '{0}' �� ������������", tabName);
        }

        public void AssertTabIsSelected(string tabName) {
            Assert.IsTrue(IsTabSelected(tabName), "������� '{0}' ���������", tabName);
        }


        public bool IsTabSelected(string tabName) {
            return Is.HasClass(GetTabScss(tabName), "active");
        }

        /// <summary>
        /// ������� �� �������
        /// </summary>
        public void SelectTab(string tabName) {
            Action.Click(GetTabSelector(tabName));
        }

        public void SelectTab(EBeelineTab tab) {
            SelectTab(tab.StringValue());
        }

        public By GetTabSelector(string tabName) {
            return ScssBuilder.CreateBy(GetTabScss(tabName));
        }
    }

    public enum EBeelineTab {
        [StringValue("�������� ������")]
        HomeBeeline,

        [StringValue("��������� ������")]
        MobileBeeline,

        [StringValue("WiFi")]
        WiFi
    }
}
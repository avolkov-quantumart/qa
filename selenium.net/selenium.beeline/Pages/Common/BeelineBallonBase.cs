using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common {
    public class BeelineBallonBase:ContainerBase {
        public readonly WebButton CloseButton;

        public BeelineBallonBase(IPage parent, string rootScss = null) : base(parent, rootScss) {
            CloseButton = parent.RegisterComponent<WebButton>("������� �����",
                                                              InnerSelector("b.m-closer-balloon"));
        }

        public override bool IsVisible() {
            return CloseButton.IsVisible();
        }
    }
}
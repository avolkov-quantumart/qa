using NUnit.Framework;
using OpenQA.Selenium;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common {
    public class CheckConnectionAlert : AlertBase {
        [WebComponent("div.address-form div.form-box[3]", ComponentName = "���")]
        public BeelineInputWithList House;

        [WebComponent("div.address-form div.form-box[2]", ComponentName = "�����")]
        public BeelineInputWithList Street;

        [WebComponent]
        public TariffAccessibleMessage TariffAccessibleMessage;
        [WebComponent]
        public TariffInaccessibleMessage TariffInaccessibleMessage;

        public CheckConnectionAlert(IPage parent) : base(parent) {
        }

        [WebComponent("span[>#connection-request-button]")]
        public BeelineButton SendApplicationButton;

        /// <summary>
        /// ��������� ����� 
        /// </summary>
        /// <param name="street">�����</param>
        /// <param name="house">����� ����</param>
        public void FillForm(string street, string house) {
            Street.TypeInAndSelectFirst(street);
            TypeInHouseAndWaitForResult(house);
        }

        private void TypeInHouseAndWaitForResult(string value) {
            House.TypeInAndSelectFirst(value);
            Wait.Until(IsResultVisible);
        }

        /// <summary>
        /// ������������ �� ��������� �������� ������
        /// </summary>
        private bool IsResultVisible() {
            return TariffAccessibleMessage.IsVisible() || TariffInaccessibleMessage.IsVisible();
        }

        /// <summary>
        /// ��������� ��� ������������ ��������� � ��� ��� ������ ���������� �� ���������� ������
        /// </summary>
        public void AssertTariffInaccessible() {
            Assert.IsTrue(TariffInaccessibleMessage.IsVisible(),
                          "�� ������������ ��������� � ��� ��� ������ ���������� ��� ���������� ������");
        }

        public override bool IsVisible() {
            return Is.Visible("h2['�������� ����������� �����']");
        }

        public override void Dismiss() {
            Action.Click(By.ClassName("popup-close"));
        }

        public void AssertTariffAccessible() {
            Assert.IsTrue(TariffAccessibleMessage.IsVisible(),
                          "�� ������������ ��������� � ��� ��� ������ �������� ��� ���������� ������");
        }

        /// <summary>
        /// ��������� ���� �� �������� ������� ��������
        /// </summary>
        public void WaitWhileLoading() {
            Wait.WhileAjax();
        }

        /// <summary>
        /// ������� ����� � ��� ��� ������� ������ ��������
        /// </summary>
        public void SetValidAddress(string street, string house) {
            FillForm(street, house);
            if (!TariffAccessibleMessage.IsVisible())
                throw Throw.FrameworkException("������ ���������� ��� ������ {0}, {1}", street, house);
            TariffAccessibleMessage.ClickByContinueButton();
        }
    }

    public class TariffInaccessibleMessage : ComponentBase {
        public TariffInaccessibleMessage(IPage parent)
            : base(parent) {
        }

        public override bool IsVisible() {
            return Is.Visible(By.Id("invalid"));
        }
    }

    public class TariffAccessibleMessage : ComponentBase {
        public TariffAccessibleMessage(IPage parent) : base(parent) {
        }

        public override bool IsVisible() {
            return Is.Visible(By.Id("valid"));
        }

        /// <summary>
        /// ���� �� ������ "����������"
        /// </summary>
        public void ClickByContinueButton() {
            Log.Action("���� �� ������ '����������'");
            Action.ClickAndWaitWhileAjax("label['����������']");
        }
    }
}
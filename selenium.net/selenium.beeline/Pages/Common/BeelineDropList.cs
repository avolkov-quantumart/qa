using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using core.Extensions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Common {
    public class BeelineDropList : ContainerBase {
        private readonly By _itemSelector;
        private readonly By _listSelector;

        public readonly WebText Selected;
        private readonly string _selectedcScss;

        public BeelineDropList(IPage parent, string rootScss = null)
            : base(parent, rootScss) {
            Selected = parent.RegisterComponent<WebText>("��������� �������", InnerSelector("span.slct"));
            _selectedcScss = InnerScss("span.slct");
            _itemSelector = InnerSelector("li>span");
            _listSelector = InnerSelector("ul");
        }

        public string SelectRandom() {
            OpenList();
            var randomItem = GetItems().RandomItem();
            SelectItem(randomItem);
            return randomItem;
        }

        public void SelectItem(string item) {
            OpenList();
            Action.Click(InnerSelector("li>span['{0}']", item));
        }

        private void OpenList() {
            if (!Is.Visible(_listSelector)) {
                Scss scss = ScssBuilder.Create(_selectedcScss);
                Js.Excecute(!string.IsNullOrWhiteSpace(scss.Css)
                                ? string.Format("$(\"{0}\").click()", _selectedcScss)
                                : string.Format(
                                    "$(document.evaluate(\"{0}\", document, null, 9, null).singleNodeValue).click()",
                                    _selectedcScss));
                Thread.Sleep(1000);
            }
        }

        private List<string> GetItems() {
            return Get.Texts(_itemSelector).Where(i=>!string.IsNullOrEmpty(i)).ToList();
        }
    }
}
using System.Collections.Generic;
using OpenQA.Selenium;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Common {
    public class DropList : ContainerBase {
        private readonly string _itemXpath;

        public DropList(IPage parentPage, string rootScss, string itemRelativeXpath)
            : base(parentPage,rootScss) {
            _itemXpath = InnerScss(itemRelativeXpath);
        }

        public void SelectFirst() {
            List<IWebElement> elements = GetItems();
            if (elements.Count == 0)
                throw Throw.FrameworkException("� ������ ����������� ��������");
            Action.Click(elements[0]);
        }

        public override bool IsVisible() {
            return Is.Visible(RootScss);
        }

        public void WaitWhileInvisible(int seconds) {
            Wait.Until(IsVisible, seconds);
        }

        public List<IWebElement> GetItems() {
            return Find.Elements(_itemXpath);
        }
    }

    public class BeelineInputWithList:ContainerBase{

        /// <summary>
        /// �������� ���� �����
        /// </summary>
        private readonly By _inputSelector;
        /// <summary>
        /// ���������� ������
        /// </summary>
        private readonly DropList _dropList;

        public BeelineInputWithList(IPage parentPage, string rootScss)
            : this(parentPage, rootScss, "li/a") {
        }

        public BeelineInputWithList(IPage parentPage, string rootScss, string itemRelativeXpath)
            : base(parentPage, rootScss) {
            _dropList = new DropList(parentPage, InnerScss("ul"), itemRelativeXpath);
            _inputSelector = ScssBuilder.CreateBy(InnerScss("input"));
        }

        /// <summary>
        /// ���������� ���� �����
        /// </summary>
        public string GetText() {
            return Get.InputValue(_inputSelector);
        }

        /// <summary>
        /// ������ ����� � ���� ����� � ������� ������ ������� � ���������� ������
        /// </summary>
        public void TypeInAndSelectFirst(string text, bool ajaxInevitable=false) {
            Log.Action("������ '{0}' � ���� '{1}'", text, ComponentName);
            Action.TypeIn(_inputSelector, text);
            Wait.WhileAjax();
            _dropList.WaitWhileInvisible(4);
            _dropList.SelectFirst();
            Wait.WhileAjax(ajaxInevitable: ajaxInevitable);
            //Browser.Action.TypeIn(houseSelector, Keys.Down + Keys.Enter, false);
        }

        public override bool IsVisible() {
            throw new System.NotImplementedException();
        }

        public string SelectRandom() {
            throw new System.NotImplementedException();
        }
    }
}
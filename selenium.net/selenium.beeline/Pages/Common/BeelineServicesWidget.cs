using core.Extensions;
using selenium.beeline.Pages.Base;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common {
    public class BeelineServicesWidget : ComponentBase {
        [WebComponent(".help-sidebar", "//li[span[normalize-space(text())='{0}']]","active")]
        public WebTabs Tabs;

        public BeelineServicesWidget(IPage parent)
            : base(parent) {
        }

        /// <summary>
        /// ������� ������� ���������� �������
        /// </summary>
        public T OpenService<T>(BeelineServiceType serviceType) where T : IComponent {
            Tabs.Select(serviceType.StringValue());
            return ParentPage.CreateComponent<T>();
        }

        public override bool IsVisible() {
            throw new System.NotImplementedException();
        }
    }
}
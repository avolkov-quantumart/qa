using System;
using OpenQA.Selenium;
using selenium.beeline.Pages.Base;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Common {
    public class LanguageSelector:ComponentBase {
        private const string RUSSIAN_RADIO_TEXT = "�� ������� �����";
        private const string ENGLISH_RADIO_TEXT = "in english";

        public LanguageSelector(IPage parent) : base(parent) {
        }

        public override bool IsVisible() {
            return Is.Visible(By.ClassName("select-region"));
        }

        public SiteLanguage GetCurrent() {
            string radioText = Get.Text(By.CssSelector(".language-chooser .checked+span"));
            switch (radioText) {
                case RUSSIAN_RADIO_TEXT: return SiteLanguage.Russian;
                case ENGLISH_RADIO_TEXT:return SiteLanguage.English;
                default:
                    throw new ArgumentOutOfRangeException("radio text");
            }
        }

        /// <summary>
        /// ������� ���� �����
        /// </summary>
        public void Select(SiteLanguage language) {
            if (GetCurrent() == language)
                return;
            Log.Action("������� ���� ����� '{0}'", language);
            string radioText;
            switch (language) {
                case SiteLanguage.Russian:
                    radioText = RUSSIAN_RADIO_TEXT;
                    break;
                case SiteLanguage.English:
                    radioText = ENGLISH_RADIO_TEXT;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("language");
            }
            string scss = string.Format("a>span['{0}']", radioText);
            Action.ClickAndWaitForRedirect(scss);
        }
    }
}
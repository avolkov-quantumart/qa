using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Common {
    public class BeelineButton : WebButton {
        private readonly string _rootScss;

        public BeelineButton(IPage parent, string rootScss)
            : base(parent, ScssBuilder.CreateBy(rootScss)) {
            _rootScss = rootScss;
        }

        public bool IsEnabled() {
            return !Is.HasClass(_rootScss, "disabled");
        }

        public void ClickAndWaitWhileAjax(bool ajaxInevitable=false) {
            Log.Action("���� �� ������ '{0}'", ComponentName);
            Action.ClickAndWaitWhileAjax(By, ajaxInevitable: ajaxInevitable);
        }

        /// <summary>
        /// ��������� ��� ������ �������
        /// </summary>
        public void AssertEnabled() {
            Assert.IsTrue(IsEnabled(), "������ '{0}' ���������", ComponentName);
        }

        /// <summary>
        /// ��������� ��� ������ ���������
        /// </summary>
        public void AssertDisabled() {
            Thread.Sleep(1000);
            Assert.IsFalse(IsEnabled(), "������ '{0}' �������", ComponentName);
        }

        public void WaitForEnabled() {
            Wait.Until(IsEnabled);
        }
    }
}
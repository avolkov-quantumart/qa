using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Common {
    public interface IMapPoint : IComponent {
        int Count { get; }
        void OpenBalloon();
        bool IsLeaf { get; }
    }
}
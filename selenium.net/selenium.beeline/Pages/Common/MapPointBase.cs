using System.Linq;
using OpenQA.Selenium;
using core;
using selenium.core.Framework.Browser;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common {
    public class MapPoint : WebElementWrap, IMapPoint {
        public MapPoint(IPage parent, IWebElement element)
            : base(parent, element) {
        }

        public virtual int Count {
            get {
                IWebElement innerElement = Find.Element(Element, By.CssSelector(".PGmap-geometry-point"), false);
                return Is.HasClass(innerElement, "large")
                           ? Get.Int(innerElement, By.CssSelector("div span"))
                           : 1;
            }
        }

        public virtual void OpenBalloon() {
            ClickAndWaitWhileAjax();
        }

        public virtual bool IsLeaf {
            get { return Count == 1; }
        }

        public bool IsKnowHowOffice() {
            IWebElement innerElement = Find.Element(Element, By.CssSelector(".PGmap-geometry-point"), false);
            var backgroundImage = Get.CssValue<string>(innerElement, ECssProperty.background_image);
            string imgPath = RegexHelper.GetString(backgroundImage, "url\\((.*)\\)");
            string imgFileName = imgPath.Split('/').Last();
            return imgFileName == "map-pin-small-knowhow.png";
        }
    }

    public class KnowHowMapPoint:MapPoint{
        public KnowHowMapPoint(IPage parent, IWebElement element) : base(parent, element) {
        }
    }
}
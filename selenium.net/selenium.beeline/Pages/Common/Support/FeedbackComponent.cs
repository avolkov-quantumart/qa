using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Common.Support;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Base {
    public class FeedbackComponent : ContainerBase {
        [WebComponent]
        public ComplaintTab Complaint;
        [WebComponent]
        public QuestionTab Question;
        [WebComponent]
        public SwindleTab Swindle;

        [WebComponent("#feedback-form>li[span[data-filter-type-id='claim']]",ComponentName = "������")]
        public BeelineToggleButton ComplaintToggle;
        [WebComponent("#feedback-form>li[span[data-filter-type-id='question']]",ComponentName = "������")]
        public BeelineToggleButton QuestionToggle;
        [WebComponent("#feedback-form>li[span[data-filter-type-id='swindle']]", ComponentName = "�������������")]
        public BeelineToggleButton SwindleToggle;
        

        public FeedbackComponent(IPage parent)
            : base(parent) {
        }

        public override string ComponentName {
            get { return "�������� �����"; }
        }

        public FeedbackComponent(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public ComplaintTab OpenComplaintTab() {
            ComplaintToggle.SelectAndWaitWhileAjax();
            return Complaint;
        }

        public QuestionTab OpenQuestionTab() {
            QuestionToggle.ClickAndWaitWhileAjax();
            return Question;
        }

        public SwindleTab OpenSwindleTab() {
            SwindleToggle.ClickAndWaitWhileAjax();
            return Swindle;
        }
    }
}
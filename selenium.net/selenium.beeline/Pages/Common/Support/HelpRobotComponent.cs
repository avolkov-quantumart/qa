using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Base {
    public class HelpRobotComponent : ContainerBase {
        public HelpRobotComponent(IPage parent)
            : base(parent) {
        }

        public override string ComponentName {
            get { return "����������� ��������"; }
        }

        public HelpRobotComponent(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }
    }
}
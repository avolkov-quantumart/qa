using NUnit.Framework;
using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common.Support {
    public class FeedbackResultForm : ContainerBase {
        [SimpleWebComponent(Css = "#FeedbackContainerBlock .chat-information>h4",ComponentName = "��� ������������")]
        public WebText Name;

        public FeedbackResultForm(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public void AssertSuccess() {
            Assert.True(Is.Visible(By.Id("SuccessText")),
                        "�� ������������ ����� �������� �������� ������� � ������������");
        }

        public void AssertNameIs(string name) {
            Assert.AreEqual(name, GetName(),
                            "� ����� ���������� �������� ������� � ��������� ������������ ������������ ���");
        }

        private string GetName() {
            return Name.Text.Replace(",", string.Empty);
        }
    }
}
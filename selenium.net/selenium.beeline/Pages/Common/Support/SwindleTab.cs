using selenium.beeline.Pages.Base;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common.Support {
    public class SwindleTab : FeedbackTabBase {
        public SwindleTab(IPage parent)
            : base(parent) {
        }

        public override string ComponentName {
            get { return "�������������"; }
        }

        [WebComponent("#ReasonBlock>ul>li[1]>span>input", "#ReasonBlock>ul>li[1]>label>span",ComponentName = "������ �� �������� �������")]
        public BeelineRadio ShortNumbersRadio;

        [WebComponent("#ReasonBlock>ul>li[2]>span>input", "#ReasonBlock>ul>li[2]>label>span", ComponentName = "������")]
        public BeelineRadio OtherRadio;

        [WebComponent("span[>input#ButtonVerifyCode]", ComponentName = "����������")]
        public BeelineButton ContinueButton;

        [WebComponent("div[>input#VerifyCode]",ComponentName = "������")]
        public BeelineInput Password;

        [SimpleWebComponent(ID = "ButtonGetVerifyCode",ComponentName = "�������� ������")]
        public WebButton GetPasswordButton;

        public SwindleTab(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }
    }
}
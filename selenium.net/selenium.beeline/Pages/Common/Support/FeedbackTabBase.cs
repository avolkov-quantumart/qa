using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Common.Support;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Base {
    public class FeedbackTabBase : ContainerBase {
        [WebComponent("#FeedbackContainerBlock #PhoneBlock,#FeedbackQuestionBeePhone")]
        public BeelinePhoneNumber Phone;

        [WebComponent("#FeedbackContainerBlock #CaptchaBlock,#FeedbackValidationContext")]
        public BeelineCaptcha Captcha;

        [SimpleWebComponent(ID = "UserQuestion",ComponentName = "��� ������")]
        public WebInput Body;

        [WebComponent("div[>#Email]",ComponentName = "Email")]
        public BeelineInput Email;

        [SimpleWebComponent(Css = "#filesContainer_Tab span:nth-child(2)",ComponentName = "��������� �����")]
        public WebText LoadFilesLink;

        [WebComponent(ComponentName="�������� ������")]
        public FilesLoader FilesLoader;

        [WebComponent("div[>input#FilterThemeId]",ComponentName = "����")]
        public BeelineDropList Theme;

        [WebComponent("div[>#UserName]",ComponentName = "��� ������������")]
        public BeelineInput Name;

        [WebComponent("root:.ButtonSubmit",ComponentName = "���������")]
        public BeelineButton SendButton;

        public FeedbackTabBase(IPage parent)
            : this(parent, "#FeedbackContainerBlock") {
        }

        [WebComponent(".chat-info-wrap")]
        public FeedbackResultForm FeedbackResult;

        public FeedbackTabBase(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public virtual void AssertSuccessFormIsVisible(string name) {
            FeedbackResult.AssertSuccess();
            FeedbackResult.AssertNameIs(name);
        }

        public void LoadFile(string filePath) {
            OpenFilesSelector().Load(filePath);
        }

        private FilesLoader OpenFilesSelector() {
            return (FilesLoader) FilesLoader.Open(() => LoadFilesLink.Click());
        }
    }
}
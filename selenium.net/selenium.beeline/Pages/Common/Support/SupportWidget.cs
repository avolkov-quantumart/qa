using System;
using System.Threading;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Base {
    public class SupportWidget : ContainerBase {
        [WebComponent]
        public FeedbackComponent Feedback;

        [WebComponent("li[span['�������� �����']]",ComponentName = "�������� �����")]
        public BeelineToggleButton FeedbackToggle;

        [WebComponent]
        public HelpRobotComponent Help;

        [WebComponent("li[span['����������� ��������']]",ComponentName = "����������� ��������")]
        public BeelineToggleButton HelpRobotToggle;

        public SupportWidget(IPage parent)
            : base(parent) {
        }

        public SupportWidget(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public FeedbackComponent OpenFeedbackComponent() {
            return Feedback.Open<FeedbackComponent>(
                () => {
                    try {
                        FeedbackToggle.Select();
                    }
                    catch (InvalidOperationException) {
                        Thread.Sleep(3000);
                        FeedbackToggle.Select();
                    }
                });
        }

        public HelpRobotComponent OpenHelpRobotComponent() {
            HelpRobotToggle.Select();
            return Help;
        }
    }
}
using OpenQA.Selenium;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.Tests;

namespace selenium.beeline.Pages.Base {
    public class QuestionTab : FeedbackTabBase {
        public QuestionTab(IPage parent)
            : base(parent) {
        }

        public override string ComponentName {
            get { return "������"; }
        }

        public QuestionTab(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        [WebComponent(".//*[@id='SubthemesGroupSelect']",ComponentName = "�������")]
        public BeelineDropList Subtheme;

        public override void AssertSuccessFormIsVisible(string name) {
            base.AssertSuccessFormIsVisible(name);
            AssertCodeIsVisible();
        }

        private void AssertCodeIsVisible() {
            TestHelper.AssertAction(() => GetCode(), "�� ������������ ��� ���������");
        }

        private int GetCode() {
            return Get.Int(By.CssSelector("#SuccessText>:last-child"));
        }
    }
}
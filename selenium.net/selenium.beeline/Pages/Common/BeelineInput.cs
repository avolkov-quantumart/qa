using System.Threading;
using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Common {
    public class BeelineTextArea : WebInput {
        public BeelineTextArea(IPage parent, string rootScss)
            : base(parent, ScssBuilder.Concat(rootScss, "textarea").By) {
        }
    }

    public class BeelineInput : WebInput {
        // �������� ���� ����� � ������� �������� ��������� ��������
        // �� ������ "��� ���??!!" ���������� � ��������������
        private readonly By _hiddenInputSelector;

        public BeelineInput(IPage parent, string rootScss)
            : this(parent, rootScss, "input") {
        }

        public BeelineInput(IPage parent, string rootScss, string hiddenInputScss)
            : base(parent, ScssBuilder.Concat(rootScss, "input").By) {
            _hiddenInputSelector = ScssBuilder.Concat(rootScss, hiddenInputScss).By;
        }

        public override string Text {
            get {
                string text = base.Text;
                return text ?? Get.InputValue(_hiddenInputSelector, false);
            }
        }

        public override void Clear() {
            Action.Clear(By);
            Action.PressKey(By, Keys.Backspace); // Black magic
            Thread.Sleep(1000);
        }

        public override void TypeIn(object value) {
            Clear();
            base.TypeIn(value);
            Thread.Sleep(500);
        }
    }
}
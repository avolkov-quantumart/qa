using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common {
    public class RegionsDropList : DropListBase {
        public RegionsDropList(IPage parent)
            : base(parent, ".region-chooser div.results") {
        }

        public override bool IsVisible() {
            return Is.Visible(RootScss);
        }

        public override By GetItemSelector(string name) {
            return InnerSelector(string.Format("li>a['{0}']", name));
        }

        public override string ItemNameScss {
            get { return InnerScss("li>a"); }
        }

        /// <summary>
        /// �������� �� �������� ������
        /// </summary>
        public void Select(string item) {
            Action.ClickAndWaitForRedirect(GetItemSelector(item));
        }
    }
}
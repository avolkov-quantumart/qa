using System.Threading;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common {
    public class BeelineCaptcha : ContainerBase {
        private const string DEFAULT_CAPTCHA = "99999";

        [SimpleWebComponent(ID = "captcha-img",ComponentName = "������")]
        public WebImage Image;

        [SimpleWebComponent(Name = "Captcha",ComponentName = "������")]
        public WebInput Input;

        public BeelineCaptcha(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public override string ComponentName {
            get { return "������"; }
        }

        /// <summary>
        /// ���������� ������ �� ���������
        /// </summary>
        public void SetDefault() {
            if (!IsVisible())
                return;
            TypeIn(DEFAULT_CAPTCHA);
            Action.ChangeFocus();
        }

        public void TypeIn(string value) {
            Input.TypeIn(value);
            Js.Excecute(string.Format("$('[name=Captcha]').val('{0}')", value));
//            if (Input.Text != value)
//                throw Throw.FrameworkException(
//                    "�� ������� ������ �������� '{0}' � ���� �����. ������� ����������: '{1}'", value, Input.Text);
        }
    }
}
using System.Text.RegularExpressions;
using selenium.beeline.Pages.Help.Mobile;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common {
    public class BeelinePhoneNumber : ContainerBase {
        public BeelineInput Code;
        public BeelineInput Phone;
        public PhoneValidationHint ValidationHint;

        public BeelinePhoneNumber(IPage parent) :
            this(parent, null) {
        }

        public BeelinePhoneNumber(IPage parent, string rootScss)
            : this(parent, rootScss, "PhoneCode", "Phone") {
        }

        public BeelinePhoneNumber(IPage parent, string rootScss, string codeInputName, string phoneInputName)
            : base(parent, rootScss) {
            string codeInputXpath = string.Format(">[name='{0}']", codeInputName);
            string phoneInputXpath = string.Format(">[name='{0}']", phoneInputName);
            Code = ParentPage.RegisterComponent<BeelineInput>("��� ��������",
                                                              InnerScss(string.Format("div[{0}]", codeInputXpath)),
                                                              codeInputXpath);
            Phone = ParentPage.RegisterComponent<BeelineInput>("�����",
                                                               InnerScss(string.Format("div[{0}]", phoneInputXpath)),
                                                               phoneInputXpath);
            ValidationHint = ParentPage.RegisterComponent<PhoneValidationHint>("Validation hint for phone",
                                                                               InnerSelector("div.form-tip"));
        }

        public BeelinePhoneNumber(IPage parent, string rootScss, string codeRootXpath, string numberRootXpath,
                                  string hintXpath)
            : base(parent, rootScss) {
            Code = ParentPage.RegisterComponent<BeelineInput>("��� ��������", InnerScss(codeRootXpath));
            Phone = ParentPage.RegisterComponent<BeelineInput>("�����", InnerScss(numberRootXpath));
            ValidationHint = ParentPage.RegisterComponent<PhoneValidationHint>("Validation hint for phone",
                                                                               InnerSelector(hintXpath));
        }

        public override string ComponentName {
            get { return "����� ��������"; }
        }

        public override bool IsVisible() {
            return Code.IsVisible() && Phone.IsVisible();
        }

        public void AssertIsEmpty() {
            Code.AssertIsEmpty();
            Phone.AssertIsEmpty();
        }

        /// <summary>
        /// ������ ������������ ����� Beeline
        /// </summary>
        public void FillWithRandomBeelineNumber() {
            Code.TypeIn(964);
            Phone.TypeIn(1112233);
            Log.WriteValue("userphone", GetFormated());
        }

        /// <summary>
        /// ��������� ������������� ����������� �������
        /// </summary>
        public void RandomFill() {
            Code.TypeInRandomNumber(3);
            Phone.TypeInRandomNumber(7);
        }

        /// <summary>
        /// �������� ���� �����
        /// </summary>
        public void Clear() {
            Code.Clear();
            Phone.Clear();
        }

        /// <summary>
        /// �������� ���������� ����� � ������� +7 964 111-22-33
        /// </summary>
        public string GetFormated() {
            string number = Phone.Text.Replace(" ", string.Empty);
            Match match = new Regex("(?<g1>\\d{3})(?<g2>\\d{2})(?<g3>\\d{2})").Match(number);
            return string.Format("+7 {0} {1}-{2}-{3}",
                                 Code.Text,
                                 match.Groups["g1"].Value,
                                 match.Groups["g2"].Value,
                                 match.Groups["g3"].Value);
        }

        public void TypeIn(string phone) {
            if (phone.Length != 10)
                throw Throw.TestException("{0} is not a phone number", phone);
            Code.TypeIn(phone.Substring(0, 3));
            Phone.TypeIn(phone.Substring(3, 7));
        }

        public void RemoveLastFromCode() {
            Code.RemoveLast();
            // ��� ���� ����� ������ ����� � ����� ����� ����� ������
            Browser.Action.ChangeFocus();   
            Browser.Action.ChangeFocus();
        }
    }
}
using OpenQA.Selenium;
using selenium.beeline.Pages.Base;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common {
    public class RegionSelector : ComponentBase{
        public RegionSelector(IPage parent)
            : base(parent) {
        }

        [SimpleWebComponent(ComponentName = "���� ������ �������", Css = ".region-chooser .search-bar input")]
        public WebInput Search;

        [WebComponent]
        public RegionsDropList DropList;

        public override bool IsVisible() {
            return Is.Visible(By.CssSelector(".region-chooser .search-bar"));
        }

        public void SelectPopular(string city) {
            var xpath = string.Format(".city-list-popular>ul>li>a['{0}']", city);
            Action.Click(xpath);
        }
    }
}
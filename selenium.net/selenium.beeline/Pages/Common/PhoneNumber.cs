using NUnit.Framework;
using OpenQA.Selenium;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Common {
    public class PhoneNumber : ComponentBase {
        public readonly WebInput Code;
        public readonly WebInput Phone;

        public PhoneNumber(IPage parent, string codeXpath, string numberXpath)
            : base(parent) {
            Code = parent.RegisterComponent<WebInput>("��� ��������", ScssBuilder.CreateBy(codeXpath));
            Phone = parent.RegisterComponent<WebInput>("����� ��������", ScssBuilder.CreateBy(numberXpath));
        }

        public override bool IsVisible() {
            return Code.IsVisible() && Phone.IsVisible();
        }

        public void TypeIn(string phone) {
            if (phone.Length != 10)
                throw Throw.TestException("{0} is not a phone number", phone);
            Code.TypeIn(phone.Substring(0, 3));
            Phone.TypeIn(phone.Substring(3, 7));
        }

        public string GetPhone() {
            return Code.Text + Phone.Text.Replace("-", string.Empty);
        }

        public void AssertEqual(string expected) {
            Assert.AreEqual(expected, GetPhone(), "������������ ����� ��������");
        }
    }
}
using NUnit.Framework;
using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common {
    public class BeelineCheckbox : ContainerBase, IWebCheckbox {
        public readonly By InputSelector;
        public readonly By LabelSelector;

        public BeelineCheckbox(IPage parent, string rootScss, string inputScss, string labelScss)
            : base(parent, rootScss) {
            LabelSelector = InnerSelector(labelScss);
            InputSelector = InnerSelector(inputScss);
        }

        public BeelineCheckbox(IPage parent, string rootScss)
            : this(parent, rootScss, "child::input", "following-sibling::label") {
        }

        public void Select() {
            if (!Checked()) {
                Log.Action("������������� ������� '{0}'", ComponentName);
                Action.Click(LabelSelector);
            }
        }

        public void Deselect() {
            if (Checked()) {
                Log.Action("������� ������� '{0}'", ComponentName);
                Action.Click(LabelSelector);
            }
        }

        private bool Checked(IWebElement element) {
            return element.GetAttribute("checked") == "true";
        }

        public bool Checked() {
            return Is.Checked(InputSelector);
        }

        public void Click() {
            Action.Click(RootSelector);
        }

        public void AssertIsDisabled() {
            Assert.AreEqual("true", Get.Attr(InputSelector, "disabled", false), "������� '{0}' �������",
                            ComponentName);
        }

        public void SelectAndWaitWhileAjax() {
            if (!Checked()) {
                Log.Action("������������� ������� '{0}'", ComponentName);
                Action.ClickAndWaitWhileAjax(LabelSelector);
            }
        }
    }
}
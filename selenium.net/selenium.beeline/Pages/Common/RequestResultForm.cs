﻿using NUnit.Framework;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common {
    public class RequestResultForm : ComponentBase {

        private const string REQUEST_ACCEPTED_TITLE = "Ваша заявка принята";

        [SimpleWebComponent(Css = ".acces-request>div>img")] 
        public WebImage GirlWithBubbles;

        [SimpleWebComponent(Css = ".acces-request h1")]
        public WebText Title;

        public RequestResultForm(IPage parent) 
            : base(parent) {
        }

        public override bool IsVisible() {
            return Title.IsVisible() && GirlWithBubbles.IsVisible();
        }

        public void AssertApplicationAccepted() {
            Assert.AreEqual(REQUEST_ACCEPTED_TITLE, Title.Text, "Заявка не принята");
        }
    }
}
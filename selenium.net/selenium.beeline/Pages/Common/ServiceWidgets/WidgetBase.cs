using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common.ServiceWidgets {
    public class WidgetBase : ContainerBase {
        public WidgetBase(IPage parent) : base(parent, ".help-content") {
        }

        public WidgetBase(IPage parent, string rootScss) : base(parent, rootScss) {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common.ServiceWidgets {
    public class BasketWidget : WidgetBase {

        [SimpleWebComponent(XPath = "/h5/a")] 
        public WebLink LinkToBasket;

        [SimpleWebComponent(XPath = "/ul/li/a")]
        public WebLink LinkToTariff;

        [WebComponent("root:div[@class='submit']/span", ComponentName = "Оформление заказа")]
        public BeelineButton OrderButton;

        public BasketWidget(IPage parent)
            : base(parent, "#main-basket-info-div") {
        }

    }
}
 
using System;
using NUnit.Framework;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common.ServiceWidgets {
    public class SmsStatusForm : ContainerBase {
        [SimpleWebComponent(Css = "p.progress>span:nth-of-type(1)")]
        protected WebText Phone;

        [SimpleWebComponent(Css = "span.rounded>span>span")]
        public WebText StatusText;

        [SimpleWebComponent(Css = ".refresh>span>em",ComponentName = "��������")]
        public WebText UpdateLink;

        [SimpleWebComponent(XPath = "//em[normalize-space(text())='����� ������� ���������']",
            ComponentName = "��������� '����� ������� ���������'")]
        public WebText WaitForAWhileMessage;

        public SmsStatusForm(IPage parent)
            : base(parent, ".sending") {
        }

        public SmsStatusForm(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public void AssertStatusIsInProgress() {
            WaitForAWhileMessage.AssertIsVisible();
            UpdateLink.AssertIsVisible();
            Assert.AreEqual(SmsSendStatus.InProgress, GetStatus(), "������������ ������ �������� ���������");
        }

        private SmsSendStatus GetStatus() {
            switch (StatusText.Text) {
                case "������� ��������":
                    return SmsSendStatus.InProgress;
                case "����������":
                    return SmsSendStatus.Sended;
                default:
                    throw new ArgumentOutOfRangeException("status text");
            }
        }

        /// <summary>
        /// ������� ���� ������������ ��������� '����� ������� ���������'
        /// </summary>
        public void WaitWhileInProgres() {
            Browser.Wait.Until(() => !WaitForAWhileMessage.IsVisible(), 60);
        }

        public void AssertStatusIsSended() {
            WaitForAWhileMessage.AssertIsHidden();
            UpdateLink.AssertIsHidden();
            Assert.AreEqual(SmsSendStatus.Sended, GetStatus(), "������������ ������ �������� ���������");
        }

        public void AssertIsVisible(string phone) {
            AssertVisible();
            Phone.AssertIsVisible();
            Assert.AreEqual(phone, GetNormalizedPhone(), "������������ ����� ��������");
        }

        private object GetNormalizedPhone() {
            // ������ ������ � �������
            string phone = Phone.Text.Replace("-", string.Empty).Replace(" ", string.Empty);
            if (phone.Length < 2)
                return phone;
            // ������ +7
            return phone.Substring(2, phone.Length - 2);
        }
    }

    public enum SmsSendStatus {
        InProgress,
        Sended
    }
}
using System.Threading;
using NUnit.Framework;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common.ServiceWidgets {

    public class SendSmsWidget : WidgetBase, ISendSmsComponent {
        public SendSmsForm Component;

        public SendSmsWidget(IPage parent)
            : base(parent) {
            Component = ParentPage.RegisterComponent<SendSmsForm>("����� �������� Sms", RootScss);
        }

        public SendSmsWidget(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public BeelinePhoneNumber Phone {
            get { return Component.Phone; }
        }

        public SmsBodyTextArea SmsBody {
            get { return Component.SmsBody; }
        }

        public BeelineCaptcha Captcha {
            get { return Component.Captcha; }
        }

        public BeelineButton SendButton {
            get { return Component.SendButton; }
        }

        public SmsStatusForm SmsStatusForm {
            get { return Component.SmsStatusForm; }
        }
    }

    public class SendSmsForm : ContainerBase, ISendSmsComponent {
        public BeelinePhoneNumber Phone { get; private set; }
        public SmsBodyTextArea SmsBody { get; private set; }
        public BeelineCaptcha Captcha { get; private set; }
        public BeelineButton SendButton { get; private set; }
        public SmsStatusForm SmsStatusForm { get; private set; }

        public BeelineCheckbox TranslitCheckbox;

        public SendSmsForm(IPage parent, string rootScss)
            : base(parent, rootScss) {
            Phone = ParentPage.RegisterComponent<BeelinePhoneNumber>("����� ��������", InnerScss(".form-phone-box"));
            Captcha = ParentPage.RegisterComponent<BeelineCaptcha>("������", InnerScss("div.captcha"));
            SmsBody = ParentPage.RegisterComponent<SmsBodyTextArea>(
                "��������� ����� Sms", InnerScss(".form-line .textarea"));
            SendButton = ParentPage.RegisterComponent<BeelineButton>("��������� ���������", InnerScss(".ButtonSendSms"));
            SmsStatusForm = ParentPage.RegisterComponent<SmsStatusForm>("����� ������� �������� sms ���������");
            TranslitCheckbox = ParentPage.RegisterComponent<BeelineCheckbox>("������������� � ��������",
                                                                             InnerScss("[>[name='CheckboxTransliterate']]"));
        }

        public void AssertLefSymbolsCountIs(int count) {
            Assert.AreEqual(count,SmsBody.LeftCount.Value<int>(), "������������ ���������� ���������� ��������");
        }

        private int GetLeftSymbolsCount() {
            return Get.Int(InnerSelector(".count"));
        }
    }

    public class SmsBodyTextArea : ContainerBase {
        public readonly WebText LeftCount;
        public readonly BeelineTextArea TextArea;

        public SmsBodyTextArea(IPage parent, string rootScss)
            : base(parent, rootScss) {
            TextArea = ParentPage.RegisterComponent<BeelineTextArea>("����� Sms", rootScss);
            LeftCount = ParentPage.RegisterComponent<WebText>("���������� ���������� ��������", InnerSelector(".count"));
        }

        public void AssertIsInvalid() {
            Assert.True(Is.HasClass(RootSelector, "invalid"), "���� '{0}' �� ������������ ��� ����������", ComponentName);
        }

        public void TypeIn(string body) {
            TextArea.TypeIn(body);
            Browser.Action.ChangeFocus();
        }
    }

    public interface ISendSmsComponent {
        BeelinePhoneNumber Phone { get; }
        SmsBodyTextArea SmsBody { get; }
        BeelineCaptcha Captcha { get; }
        BeelineButton SendButton { get; }
        SmsStatusForm SmsStatusForm { get; }
    }
}
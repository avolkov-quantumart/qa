using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common.ServiceWidgets {
    /// <summary>
    /// ������
    /// </summary>
    public class PaymentWidget : WidgetBase {
        [WebComponent("div[>span['��������:']]", ComponentName = "��� �������")]
        public BeelineDropList Type;

        [WebComponent(".account>div.form-select", ComponentName = "��� ����� ������")]
        public BeelineDropList AccountCode;

        [SimpleWebComponent(Name = "ModemPhone", ComponentName = "����� ����� ������")]
        public WebInput AccountNumber;

        [SimpleWebComponent(Name = "Sum", ComponentName = "����� ������")]
        public WebInput Sum;

        public BeelineButton PayButton;

        public PaymentWidget(IPage parent) : base(parent) {
            PayButton = ParentPage.RegisterComponent<BeelineButton>("��������", InnerScss("span.ButtonSend"));
        }

        [WebComponent("#PhoneBlock", ComponentName = "����� ��������")]
        public BeelinePhoneNumber Phone;

        public override bool IsVisible() {
            return Type.IsVisible() && AccountCode.IsVisible() && AccountNumber.IsVisible() && Sum.IsVisible();
        }
    }
}
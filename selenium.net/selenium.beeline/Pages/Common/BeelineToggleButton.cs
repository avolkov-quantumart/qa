using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common {
    public class BeelineToggleButton : WebToggleButton {
        public BeelineToggleButton(IPage parent, string scssSelector, string activeClass)
            : base(parent, scssSelector, activeClass) {
        }

        public BeelineToggleButton(IPage parent, string scssSelector)
            : base(parent, scssSelector, "active") {
        }
    }
}
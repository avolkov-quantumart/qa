﻿using TechTalk.SpecFlow;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Common.Alerts {
    [Binding]
    public class ConnectHouseApplicationAlert : AlertBase {
        public ConnectHouseApplicationAlert(IPage parent)
            : base(parent) {
        }

        [WebComponent("span[>#Street]")]
        public BeelineInput Street;

        [WebComponent("span[>#Building]")]
        public BeelineInput House;

        [WebComponent("span[>#Flat]")]
        public BeelineInput Apartment;

        [WebComponent("span[>#LastName]")]
        public BeelineInput LastName;

        [WebComponent("span[>#FirstName]")]
        public BeelineInput FirstName;

        [WebComponent("span[>#PatronymicName]")]
        public BeelineInput PatronymicName;

        [WebComponent("span[>#Email]")]
        public BeelineInput Email;

        [WebComponent("#PhoneNumber","PhoneCode","PhoneNumber")]
        public BeelinePhoneNumber PhoneNumber;

        [WebComponent("span[>#connection-ticket-submit]")]
        public BeelineButton SendButton;

        [SimpleWebComponent(Scss = "#connection-ticket-form-wrapper>h1['Заявка отправлена']")]
        public WebText ApplicationWasSentMessage;

        public override bool IsVisible() {
            return Is.Visible("#connection-ticket-form-wrapper");
        }

        public override void Dismiss() {
            throw new System.NotImplementedException();
        }
    }
}

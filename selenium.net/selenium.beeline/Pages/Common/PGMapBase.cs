using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using core.Extensions;
using selenium.beeline.Pages.Customers.BeelineOnMap;
using selenium.core.Exceptions;
using selenium.core.Framework.Browser;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Common {
    public abstract class PGMapBase : ContainerBase {
        private string TILE_XPATH = ".PGmap-layer-container-layers>div>img[src~'{0}']";

        public PGMapBase(IPage parent, string rootScss = "div.PGmap")
            : base(parent, rootScss) {
        }

        protected virtual By PointSelector {
            get { return ScssBuilder.CreateBy("div[>.PGmap-geometry-point]"); }
        }

        protected List<string> GetTileUrls(string urlMarker = "") {
            return Get.ImgSrcs(string.Format(TILE_XPATH, urlMarker));
        }

        protected IWebElement GetTile(string tile) {
            tile = tile.Replace("http://", string.Empty);
            return Find.ElementFastS(string.Format(TILE_XPATH, tile));
        }

        public List<IWebElement> GetVisiblePoints() {
            return Find.VisibleElements(PointSelector);
        }

        public List<IWebElement> GetPoints() {
            return Find.VisibleElements(PointSelector);
        }

        /// <summary>
        /// �������� ������ ����� �� �����
        /// </summary>
        public List<T> GetPoints<T>() where T : IMapPoint {
            return GetPoints().Select(e => ParentPage.CreateComponent<T>(e)).ToList();
        }

        /// <summary>
        /// �������� ���������� ����� �� �����
        /// </summary>
        public int GetPointsCount<T>() where T : IMapPoint {
            return GetPoints<T>().Sum(p => p.Count);
        }

        /// <summary>
        /// �������� ������ �����, ����������� �� ��������� ������ �����
        /// </summary>
        protected List<T> GetPointsOnTile<T>(string tile) where T : IMapPoint {
            var pointsOnTile = new List<T>();
            IWebElement element = GetTile(tile);
            Rectangle tileBounds = Get.Bounds(element);
            List<IWebElement> points = GetVisiblePoints();
            foreach (IWebElement point in points) {
                Point pointCoord = Get.Point(point);
                if (tileBounds.Contains(pointCoord))
                    pointsOnTile.Add(ParentPage.CreateComponent<T>(point));
            }
            return pointsOnTile;
        }

        public void ClickByPointOnTile(string tile) {
            OfficeMapPoint randomOffice = GetPointsOnTile<OfficeMapPoint>(tile).RandomItem();
            randomOffice.ClickAndWaitWhileAjax();
        }

        public T GetBalloon<T>() where T : IComponent {
            return ParentPage.CreateComponent<T>(InnerScss("[.b-balloon__content]"));
        }

        public void AssertTileContainsPoint(string tile) {
            List<OfficeMapPoint> offices = GetPointsOnTile<OfficeMapPoint>(tile);
            Assert.IsNotEmpty(offices, "������ '{0}' �� �������� �� ����� �����", tile);
        }

        public void AssertHasTile(string tileUrl) {
            Assert.IsNotNull(GetTile(tileUrl), "�� ����� �� ������������ ������ '{0}'", tileUrl);
        }

        /// <summary>
        /// �������� �� ������������ ����� �������
        /// </summary>
        public void ClickByRandomPoint<T>() where T : IMapPoint {
            do {
                List<T> wifiPoints = GetPoints<T>();
                if (wifiPoints.Count == 0)
                    throw Throw.FrameworkException("�� ����� ����������� ����� wifi �������");
                T point = wifiPoints.FirstOrDefault(p => p.IsLeaf);
                if (point != null) {
                    point.OpenBalloon();
                    break;
                }
                // ���������� ����� ������� �� ������ � ����������� ������� �������
                T largePoint = wifiPoints.First(p => p.Count != 1);
                largePoint.OpenBalloon();
            } while (true);
        }
    }
}
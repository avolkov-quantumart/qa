using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using core.Extensions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Base.Tariffs {
    public abstract class CardsBase<T, TG> : ListBase<T> where T : ICard, IComponent {
        protected CardsBase(IPage parent)
            : this(parent, ".base-tariffs") {
        }

        protected CardsBase(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public virtual string GroupIdScss {
            get { return InnerScss("div.tariffs-group div.group-header>h3"); }
        }

        public override string ItemIdScss {
            get { return InnerScss("div.card h3>strong>a"); }
        }

        public virtual List<string> GetGroupIds() {
            return Get.Texts(GroupIdScss);
        }

        /// <summary>
        /// �������� ������ �������� �� ��������
        /// </summary>
        public List<ICard> GetCards(Func<ICard, bool> filter = null) {
            IEnumerable<ICard> items = base.GetItems().Cast<ICard>();
            List<ICard> groups =
                GetGroupIds().Select(id => Activator.CreateInstance(typeof (TG), this, id)).Cast<ICard>().ToList();
            List<ICard> allCards = items.Concat(groups).ToList();
            return filter == null ? allCards : allCards.Where(filter).ToList();
        }
        
        /// <summary>
        /// ������� � �������� ������������� ������ �� ������
        /// </summary>
        public ITariff GoToRandomTariffDetails() {
            return GoToRandomTariffDetails(new List<string>());
        }

        public ITariff GoToRandomTariffDetails(List<string> excluded) {
            ITariff randomTariff = GetRandomTariff(excluded);
            randomTariff.ClickByDetailsButton();
            Thread.Sleep(1000);
            return randomTariff;
        }

        private ITariff GetRandomTariff(List<string> excluded) {
            ICard randomCard = GetCards().RandomItem(card => excluded.Contains(card.ID));
            ITariff randomTariff;
            if (randomCard is ITariff)
                randomTariff = randomCard as ITariff;
            else
                randomTariff = (randomCard as IWebList).GetItems<ITariff>().RandomItem();
            return randomTariff;
        }
    }
}
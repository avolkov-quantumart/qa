using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Base.Tariffs
{
    public interface ITariff:IItem {
        void ClickByDetailsButton();
        string Name { get; }
    }
}

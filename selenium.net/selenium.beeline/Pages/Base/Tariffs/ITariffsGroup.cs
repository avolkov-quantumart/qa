using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Base.Tariffs {
    public interface ITariffsGroup<T> : IWebList<T> where T : IItem {
    }
}
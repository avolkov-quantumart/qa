using System;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Base.Tariffs {
    public abstract class GroupTariff:ItemBase {
        public GroupTariff(IContainer container, string id) : base(container, id) {
        }

        public override string ItemScss {
            get { return ContainerInnerScss(""); }
        }
    }
}
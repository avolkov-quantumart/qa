using System;
using System.Drawing;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using core.Extensions;
using selenium.beeline.Pages.Products.Home.Tariffs.Internet;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Base.Tariffs {
    public abstract class GroupTariffBase : ItemBase, IGroupTariff {
        private readonly By _submitButtonSelector;

        public ITariffsGroup TariffsGroup {
            get { return (ITariffsGroup) Container; }
        }

        public GroupTariffBase(ITariffsGroup container, string id)
            : base(container, id) {
            _submitButtonSelector = InnerSelector(".submit>.button");
        }

        /// <summary>
        /// ���� �� ������ "���������"
        /// </summary>
        public void ClickByDetailsButton() {
            Log.Action("���� �� ������ '���������' ������� '{0}'", ID);
            TariffsGroup.ScrollToLeft();
            try {
                Browser.WithOptions(() => Action.ClickAndWaitForRedirect(_submitButtonSelector,ajaxInevitable:true), findSingle: false);
            }
            catch (InvalidOperationException e) {
                Log.Info(e.Message);
            }
            if (!State.PageIs<TariffPageBase>()) {
                TariffsGroup.ScrollToRight();
                Browser.WithOptions(() => Action.ClickAndWaitForRedirect(_submitButtonSelector,ajaxInevitable:true), findSingle: false);
            }
        }

        public string Name {
            get { return ID; }
        }

        public Rectangle GetBounds() {
            return Get.Bounds(RootScss);
        }
        public override string ItemScss {
            get { return string.Format("li[h5>strong>a['{0}']]", ID); }
        }
    }

    public interface IGroupTariff : ITariff {
        Rectangle GetBounds();
    }
}
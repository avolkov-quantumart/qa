using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Base.Tariffs {
    public abstract class CardBase:ItemBase,ICard {
        protected CardBase(IContainer container, string id) : base(container, id) {
        }

        public override string ItemScss {
            get { return ContainerInnerScss("div.card[h3>strong>a['{0}']]", ID); }
        }

        public override bool IsVisible() {
            throw new System.NotImplementedException();
        }
    }
}
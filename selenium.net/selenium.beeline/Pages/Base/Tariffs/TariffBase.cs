using OpenQA.Selenium;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Base.Tariffs {
    public class TariffBase : CardBase, ITariff {
        private readonly By _submitButtonSelector;

        public TariffBase(IContainer container, string id)
            : base(container, id) {
                _submitButtonSelector = InnerSelector(".submit .btn-link");
        }

        /// <summary>
        /// ���� �� ������ "���������"
        /// </summary>
        public void ClickByDetailsButton() {
            Log.Action("���� �� ������ '���������'");
            Action.ClickAndWaitForRedirect(_submitButtonSelector,ajaxInevitable:true);
        }

        public string Name {
            get { return ID; }
        }
    }
}
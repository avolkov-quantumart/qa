using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Base.Tariffs {
    public class TariffsGroup<T> : CardBase,ITariffsGroup, IWebList<T> where T : ITariff {
        public WebText ScrollBar;

        protected TariffsGroup(IContainer container, string id) : base(container, id) {
            ScrollBar = ParentPage.RegisterComponent<WebText>("������ ��� ������ �������",
                                                              InnerSelector("*[@class='mCSB_dragger']"));
        }

        #region IWebList<T> Members

        public virtual string ItemIdScss {
            get { return InnerScss("ul>li>h5>strong>a"); }
        }

        public virtual List<string> GetIds() {
            return Get.Texts(ItemIdScss, true);
        }

        public virtual List<T> GetItems() {
            //return GetIds().Select(id => Activator.CreateInstance(typeof(T), this, id)).Cast<T>().ToList();
            return WebPageBuilder.CreateItems<T>(this, GetIds());
        }

        #endregion

        public override string ItemScss {
            get { return ContainerInnerScss("div.tariffs-group[div.group-header>h3['{0}']]", ID); }
        }

        public override bool IsVisible() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// �������� ����� ��� �������� �� ������������� ����������
        /// </summary>
        public ITariff GetAnotherTariff(string tariffName) {
            return GetItems().FirstOrDefault(t => !tariffName.Equals(t.ID));
        }

        public List<T1> GetItems<T1>() {
            return GetItems().Cast<T1>().ToList();
        }

        public void ScrollToLeft() {
            if (ScrollBar.IsVisible()) {
                ScrollBar.DragByHorizontal(-1000);
                Thread.Sleep(500); // ����� ��������� �.�. ��������� ������� � ����������� �� �����
            }
        }

        public void ScrollToRight() {
            if (ScrollBar.IsVisible()) {
                ScrollBar.DragByHorizontal(1000);
                Thread.Sleep(500); // ����� ��������� �.�. ��������� ������� � ����������� �� �����
            }
        }
    }

    public interface ITariffsGroup : IContainer {
        void ScrollToLeft();
        void ScrollToRight();
    }
}
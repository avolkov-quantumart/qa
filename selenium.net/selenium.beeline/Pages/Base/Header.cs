using System;
using OpenQA.Selenium;
using selenium.beeline.Pages.Common;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Base {
    public class Header : ComponentBase {
        [SimpleWebComponent(Css = ".header-feedback-link a",ComponentName = "������ ������")]
        public WebLink AskQuestionLink;

        [WebComponent(ComponentName="�������")]
        public HeaderBasket Basket;

        [SimpleWebComponent(ComponentName = "������� ������", Css = ".region .region-select-link")]
        public WebText CurrentRegion;

        [WebComponent(ComponentName="����� �����")]
        public LanguageSelector LanguageSelector;

        [WebComponent(ComponentName = "�������� ������� �����")]
        public RegionSelector RegionSelector;

        [WebComponent(".head-help.chat",ComponentName = "������ ������")]
        public SupportWidget Support;

        public Header(IPage parent)
            : base(parent) {
        }

        public override bool IsVisible() {
            throw new NotImplementedException();
        }

        public SupportWidget OpenSupportWidget() {
            return (SupportWidget) Support.Open(() => AskQuestionLink.ClickAndWaitWhileAjax(ajaxInevitable: true));
        }

        /// <summary>
        /// ������� ��������� ����� �����
        /// </summary>
        public LanguageSelector OpenLanguageSelector() {
            if (!LanguageSelector.IsVisible())
                Action.Click(By.ClassName("lang"));
            return LanguageSelector;
        }

        /// <summary>
        /// ���������� ������ ����� ������������� ������
        /// </summary>
        public SiteLanguage GetIconLanguage() {
            string imgFile = Get.ImgFileName(By.CssSelector(".lang img"));
            switch (imgFile) {
                case "header-lang-ru.png":
                    return SiteLanguage.Russian;
                case "header-lang-eng.png":
                    return SiteLanguage.English;
                default:
                    throw new ArgumentOutOfRangeException("unknown file name");
            }
        }

        /// <summary>
        /// ���������� ������ ����� ������������� CultureCode ��������
        /// </summary>
        public SiteLanguage GetCultureLanguage() {
            string text = Get.Text(By.ClassName("culture-select-link"));
            switch (text.ToLower()) {
                case "ru":
                    return SiteLanguage.Russian;
                case "en":
                    return SiteLanguage.English;
                default:
                    throw new ArgumentOutOfRangeException("unknown culture code");
            }
        }

        /// <summary>
        /// ������� ��������� ������ �������
        /// </summary>
        public RegionSelector OpenRegionsSelector() {
            if (!RegionSelector.IsVisible())
                CurrentRegion.Click();
            if (!RegionSelector.IsVisible())
                throw Throw.FrameworkException("�� ����������� ��������� ������ �������");
            return RegionSelector;
        }
    }
}
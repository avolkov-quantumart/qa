using System;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Base {
    public class Navigation : ContainerBase,IOverlay {
        [WebComponent("root:>.search-bar",ComponentName = "���������� �����")]
        public GlobalSearch Search;
        public Navigation(IPage parent)
            : base(parent, ".nav-section") {
        }

        public override bool IsVisible() {
            throw new System.NotImplementedException();
        }

        public bool IsMatch(Exception e) {
            throw new NotImplementedException();
        }

        public void Close() {
            (ParentPage as BeelinePageBase).Header.AskQuestionLink.MouseOver();
        }
    }
}
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Base {
    public class GlobalSearch : BeelineInputWithList {
        public GlobalSearch(IPage parent, string rootScss)
            : base(parent, rootScss,"li/span/strong/em") {
        }

        public void SearchFor(string query) {
            TypeInAndSelectFirst(query);
            Browser.State.Actualize();
        }
    }
}
using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Base {
    public class WebTabs : ContainerBase {
        public string ItemScss { get; private set; }
        public string SelectedClass { get; private set; }

        public WebTabs(IPage parent)
            : base(parent) {
        }

        public WebTabs(IPage parent, string rootScss, string itemScss, string selectedClass)
            : base(parent, rootScss) {
            ItemScss = itemScss;
            SelectedClass = selectedClass;
        }

        public void Select(string tabName) {
            if (!IsActive(tabName))
                Action.Click(TabSelector(tabName));
        }

        public bool IsActive(string tabName) {
            return Is.HasClass(TabSelector(tabName), SelectedClass);
        }

        private By TabSelector(string tabName) {
            return ScssBuilder.CreateBy(ItemScss, tabName);
        }

        public void SelectAndWaitWhileAjaxRequests(string tabName, int sleepTimeout = 0, bool ajaxInevitable = false) {
            if (!IsActive(tabName))
                Action.ClickAndWaitWhileAjax(TabSelector(tabName), sleepTimeout, ajaxInevitable);
        }
    }
}
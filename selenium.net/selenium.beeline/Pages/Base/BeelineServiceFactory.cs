﻿/**
* Created by VolkovA on 28.02.14.
*/

using System.Collections.Generic;
using selenium.beeline.Pages.Customers.press;
using selenium.core.Framework.Service;

namespace selenium.beeline.Pages.Base {
    public class BeelineServiceFactory : ServiceFactory {
        private const string DEV_DOMAIN = "dr-bee.ru";
        private const string PROD_DOMAIN = "beeline.ru";

        #region ServiceFactory Members

        public Router createRouter() {
            var router = new SelfMatchingPagesRouter();
            router.RegisterDerivedPages<BeelinePageBase>();
            router.RegisterEmailPage<SubscriptionActivationEmail>();
            return router;
        }

        public BaseUrlPattern createBaseUrlPattern() {
            var urlRegexBuilder = new BaseUrlRegexBuilder(new List<string> {DEV_DOMAIN, PROD_DOMAIN});
            urlRegexBuilder.SetAbsolutePathPattern("(\\w{2}-\\w{2}/)?");
            return new BaseUrlPattern(urlRegexBuilder.Build());
        }

        public BaseUrlInfo getDefaultBaseUrlInfo() {
            return new BaseUrlInfo("moskva", DEV_DOMAIN, "/");
        }

        public Service createService() {
            return new BeelineService(getDefaultBaseUrlInfo(), createBaseUrlPattern(), createRouter());
        }

        #endregion
    }
}
using System;
using OpenQA.Selenium;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Base {
    public class BreadCrumbs : ComponentBase {
        private readonly By _prevStepSelector;

        public BreadCrumbs(IPage parent) : base(parent) {
            _prevStepSelector = By.CssSelector(".crumbs li:nth-last-child(2)");
        }

        public override bool IsVisible() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ������� �� ������� ����
        /// </summary>
        public void StepBack() {
            Log.Action("������� �� ���� ��� ����� �� '������� �������'");
            Action.ClickAndWaitForRedirect(_prevStepSelector);
        }
    }
}
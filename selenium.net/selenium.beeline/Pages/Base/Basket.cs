using NUnit.Framework;
using OpenQA.Selenium;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Base {
    public class Basket : ComponentBase {
        public Basket(IPage parent) : base(parent) {
        }

        public override string ComponentName {
            get { return "�������"; }
        }

        public override bool IsVisible() {
            throw new System.NotImplementedException();
        }

        public string TariffName {
            get { return Get.Text("div#main-basket-info-div>ul>li>a"); }
        }

        /// <summary>
        /// ��������� ��� ��� ������� � ������ ������������� ����������
        /// </summary>
        public void AssertTariffIs(string tariffName, string msg) {
            Assert.AreEqual(tariffName, TariffName, msg);
        }
    }
}
﻿using selenium.core.Framework.Service;

namespace selenium.beeline.Pages.Base
{
    internal class BeelineService : ServiceImpl {
        public BeelineService(BaseUrlInfo defaultBaseUrlInfo, BaseUrlPattern pattern, Router router)
            : base(defaultBaseUrlInfo, pattern, router) {
        }
    }
}

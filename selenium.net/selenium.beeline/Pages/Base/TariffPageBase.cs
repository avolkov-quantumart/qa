using System.Text.RegularExpressions;
using NUnit.Framework;
using OpenQA.Selenium;

namespace selenium.beeline.Pages.Base {
    public abstract class TariffPageBase : BeelinePageBase {
        private const string TARIFF_URL_NAME = "tariff_url";

        public string TariffUrlName {
            get { return Data[TARIFF_URL_NAME]; }
        }

        public string GetTariffName() {
            string text = Get.Text(By.CssSelector(".tariff-description h1"));
            return new Regex("�(?<name>[^�]*)�").Match(text).Groups["name"].Value;
        }

//        public override UriMatchResult Match(RequestData requestData, BaseUrlInfo baseUrlInfo) {
//            string realPath = requestData.Url.AbsolutePath;
//            realPath = realPath.Substring(baseUrlInfo.AbsolutePath.Length).CutLast('/');
//            if (realPath.StartsWith(AbsolutePath)) {
//                string tail = realPath.Substring(AbsolutePath.Length);
//                string[] folders = tail.Split('/');
//                if (folders.Length == 1) {
//                    var data = new Dictionary<string, string> {{TARIFF_URL_NAME, folders[0]}};
//                    return new UriMatchResult(true, data);
//                }
//            }
//            return UriMatchResult.Unmatched();
//        }

        public void AssertTariffNameIs(string expected) {
            Assert.AreEqual(expected, GetTariffName(), "�������� ������ �� ������������� ����������");
        }
    }
}
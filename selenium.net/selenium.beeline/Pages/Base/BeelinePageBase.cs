using NUnit.Framework;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Base {
    public abstract class BeelinePageBase : SelfMatchingPageBase {
        [WebComponent(ComponentName="������� ������")]
        public BreadCrumbs BreadCrumbs;

        [WebComponent(ComponentName="�����")]
        public Footer Footer;

        [WebComponent(ComponentName="�����")]
        public Header Header;

        [WebComponent(ComponentName="������ ���������")]
        public Navigation Navigation;

        [WebComponent(ComponentName="������ � ���������")]
        public BeelineServicesWidget Services;

        /// <summary>
        /// ��������� ���� ��������
        /// </summary>
        public void AssertLanguage(SiteLanguage language) {
            Assert.AreEqual(language, Header.GetIconLanguage(), "������ �� ������������� �������� ����� ��������");
            Assert.AreEqual(language, Header.GetCultureLanguage(),
                            "Culture Code �� ������������� �������� ����� ��������");
        }

        /// <summary>
        /// ��������� ������ �����
        /// </summary>
        public void AssertRegion(string region) {
            Assert.AreEqual(region, Header.CurrentRegion.Text,
                            "������ �����, ������������ �� �������� �� ������������� ����������");
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;
using OpenQA.Selenium;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Help.Mobile {
    public class InternetInstructionsPage : BeelinePageBase {
        private const string DEFAULT_PHONE_ICON = "mobile-instructions.png";

        [WebComponent]
        public AutomaticInstruction AutomaticInstruction;

        [WebComponent("#widgetarea .button", ComponentName = "����� ����������")]
        public BeelineButton FindInstructionButton;

        [WebComponent]
        public ManualInstruction ManualInstruction;

        [WebComponent("div.mobile-model>div.input", ComponentName = "������ ��������")]
        public BeelineInputWithList PhoneModel;

        public override string AbsolutePath {
            get { return "customers/help/mobile/mobilnyy-internet/nastroyka-interneta-na-mobilnom-telefone/"; }
        }

        /// <summary>
        /// ����� ���������� ��� ��������� ������ ��������
        /// </summary>
        public void FindInstruction(string phoneModel) {
            PhoneModel.TypeInAndSelectFirst(phoneModel);
            FindInstructionButton.WaitForEnabled();
            FindInstructionButton.ClickAndWaitWhileAjax();
        }

        /// <summary>
        /// ��������� ��� ������������ ��������� ����������� ��������
        /// </summary>
        public void AssertIconFileIs(string iconFile) {
            Assert.AreEqual(iconFile, GetPhoneIcon());
        }

        /// <summary>
        /// �������� ��� ����� � ������������ ��������
        /// </summary>
        public string GetPhoneIcon() {
            return Get.ImgFileName(By.CssSelector(".instructions-scr>img"));
        }

        /// <summary>
        /// ����� ������� � ��������������� �����������
        /// </summary>
        public AutomaticInstruction FindAutomaticInstruction() {
            FindInstruction("Motorola C380");
            if (!AutomaticInstruction.IsVisible())
                throw Throw.FrameworkException("�� ������������ ������ �� �������������� ���������");
            return AutomaticInstruction;
        }

        /// <summary>
        /// ���������, ��� ������ "����� ����������" �������
        /// </summary>
        public void AssertFindInstructionButtonEnabled() {
            Assert.IsTrue(FindInstructionButton.IsEnabled(), "������ '����� ����������' ���������");
        }

        /// <summary>
        /// ����� ���������� �� ������ ��������� ��������
        /// </summary>
        public ManualInstruction FindManualInstruction() {
            FindInstruction("Apple iPad");
            if (!ManualInstruction.IsVisible())
                throw Throw.FrameworkException("�� ������������ ���������� �� ������ ���������");
            return ManualInstruction;
        }
    }

    public abstract class InstructionBase : ContainerBase{
        private readonly By _titleSelector;

        protected InstructionBase(IPage parent)
            : base(parent) {
            _titleSelector = By.CssSelector("#instructionblock h3 strong");
        }

        /// <summary>
        /// �������� ������ �������� �� ��������� ����������
        /// </summary>
        public string GetPhoneModelFromTitle() {
            string text = Get.Text(_titleSelector);
            return new Regex("���������� � ����������� ��� (?<model>.*)").Match(text).Groups["model"].Value.Trim();
        }
    }

    public class ManualInstruction : InstructionBase, IWebList<InstructionStep>
    {
        private readonly By _stepSelector;

        public ManualInstruction(IPage parent)
            : base(parent) {
            _stepSelector = By.ClassName("instruction-step");
        }

        #region IWebList Members

        public string ItemIdScss {
            get { return "div.instruction-step>h4"; }
        }

        public List<string> GetIds() {
            return Get.Texts(ItemIdScss);
        }

        public List<InstructionStep> GetItems() {
            return GetIds().Select(id => new InstructionStep(this, id)).ToList();
        }

        #endregion

        public override bool IsVisible() {
            return Is.Visible(_stepSelector);
        }

        public void AssertName(string phoneModel) {
            Assert.AreEqual(phoneModel, GetPhoneModelFromTitle(),
                            "� ������ ��������� ���������� ������������ ������������ ������ ��������");
        }

        /// <summary>
        /// ��������� ��� ���������� �� ����� - �������� ������ �����, ������ ��� �������� ��������
        /// </summary>
        public void AssertContainsSteps() {
            List<InstructionStep> items = GetItems();
            Assert.IsFalse(items.Count == 0, "���������� �� �������� �����");
            foreach (var item in items)
                item.AssertNotEmpty();
        }

        public List<T> GetItems<T>() {
            return GetItems().Cast<T>().ToList();
        }
    }

    public class InstructionStep : ItemBase {
        public InstructionStep(IContainer container, string id)
            : base(container, id) {
        }

        public override string ItemScss {
            get { return ContainerInnerScss("div[contains(@class,'instruction-step')][h4[text()='{0}']]", ID); }
        }

        public override bool IsVisible() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ��������� ��� ��� ���������� �������� ��������
        /// </summary>
        public void AssertNotEmpty() {
            string content = GetContent().Replace("\r", string.Empty).Replace("\n", string.Empty);
            Assert.IsFalse(string.IsNullOrWhiteSpace(content), "{0} �� �������� ��������", ID);
        }

        /// <summary>
        /// �������� ���������� ���� ����������
        /// </summary>
        private string GetContent() {
            return Get.Text(Scss.Concat(ItemScss,"div"));
        }
    }

    public class AutomaticInstruction : InstructionBase {
        private readonly By _sendSmsLinkSelector;
        private readonly By _titleSelector;
        [WebComponent]
        public SendSmsSettingsAlert SendSmsSettingsAlert;

        public AutomaticInstruction(IPage parent)
            : base(parent) {
            _sendSmsLinkSelector = By.Id("getSmsLink");
            _titleSelector = By.CssSelector("#instructionblock h3 strong");
        }

        public override bool IsVisible() {
            return Is.Visible(_sendSmsLinkSelector);
        }

        /// <summary>
        /// ���������, ��� � ���������� ������������ ���������� ������ ��������
        /// </summary>
        public void AssertName(string phoneModel) {
            Assert.AreEqual(phoneModel, GetPhoneModelFromTitle(),
                            "� ������ ��������� ���������� ������������ ������������ ������ ��������");
            Assert.AreEqual(phoneModel, GetPhoneModelFromLink(),
                            "� ������ ������ �� ��������� �������� ������������ ������������ ������ ��������");
        }

        /// <summary>
        /// �������� ������ �������� �� ������ �� ��������� ��������
        /// </summary>
        public string GetPhoneModelFromLink() {
            string text = Get.Text(_sendSmsLinkSelector);
            return new Regex("��������� �������������� ��������� �� SMS ��� (?<model>.*)").Match(text).Groups["model"].
                Value.Trim();
        }

        /// <summary>
        /// ������� ����� �������� Sms � �����������
        /// </summary>
        public SendSmsSettingsAlert OpenSendSmsSettingsAlert() {
            if (!State.HtmlAlertIs<SendSmsSettingsAlert>()) {
                ClickBySendSmsLink();
                if (!State.HtmlAlertIs<SendSmsSettingsAlert>())
                    throw Throw.FrameworkException("�� ������������ ���� �������� ��������");
            }
            return State.HtmlAlertAs<SendSmsSettingsAlert>();
        }

        /// <summary>
        /// ���� �� ������ �� ��������� ��������
        /// </summary>
        private void ClickBySendSmsLink() {
            Log.Action("���� �� ������ �� ��������� �������� � ����������");
            Action.ClickAndWaitForAlert(_sendSmsLinkSelector);
        }
    }

    public class SendSmsSettingsAlert : AlertBase {
        [WebComponent("#PopupContentContainer .button", ComponentName = "��������")]
        public BeelineButton SendButton;

        public SendSmsSettingsAlert(IPage parent)
            : base(parent) {
        }

        public override bool IsVisible() {
            return Is.Visible("['SMS � �����������']");
        }

        public override void Dismiss() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ��������� ��� � ������ ������������ ���������� ������ ��������
        /// </summary>
        public void AssertModelPhone(string modelPhone) {
            Assert.AreEqual(modelPhone, GetPhoneModelFromHeader(),
                            "� ��������� ���� �������� �������� ����� Sms ������������ ������������ ������ ��������");
            Assert.AreEqual(modelPhone, GetPhoneModelFromHint(),
                            "� ������ ���� �������� �������� ����� Sms ������������ ������������ ������ ��������");
        }

        /// <summary>
        /// �������� ������ �������� �� ���������
        /// </summary>
        private string GetPhoneModelFromHeader() {
            return Get.Text(By.CssSelector(".change-popup>fieldset>h3>strong>span")).Trim();
        }

        /// <summary>
        /// �������� ������ �������� �� ���������
        /// </summary>
        private string GetPhoneModelFromHint() {
            string text = Get.Text(By.CssSelector("#PopupContentContainer .form-line em span")).Trim();
            return new Regex("�� ��������� ����� �������� ����� ���������� SMS � ����������� " +
                             "�������� ��� (?<model>.*)").Match(text).Groups["model"].Value.Trim();
        }

        /// <summary>
        /// ���������, ��� ������ "��������" ���������
        /// </summary>
        public void AssertSendButtonDisabled() {
            Assert.IsFalse(SendButton.IsEnabled(), "������ '��������' �������");
        }
    }
}
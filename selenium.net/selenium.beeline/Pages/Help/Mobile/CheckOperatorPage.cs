using NUnit.Framework;
using OpenQA.Selenium;
using core;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Help.Mobile {
    public class CheckOperatorPage : BeelinePageBase {
        [SimpleWebComponent(ComponentName = "������", Css = ".captcha .input input")]
        public WebInput Captcha;

        [SimpleWebComponent(ComponentName = "Captcha image", ID = "captcha-img")]
        public WebImage CaptchaImage;

        [SimpleWebComponent(ComponentName = "Validation hint for captcha", Css = "div[validation-for='Captcha']")]
        public CaptchaValidationHint CaptchaValidationHint;

        [WebComponent("#CheckOperatorForm .ButtonCheck",
            ComponentName = "��������")]
        public BeelineButton CheckButton;

        [SimpleWebComponent(ComponentName = "���������� �� ���������", Css = ".special.operator h4")]
        public OperatorMessage OperatorMessage;

        [WebComponent("#CheckOperatorForm .form-phone-box",
            "div[>span>input[name='PhoneCode']]", "div[>input[name='Phone']]",
            "div.form-tip")]
        public BeelinePhoneNumber PhoneNumber;

        [SimpleWebComponent(ComponentName = "�������� ��������", ClassName = "captcha-refresh")]
        public WebLink UpdateCaptchaLink;

        public override string AbsolutePath {
            get { return "customers/help/mobile/upravlenie-uslugami/mnp-check/"; }
        }

        public void AssertRegionIs(string value) {
            Assert.AreEqual(value, OperatorMessage.GetRegion(),
                            "������ �� ������������� ���������� � ������������ ��� ������� ������");
        }

        public void AssertOperatorIs(string value) {
            Assert.AreEqual(value, OperatorMessage.GetOperator(),
                            "�������� �� ������������� ���������� � ������������ ��� ������� ������.");
        }

        /// <summary>
        /// ��������� ��� ���� � �����, ������� � ������� �����
        /// </summary>
        public void AssertAllFieldsAreEmpty() {
            PhoneNumber.Code.AssertIsEmpty();
            PhoneNumber.Phone.AssertIsEmpty();
            AssertCaptchaIsEmpty();
        }

        /// <summary>
        /// ��������� ��� ���� � ������� ������
        /// </summary>
        private void AssertCaptchaIsEmpty() {
            Captcha.AssertIsEmpty();
        }

        /// <summary>
        /// ��������� ���� ������������� �������
        /// </summary>
        public void RandomFill() {
            PhoneNumber.RandomFill();
            Captcha.TypeInRandomNumber(5);
        }
    }

    public class PhoneValidationHint : WebText {
        public PhoneValidationHint(IPage parent, By @by) : base(parent, @by) {
        }

        public void AssertPhoneIsEmptyVisible() {
            AssertIsVisible();
            Assert.AreEqual("�� ������ �������", Text);
        }

        public void AssertNotBeelineErrorVisible() {
            AssertIsVisible();
            Assert.AreEqual("���������� ������ ����� �������� ��������", Text);
        }
    }

    public class CaptchaValidationHint : WebText {
        public CaptchaValidationHint(IPage parent, By by) : base(parent, by) {
        }

        public void AssertCaptchaIsEmptyVisible() {
            Assert.AreEqual("������� �� ���������", Text);
        }

        public void AssertCaptchaMustHave5SymbolsVisible() {
            Assert.AreEqual("������� ������ ��������� 5 ����", Text);
        }
    }

    public class OperatorMessage : SimpleWebComponent {
        public OperatorMessage(IPage parent, By @by) : base(parent, @by) {
        }

        public string GetText() {
            return Get.Text(By);
        }

        public string GetOperator() {
            return RegexHelper.GetString(GetText(), "�������� �(?<operator>[^�].*)�");
        }

        public string GetRegion() {
            return RegexHelper.GetString(GetText(), "�������� �(?:[^�].*)�, (?<region>.*)");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers {
    public class SearchPage : BeelinePageBase {
        [WebComponent("span[#SearchInAllRegions]",ComponentName = "Поиск по всем регионам")]
        public BeelineCheckbox AllRegionsCheckbox;

        [WebComponent(".content-wrap span['Бизнесу']", "highlight",ComponentName = "Бизнесу")]
        public BeelineToggleButton BuisnessToggle;

        [WebComponent(".content-wrap span['Частным лицам']", "highlight",ComponentName = "Частным лицам")]
        public BeelineToggleButton ClientsPartitionToggle;

        [WebComponent(".content-wrap span['Партнерам']", "highlight",ComponentName = "Партнерам")]
        public BeelineToggleButton PartnersPartitionToggle;

        [WebComponent(".content-block div[.search-bar-wrap]",
            "li>span>strong>em",ComponentName = "Поиск")]
        public BeelineInputWithList Search;

        [WebComponent]
        public SearchCategories Categories;

        [WebComponent]
        public SearchResults SearchResults;

        public override string AbsolutePath {
            get { return "customers/search/"; }
        }

        public void SearchFor(string query) {
            Search.TypeInAndSelectFirst(query);
        }
    }

    public class SearchComponent : ContainerBase {
        public SearchComponent(IPage parent) : base(parent) {
        }

        public SearchComponent(IPage parent, string rootScss) : base(parent, rootScss) {
        }

        public void Search(string query) {
            throw new NotImplementedException();
        }
    }

    public class SearchResults : ListBase<SearchResult> {
        public SearchResults(IPage parent)
            : base(parent, ".search-results") {
        }

        public override string ItemIdScss {
            get { return InnerScss("h4>strong>a"); }
        }

        public override List<string> GetIds() {
            return Get.Hrefs(ItemIdScss);
        }
    }

    public class SearchResult : ItemBase {
        private const string BUISNESS_SUBDOMAIN = "ms-nwbdvlp001 b2b";
        private const string PARTNERS_SUBDOMAIN = "partners";

        public SearchResult(IContainer container, string id)
            : base(container, id) {
        }

        public override string ItemScss {
            get { return ContainerInnerScss("child::div[h4/strong/a[@href='{0}']]", ID); }
        }

        /// <summary>
        /// Получить префикс региона из ссылки на страницу результата
        /// </summary>
        public string GetRegion() {
            return GetLinkSubdomain();
        }

        public bool ContainsPhrase(string phrase) {
            return GetResultContent().Contains(phrase);
        }

        /// <summary>
        /// Получить весь контент карточки результата
        /// </summary>
        private string GetResultContent() {
            return Get.Text(InnerSelector("h4>strong>a")) + Environment.NewLine +
                   Get.Text(InnerSelector("p"));
        }

        public bool ContainsAllPhraseWords(string phrase) {
            string content = GetResultContent();
            string[] words = phrase.Split(' ');
            return words.All(t => content.Contains(t.Trim()));
        }

        public bool ContainsSomePhraseWords(string phrase) {
            string content = GetResultContent();
            string[] words = phrase.Split(' ');
            return words.Any(t => content.Contains(t.Trim()));
        }

        public List<string> GetTags() {
            return Get.Texts(InnerSelector("ul.tags>li>strong"));
        }

        public void AssertOnClientSite() {
            string linkSubdomain = GetLinkSubdomain();
            Assert.AreNotEqual(BUISNESS_SUBDOMAIN, linkSubdomain);
            Assert.AreNotEqual(PARTNERS_SUBDOMAIN, linkSubdomain);
        }

        private string GetLinkSubdomain() {
            string host = new Uri(ID).Host;
            return host.Substring(0, host.IndexOf('.'));
        }

        public void AssertOnBuisnessSite() {
            Assert.True(BUISNESS_SUBDOMAIN.Contains(GetLinkSubdomain()));
        }

        public void AssertOnPartnersSite() {
            Assert.AreEqual(PARTNERS_SUBDOMAIN, GetLinkSubdomain());
        }
    }

    public class SearchCategories : BeelineTabs {
        public SearchCategories(IPage parent)
            : base(parent, ".filter") {
        }

        public virtual string Selected {
            get { return Get.Text(InnerSelector("li.active>h5>span")); }
        }

        public override string GetTabScss(string tabName) {
            return InnerScss("li[h5>span['{0}']]", tabName);
        }

        public override string TabNameScss {
            get { return InnerScss("li>h5>span"); }
        }

        public void AssertEachCategoryHasResults() {
            List<string> tabNames = GetTabNames();
            foreach (string tabName in tabNames) {
                int resultsCount = GetTabResultsCount(tabName);
                Assert.Greater(resultsCount, 0, "Вкладка '{0}' не содержит результатов поиска", resultsCount);
            }
        }

        public int GetTabResultsCount(string tabName) {
            return Get.Int(GetTabScss(tabName) + "/h5");
        }
    }
}
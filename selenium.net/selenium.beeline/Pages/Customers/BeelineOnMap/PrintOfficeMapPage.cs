using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using core.Extensions;
using selenium.beeline.Pages.Base;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap
{
    public class PrintOfficeMapPage : BeelinePageBase {
        private const string OFFICE_ID = "office_id";

        [SimpleWebComponent(Css = ".office-info p:nth-of-type(1)",ComponentName = "�����")]
        public WebText Address;

        [SimpleWebComponent(Css = ".office-info .metro-station",ComponentName = "������� �����")]
        public WebText MetroStation;

        [SimpleWebComponent(Css = ".office-info .text-marker",ComponentName = "��� �����")]
        public WebText FindHint;

        public override string AbsolutePath {
            get { return "print/officecard/"; }
        }

        public override UriMatchResult Match(core.Framework.Service.RequestData requestData, core.Framework.Service.BaseUrlInfo baseUrlInfo) {
            string realPath = requestData.Url.AbsolutePath;
            realPath = realPath.Substring(baseUrlInfo.AbsolutePath.Length).CutLast('/');
            if (realPath.StartsWith(AbsolutePath)) {
                string tail = realPath.Substring(AbsolutePath.Length);
                string[] folders = tail.Split('/');
                if (folders.Length == 1) {
                    var data = new Dictionary<string, string> {{OFFICE_ID, folders[0]}};
                    return new UriMatchResult(true, data);
                }
            }
            return base.Match(requestData, baseUrlInfo);
        }

        public void AssertMapIsVisible() {
            Assert.IsTrue(IsMapVisible(), "����� �� ������������");
        }

        private bool IsMapVisible() {
            var tilesCount = Get.Count("#office_card_map div.PGmap-layer-container-layers>div>img");
            return tilesCount > 12;
        }

        public void AssertAddressIs(string address) {
            Assert.AreEqual(address, Address.Text, "������ �� ������������� ����������");
        }

        public void AssertMetroStationIs(string metroStation) {
            string actual = MetroStation.IsVisible() ? MetroStation.Text : null;
            Assert.AreEqual(metroStation, actual);
        }

        public void AssertFindHintIs(string findHint) {
            Assert.AreEqual(findHint, FindHint.Text);
        }

        public void AssertNearestOfficesIs(List<NearestOffice> expectedOffices) {
            var expected = expectedOffices.Select(o => o.Name + ": " + o.Address).OrderBy(o => o).ToList();
            var actual = GetNearestOffices().OrderBy(o => o).ToList();
            for (int i = 0; i < expected.Count; i++)
                Assert.AreEqual(expected[i], actual[i], "������������ ����� � ������ ���������� �������");
        }

        private List<string> GetNearestOffices() {
            return new List<string> {
                                        Get.Text(By.CssSelector(".print-content-block>p:nth-of-type(1)")),
                                        Get.Text(By.CssSelector(".print-content-block>p:nth-of-type(2)")),
                                        Get.Text(By.CssSelector(".print-content-block>p:nth-of-type(3)"))
                                    };
        }

        public void AssertServicesIs(List<OfficeService> expectedServices) {
            var expected = expectedServices.Select(s => s.ID).OrderBy(o => o).ToList();
            var actual = GetServiceNames().OrderBy(o => o).ToList();
            for (int i = 0; i < expected.Count; i++)
                Assert.AreEqual(expected[i], actual[i],
                                "������������ ������ � ������ ��������, ��������������� � ������");
        }

        /// <summary>
        /// �������� �������� ��������, ��������������� � ������
        /// </summary>
        private List<string> GetServiceNames() {
            return Get.Texts(By.CssSelector(".print-services-list>li"));
        }
    }
}

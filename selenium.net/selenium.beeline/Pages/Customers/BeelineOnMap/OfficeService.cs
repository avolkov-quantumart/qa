using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class OfficeService : ItemBase {
        public OfficeService(IContainer container, string id)
            : base(container, id) {
        }

        public override string ItemScss {
            get { return ContainerInnerScss("tr[td[2]/h5/strong[text()='{0}']]", ID); }
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class OfficesMap : PGMapBase {

        public OfficesMap(IPage parent)
            : base(parent) {
        }

        public void AssertContainsKnowHowOffices() {
            List<MapPoint> points = GetPoints<MapPoint>();
            int knowhowOfficesCount = points.Count(p => p.IsKnowHowOffice());
            Assert.Greater(knowhowOfficesCount, 1, "�� ����� ����������� ������ ��� ���");
        }
    }
}
using System;
using OpenQA.Selenium;
using selenium.core.Framework.Browser;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class SunIcon : SimpleWebComponent {
        public SunIcon(IPage parent, By by)
            : base(parent, by) {
        }

        /// <summary>
        /// �������� css �������� left ������
        /// </summary>
        public int Top() {
            return Get.CssValue<int>(By, ECssProperty.top);
        }

        /// <summary>
        /// �������� css �������� left ������
        /// </summary>
        public decimal Left() {
            return Get.CssValue<decimal>(By, ECssProperty.left);
        }
    }
}
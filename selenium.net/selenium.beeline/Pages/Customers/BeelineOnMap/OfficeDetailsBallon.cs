using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class OfficeDetailsBallon : BeelineBallonBase {
        public readonly WebText LoadMarker;
        public readonly WebText FullServicesListLink;
        public readonly WebText WorkTime;
        public WebText Metro;
        public readonly WebText Address;
        public readonly WebLink PrintLink;
        public readonly WebText WayToOffice;

        public OfficeDetailsBallon(IPage parent, string rootScss)
            : base(parent, rootScss) {
            LoadMarker = parent.RegisterComponent<WebText>("�������������",
                                                           InnerSelector(".office-mark>span>em"));
            FullServicesListLink = parent.RegisterComponent<WebText>("������ ������ �����",
                                                                     InnerSelector("div.mark-info>span"));
            WorkTime = parent.RegisterComponent<WebText>("������� �����",
                                                         InnerSelector("div.mark-information-content>p[2]"));
            Metro = parent.RegisterComponent<WebText>("�����",
                                                      InnerSelector(
                                                          "div.mark-information-content>p[3]>span.metro-station"));
            Address = parent.RegisterComponent<WebText>("�����",
                                                        InnerSelector("div.mark-information-content>p[3]"));
            PrintLink = parent.RegisterComponent<WebLink>("�����������",
                                                          InnerSelector("div.mark-information-content>p.print>a"));
            WayToOffice = parent.RegisterComponent<WebText>("��� ���������",
                                                            InnerSelector("div.mark-information-content>span"));
        }

        //[SimpleWebComponent(Scss = "todo:set selector")]
        public WebText ShareLink;
    }
}
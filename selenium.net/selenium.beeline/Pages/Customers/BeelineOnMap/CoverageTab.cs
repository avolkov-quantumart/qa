using System;
using NUnit.Framework;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class CoverageTab : ComponentBase {
        [WebComponent(".//*[@id='search_content']", ComponentName = "����� ������")]
        public BeelineInputWithList Search;

        [WebComponent]
        public CoverageMap CoverageMap;

        [WebComponent]
        public WifiMap WifiMap;

        [SimpleWebComponent(Css = "li label[for='coverage_filter_2g']", ComponentName = "���� �������� 2G")]
        public WebRadioButton Radio2G;

        [SimpleWebComponent(Css = "li label[for='coverage_filter_3g']", ComponentName = "���� �������� 3G")]
        public WebRadioButton Radio3G;

        [SimpleWebComponent(Css = "li label[for='coverage_filter_4g']", ComponentName = "���� �������� 4G")]
        public WebRadioButton Radio4G;

        [SimpleWebComponent(Css = "li label[for='coverage_filter_metro']", ComponentName = "����� � �����")]
        public WebRadioButton RadioMetro;

        [SimpleWebComponent(Css = "li label[for='coverage_filter_wifi']", ComponentName = "��������� Wi-Fi")]
        public WebRadioButton RadioWifi;

        [WebComponent(".//*[@id='map_filter_wifi']/ul/li[2]", ComponentName = "����������")]
        public BeelineToggleButton FreeWifiToggle;

        [WebComponent(".//*[@id='map_filter_wifi']/ul/li[1]", ComponentName = "�������")]
        public BeelineToggleButton PaidWifiToggle;

        [SimpleWebComponent(Css = "#metro_content>img",ComponentName = "����� �������� � �����")]
        public WebImage MetroCoverageImage;

        [WebComponent("[data-id='metro_filter_cover_2g']", ComponentName = "Metro 2G tab")]
        public BeelineToggleButton Metro2GCoverageToggle;

        [WebComponent("[data-id='metro_filter_cover_3g']", ComponentName = "Metro 3G tab")]
        public BeelineToggleButton Metro3GCoverageToggle;


        public CoverageTab(IPage parent)
            : base(parent) {
        }

        public override bool IsVisible() {
            throw new NotImplementedException();
        }

        public void AssertMetro3GCoverageIsVisible() {
            Assert.AreEqual("3g.png", MetroCoverageImage.GetFileName(), "�� ������������ ����� �������� 3G � �����");
        }

        public void AssertMetro2GCoverageIsVisible() {
            Assert.AreEqual("2g.png", MetroCoverageImage.GetFileName(), "�� ������������ ����� �������� 2G � �����");
        }
    }
}
using System;
using core.Extensions;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class BeelineOnMapPage : BeelinePageBase {
        [WebComponent]
        public CoverageTab CoverageTab;

        [WebComponent("#btn_coverage", ComponentName = "���� �������� �����")]
        public BeelineToggleButton CoverageToggle;

        [WebComponent]
        public OfficesTab OfficesTab;

        [WebComponent("#btn_offices", ComponentName = "������� � ������������")]
        public BeelineToggleButton OfficesToggle;

        public override string AbsolutePath {
            get { return "customers/beeline-on-map/"; }
        }
    }

    public class OfficesFilter : ContainerBase {
        [WebComponent("root:li.offices_filter>span[[name='offices_filter_offices']]")]
        public BeelineCheckbox OfficesCheckbox;

        [WebComponent("root:li.offices_filter>span[[name='offices_filter_dealer']]")]
        public BeelineCheckbox DealerCheckbox;

        [WebComponent("root:li.offices_filter>span[[name='offices_filter_mobileService']]")]
        public BeelineCheckbox MobileServiceCheckbox;

        [WebComponent("root:li.offices_filter>span[[name='offices_filter_home']]")]
        public BeelineCheckbox HomeCheckbox;

        [WebComponent("root:li.offices_filter>span[[name='offices_filter_intercityCards']]")]
        public BeelineCheckbox IntercityCardsCheckbox;

        [WebComponent("root:li.offices_filter>span[[name='offices_filter_cards']]")]
        public BeelineCheckbox CardsCheckbox;

        [WebComponent("root:li.offices_filter>span[[name='offices_filter_copy']]")]
        public BeelineCheckbox CopyCheckbox;

        [WebComponent("root:li.offices_filter>span[[name='offices_filter_devices']]")]
        public BeelineCheckbox DevicesCheckbox;

        [WebComponent("root:li.offices_filter>span[[name='offices_filter_knowhow']]")]
        public BeelineCheckbox KnowHowCheckbox;

        public OfficesFilter(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public void SelectOnly(OfficeFilterOption option) {
            DeselectAll();
            Select(option);
        }

        private void Select(OfficeFilterOption option) {
            switch (option) {
                case OfficeFilterOption.offices:
                    OfficesCheckbox.Select();
                    break;
                case OfficeFilterOption.dealer:
                    DealerCheckbox.Select();
                    break;
                case OfficeFilterOption.mobileService:
                    MobileServiceCheckbox.Select();
                    break;
                case OfficeFilterOption.home:
                    HomeCheckbox.Select();
                    break;
                case OfficeFilterOption.intercityCards:
                    IntercityCardsCheckbox.Select();
                    break;
                case OfficeFilterOption.cards:
                    CardsCheckbox.Select();
                    break;
                case OfficeFilterOption.copy:
                    CopyCheckbox.Select();
                    break;
                case OfficeFilterOption.devices:
                    DealerCheckbox.Select();
                    break;
                case OfficeFilterOption.knowhow:
                    KnowHowCheckbox.Select();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("option");
            }
            Wait.WhileAjax(ajaxInevitable: true);
        }

        private void DeselectAll() {
            OfficesCheckbox.Deselect();
            DealerCheckbox.Deselect();
            MobileServiceCheckbox.Deselect();
            HomeCheckbox.Deselect();
            IntercityCardsCheckbox.Deselect();
            CardsCheckbox.Deselect();
            CopyCheckbox.Deselect();
            DevicesCheckbox.Deselect();
            KnowHowCheckbox.Deselect();
            Wait.WhileAjax(ajaxInevitable: true);
        }
    }

    public enum OfficeFilterOption {
        [StringValue("����� � ������ ������������")]
        offices,
        [StringValue("������ � ��������")]
        dealer,
        [StringValue("������ ��������� �����")]
        mobileService,
        [StringValue("�������� ��������, ��������� � �����������")]
        home,
        [StringValue("����� ���������")]
        intercityCards,
        [StringValue("����� ������ ����� �����")]
        cards,
        [StringValue("������ �����������")]
        copy,
        [StringValue("������������ � ����������")]
        devices,
        [StringValue("������������������ �������� ����������� \"��� ���\"")]
        knowhow
    }
}
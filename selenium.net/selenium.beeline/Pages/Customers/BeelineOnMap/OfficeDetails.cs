using System;
using System.Collections.Generic;
using System.Linq;
using core;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class OfficeDetails : ItemBase {
        public readonly WebText ServicesListLink;
        public readonly WebText WayToOffice;
        public readonly LoadMarker LoadMarker;

        public readonly ServicesList ServicesList;
        public readonly SingleOfficeMap OfficeMap;
        public readonly LoadChart LoadChart;

        public OfficeDetails(IContainer container, string id)
            : base(container, id) {
            ServicesListLink = container.ParentPage.RegisterComponent<WebText>(
                "������ �����",
                InnerSelector("following-sibling::tr[1][contains(@class,'table-links')]/" +
                              "descendant::em[text()='������ �����']"));
            WayToOffice = container.ParentPage.RegisterComponent<WebText>(
                "��� ���������",
                InnerSelector("following-sibling::tr[1][contains(@class,'table-links')]/" +
                              "descendant::em[text()='��� ���������']"));
            LoadMarker = container.ParentPage.RegisterComponent<LoadMarker>(
                string.Format("������ �������� ������ '{0}'", id),
                InnerScss("following-sibling::tr[1]/descendant::span[contains(@class,'office-mark')]"));

            ServicesList = container.ParentPage.RegisterComponent<ServicesList>(
                string.Format("������ �������� ������ '{0}'", id),
                InnerScss("following-sibling::tr[2][contains(@class,'open-services')]/td/div[1]"));
            OfficeMap = container.ParentPage.RegisterComponent<SingleOfficeMap>(
                string.Format("����� � ������������� ������'{0}'", id),
                InnerScss("following-sibling::tr[2][contains(@class,'open-services')]/td/div[2]"));
            LoadChart = container.ParentPage.RegisterComponent<LoadChart>(
                string.Format("��������� �������� ������ '{0}'", id),
                InnerScss("following-sibling::tr[2][contains(@class,'open-services')]/td/div[3]"));
        }

        public override string ItemScss {
            get { return ContainerInnerScss("tr[descendant::h4[@id='office_{0}']]", ID); }
        }

        /// <summary>
        /// ������� ������ ������������� ������
        /// </summary>
        public void OpenLoadChart() {
            if (!LoadChart.IsVisible())
                LoadMarker.ClickAndWaitWhileAjaxRequests();
            if (!LoadChart.IsVisible())
                throw Throw.FrameworkException("�� ��������� ��������� ������������� ������");
        }

        protected override string RootScss {
            get { return ItemScss; }
        }

        public override bool IsVisible() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// �������� ������ ������
        /// </summary>
        public string GetAddress() {
            string text = Get.Text(InnerSelector(">td>p"));
            return RegexHelper.GetString(text, @"\r\n\r\n([^\r]+)");
        }

        /// <summary>
        /// �������� ������� ����� �� ������� ��������� �����
        /// </summary>
        public string GetMetroStation() {
            return Get.TextS(InnerSelector("span.metro-station"));
        }

        /// <summary>
        /// �������� �������� ���� ��� ����� ����� �����
        /// </summary>
        public string GetFindHint() {
            OpenOfficeMap();
            return OfficeMap.GetFindHint();
        }

        public void OpenOfficeMap() {
            OfficeMap.Open(() => WayToOffice.ClickAndWaitWhileAjaxRequests());
        }

        /// <summary>
        /// �������� ������ ��������� �������
        /// </summary>
        public List<NearestOffice> GetNearestOfficeAdresses() {
            OpenLoadChart();
            return LoadChart.GetNearestOffices().ToList();
        }

        /// <summary>
        /// �������� ������ ��������, ��������������� � ������
        /// </summary>
        public List<OfficeService> GetServices() {
            OpenServicesList();
            return ServicesList.GetItems();
        }

        /// <summary>
        /// ������� ������ ��������, ��������������� � ������
        /// </summary>
        public void OpenServicesList() {
            ServicesList.Open(() => ServicesListLink.ClickAndWaitWhileAjaxRequests());
        }
    }
}
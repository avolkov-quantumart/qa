using System;
using System.Text.RegularExpressions;
using NUnit.Framework;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class LoadMarker : WebText {
        [WebComponent("root:span>em",ComponentName = "Load marker text")]
        public WebText MarkerText;

        private readonly Regex _queueRegex =
            new Regex("������ � ����� ������� (?<minutes>\\d+) �����(\\w?) \\((?<length>\\d+) �������(\\w?)\\)",
                      RegexOptions.Compiled);

        private readonly Regex _usualRegex = new Regex("������ � (?:\\d{1,2}:\\d\\d) (?<text>.+)", RegexOptions.Compiled);

        public LoadMarker(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public EOfficeLoad GetLoad() {
            if (Is.HasClass(By, "low"))
                return EOfficeLoad.Low;
            if (Is.HasClass(By, "medium"))
                return EOfficeLoad.Medium;
            if (Is.HasClass(By, "high"))
                return EOfficeLoad.High;
            return EOfficeLoad.OfficeClosed;
        }

        public void AssertLoadColorMatchTheMarkerText() {
            EOfficeLoad colorLoad = GetLoad();
            EOfficeLoad textLoad;
            if (HasQueueLength()) {
                int queueLength = GetQueueLegth();
                if (queueLength >= 0 && queueLength < 4)
                    textLoad = EOfficeLoad.Low;
                else if (queueLength >= 4 && queueLength < 9)
                    textLoad = EOfficeLoad.Medium;
                else if (queueLength > 9)
                    textLoad = EOfficeLoad.High;
                else throw new ArgumentOutOfRangeException("queueLengthLoad");

            }
            else if (HasUsualLoad()) {
                string usualLoadText = GetUsualLoadText();
                switch (usualLoadText) {
                    case "���� ����� ����":
                        textLoad = EOfficeLoad.Low;
                        break;
                    case "� ����� ��������� �������":
                        textLoad = EOfficeLoad.Medium;
                        break;
                    case "���� ��������":
                        textLoad = EOfficeLoad.High;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("usualLoadText");
                }
            }
            else
                throw new ArgumentOutOfRangeException("invalid text marker");
            Assert.AreEqual(colorLoad, textLoad, "���� ������� ������������� �� ������������� ����� �������");
        }

        private string GetUsualLoadText() {
            return _usualRegex.Match(MarkerText.Text).Groups["text"].Value;
        }

        private int GetQueueLegth() {
            return int.Parse(_queueRegex.Match(MarkerText.Text).Groups["length"].Value);
        }

        public bool HasQueueLength() {
            if (!IsVisible())
                return false;
            Match match = _queueRegex.Match(MarkerText.Text);
            return match.Success;
        }

        public bool HasUsualLoad() {
            if (!IsVisible())
                return false;
            Match match = _usualRegex.Match(MarkerText.Text);
            return match.Success;
        }
    }
}
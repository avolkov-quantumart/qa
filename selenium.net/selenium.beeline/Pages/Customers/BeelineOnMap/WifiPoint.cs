using OpenQA.Selenium;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class WifiPoint : MapPoint {
        public WifiPoint(IPage parent, IWebElement element)
            : base(parent, element) {
        }

        public override string ComponentName {
            get { return "Wifi ����� ������� �� �����"; }
        }

        public override void OpenBalloon() {
            ClickAndWaitWhileAjax(ajaxInevitable: true);
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using core;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class OfficesList : ListBase<OfficeDetails> {
        public OfficesList(IPage parent) : this(parent, "#list_content") {
        }

        public OfficesList(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public override string ItemIdScss {
            get { return InnerScss("tbody>tr>td[1]>div>h4"); }
        }

        public override bool IsVisible() {
            return Is.Visible(RootScss);
        }

        public override List<string> GetIds() {
            var elements = Find.Elements(ItemIdScss);
            return elements.Select(e => RegexHelper.GetString(e.GetAttribute("id"), "office_(\\d+)")).ToList();
        }
    }
}
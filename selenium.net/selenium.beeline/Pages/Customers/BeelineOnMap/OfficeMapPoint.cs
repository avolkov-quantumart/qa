using OpenQA.Selenium;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class OfficeMapPoint : MapPoint {
        public OfficeMapPoint(IPage parent, IWebElement element)
            : base(parent, element) {
        }

        public override string ComponentName {
            get { return "����� Beeline �� �����"; }
        }
    }
}
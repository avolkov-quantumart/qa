using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class OfficesTab : ContainerBase {
        [WebComponent("#btn_layout_list,[data-id='offices_layout_type_list']", ComponentName = "������")]
        public BeelineToggleButton AsListToggle;

        [WebComponent(ComponentName = "������ �������")]
        public OfficesList OfficesList;

        [WebComponent(ComponentName = "����� �������")]
        public OfficesMap OfficesMap;

        [WebComponent("#btn_layout_map,[data-id='offices_layout_type_map']", ComponentName = "�� �����")]
        public BeelineToggleButton OnMapToggle;

        [WebComponent("#search_content", ComponentName = "����� ������")]
        public BeelineInputWithList Search;

        public OfficesTab(IPage parent)
            : base(parent, null) {
        }

        public OfficesTab(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        [WebComponent("#area_offices")]
        public OfficesFilter Filter;

        /// <summary>
        /// ������� ������ �������
        /// </summary>
        public OfficesList OpenOfficesList() {
            OfficesList.Open(() => AsListToggle.ClickAndWaitWhileAjax(ajaxInevitable: true));
            return OfficesList;
        }

        /// <summary>
        /// ������� ����� �������
        /// </summary>
        public OfficesMap OpenOfficesMap() {
            OfficesMap.Open(() => OnMapToggle.ClickAndWaitWhileAjax(ajaxInevitable: true));
            return OfficesMap;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using OpenQA.Selenium;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class CoverageMap : PGMapBase {
        private const string COVERAGE_COLOR_3G4G = "78ffaa00";
        private const string COVERAGE_COLOR_2G = "78fbc86c";

        public CoverageMap(IPage parent)
            : base(parent) {
        }

        public override bool IsVisible() {
            throw new NotImplementedException();
        }

        public decimal Get2GCoveragePercent() {
            return GetCoveragePercent(COVERAGE_COLOR_2G,"beeline2g");
        }

        public decimal Get3GCoveragePercent()
        {
            return GetCoveragePercent(COVERAGE_COLOR_3G4G, "beeline3g");
        }

        public decimal Get4GCoveragePercent() {
            return GetCoveragePercent(COVERAGE_COLOR_3G4G, "beeline4g");
        }

        /// <summary>
        /// �������� ������� ��������
        /// </summary>
        public decimal GetCoveragePercent(string coverageColor,string urlMarker) {
            List<string> urls = GetTileUrls(urlMarker);
            List<Bitmap> tiles = LoadTileImages(urls);
            int total = 0;
            int yellow = 0;
            foreach (Bitmap bitmap in tiles) {
                total += bitmap.Width*bitmap.Height;
                for (int i = 0; i < bitmap.Width; i++) {
                    for (int j = 0; j < bitmap.Height; j++) {
                        Color pixel = bitmap.GetPixel(i, j);
                        if (coverageColor == pixel.Name)
                            yellow++;
                    }
                }
            }
            return ((decimal) yellow/total)*100;
        }

        private List<Bitmap> LoadTileImages(List<string> urls) {
            var client = new WebClient();
            return urls.Select(url => new Bitmap(new MemoryStream(client.DownloadData((string) url)))).ToList();
        }
    }
}
using core;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class NearestOffice : ContainerBase {
        public string Address;
        public string Name;

        public NearestOffice(IPage parent, string rootScss)
            : base(parent, rootScss) {
            Address = GetAddress();
            Name = Get.Text(InnerSelector("td[1]>span"));
        }

        private string GetAddress() {
            var text = Get.Text(InnerSelector("td[2]"));
            return RegexHelper.GetString(text, "(.*),\\s\\d+,\\d+\\s��\\.");
        }
    }
}
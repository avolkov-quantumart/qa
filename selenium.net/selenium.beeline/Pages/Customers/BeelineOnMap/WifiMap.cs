using NUnit.Framework;
using OpenQA.Selenium;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class WifiMap : PGMapBase {

        protected override By PointSelector {
            get { return Scss.GetBy("div[>.wifi]"); }
        }

        public WifiMap(IPage parent)
            : base(parent) {
        }

        public void AssertPointsCountGreaterThan(int count) {
            Assert.Greater(GetPointsCount<WifiPoint>(), count, "���������� wifi ����� �� �����������");
        }

        /// <summary>
        /// ������������ �� ������ ��������� ����� �������
        /// </summary>
        public bool BalloonIsVisible() {
            return GetBalloon<OfficeDetailsBallon>().IsVisible();
        }

        public void AssertBaloonIsVisible() {
            Assert.True(BalloonIsVisible(), "����� � ��������� ����� ������� �� �����������");
        }
    }
}
using System.Collections.Generic;
using NUnit.Framework;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class ServicesList : ListBase<OfficeService> {
        public ServicesList(IPage parent, string rootScss)
            : base(parent, rootScss) {
        }

        public override string ItemIdScss {
            get { return InnerScss("td>h5>strong"); }
        }

        public void AssertContainsKnowHowService() {
            List<string> serviceNames = GetIds();
            Assert.Contains("������������������ ������� ����������� \"��� ���\"", serviceNames,
                            "����� �� �������� ������������������ ��������� ����������� \"��� ���\"");
        }
    }
}
using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class LoadChart : ContainerBase {
        private const double CHART_LEFT_INDENT = 41.5;
        public SunIcon SunIcon;

        public LoadChart(IPage parent, string rootScss)
            : base(parent, rootScss) {
            SunIcon = parent.RegisterComponent<SunIcon>("Sun icon", InnerSelector("span.sun-icon"));
        }

        /// <summary>
        /// ��������� ��� ������ ��������� � ���������� ���������
        /// </summary>
        public void AssertSunIsUnder(DateTime time) {
            int hour = GetHourOnChart(time); 
            var iconLeft = SunIcon.Left();
            int rangeLeft = GetPositionForHour(hour - 1);
            int rangeRight = GetPositionForHour(hour + 1);
            Assert.IsTrue(iconLeft > rangeLeft, "������ ������� ����� ������������ ����������� ���������");
            Assert.IsTrue(iconLeft < rangeRight, "������ ������� ������ ������������ ����������� ���������");
        }

        /// <summary>
        /// ���������� ������ ���� �� ������� ������������� ��������� �����
        /// </summary>
        private int GetHourOnChart(DateTime time) {
            return time.Minute >= 30 ? time.Hour + 1 : time.Hour;
        }

        /// <summary>
        /// ��������� ������� � ������� ������ ��������� � ��������� ���
        /// </summary>
        private int GetPositionForHour(int hour) {
            int openHour = GetOpenHour();
            int openRange = GetOpenRange();
            int hourWidth = 750/openRange;
            return (int) (CHART_LEFT_INDENT + hourWidth*(hour - openHour)); // ��������� ��������� ������� �����
        }

        /// <summary>
        /// �������� ��� �������� ������
        /// </summary>
        public int GetCloseHour() {
            return Get.Int(InnerSelector("table.capacity-table>tbody>tr[1]>td[last()]"));
        }

        /// <summary>
        /// �������� ��� �������� ������
        /// </summary>
        public int GetOpenHour() {
            return Get.Int(InnerSelector("table.capacity-table>tbody>tr[1]>td[2]"));
        }

        /// <summary>
        /// �������� ������� ����������� ������ ������
        /// </summary>
        public int GetOpenRange() {
            return Get.Texts(
                InnerSelector("table.capacity-table>tbody>tr[1]>td")).Count - 1;
        }

        /// <summary>
        /// ��������� ��� ������� ��������� �� ��������� ����
        /// </summary>
        public void AssertSelectedTimeIs(DateTime time) {
            if (!IsDayLoadDisplayed(time))
                return;
            Assert.IsTrue(SelectedTimeIs(time), "������� ��������� ������������ �����");
        }

        private bool SelectedTimeIs(DateTime date) {
            string xpath = string.Format("{0}/div/span[@class='ind-node']", HourLoadXpath(date));
            return Is.Visible(xpath);
        }

        /// <summary>
        /// Xpath ��� ����������� �������� ������ � ��������� �����
        /// </summary>
        private string HourLoadXpath(DateTime time) {
            int hour = GetHourOnChart(time);
            int tdIndex = hour - 7;
            return string.Format("{0}/td[{1}]", DayLoadScss(time), tdIndex);
        }

        public bool IsDayLoadDisplayed(DateTime time) {
            return Is.Visible(DayLoadScss(time));
        }

        /// <summary>
        /// Xpath ��� ����������� �������� ������ � ��������� ����
        /// </summary>
        private string DayLoadScss(DateTime time) {
            string weekDay;
            switch (time.DayOfWeek) {
                case DayOfWeek.Sunday:
                    weekDay = "��";
                    break;
                case DayOfWeek.Monday:
                    weekDay = "��";
                    break;
                case DayOfWeek.Tuesday:
                    weekDay = "��";
                    break;
                case DayOfWeek.Wednesday:
                    weekDay = "��";
                    break;
                case DayOfWeek.Thursday:
                    weekDay = "��";
                    break;
                case DayOfWeek.Friday:
                    weekDay = "��";
                    break;
                case DayOfWeek.Saturday:
                    weekDay = "��";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return InnerScss(string.Format("tr[td>strong['{0}']]", weekDay));
        }

        public EOfficeLoad GetLoad(DateTime time) {
            var attr = Get.Attr(HourLoadXpath(time), "class");
            switch (attr) {
                case "low-cap":
                    return EOfficeLoad.Low;
                case "medium-cap":
                    return EOfficeLoad.Medium;
                case "high-cap":
                    return EOfficeLoad.High;
                default:
                    throw new ArgumentOutOfRangeException("load class");
            }
        }

        public void AssertTimeLoadIs(DateTime time, EOfficeLoad load) {
            Assert.AreEqual(load, GetLoad(time), "�������� ��� {0} �� ������������� ���������", time.ToShortDateString());
        }

        /// <summary>
        /// �������� ������ ��������� �������
        /// </summary>
        public List<NearestOffice> GetNearestOffices() {
            string xpath = InnerScss("#nearest_offices>table>tbody>tr");
            var count = Get.Count(xpath);
            var offices = new List<NearestOffice>();
            for (int i = 1; i <= count; i++)
                offices.Add(new NearestOffice(ParentPage, string.Format("{0}[{1}]", xpath, i)));
            return offices;
        }
    }
}
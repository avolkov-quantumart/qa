namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public enum EOfficeLoad {
        Low,
        Medium,
        High,
        OfficeClosed
    }
}
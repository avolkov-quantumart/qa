using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.BeelineOnMap {
    public class SingleOfficeMap : ContainerBase {
        public WebLink PrintLink;

        public SingleOfficeMap(IPage parent, string rootScss)
            : base(parent, rootScss)
        {
            PrintLink = parent.RegisterComponent<WebLink>("Распечатать", InnerSelector("a['Распечатать']"));
        }

        public string GetFindHint() {
            return Get.TextS(InnerSelector("div.map-header>p"));
        }
    }
}
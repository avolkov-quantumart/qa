using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using core.Extensions;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Help.Mobile;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.press {
    public class SubscribeManagementComponent :ComponentBase {
        [WebComponent("div#subscrArea>div.toggle","")]
        public BeelineTabs Tabs;

        [SimpleWebComponent(ID = "subscConfirmation",ComponentName = "��������� �����")]
        public WebText CheckMailForActivationMessage;

        [SimpleWebComponent(ID = "unsubscConfirmation",ComponentName = "��������� �����")]
        public WebText CheckMailForCancelationMessage;

        [WebComponent("div[>input#Email]",ComponentName = "Email")]
        public BeelineInput Email;

        [WebComponent("#subscribeTab .SubscribeBtnArea",ComponentName = "�����������")]
        public BeelineButton SubscribeButton;

        [WebComponent("#unsubscribeTab .UnsubscribeBtnArea",ComponentName = "�������� ��������")]
        public BeelineButton UnsubscribeButton;

        [WebComponent("#subscribeCaptchaContainer")]
        public BeelineCaptcha SubscribeCaptcha;

        [WebComponent("#unsubscribeCaptchaContainer")]
        public BeelineCaptcha UnsubscribeCaptcha;

        [WebComponent("span[>input#selectAll]", ComponentName = "�������� ���")]
        public BeelineCheckbox SelectAllCheckbox;

        private readonly By _themeSelector;

        public SubscribeManagementComponent(IPage parent)
            : base(parent) {
            _themeSelector = By.CssSelector("#subscribeTab .checkblock span+label");
        }

        public override bool IsVisible() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ��������� ��� ������������ ��������� "�� ��� �������� ����� ���� ������� ������������� ��������. ��������� �� ������ � ������."
        /// </summary>
        public void AssertCheckMailForActivationMessage() {
            Assert.True(CheckMailForActivationMessage.IsVisible(), "�� ������������ ��������� �� ��������� ��������");
        }

        /// <summary>
        /// ��������� ��� ������������ ��������� "�� ��� �������� ����� ���� ������� ������������� ������ ��������. ��������� �� ������ � ������."
        /// </summary>
        public void AssertCheckMailForCancellationMessage() {
            Assert.True(CheckMailForCancelationMessage.IsVisible(), "�� ������������ ��������� �� ������ ��������");
        }

        /// <summary>
        /// �������� ��������� ���������� ������� ��������
        /// </summary>
        public List<string> SelectRandomThemes(int count) {
            DeselectAllThemes();
            List<IWebElement> elements = Find.Elements(_themeSelector);
            var randomThemes = new List<string>();
            for (int i = 0; i < count; i++) {
                if (elements.Count == 0)
                    break;
                var randomItem = elements.RandomItem();
                elements.Remove(randomItem);
                Log.Action("�������� �������� '{0}'", randomItem.Text);
                randomItem.Click();
                randomThemes.Add(randomItem.Text);
            }
            return randomThemes;
        }

        /// <summary>
        /// ����� ��������� �� ���� �������
        /// </summary>
        private void DeselectAllThemes() {
            SelectAllCheckbox.Deselect();
        }

        /// <summary>
        /// �������� ������ ���������� �������
        /// </summary>
        public List<string> GetSelectedThemes() {
            return Get.Texts(By.CssSelector("#subscribeTab .checkblock span.checked+label"));
        }

        public void Subscribe(string email) {
            OpenSubscriptionTab();
            Email.TypeIn(email);
            SubscribeCaptcha.SetDefault();
            FD.subscription.subscription_themes = SelectRandomThemes(3);
            SubscribeButton.ClickAndWaitWhileAjax();
        }

        private void OpenSubscriptionTab() {
            Tabs.SelectTab("�����������");
        }

        private void OpenCancelSubscriptionTab() {
            Tabs.SelectTab("���������� �� ��������");
        }

        /// <summary>
        /// ������� �������� ��� ���������� email
        /// </summary>
        public void RemoveSubscription(string email) {
            OpenCancelSubscriptionTab();
            Email.TypeIn(email);
            UnsubscribeCaptcha.SetDefault();
            UnsubscribeButton.ClickAndWaitWhileAjax();
        }
    }
}
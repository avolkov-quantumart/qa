using System.Collections.Generic;
using OpenQA.Selenium;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.press {
    public class SubscribeAlert : AlertBase {
        [WebComponent]
        public SubscribeManagementComponent Component;

        public SubscribeAlert(IPage parent)
            : base(parent) {
        }

        public BeelineInput Email {
            get { return Component.Email; }
        }

        public BeelineCaptcha SubscribeCaptcha {
            get { return Component.SubscribeCaptcha; }
        }

        public override void Dismiss() {
            throw new System.NotImplementedException();
        }

        public override void Accept() {
            Component.SubscribeButton.ClickAndWaitWhileAjax();
        }

        public override bool IsVisible() {
            return Is.Visible("legend['���������� ���������']");
        }

        public void AssertCheckMailForActivationMessage() {
            Component.AssertCheckMailForActivationMessage();
        }

        public void AssertCheckMailForCancellationMessage() {
            Component.AssertCheckMailForCancellationMessage();
        }

        public List<string> SelectRandomThemes(int number) {
            return Component.SelectRandomThemes(number);
        }

        public void Subscribe(string email) {
            Component.Subscribe(email);
        }

        public void RemoveSubscription(string email) {
            Component.RemoveSubscription(email);
        }
    }
}
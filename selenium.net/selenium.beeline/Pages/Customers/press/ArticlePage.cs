﻿using selenium.beeline.Pages.Base;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.press {
    public class ArticlePage : BeelinePageBase {
        public override string AbsolutePath {
            get { return "customers/press/news/details/{articleid}/"; }
        }

        [SimpleWebComponent(Css = ".content-wrap h1", ComponentName = "Заголовок")]
        public WebText Title;

        [SimpleWebComponent(Css = ".left-nav>a", ComponentName = "Предыдущая новость")]
        public WebLink PreviousLink;

        [SimpleWebComponent(Css = ".right-nav>a", ComponentName = "Следующая новость")]
        public WebLink NextLink;
    }
}

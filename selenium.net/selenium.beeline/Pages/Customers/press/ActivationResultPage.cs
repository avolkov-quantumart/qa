﻿using NUnit.Framework;
using selenium.beeline.Pages.Base;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.press {
    public class ActivationResultPage : BeelinePageBase {
        [SimpleWebComponent(Css = ".align.subscribe>h1",ComponentName = "Статус")]
        public WebText StatusMessage;

        [SimpleWebComponent(Css = ".align.subscribe>img[alt='Подписка на новости']",ComponentName = "Подписка оформлена успешно")]
        public WebImage SuccessIcon;

        public override string AbsolutePath {
            get { return "newssubscriptions/activate/"; }
        }

        /// <summary>
        /// Проверить что отображается сообщение о том что подписка оформлена успешно
        /// </summary>
        public void AssertSubscriptionIsActivated(string email) {
            string expected = string.Format("{0}  подписан на новости!", email);
            Assert.AreEqual(expected, StatusMessage.Text,
                            "Не отображается сообщение о том что подписка на новости оформлена успешна");
            Assert.True(SuccessIcon.IsVisible(),
                        "Не отображается иконка, означающая что подписка на новости оформлена успешно");
        }

        /// <summary>
        /// Проверить что отображается сообщение о том что ссылка активации устарела
        /// </summary>
        public void AssertActivationLinkIsOld() {
            Assert.AreEqual("К сожалению, ссылка устарела или не существует.", StatusMessage.Text);
            Assert.False(SuccessIcon.IsVisible(),
                         "Отображается иконка, означающая что подписка на новости оформлена успешно");
        }
    }
}
﻿using System.Collections.Generic;
using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.press {
    public class SubscriptionActivationEmail : EmailPageBase {
        [SimpleWebComponent(LinkText = "подписаться")]
        public WebLink ActivationLink;

        [SimpleWebComponent(LinkText = "изменить категории подписки")]
        public WebLink ChangeThemesLink;

        /// <summary>
        /// Получить тематики на которые будет выполнена подписка
        /// </summary>
        public List<string> GetThemes() {
            return Get.Texts(By.CssSelector("td>h3+div>div"));
        }
    }

    public class ChangeSubscriptionThemesEmail : EmailPageBase {
        [SimpleWebComponent(LinkText = "подписаться")]
        public WebLink ChangeLink;

        public List<string> GetNewThemes() {
            return Get.Texts("td[h3>span['Новые категории подписки']]>div>div");
        }

        public List<string> GetOldThemes() {
            return Get.Texts("td[>h3>span['Категории, на которые вы уже подписаны']]>div>div");
        }
    }

    public class SubscriptionCancelationEmail : EmailPageBase {
        [SimpleWebComponent(LinkText = "прекратить подписку")]
        public WebLink ConfirmationLink;
    }
}
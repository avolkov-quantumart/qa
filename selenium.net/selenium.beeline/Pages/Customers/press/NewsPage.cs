﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using core.Extensions;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.beeline.Pages.Help.Mobile;
using selenium.core.Exceptions;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;
using selenium.core.SCSS;

namespace selenium.beeline.Pages.Customers.press {
    public class NewsPage : BeelinePageBase {
        public override string AbsolutePath {
            get { return "customers/press/news/"; }
        }

        [WebComponent]
        public MonthFilter MonthFilter;

        [WebComponent]
        public NewsList NewsList;

        [WebComponent]
        public YearFilter YearFilter;

        [WebComponent("div[>span>input#btn_subscribe]", ComponentName = "Оформление подписки")]
        public BeelineButton SubscribeButton;

        [WebComponent]
        public SubscribeAlert SubscribeAlert;

        /// <summary>
        /// Оформить подписку на новости для указанного email-а
        /// </summary>
        public void Subscribe(string email) {
            OpenSubscribeAlert();
            SubscribeAlert.Subscribe(email);
        }

        private void OpenSubscribeAlert() {
            SubscribeAlert.Open(() => SubscribeButton.ClickAndWaitWhileAjax());
        }

        public void RemoveSubscription(string email) {
            OpenSubscribeAlert();
            SubscribeAlert.RemoveSubscription(email);
        }
    }

    public class MonthFilter : ContainerBase {
        public MonthFilter(IPage parent)
            : base(parent, "#NewsPage_YearNews_MonthFilter") {
        }

        /// <summary>
        /// Выбранный месяц
        /// </summary>
        public int Selected {
            get { return Get.Attr<int>(InnerSelector("li.active"), "data-id"); }
        }

        public int SelectRandom(params int[] exclude) {
            var excludeList = exclude.ToList();
            int random;
            do {
                random = GetAll().RandomItem();
            } while (excludeList.Contains(random));
            Select(random);
            return random;
        }

        private void Select(int month) {
            Action.ClickAndWaitWhileAjax(InnerSelector("li[data-id='{0}']", month));
        }

        private List<int> GetAll() {
            return Get.Attrs<int>(InnerSelector("li"), "data-id");
        }
    }

    public class NewsList : ContainerBase {
        public NewsList(IPage parent)
            : base(parent, "#NewsPage_YearNews_Content") {
        }

        /// <summary>
        /// Получить список статей
        /// </summary>
        public Dictionary<string, Article> GetArticles() {
            var articlesCount = GetArticlesCount();
            var articles = new Dictionary<string, Article>();
            DateTime? curDate = null;
            for (int i = 1; i <= articlesCount; i++) {
                string dateScss = InnerScss("li[{0}]>div.news-date", i);
                if (Is.Visible(dateScss)) {
                    int day = Get.Int(Scss.Concat(dateScss, ">strong"));
                    int month = ParseMonth(Get.Text(Scss.Concat(dateScss, ">.month")));
                    curDate = new DateTime(2000, month, day);
                }
                string articleXpath = InnerScss("li[{0}]>div.news-block", i);
                var linkSelector = Scss.GetBy(articleXpath, ">h3>a");
                string title = Get.Text(linkSelector);
                string url = Get.AbsoluteHref(linkSelector);
                string summary = Get.Texts(Scss.Concat( articleXpath , ">div>p"), true)
                    .Aggregate(string.Empty, (current, t) => current + (t + Environment.NewLine));
                if (curDate == null)
                    throw Throw.FrameworkException("Unable to get article date");
                if (!articles.ContainsKey(title))
                    articles.Add(title, new Article((DateTime)curDate, title, summary, url));
            }
            return articles;
        }

        private int ParseMonth(string sMonth) {
            sMonth = sMonth.Trim().ToLower();
            switch (sMonth) {
                case "января":
                    return 1;
                case "февраля":
                    return 2;
                case "марта":
                    return 3;
                case "апреля":
                    return 4;
                case "мая":
                    return 5;
                case "июня":
                    return 6;
                case "июля":
                    return 7;
                case "августа":
                    return 8;
                case "сентября":
                    return 9;
                case "октября":
                    return 10;
                case "ноября":
                    return 11;
                case "декабря":
                    return 12;
                default:
                    throw new ArgumentOutOfRangeException("sMonth");
            }
        }

        public int GetArticlesCount() {
            return Get.Count(InnerSelector("div.news-block"));
        }

        /// <summary>
        /// Перейти на статью, находящуюся в середине списка
        /// </summary>
        public Article GoToNotFirstArticle() {
            var articles = GetArticles().Values.ToList();
            if (articles.Count == 0)
                Throw.FrameworkException("Список новостей пуст");
            Article article = articles.RandomItem(articles.First());
            Browser.Action.ClickAndWaitForRedirect(By.CssSelector(string.Format("a[href='{0}']", article.Url)));
            return article;
        }
    }

    public class Article {
        public readonly DateTime Date;
        public readonly string Summary;
        public readonly string Title;
        public readonly string Url;

        public Article(string title) {
            Title = title;
            Date = default(DateTime);
            Summary = null;
        }

        public Article(DateTime date, string title, string summary, string url) {
            Date = date;
            Title = title;
            Summary = summary;
            Url = url;
        }

        public override bool Equals(object obj) {
            return obj is Article && (obj as Article).Title == Title;
        }
    }

    public class YearFilter : ContainerBase {
        public YearFilter(IPage parent)
            : base(parent, "#NewsPage_Index_YearFilter") {
        }

        public List<int> GetAll() {
            return Get.Ints(InnerSelector("li>span"));
        }

        public int SelectRandom(Func<int, bool> excludeFilter = null) {
            excludeFilter = excludeFilter ?? (year => false);
            int random;
            do {
                random = GetAll().RandomItem();
            } while (excludeFilter.Invoke(random));
            Select(random);
            return random;
        }

        private void Select(int year) {
            Log.Action("Select year '{0}' in news filter", year);
            Action.ClickAndWaitWhileAjax(InnerSelector("li>span['{0}']", year));
        }

        public void SelectNewYear() {
            var current = GetCurrent();
            SelectRandom(year => year == current);
        }

        public decimal GetCurrent() {
            return Get.Int(InnerSelector("li.active"));
        }
    }
}

using NUnit.Framework;
using OpenQA.Selenium;
using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Pages.Customers.how {
    public class OnlinePaymentPage : BeelinePageBase {
        [SimpleWebComponent(LinkText = "QIWI-�������", ComponentName = "QIWI-�������")]
        public WebLink QiwiLink;

        [WebComponent("#servicesFilterButtons")]
        public BeelineTabs Tabs;

        [SimpleWebComponent(LinkText = "WebMoney", ComponentName = "WebMoney")]
        public WebLink WebmoneyLink;

        public override string AbsolutePath {
            get { return "customers/how-to-pay/popolnit-schet/s-elektronnykh-koshelkov/"; }
        }

        public void OpenMobileTab() {
            Tabs.SelectTab(EBeelineTab.MobileBeeline);
        }

        public void OpenHomeTab() {
            Tabs.SelectTab(EBeelineTab.HomeBeeline);
        }

        public void OpenWifiTab() {
            Tabs.SelectTab(EBeelineTab.WiFi);
        }

        public void AssertMobilePaymentInstructionIsDisplayed() {
            QiwiLink.AssertVisible();
            WebmoneyLink.AssertVisible();
            AssertInstructionContains("��� �������� ��������� �������� � ������� QIWI ��� WebMoney");
        }

        public void AssertHomePaymentInstructionIsDisplayed() {
            QiwiLink.AssertVisible();
            WebmoneyLink.AssertVisible();
            AssertInstructionContains("��� �������� �������� �������� � ������� QIWI ��� WebMoney");
        }

        public void AssertWifiPaymentInstructionIsDisplayed() {
            QiwiLink.AssertNotVisible();
            WebmoneyLink.AssertNotVisible();
            AssertInstructionContains("��� �������� �������� WiFi ������.�������� ��� WebMoney");
        }

        private void AssertInstructionContains(string pattern) {
            string instruction = GetInstruction();
            Assert.True(instruction.Contains(pattern), "���������� �� �������� ����� '{0}'", pattern);
        }

        private string GetInstruction() {
            return Browser.Get.TextsAsString(".content-block>div");
        }
    }
}
﻿using selenium.beeline.Pages.Base;
using selenium.beeline.Pages.Common;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Steps.Pages.Customers.how_to_pay.popolnit_schet {
    public class PaymentPage : BeelinePageBase {
        public override string AbsolutePath {
            get { return "customers/how-to-pay/popolnit-schet/"; }
        }

        [WebComponent("span.ButtonSendPayment",ComponentName = "Оплатить")]
        public BeelineButton GoToPaymentButton;

        [WebComponent("#servicesFilterButtons")]
        public BeelineTabs Tabs;

        [WebComponent]
        public PaymentHomeTab HomeTab;

        [WebComponent]
        public PaymentMobileTab MobileTab;

        public void OpenHomeTab() {
            Tabs.SelectTab(EBeelineTab.HomeBeeline);
        }

        public void OpenMobileTab() {
            Tabs.SelectTab(EBeelineTab.MobileBeeline);
        }
    }
}

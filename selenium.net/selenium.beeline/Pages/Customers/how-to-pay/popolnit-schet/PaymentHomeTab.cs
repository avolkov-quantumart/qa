using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Steps.Pages.Customers.how_to_pay.popolnit_schet {
    public class PaymentHomeTab : ContainerBase {
        public PaymentHomeTab(IPage parent)
            : base(parent, "div[>h3>strong['������� ���������� ����� ���������� ������']]") {
        }

        [WebComponent]
        public CardPaymentBlock PaymentBlock;
    }

    public class CardPaymentBlock : ContainerBase {
        public readonly WebInput AccountNumber;
        public readonly WebInput Sum;

        public CardPaymentBlock(IPage parent)
            : base(parent, "div[>h3>strong['������� ���������� ����� ���������� ������']]") {
            AccountNumber = parent.RegisterComponent<WebInput>("����� ��������", InnerSelector("[name='ModemPhone']"));
            Sum = parent.RegisterComponent<WebInput>("�����", InnerSelector("[name='Sum']"));
        }

        public override bool IsVisible() {
            return AccountNumber.IsVisible() && Sum.IsVisible();
        }
    }
}
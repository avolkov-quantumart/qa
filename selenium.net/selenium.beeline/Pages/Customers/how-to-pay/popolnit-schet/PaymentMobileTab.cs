using selenium.beeline.Pages.Common;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace selenium.beeline.Steps.Pages.Customers.how_to_pay.popolnit_schet {
    public class PaymentMobileTab : ComponentBase {
        [WebComponent("[name='PhoneContainer']")]
        public BeelinePhoneNumber Phone;

        public PaymentMobileTab(IPage parent)
            : base(parent) {
        }

        public override bool IsVisible() {
            return Phone.IsVisible();
        }
    }
}
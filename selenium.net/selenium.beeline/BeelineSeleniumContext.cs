using GMail;
using NUnit.Framework;
using TechTalk.SpecFlow;
using selenium.beeline.Pages.Base;
using selenium.core;
using selenium.core.Framework.Service;

namespace selenium.beeline {
    [Binding]
    public class BeelineSeleniumContext : SeleniumContext<BeelineSeleniumContext> {
        protected override void InitWeb() {
            try {
                Web.RegisterService(new BeelineServiceFactory());
                Web.RegisterService(new GMailServiceFactory());
            }
            catch (RouterInitializationException e) {
                Log.FatalError("Unable to initialize service", e);
            }
        }
    }
}
using System.Collections.Generic;
using System.Text;
using HtmlAgilityPack;

namespace reporting {
    internal class SpecflowReport : HtmlDocument
    {
        public void InsertArtifactLinks(List<Scenario> scenarios) {
            foreach (Scenario scenario in scenarios) {
                // ����� ���� � ������ ��������
                string logXpath = string.Format(
                    "//h3[span[@title='{0}Feature']]/following-sibling::table[1]/descendant::tr[descendant::span[contains(@title,'{1}')]]/following-sibling::tr[1]/td",
                    scenario.FeatureId, scenario.ScenarioId);
                HtmlNode logNode = DocumentNode.SelectSingleNode(logXpath);
                if (logNode == null)
                    continue;

                // ������������� html ��� �� �������� �� ���������
                HtmlNode artifactLinksNode = GenerateArtifactLinks(scenario);

                // �������� ���� �� �������� ����� ���� � �����
                logNode.ParentNode.InsertAfter(artifactLinksNode, logNode);
            }
        }

        /// <summary>
        /// ������������� Html ��� �� �������� �� ��������� ���������� ��������
        /// </summary>
        private HtmlNode GenerateArtifactLinks(Scenario scenario) {
            var sb = new StringBuilder();
            var node = CreateElement("td");
            foreach (Artifact artifact in scenario.Artifacts) {
                sb.AppendFormat("<a href='{0}'>{1}</a>", artifact.RelativeFileUri, artifact.Type);
                sb.AppendLine();
            }
            node.InnerHtml = sb.ToString();
            return node;
        }
    }
}
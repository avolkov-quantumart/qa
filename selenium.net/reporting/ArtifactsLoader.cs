using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace reporting {
    internal class ArtifactsLoader {
        private readonly Regex _artifactFileRegex =
            new Regex("(?<featureId>\\w+)_(?<scenarioId>\\w+)_(?<date>\\d+_\\d+)_(?<type>\\w+)", RegexOptions.Compiled);

        private readonly string _folder;

        public ArtifactsLoader(string folder) {
            _folder = folder;
        }

        /// <summary>
        /// ��������� ��������� ����������
        /// </summary>
        public List<Scenario> Load(string rootFolder) {
            var directoryInfo = new DirectoryInfo(_folder);
            FileSystemInfo[] fileSystemInfos = directoryInfo.GetFileSystemInfos();
            var scenarios = new Dictionary<string, Scenario>();
            foreach (FileSystemInfo fileSystemInfo in fileSystemInfos) {
                // �������� ��������� ���������
                Match match = _artifactFileRegex.Match(fileSystemInfo.Name);
                if (!match.Success)
                    continue;
                DateTime time = DateTime.ParseExact(match.Groups["date"].Value,
                                                    "yyyyMMdd_HHmmss",
                                                    CultureInfo.InvariantCulture);
                string artifactFileName = Path.Combine(_folder, fileSystemInfo.Name);
                var artifact = new Artifact(artifactFileName, time, match.Groups["type"].Value, rootFolder);

                // �������� �������� ���������
                Scenario scenario;
                string featureId = match.Groups["featureId"].Value;
                string scenarioId = match.Groups["scenarioId"].Value;
                string id = featureId + "." + scenarioId;
                if (scenarios.ContainsKey(id))
                    scenario = scenarios[id];
                else {
                    scenario = new Scenario(featureId, scenarioId);
                    scenarios.Add(id, scenario);
                }

                // �������� �������� � ������ ���������� ��������
                scenario.AddArtifact(artifact);
            }
            return scenarios.Values.ToList();
        }
    }
}
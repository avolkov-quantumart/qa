using System.Collections.Generic;

namespace reporting {
    internal class Scenario {
        public Scenario(string featureId, string scenarioId) {
            FeatureId = featureId;
            ScenarioId = scenarioId;
            Artifacts = new List<Artifact>();
        }

        public string ScenarioId { get; set; }
        public string FeatureId { get; set; }
        public List<Artifact> Artifacts { get; private set; }

        public void AddArtifact(Artifact artifact) {
            Artifacts.Add(artifact);
        }
    }
}
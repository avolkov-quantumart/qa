﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Ionic.Zip;

namespace reporting {
    internal class Program {
        private static void Main(string[] args) {
            if (args == null || args.Length != 3) {
                PrintHelp();
                return;
            }
            string rootFolder = args[0];
            string reportHtmlFile = Path.Combine(rootFolder, args[1]);
            string reportZipFile = Path.Combine(rootFolder, args[1].Split('.')[0] + ".zip");

            string artifactsFolder = Path.Combine(rootFolder, args[2]);

            // Загрузить список артефактов
            var artifactsLoader = new ArtifactsLoader(artifactsFolder);
            List<Scenario> scenarios = artifactsLoader.Load(rootFolder);

            // Загрузить отчет SpecFlow
            var report = new SpecflowReport();
            report.Load(reportHtmlFile);

            // Добавить ссылки на артефакты
            report.InsertArtifactLinks(scenarios);

            // Сохранить измененный отчет
            report.Save(reportHtmlFile);

            // Заархивировать отчет и все артефакты
            using (var zip = new ZipFile()) {
                foreach (var scenario in scenarios)
                    foreach (var artifact in scenario.Artifacts)
                        zip.AddFile(artifact.FilePath, artifact.RelativeFolderPath);
                zip.AddFile(reportHtmlFile, string.Empty);
                zip.Save(reportZipFile);
            }

            var recipients = new List<string> { "volkova@quantumart.ru", "smirnova@quantumart.ru", "semenetsl@quantumart.ru" };
            SendMail(scenarios.Count(s => s.Artifacts.Count != 0), reportZipFile, recipients);
        }

        private static void SendMail(int totalFailed, string zipFile, List<string> recipients) {
            var fromAddress = "quantumart.selenium@gmail.com";
            const string PASSWORD = "ViolettaLebedeva85";
            string subject = string.Format("Selenium tests report(failed:{0})", totalFailed);
            string body = "";
            var smtp = new SmtpClient {
                                          Host = "smtp.gmail.com",
                                          Port = 587,
                                          EnableSsl = true,
                                          DeliveryMethod = SmtpDeliveryMethod.Network,
                                          UseDefaultCredentials = false,
                                          Credentials = new NetworkCredential(fromAddress, PASSWORD)
                                      };
            using (var message = new MailMessage()) {
                message.From = new MailAddress(fromAddress);
                message.Subject = subject;
                message.Body = body;
                foreach (string recipient in recipients)
                    message.To.Add(new MailAddress(recipient));
                message.Attachments.Add(new Attachment(zipFile));
                smtp.Send(message);
            }
        }

        private static void PrintHelp() {
            Console.WriteLine("Usage: reporting SpecFlowReportFile ArtifactsFolder");
        }
    }
}
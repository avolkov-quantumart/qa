using System;
using System.IO;

namespace reporting {
    internal class Artifact {
        public Artifact(string fileName, DateTime time, string type, string rootFolder) {
            FilePath = fileName;
            Time = time;
            Type = type;
            RootFolder = rootFolder;
        }

        public string FilePath { get; set; }

        public string RootFolder { get; set; }

        public string RelativeFilePath {
            get {
                if (string.IsNullOrEmpty(RootFolder))
                    throw new Exception("�� ������� �������� �����");
                string s = FilePath.Substring(RootFolder.Length);
                if (s.StartsWith("\\"))
                    s = s.Substring(1);
                return s;
            }
        }

        public string RelativeFolderPath {
            get { return Path.GetDirectoryName(RelativeFilePath); }
        }

        public string RelativeFileUri {
            get { return RelativeFilePath.Replace("\\", "/"); }
        }

        public DateTime Time { get; set; }
        public string Type { get; set; }
    }
}
/**
 * Created by VolkovA on 27.02.14.
 */

using selenium.core.Framework.Browser;
using selenium.core.Framework.Page;
using selenium.core.Logging;
using selenium.core.Tests;

namespace selenium.mgts.Tests.Base {
    public abstract class MgtsTestBase<T> : TestBase<T> where T : IPage {
        protected override Browser Browser {
            get { return MgtsSeleniumContext.Inst.Browser; }
        }

        protected override TestLogger Log {
            get { return MgtsSeleniumContext.Inst.Log; }
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using selenium.mgts.Framework.Base;

namespace selenium.mgts.Tests {
    [TestClass]
    public class NamespaceSetup {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context) {
            MgtsSeleniumContext.Inst.Init();
        }

        [AssemblyCleanup]
        public static void SuiteTearDown() {
            MgtsSeleniumContext.Inst.Destroy();
        }
    }
}
﻿/**
 * Created by VolkovA on 27.02.14.
 */

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using selenium.mgts.Framework.ConnectApplication;
using selenium.mgts.Tests.Base;

namespace selenium.mgts.Tests.ConnectApplication {
    [TestClass]
    public class CheckAddressTest : MgtsTestBase<CheckAddressPage> {
        [TestInitialize]
        public void TestSetup() {
            Page = Browser.Go.ToPage<CheckAddressPage>();
        }

        [TestMethod]
        public void GPON_accessible() {
            // .Arrange
            // .Act
            Page.StreetInput.TypeIn("улица Вешних Вод");
            Page.HouseNumberInput.TypeIn(2);
            Page.BuildingInput.TypeIn(2);
            Page.ContinueButton.Click();
            // .Assert
            Page.SelectServices.AssertGponWillBeAccessibleAt(new DateTime(2014, 4, 11));
        }
    }
}
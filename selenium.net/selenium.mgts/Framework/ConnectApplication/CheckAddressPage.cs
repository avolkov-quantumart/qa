﻿/**
 * Created by VolkovA on 27.02.14.
 */

using System;
using OpenQA.Selenium;
using selenium.core.Framework.PageElements;
using selenium.mgts.Framework.Base;

namespace selenium.mgts.Framework.ConnectApplication {
    public class CheckAddressPage : MgtsPageBase {
        [SimpleWebComponent(ComponentName = "Номер дома", ID = "Address_Building")]
        public WebInput BuildingInput;

        [SimpleWebComponent(ComponentName = "Продолжить", LinkText = "Продолжить")]
        public WebButton ContinueButton;

        [SimpleWebComponent(ComponentName = "Номер дома", ID = "Address_House")]
        public WebInput HouseNumberInput;

        public SelectServicesComponent SelectServices;

        [SimpleWebComponent(ComponentName = "Название улицы", ID = "Address_Street")]
        public WebInput StreetInput;

        public override string AbsolutePath {
            get { return "gpon/checkaddress/"; }
        }

        // Выбрать регион
        public void SelectRegion(String region) {
            Log.Action("Выбрать регион '{0}'", region);
            Action.select(By.Id("Address_City"), region);
        }
    }
}
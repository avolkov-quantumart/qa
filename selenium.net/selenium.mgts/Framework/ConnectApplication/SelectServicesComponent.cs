﻿/**
 * Created by VolkovA on 27.02.14.
 */

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using selenium.core.Framework.Page;

namespace selenium.mgts.Framework.ConnectApplication {
    public class SelectServicesComponent : ComponentBase {
        private readonly By _connectionMessage1Selector;
        private readonly By _connectionMessage2Selector;

        public SelectServicesComponent(IPage parent)
            : base(parent) {
            _connectionMessage1Selector = By.CssSelector("#formRequest .intro-text p:nth-of-type(1)");
            _connectionMessage2Selector = By.CssSelector("#formRequest .intro-text p:nth-of-type(2)");
        }

        // Проверяет что отображается сообщение о том что GPON будет доступен к указанной дате
        public void AssertGponWillBeAccessibleAt(DateTime date) {
            String msg1 = Get.Text(_connectionMessage1Selector);
            String msg2 = Get.Text(_connectionMessage2Selector);
            Assert.AreEqual(msg1,
                          "Предлагаем подключить услуги Интернет, Домашнее цифровое ТВ по технологии ADSL и установить Домашний телефон.");
            String msg2Expected = string.Format("До {0} Ваш дом будет подключен к технологии GPON с возможностью " +
                                                "пользоваться услугами Интернет на скорости до 350 Мбит/с и смотреть " +
                                                "цифровое телевидение высокой четкости.", date.ToString("dd.MM.yyyy"));
            Assert.AreEqual("Предлагаем подключить услуги Интернет, Домашнее цифровое ТВ по технологии ADSL " +
                            "и установить Домашний телефон.", msg1);
            Assert.AreEqual(msg2Expected, msg2);
        }

        public override bool IsVisible() {
            throw new NotImplementedException();
        }
    }
}
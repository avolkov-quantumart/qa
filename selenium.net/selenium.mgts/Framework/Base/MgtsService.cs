/**
 * Created by VolkovA on 28.02.14.
 */

using selenium.core.Framework.Service;

namespace selenium.mgts.Framework.Base {
    public class MgtsService : ServiceImpl {
        public MgtsService(BaseUrlInfo defaultBaseUrlInfo, BaseUrlPattern pattern, Router router)
            : base(defaultBaseUrlInfo, pattern, router) {
        }
    }
}
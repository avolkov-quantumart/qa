/**
* Created by VolkovA on 28.02.14.
*/

using System.Collections.Generic;
using selenium.core.Framework.Service;
using selenium.mgts.Framework.Base;

public class MgtsServiceFactory : ServiceFactory {
    #region ServiceFactory Members

    public Router createRouter() {
        var router = new SelfMatchingPagesRouter();
        router.RegisterDerivedPages<MgtsPageBase>();
        return router;
    }

    public BaseUrlPattern createBaseUrlPattern() {
        var urlRegexBuilder = new BaseUrlRegexBuilder(new List<string> {"mgts.ru"});
        return new BaseUrlPattern(urlRegexBuilder.Build());
    }

    public BaseUrlInfo getDefaultBaseUrlInfo() {
        return new BaseUrlInfo(null, "mgts.ru", "/");
    }

    public Service createService() {
        return new MgtsService(getDefaultBaseUrlInfo(), createBaseUrlPattern(), createRouter());
    }

    #endregion
}
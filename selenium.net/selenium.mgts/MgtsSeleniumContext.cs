/**
 * Created by VolkovA on 28.02.14.
 */

using selenium.core;
using selenium.core.Framework.Service;

namespace selenium.mgts {
    public class MgtsSeleniumContext : SeleniumContext<MgtsSeleniumContext> {
        protected override void InitWeb() {
            try {
                Web.RegisterService(new MgtsServiceFactory());
            }
            catch (RouterInitializationException e) {
                Log.FatalError("Unable to initialize service", e);
            }
        }
    }
}
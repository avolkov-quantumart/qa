namespace selenium.core.Framework.Page {
    public interface IItem {
        string ID { get; }
        string ItemScss { get; }
    }
}
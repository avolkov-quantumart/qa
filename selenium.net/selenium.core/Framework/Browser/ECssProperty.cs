using core.Extensions;

namespace selenium.core.Framework.Browser {
    public enum ECssProperty {
        [StringValue("left")]
        left,
        [StringValue("top")]
        top,
        [StringValue("width")]
        width,
        [StringValue("height")]
        height,
        [StringValue("background-image")]
        background_image
        
    }
}
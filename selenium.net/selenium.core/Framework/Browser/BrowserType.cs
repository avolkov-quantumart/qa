/**
 * Created by VolkovA on 27.02.14.
 */

namespace selenium.core.Framework.Browser {
    public enum BrowserType {
        FIREFOX,
        CHROME
    }
}
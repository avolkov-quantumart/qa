﻿namespace GMail.Pages {
    public class PersonalInfoPage : GMailPageBase {
        public override string AbsolutePath {
            get { return "settings/personalinfo"; }
        }
    }
}

﻿using System;
using OpenQA.Selenium;
using selenium.core.Framework.Page;
using selenium.core.Framework.PageElements;

namespace GMail.Pages {
    public abstract class GMailPageBase : SelfMatchingPageBase {
        public ServicesTable ServicesTable;
    }

    public class ServicesTable : ComponentBase {
        [SimpleWebComponent(Css = "#gbwa>.gb_Ra>.gb_Aa")]
        public WebLink Icon;

        public ServicesTable(IPage parent)
            : base(parent) {
        }

        public override bool IsVisible() {
            return Is.Visible(By.CssSelector("#gbwa>div:nth-child(2)"));
        }

        /// <summary>
        /// Открыть компонент
        /// </summary>
        public void Open() {
            Open(() => Icon.Click());
        }

        public void SwitchToGMail() {
            SwitchTo(EGoogleService.GMail);
        }

        /// <summary>
        /// Перейти на указанный сервия Google
        /// </summary>
        public void SwitchTo(EGoogleService service) {
            Open();
            switch (service) {
                case EGoogleService.GMail:
                    Action.Click(By.Id("#gb23"));
                    break;
                default:
                    throw new ArgumentOutOfRangeException("service");
            }
        }
    }

    public enum EGoogleService {
        GMail
    }
}

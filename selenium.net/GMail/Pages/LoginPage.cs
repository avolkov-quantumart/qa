﻿using selenium.core.Framework.PageElements;

namespace GMail.Pages {
    public class LoginPage : GMailPageBase {
        [SimpleWebComponent(ID = "Email",ComponentName = "Email")]
        public WebInput Email;

        [SimpleWebComponent(ID = "Passwd",ComponentName = "Пароль")]
        public WebInput Password;

        [SimpleWebComponent(ID = "signIn",ComponentName = "Войти")]
        public WebButton SignIn;

        public override string AbsolutePath {
            get { return "ServiceLogin"; }
        }

        public void Login(string email, string password) {
            Email.TypeIn(email);
            Password.TypeIn(password);
            SignIn.ClickAndWaitForRedirect();
        }
    }
}

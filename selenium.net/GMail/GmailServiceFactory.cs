﻿using System;
using System.Collections.Generic;
using GMail.Pages;
using selenium.core.Framework.Service;

namespace GMail {
    public class GMailServiceFactory : ServiceFactory {
        public Router createRouter() {
            var router = new SelfMatchingPagesRouter();
            router.RegisterDerivedPages<GMailPageBase>();
            return router;
        }

        public BaseUrlPattern createBaseUrlPattern() {
            var urlRegexBuilder = new BaseUrlRegexBuilder(new List<string> {"google.com"});
            return new BaseUrlPattern(urlRegexBuilder.Build());
        }

        public BaseUrlInfo getDefaultBaseUrlInfo() {
            return new BaseUrlInfo("accounts", "google.com", "/");
        }

        public Service createService() {
            return new GMailService(getDefaultBaseUrlInfo(), createBaseUrlPattern(), createRouter());
        }
    }

    public class GMailService : ServiceImpl {
        public GMailService(BaseUrlInfo defaultBaseUrlInfo, BaseUrlPattern baseUrlPattern, Router router)
            : base(defaultBaseUrlInfo, baseUrlPattern, router) {
        }
    }
}

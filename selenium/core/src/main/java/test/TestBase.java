package test;

import framework.browser.Browser;
import framework.page.Page;
import logging.TestLogger;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by VolkovA on 26.02.14.
 */
public abstract class TestBase extends Assert {
    public SeleniumContext seleniumContext;

    @BeforeSuite(alwaysRun = true)
    public void suiteSetUp(ITestContext ngContext){
        seleniumContext = initSeleniumContext(ngContext);
    }

    protected Browser browser(){
        return seleniumContext.getBrowser();
    }

    protected TestLogger log(){
        return seleniumContext.getLogger();
    }

    // Инициализация тестового контекста
    protected abstract SeleniumContext initSeleniumContext(ITestContext ngContext);

    @AfterSuite(alwaysRun=true)
    public void suiteTearDown(){
        browser().Destroy();
    }

    // Создать не иницилизированный дополнительными параметрами экземпляр страницы
    protected <T extends Page> T pdummy(Class<T> pageClass) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        T page = pageClass.newInstance();
        page.Activate(seleniumContext);
        return page;
    }

    // Приведение объекта текущего объекта страницы к указанному типу
    protected  <T extends Page> T pas(Class<T> pageClass) {
        return browser().state.PageAs(pageClass);
    }
}
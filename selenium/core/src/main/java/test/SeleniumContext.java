package test;

import framework.browser.Browser;
import framework.browser.BrowserType;
import framework.browser.BrowsersCache;
import framework.services.Web;
import logging.TestLogger;
import logging.TestLoggerImpl;
import org.testng.ITestContext;

/**
 * Created by VolkovA on 26.02.14.
 */
public abstract class SeleniumContext {
    // Кэш браузеров.
    private BrowsersCache _browsersCache;
    // Логгер тестов
    private TestLogger _log;
    // Коллекция зарегистрированных сервисов
    private Web _web;
    // Тестовый контекст который инициализируется в TestNG
    // Содержит парамаетры запуска теста описанные в testing.xml
    private ITestContext testngContext;

    public Web getWeb() {
        return _web;
    }

    public TestLogger getLogger() {
        return _log;
    }

    public Browser getBrowser(){
        return _browsersCache.getBrowser(BrowserType.CHROME);
    }

    public SeleniumContext(ITestContext testngContext) {
        this.testngContext = testngContext;
        _log = new TestLoggerImpl();
        _web = initWeb();
        this._browsersCache = new BrowsersCache(this);
    }

    // Инициализация тестового окружения
    protected abstract Web initWeb();
}

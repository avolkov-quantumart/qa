package logging;

import framework.services.RouterInitializationException;

/**
 * Created by VolkovA on 27.02.14.
 */
public interface TestLogger {
    // Залогировать действие
    public void Action(String message, Object... args);
    // Залогировать критическуюж ошибку
    void FatalError(String s, Throwable e);
}

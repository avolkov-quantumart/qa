package framework.page;

import framework.browser.Browser;
import framework.services.RequestData;
import logging.TestLogger;
import test.SeleniumContext;

/**
 * Created by VolkovA on 27.02.14.
 * Базовый класс для страниц со статичным Url
 */
public abstract class PageWithFixedUrl extends PageImpl{
    public abstract String getPath();
    public abstract boolean isPageFor(RequestData requestData);
}

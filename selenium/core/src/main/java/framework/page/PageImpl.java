package framework.page;

import framework.browser.Browser;
import framework.services.BaseUrlInfo;
import logging.TestLogger;
import test.SeleniumContext;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by VolkovA on 27.02.14.
 */
public abstract class PageImpl extends PageElementImpl implements Page {
    private BaseUrlInfo _baseUrlInfo;
    private Set<Component> _components;

    protected PageImpl() {
        _components = new HashSet<Component>();
    }

    @Override
    public BaseUrlInfo getBaseUrlInfo() {
        return _baseUrlInfo;
    }

    @Override
    public void setBaseUrlInfo(BaseUrlInfo _baseUrlInfo) {
        this._baseUrlInfo = _baseUrlInfo;
    }

    // Перейти на страницу, соответствующую текущему классу
    public void go(){
        browser().go.ToPage(this);
    }

    @Override
    public void Activate(SeleniumContext seleniumContext) {
        super.Activate(seleniumContext);
        for(Component component:_components)
            component.Activate(seleniumContext);
    }

    // Зарегистрировать компонент
    public <T extends Component> T registerComponent(Class<T> componentClass){
        try {
            T component = componentClass.newInstance();
            _components.add(component);
            return component;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
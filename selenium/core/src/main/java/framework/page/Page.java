package framework.page;

import framework.browser.Browser;
import framework.services.BaseUrlInfo;
import logging.TestLogger;
import test.SeleniumContext;

/**
 * Created by VolkovA on 27.02.14.
 */
public interface Page extends PageElement {
    BaseUrlInfo getBaseUrlInfo();
    void setBaseUrlInfo(BaseUrlInfo baseUrlInfo);
}

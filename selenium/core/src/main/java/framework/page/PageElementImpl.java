package framework.page;

import framework.browser.Browser;
import logging.TestLogger;
import test.SeleniumContext;

/**
 * Created by VolkovA on 06.03.14.
 */
public class PageElementImpl implements PageElement {
    private SeleniumContext _seleniumContext;
    @Override
    public void Activate(SeleniumContext seleniumContext) {
        _seleniumContext = seleniumContext;
    }

    @Override
    public Browser browser() {
        return _seleniumContext.getBrowser();
    }

    @Override
    public TestLogger log(){
        return _seleniumContext.getLogger();
    }
}

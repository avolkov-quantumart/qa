package framework.page;

import framework.browser.Browser;
import logging.TestLogger;
import test.SeleniumContext;

/**
 * Created by VolkovA on 06.03.14.
 */
public interface PageElement {
    void Activate(SeleniumContext seleniumContext);
    Browser browser();
    TestLogger log();
}

package framework.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by VolkovA on 27.02.14.
 */
public class FirefoxDriverManager implements DriverManager {
    FirefoxDriver _driver;

    public WebDriver getDriver(){
        return _driver;
    }

    @Override
    public void initDriver() {
        _driver = new FirefoxDriver();
    }

    @Override
    public void destroyDriver() {
        _driver.quit();
    }
}

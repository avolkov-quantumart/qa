package framework.browser;

import test.SeleniumContext;

/**
 * Created by VolkovA on 26.02.14.
 */
public class Browser extends DriverFacade{
    private DriverManager _driverManager;

    public BrowserWait wait;
    public BrowserGo go;
    public BrowserWindow window;
    public BrowserAction action;
    public BrowserState state;
    public BrowserAlert alert;
    public BrowserIs is;
    public BrowserGet get;
    public BrowserFind find;

    public Browser(SeleniumContext seleniumContext, DriverManager driverManager) {
        super(seleniumContext,null);
        _driverManager = driverManager;
        _driverManager.initDriver();
        driver = _driverManager.getDriver();
        find = new BrowserFind(seleniumContext, driver);
        get = new BrowserGet(seleniumContext, driver,find);
        is = new BrowserIs(seleniumContext,driver);
        alert = new BrowserAlert(seleniumContext, driver);
        state = new BrowserState(seleniumContext, driver);
        action = new BrowserAction(seleniumContext, driver,find);
        window = new BrowserWindow(seleniumContext,driver);
        go = new BrowserGo(seleniumContext,driver,state);
        wait = new BrowserWait(seleniumContext,driver);
    }

    // Уничтожить драйвер(закрывает все открытые окна браузер)
    public void Destroy(){
        _driverManager.destroyDriver();
    }

    // Пересоздать драйвер
    public void Recreate(){
    }
}




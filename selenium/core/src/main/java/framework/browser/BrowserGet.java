package framework.browser;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import test.SeleniumContext;

/**
* Created by VolkovA on 27.02.14.
*/ // Методы для получения данных со страницы
public class BrowserGet extends DriverFacade{
    BrowserFind find;
    public BrowserGet(SeleniumContext seleniumContext,WebDriver driver, BrowserFind find) {
        super(seleniumContext,driver);
        this.find = find;
    }

    // Получить содержимое элемента
    public String text(By by) {
        return find.element(by).getText();
    }
}

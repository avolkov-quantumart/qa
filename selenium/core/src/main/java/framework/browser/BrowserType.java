package framework.browser;

/**
 * Created by VolkovA on 27.02.14.
 */
public enum BrowserType {
    FIREFOX,
    CHROME
}

package framework.browser;

import org.openqa.selenium.WebDriver;
import test.SeleniumContext;

/**
* Created by VolkovA on 27.02.14.
*/ // Методы для ожидания изменения состояния браузера
public class BrowserWait extends DriverFacade{
    public BrowserWait(SeleniumContext seleniumContext, WebDriver driver) {
        super(seleniumContext,driver);
    }
}

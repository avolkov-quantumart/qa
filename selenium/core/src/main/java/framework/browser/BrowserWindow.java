package framework.browser;

import org.openqa.selenium.WebDriver;
import test.SeleniumContext;

/**
* Created by VolkovA on 27.02.14.
*/ // Методы для работы с окнами/вкладками браузера
public class BrowserWindow extends DriverFacade{
    public BrowserWindow(SeleniumContext seleniumContext,WebDriver driver) {
        super(seleniumContext,driver);
    }
}

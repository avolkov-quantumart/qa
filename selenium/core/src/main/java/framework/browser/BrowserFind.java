package framework.browser;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import test.SeleniumContext;

/**
* Created by VolkovA on 27.02.14.
*/ // Методы для поиска элементов на странице
public class BrowserFind extends DriverFacade {
    public BrowserFind(SeleniumContext seleniumContext,WebDriver driver) {
        super(seleniumContext,driver);
    }

    public WebElement element(By by) {
        return driver.findElement(by);
    }
}

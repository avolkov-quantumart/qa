package framework.browser;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import test.SeleniumContext;

/**
* Created by VolkovA on 27.02.14.
*/ // Методы для выполнения действий со страницей
public class BrowserAction extends DriverFacade{
    private BrowserFind find;

    public BrowserAction(SeleniumContext seleniumContext,WebDriver driver, BrowserFind find) {
        super(seleniumContext,driver);
        this.find = find;
    }

    // Выбрать опцию в html теге select
    public void select(By by, String value) {
        WebElement select = find.element(by);
        Select dropDown = new Select(select);
        dropDown.selectByValue(value);
    }

    // Ввести значение в поле ввода
    public void typeIn(By by, Object value) {
        WebElement element = find.element(by);
        element.sendKeys(value.toString());
    }

    // Клик по элементу
    public void click(By by) {
        WebElement element = find.element(by);
        element.click();
    }
}
package framework.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by VolkovA on 03.03.14.
 */
public class ChromeDriverFacrory implements DriverManager {
    private WebDriver _driver;
    private ChromeDriverService _service;

    @Override
    public void initDriver() {
        //File chromedriverFile = new File(Paths.get("").toUri().getRawPath() + "core/src/main/resources/chromedriver.exe");
        File chromedriverFile = new File("c://selenium/chromedriver.exe");
        _service = new ChromeDriverService.Builder()
                .usingDriverExecutable(chromedriverFile)
                .usingAnyFreePort()
                .build();
        try{
            _service.start();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        _driver = new RemoteWebDriver(_service.getUrl(),
                DesiredCapabilities.chrome());
    }

    @Override
    public WebDriver getDriver() {
        return _driver;
    }

    @Override
    public void destroyDriver() {
        _driver.quit();
        _service.stop();
    }

}

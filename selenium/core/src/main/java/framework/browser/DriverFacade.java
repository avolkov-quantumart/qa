package framework.browser;

import framework.services.Web;
import logging.TestLogger;
import org.openqa.selenium.WebDriver;
import test.SeleniumContext;

/**
* Created by VolkovA on 27.02.14.
*/
public abstract class DriverFacade {
    protected WebDriver driver;
    protected SeleniumContext seleniumContext;
    public DriverFacade(SeleniumContext seleniumContext, WebDriver driver) {
        this.seleniumContext = seleniumContext;
        this.driver = driver;
    }

    protected Web web(){
        return seleniumContext.getWeb();
    }

    protected TestLogger log(){
        return seleniumContext.getLogger();
    }


}

package framework.browser;

import org.openqa.selenium.WebDriver;
import test.SeleniumContext;

/**
* Created by VolkovA on 27.02.14.
*/ // Методы для проверки состояния страниц
public class BrowserIs extends DriverFacade{
    public BrowserIs(SeleniumContext seleniumContext, WebDriver driver) {
        super(seleniumContext,driver);
    }
}

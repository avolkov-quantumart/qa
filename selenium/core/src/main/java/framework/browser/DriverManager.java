package framework.browser;

import org.openqa.selenium.WebDriver;

import java.io.IOException;

/**
 * Created by VolkovA on 27.02.14.
 */
public interface DriverManager {
    void initDriver();
    WebDriver getDriver();
    void destroyDriver();
}

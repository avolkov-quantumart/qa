package framework.browser;

import framework.page.Page;
import framework.services.RequestData;
import framework.services.Web;
import logging.TestLogger;
import org.openqa.selenium.WebDriver;
import test.SeleniumContext;

/**
* Created by VolkovA on 27.02.14.
*/ // Методы для навигации по страницам
public class BrowserGo extends DriverFacade{
    private BrowserState _state;

    public BrowserGo(SeleniumContext seleniumContext,WebDriver driver, BrowserState state) {
        super(seleniumContext, driver);
        _state = state;
    }

    // Определение Url, соответствующее классу страницы и переход на него
    public void ToPage(Page page){
        RequestData requestData = web().getRequestData(page);
        ToUrl(requestData);
    }

    public void ToUrl(String url){
        ToUrl(new RequestData(url));
    }

    // Переход на указанный Url в текущем окне браузера
    public void ToUrl(RequestData requestData){
        log().Action("Navigating to url: %s", requestData.getUrl());
        driver.navigate().to(requestData.getUrl());
        _state.Actualize();
    }
}

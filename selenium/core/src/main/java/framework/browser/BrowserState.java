package framework.browser;

import com.thoughtworks.selenium.Selenium;
import framework.page.Page;
import framework.services.RequestData;
import framework.services.Service;
import framework.services.ServiceMatchResult;
import framework.services.Web;
import logging.TestLogger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import test.SeleniumContext;

/**
* Created by VolkovA on 27.02.14.
*/ // Текущее состояние браузера
public class BrowserState extends DriverFacade{
    // Объект для работы со страницей в активном окне браузера
    public Page Page;

    // Объект для работы с системным алертом, отображаемым в активной странице браузера
    public Alert SystemAlert;

    // Объект для работы с html имитацией алерта, отображаемой в активной странице браузера
    public Alert HtmlAlert;

    // Получить активный алерт
    // Когда нужно убрать алерты для продолжения работы со страницей нужно сначала закрыть системный алерт,
    // потом html алерт. Для получения очередного алерта который нужно закрыть можно использовать данную функцию.
    public Alert getActiveAlert(){
        return SystemAlert!=null?SystemAlert:HtmlAlert;
    }

    public BrowserState(SeleniumContext seleniumContext, WebDriver driver) {
        super(seleniumContext,driver);
    }

    // Приведение текущей страницы к нужному типу
    public <T extends Page> T PageAs(Class<T> pageClass){
        return (T)Page;
    }

    // Проверка соответствия класса текущей страницы указанному типу
    public <T extends Page> boolean PageIs(Class<T> pageClass){
        if(Page==null)
            return false;
        return Page.getClass().equals(pageClass);
    }

    // Определение текущего состояния браузера
    public void Actualize(){
        RequestData requestData = new RequestData(driver.getCurrentUrl(),
                driver.manage().getCookies());
        ActualizePage(requestData);
    }

    // Определение класса для работы с текущей активной страницей браузера
    private void ActualizePage(RequestData requestData){
        Page = null;
        try{
            ServiceMatchResult result = web().matchService(requestData);
            if(result!=null){
                Page = result.getService().getPage(requestData,result.getBaseUrlInfo());
                Page.Activate(seleniumContext);
            }
        }
        catch(Exception e){
            log().FatalError("Unable to get service for url",e);
        }
    }
}

package framework.browser;

import test.SeleniumContext;

import java.util.HashMap;

/**
 * Created by VolkovA on 26.02.14.
 */
public class BrowsersCache {
    private HashMap<BrowserType,Browser> _browsers;
    private SeleniumContext _seleniumContext;


    public BrowsersCache(SeleniumContext seleniumContext) {
        _seleniumContext = seleniumContext;
        _browsers = new HashMap<BrowserType, Browser>();
    }

    public Browser getBrowser(BrowserType browserType){
        if(_browsers.containsKey(browserType))
            return _browsers.get(browserType);
        Browser browser = createBrowser(browserType);
        _browsers.put(browserType,browser);
        return browser;
    }

    private Browser createBrowser(BrowserType browserType) {
        DriverManager driverManager = getDriverFactory(browserType);
        return new Browser(_seleniumContext, driverManager);
    }

    private DriverManager getDriverFactory(BrowserType browserType) {
        switch (browserType){
            case FIREFOX:
                return new FirefoxDriverManager();
            case CHROME:
                return new ChromeDriverFacrory();
            default:
                return null;
        }
    }
}

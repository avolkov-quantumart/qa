package framework.services;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by VolkovA on 03.03.14.
 */
public class BaseUrlInfo {
    public String getSubDomain() {
        return _subDomain;
    }

    public String getDomain() {
        return _domain;
    }

    public String getAbsolutePath() {
        return _absolutePath;
    }

    private String _subDomain;
    private String _domain;
    private String _absolutePath;

    public BaseUrlInfo(String subDomain, String domain, String absolutePath) {
        _subDomain = subDomain;
        _domain = domain;
        _absolutePath = absolutePath;
    }

    public BaseUrlInfo ApplyActual(BaseUrlInfo baseUrlInfo) {
        String subDomain = baseUrlInfo==null || StringUtils.isEmpty(baseUrlInfo.getSubDomain())?
                getSubDomain():
                baseUrlInfo.getSubDomain();
        String domain = baseUrlInfo==null || StringUtils.isEmpty(baseUrlInfo.getDomain())?
                getDomain():
                baseUrlInfo.getDomain();
        String absolutePath = baseUrlInfo == null || StringUtils.isEmpty(baseUrlInfo.getAbsolutePath())?
                getAbsolutePath():
                baseUrlInfo.getAbsolutePath();
        return new BaseUrlInfo(subDomain,domain,absolutePath);
    }

    // Сформировать BaseUrl
    public String getBaseUrl() {
        String s = getDomain()+getAbsolutePath();
        if(StringUtils.isNotEmpty(getSubDomain()))
            s=getSubDomain()+"."+s;
        return s;
    }
}

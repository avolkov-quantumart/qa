package framework.services;

public class BaseUrlMatchResult{
    public BaseUrlMatchLevel Level;
    public String Domain;
    public String SubDomain;
    public String AbsolutePath;

    public static BaseUrlMatchResult Unmatched(){
        return new BaseUrlMatchResult(BaseUrlMatchLevel.Unmatched,null,null,null);
    }

    public BaseUrlMatchResult(BaseUrlMatchLevel level, String subDomain, String domain,  String absolutePath) {
        Level = level;
        SubDomain = subDomain;
        Domain = domain;
        AbsolutePath = absolutePath;
    }

    public BaseUrlInfo getBaseUrlInfo(){
        return new BaseUrlInfo(SubDomain,  Domain, AbsolutePath);
    }
}

package framework.services;

import framework.page.Page;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by VolkovA on 27.02.14.
 * Коллекция поддерживаемых сервисов
 */
public class Web {
    // Список зарегистрированных сервисов
    Set<Service> _services;

    public Web() {
        _services = new HashSet<Service>();
    }

    // Определение сервиса, который должен обработать запрос(DNS маршрутизация и маршрутизация внутри домена)
    public ServiceMatchResult matchService(RequestData request) throws Exception {
        ServiceMatchResult baseDomainMatch=null;
        for(Service service:_services){
            BaseUrlPattern baseUrlPattern = service.getBaseUrlPattern();
            BaseUrlMatchResult result = baseUrlPattern.Match(request.getUrl());
            if(result.Level==BaseUrlMatchLevel.FullDomain)
                return new ServiceMatchResult(service, result.getBaseUrlInfo());
            if(result.Level==BaseUrlMatchLevel.BaseDomain){
                if(baseDomainMatch!=null)
                    throw new Exception(String.format("Two BaseDomain matches for url %s",request.getUrl()));
                baseDomainMatch = new ServiceMatchResult(service,result.getBaseUrlInfo());
            }
        }
        return baseDomainMatch;
    }

    // Поиск страницы в зарегистрированных сервисах
    // и получение ее Url
    public RequestData getRequestData(Page page) {
        for(Service service:_services){
            RequestData requestData = service.getRequestData(page);
            if(requestData==null)
                continue;
            return requestData;
        }
        return null;
    }

    // Зарегистрировать сервис
    public void registerService(ServiceFactory serviceFactory) throws RouterInitializationException {
        _services.add(serviceFactory.createService());
    }
}

package framework.services;

import org.openqa.selenium.Cookie;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by VolkovA on 27.02.14.
 */
public class RequestData {
    private String _url;
    private Set<Cookie> _cookies;

    public RequestData(String url) {
        this(url,new HashSet<Cookie>());
    }

    public RequestData(String url, Set<Cookie> cookies) {
        this._url = url;
        this._cookies = cookies;
    }

    public String getUrl() {
        return _url;
    }

    public void setUrl(String url) {
        this._url = url;
    }

    public Set<Cookie> getCookies() {
        return _cookies;
    }

    public void setCookies(Set<Cookie> cookies) {
        this._cookies = cookies;
    }
}

package framework.services;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by VolkovA on 28.02.14.
 */
public class BaseUrlRegexBuilder {
    String subDomainPattern ="((?<optionalsubdomain>[^\\.]+)\\.)?";
    String domainPattern = null;
    String absolutePathPattern ="";

    public BaseUrlRegexBuilder(String domain) {
        this(new HashSet<String>(Arrays.asList(domain)));
    }

    public BaseUrlRegexBuilder(Set<String> domains) {
        domainPattern = generateDomainsPattern(domains);
    }

    private String generateDomainsPattern(Set<String> domains) {
        String s = "";
        for(String domain: domains)
            s+=domain+"|";
        s=s.substring(0,s.length()-1);
        s=s.replace(".","\\.");
        return String.format("(?<domain>(%s))",s);
    }

    public void setSubDomain(String value){
        subDomainPattern = String.format("(?<subdomain>%s)\\.",value);
    }

    public void setAbsolutePathPattern(String pattern){
        absolutePathPattern = String.format("(?<abspath>\\/%s)",pattern);
    }

    // Сформировать Regex паттерн для BaseUrl сервиса
    public String Build(){
        return  "(http(|s)://|)(www.|)" +
                subDomainPattern+
                domainPattern +
                absolutePathPattern+
                ".*";
    }
}

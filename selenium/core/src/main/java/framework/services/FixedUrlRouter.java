package framework.services;

import framework.page.Page;
import framework.page.PageWithFixedUrl;

import java.net.MalformedURLException;

/**
 * Created by VolkovA on 28.02.14.
 */
public class FixedUrlRouter extends RouterBase{
    public void RegisterPage(PageWithFixedUrl page){
        registerRequestAction(new FixedUrlRequestAction(page));
    }

    @Override
    public RequestData getRequest(Page page, BaseUrlInfo defaultBaseUrlInfo) {
        for(RequestAction requestAction: requestActions) {
            RequestData request;
            request = requestAction.getRequest(page, defaultBaseUrlInfo);
            if(request!=null)
                return request;
        }
        return null;
    }

    @Override
    public Page getPage(RequestData requestData, BaseUrlInfo baseUrlInfo) throws IllegalAccessException, InstantiationException, MalformedURLException {
        for(RequestAction requestAction: requestActions) {
            Page page = requestAction.getPage(requestData, baseUrlInfo);
            if(page!=null)
                return page;
        }
        return null;
    }
}
package framework.services;

public enum BaseUrlMatchLevel{
    Unmatched,
    FullDomain,
    BaseDomain
}

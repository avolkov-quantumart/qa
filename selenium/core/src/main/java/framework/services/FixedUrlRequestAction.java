package framework.services;

import framework.page.Page;
import framework.page.PageWithFixedUrl;
import org.apache.commons.lang3.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;

public class FixedUrlRequestAction implements RequestAction{
    private PageWithFixedUrl _dummyPage;
    public FixedUrlRequestAction(PageWithFixedUrl dummyPage) {
        _dummyPage = dummyPage;
    }

    @Override
    public Page getPage(RequestData requestData, BaseUrlInfo baseUrlInfo) throws MalformedURLException, IllegalAccessException, InstantiationException {
        URL urlObj = new URL(requestData.getUrl());
        String realPath = urlObj.getPath();
        realPath = realPath.substring(baseUrlInfo.getAbsolutePath().length());
        String pagePath = _dummyPage.getPath();
        if(!pagePath.equals(realPath))
            return null;
        PageWithFixedUrl page = _dummyPage.getClass().newInstance();
        page.setBaseUrlInfo(baseUrlInfo);
        return page;
    }

    @Override
    public RequestData getRequest(Page page, BaseUrlInfo defaultBaseUrlInfo) {
        if(page.getClass()!=_dummyPage.getClass())
            return null;
        PageWithFixedUrl pageWithFixedUrl = (PageWithFixedUrl)page;
        BaseUrlInfo baseUrlInfo = defaultBaseUrlInfo.ApplyActual(page.getBaseUrlInfo());
        String url = String.format("http://%s%s", baseUrlInfo.getBaseUrl(), pageWithFixedUrl.getPath());
        return new RequestData(url);
    }
}

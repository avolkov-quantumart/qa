package framework.services;

import framework.page.Page;

import java.net.MalformedURLException;

/**
 * Created by VolkovA on 27.02.14.
 */
public interface Router {
    RequestData getRequest(Page page, BaseUrlInfo defaultBaseUrlInfo);
    Page getPage(RequestData requestData, BaseUrlInfo baseUrlInfo) throws IllegalAccessException, InstantiationException, MalformedURLException;
}


package framework.services;

import org.apache.commons.lang3.StringUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BaseUrlPattern{
    private String _regexPattern;

    public BaseUrlPattern(String regexPattern) {
        _regexPattern = regexPattern;
    }

    // Соответствует ли указанный Url шаблону
    public BaseUrlMatchResult Match(String url) {
        Pattern p = Pattern.compile(_regexPattern);
        Matcher m = p.matcher(url);
        if(!m.matches())
            return BaseUrlMatchResult.Unmatched();
        String domain = m.group("domain");
        String abspath = hasGroup(m,"abspath")?m.group("abspath"):"/";

        // У сервиса есть жестко заданный поддомен и он совпадает с поддоменом в Url
        if(hasGroup(m,"subdomain"))
            return new BaseUrlMatchResult(BaseUrlMatchLevel.FullDomain,m.group("subdomain"),domain,abspath);

        String optionalsubdomain = m.group("optionalsubdomain");
        // У сервиса нет жестко заданного поддомена и в Url также нет поддомена
        if(StringUtils.isEmpty(optionalsubdomain))
            return new BaseUrlMatchResult(BaseUrlMatchLevel.FullDomain,null,domain,abspath);

        // У сервиса нет жестко заданного поддомена, но в Url поддомен имеется
        return new BaseUrlMatchResult(BaseUrlMatchLevel.BaseDomain, optionalsubdomain,domain, abspath);
    }

    // Проверить, имеется ли группа в паттерне
    private boolean hasGroup(Matcher matcher, String groupName){
        try {
            matcher.group(groupName);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}

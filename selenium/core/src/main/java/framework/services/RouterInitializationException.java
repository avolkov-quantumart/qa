package framework.services;

public class RouterInitializationException extends Exception{
    public RouterInitializationException(Throwable cause) {
        super(cause);
    }
}

package framework.services;

/**
 * Created by VolkovA on 03.03.14.
 */
public class ServiceMatchResult {
    private Service _service;

    public Service getService() {
        return _service;
    }

    public BaseUrlInfo getBaseUrlInfo() {
        return _baseUrlInfo;
    }

    private BaseUrlInfo _baseUrlInfo;

    public ServiceMatchResult(Service service, BaseUrlInfo baseUrlInfo) {
        _service = service;
        _baseUrlInfo = baseUrlInfo;
    }
}

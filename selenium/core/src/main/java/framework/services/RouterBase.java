package framework.services;

import java.util.HashSet;
import java.util.Set;

public abstract class RouterBase implements Router{
    protected Set<RequestAction> requestActions;

    protected RouterBase() {
        requestActions = new HashSet<RequestAction>();
    }

    protected void registerRequestAction(RequestAction requestAction){
        requestActions.add(requestAction);
    }
}

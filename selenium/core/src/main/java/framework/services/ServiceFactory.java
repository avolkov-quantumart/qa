package framework.services;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by VolkovA on 28.02.14.
 */
public interface ServiceFactory {
    // Создать маршрутизатор страниц(сопоставление Url-->Страницы) для сервиса
    Router createRouter() throws RouterInitializationException;
    // Создать сервис
    Service createService() throws RouterInitializationException;
    // Паттерн для Url, которым соответствует сервис
    BaseUrlPattern createBaseUrlPattern();
    // Дефолтные параметры базового Url
    BaseUrlInfo getDefaultBaseUrlInfo();
}

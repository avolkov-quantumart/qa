package framework.services;

import framework.page.Page;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by VolkovA on 28.02.14.
 */
public abstract class ServiceImpl implements Service {
    // Параметры BaseUrl по умолчанию
    private BaseUrlInfo _defaultBaseUrlInfo;
    // Шаблон для базовых Url, которым соответствует данный сервис
    private BaseUrlPattern _baseUrlPattern;
    // Маршрутизатор Url-->класс страницы, класс страницы-->Url
    protected Router _router;

    public ServiceImpl(BaseUrlInfo defaultBaseUrlInfo, BaseUrlPattern baseUrlPattern, Router router) {
        _defaultBaseUrlInfo = defaultBaseUrlInfo;
        _baseUrlPattern = baseUrlPattern;
        _router = router;
    }

    @Override
    public BaseUrlInfo getDefaultBaseUrlInfo(){
        return _defaultBaseUrlInfo;
    }

    @Override
    public Router getRouter() {
        return _router;
    }

    @Override
    public BaseUrlPattern getBaseUrlPattern() {
        return _baseUrlPattern;
    }
}

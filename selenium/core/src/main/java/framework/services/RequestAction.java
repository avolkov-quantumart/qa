package framework.services;

import framework.page.Page;

import java.net.MalformedURLException;
import java.util.Collection;

/**
 * Created by VolkovA on 27.02.14.
 */
public interface RequestAction {
    Page getPage(RequestData requestData, BaseUrlInfo baseUrlInfo) throws MalformedURLException, IllegalAccessException, InstantiationException;
    RequestData getRequest(Page page, BaseUrlInfo defaultBaseUrlInfo);
}

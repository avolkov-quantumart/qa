package framework.services;

import framework.page.Page;

import java.net.MalformedURLException;

/**
 * Created by VolkovA on 27.02.14.
 */
public interface Service {
    public String DEFAULT_ABSOLUTE_PATH = "/";
    // Роутер сервиса
    Router getRouter();
    // Получить паттерн, определяющий каким Url соответствует сервис
    BaseUrlPattern getBaseUrlPattern();
    // Получить параметры BaseUrl по умолчанию
    BaseUrlInfo getDefaultBaseUrlInfo();
    // Получить экземпляр класса страницы для указанного запроса
    Page getPage(RequestData requestData, BaseUrlInfo baseUrlInfo) throws IllegalAccessException, MalformedURLException, InstantiationException;
    // Получить запрос для указанного экземпляра класса страницы
    RequestData getRequestData(Page page);

}

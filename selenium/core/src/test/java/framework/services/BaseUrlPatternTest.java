package framework.services;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by VolkovA on 28.02.14.
 */
public class BaseUrlPatternTest extends Assert {
    @DataProvider(name = "matchData")
    public static Object[][] getCandidateLocalesData() {
        return new Object[][]{
                {null,new HashSet<String>(Arrays.asList("mts.ru")),null,"http://mts.ru",null,"mts.ru","/"},
                {null,new HashSet<String>(Arrays.asList("mts.ru")),null,"http://login.mts.ru","login","mts.ru","/"},
                {null,new HashSet<String>(Arrays.asList("mts.ru")),null,"http://mts.ru/en-US",null,"mts.ru","/"},
                {"spb",new HashSet<String>(Arrays.asList("mts.ru")),"\\w{2}-\\w{2}","http://spb.mts.ru/en-US/sdfsd","spb","mts.ru","/en-US"}
        };
    }

    @Test(dataProvider = "matchData")
    public void match(String subDomain,Set<String> domains, String absolutePathPattern,
                      String url, String expectedSubdomain, String expectedDomain, String expectedAbsolutePath) throws MalformedURLException {
        // .Arrange
        BaseUrlRegexBuilder builder = new BaseUrlRegexBuilder(domains);
        if(subDomain!=null)
            builder.setSubDomain(subDomain);
        if(absolutePathPattern!=null)
            builder.setAbsolutePathPattern(absolutePathPattern);
        BaseUrlPattern pattern = new BaseUrlPattern(builder.Build());

        // .Act
        BaseUrlMatchResult result = pattern.Match(url);

        // .Assert
        assertEquals(result.SubDomain,expectedSubdomain);
        assertEquals(result.Domain,expectedDomain);
        assertEquals(result.AbsolutePath,expectedAbsolutePath);
    }
}

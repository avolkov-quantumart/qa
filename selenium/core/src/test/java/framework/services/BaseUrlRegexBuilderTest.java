package framework.services;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.HashSet;

/**
 * Created by VolkovA on 28.02.14.
 */
public class BaseUrlRegexBuilderTest extends Assert {
    @Test
    public void pattern_starts_with_http_www(){
        // .Arrange
        BaseUrlRegexBuilder builder = new BaseUrlRegexBuilder("mts.ru");
        // .Act
        String pattern = builder.Build();
        // .Assert
        assertTrue(pattern.startsWith("(http(|s)://|)(www.|)"));
    }

    @Test
    public void build_pattern_for_one_domain(){
        // .Arrange
        BaseUrlRegexBuilder builder = new BaseUrlRegexBuilder("mts.ru");
        // .Act
        String pattern = builder.Build();
        // .Assert
        assertEquals(pattern,"(http(|s)://|)(www.|)((?<optionalsubdomain>[^\\.]+)\\.)?(?<domain>(mts\\.ru)).*");
    }

    @Test
    public void build_pattern_for_3_domains(){
        // .Arrange
        HashSet<String> domains = new HashSet<String>();
        domains.add("mts.ru");
        domains.add("mts.com");
        domains.add("mtsmirror.ru");
        BaseUrlRegexBuilder builder = new BaseUrlRegexBuilder(domains);
        // .Act
        String pattern = builder.Build();
        // .Assert
        assertEquals(pattern,"(http(|s)://|)(www.|)((?<optionalsubdomain>[^\\.]+)\\.)?(?<domain>(mts\\.ru|mts\\.com|mtsmirror\\.ru)).*");
    }

    @Test
    public void build_pattern_for_subdomain(){
        // .Arrange
        BaseUrlRegexBuilder builder = new BaseUrlRegexBuilder("mts.ru");
        // .Act
        builder.setSubDomain("login");
        String pattern = builder.Build();
        // .Assert
        assertEquals(pattern,"(http(|s)://|)(www.|)(?<subdomain>login)\\.(?<domain>(mts\\.ru)).*");
    }

    @Test
    public void build_pattern_for_abspath(){
        // .Arrange
        BaseUrlRegexBuilder builder = new BaseUrlRegexBuilder("mts.ru");
        // .Act
        builder.setAbsolutePathPattern("\\d{2}-\\d{2}");
        String pattern = builder.Build();
        // .Assert
        assertEquals(pattern,"(http(|s)://|)(www.|)((?<optionalsubdomain>[^\\.]+)\\.)?(?<domain>(mts\\.ru))(?<abspath>\\/\\d{2}-\\d{2}).*");
    }
}
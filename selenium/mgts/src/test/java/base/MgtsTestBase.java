package base;

import framework.ConnectApplication.CheckAddressPage;
import framework.base.MgtsSeleniumContext;
import org.testng.ITestContext;
import test.TestBase;
import test.SeleniumContext;

/**
 * Created by VolkovA on 27.02.14.
 */
public class MgtsTestBase extends TestBase {
    @Override
    protected SeleniumContext initSeleniumContext(ITestContext ngContext) {
        MgtsSeleniumContext seleniumContext = new MgtsSeleniumContext(ngContext);
        seleniumContext.initWeb();
        return seleniumContext;
    }
}

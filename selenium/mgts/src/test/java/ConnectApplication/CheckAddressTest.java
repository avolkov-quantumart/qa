package ConnectApplication;

import base.MgtsTestBase;
import framework.ConnectApplication.CheckAddressPage;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 * Created by VolkovA on 27.02.14.
 */
public class CheckAddressTest extends MgtsTestBase {
    @BeforeTest
    void TestSetup() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        pdummy(CheckAddressPage.class).go();
    }

    @Test(description = "Проверяет что для заданного адреса доступно подключение по технологии GPON")
    public void GPON_accessible() {
        // .Arrange
        CheckAddressPage _page = pas(CheckAddressPage.class);
        // .Act
        _page.typeInStreet("улица Вешних Вод");
        _page.typeInHouseNumber(2);
        _page.typeInBuilding(2);
        _page.ClickByNextButton();
        _page = pas(CheckAddressPage.class);
        // .Assert
        _page.selectServices.assertGponWillBeAccessibleAt(new GregorianCalendar(2014, 3, 4).getTime());
    }
}
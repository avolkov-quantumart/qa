package framework.base;

import framework.page.Page;
import framework.services.*;

import java.net.MalformedURLException;

/**
 * Created by VolkovA on 28.02.14.
 */
public class MgtsService extends ServiceImpl {
    public MgtsService(BaseUrlInfo defaultBaseUrlInfo, BaseUrlPattern pattern, Router router) {
        super(defaultBaseUrlInfo,pattern,router);
    }

    @Override
    public Page getPage(RequestData requestData, BaseUrlInfo baseUrlInfo) throws IllegalAccessException, MalformedURLException, InstantiationException {
        return _router.getPage(requestData, baseUrlInfo);
    }

    @Override
    public RequestData getRequestData(Page page) {
        return _router.getRequest(page, getDefaultBaseUrlInfo());
    }
}
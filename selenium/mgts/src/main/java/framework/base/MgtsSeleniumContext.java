package framework.base;

import framework.base.MgtsServiceFactory;
import framework.services.RouterInitializationException;
import framework.services.Web;
import org.testng.ITestContext;
import test.SeleniumContext;

/**
 * Created by VolkovA on 28.02.14.
 */
public class MgtsSeleniumContext extends SeleniumContext{
    public MgtsSeleniumContext(ITestContext testngContext) {
        super(testngContext);
    }

    @Override
    public Web initWeb() {
        Web web = new Web();
        try{
            web.registerService(new MgtsServiceFactory());
        }catch (RouterInitializationException e){
            getLogger().FatalError("Unable to initialize service", e);
        }
        return web;
    }

}

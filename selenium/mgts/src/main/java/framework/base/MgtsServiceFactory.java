package framework.base;

import framework.ConnectApplication.CheckAddressPage;
import framework.services.*;

import java.util.Arrays;
import java.util.HashSet;

/**
* Created by VolkovA on 28.02.14.
*/
public class MgtsServiceFactory implements ServiceFactory {

    @Override
    public Router createRouter() throws RouterInitializationException {
        FixedUrlRouter router = new FixedUrlRouter();
        router.RegisterPage(new CheckAddressPage());
        return router;
    }

    @Override
    public BaseUrlPattern createBaseUrlPattern(){
        BaseUrlRegexBuilder urlRegexBuilder = new BaseUrlRegexBuilder(new HashSet<String>(Arrays.asList("mgts.ru")));
        return new BaseUrlPattern(urlRegexBuilder.Build());
    }

    @Override
    public BaseUrlInfo getDefaultBaseUrlInfo() {
        return new BaseUrlInfo("barnaul","mgts.ru",Service.DEFAULT_ABSOLUTE_PATH);
    }

    @Override
    public Service createService() throws RouterInitializationException {
        return new MgtsService(getDefaultBaseUrlInfo(),createBaseUrlPattern(),createRouter());
    }
}
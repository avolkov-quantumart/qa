package framework.ConnectApplication;

import framework.page.PageWithFixedUrl;
import framework.services.RequestData;
import org.openqa.selenium.By;

/**
 * Created by VolkovA on 27.02.14.
 */
public class CheckAddressPage extends PageWithFixedUrl {
    public SelectServicesComponent selectServices;

    public CheckAddressPage() {
        this.selectServices = registerComponent(SelectServicesComponent.class);
    }

    @Override
    public String getPath() {
        return "gpon/checkaddress/";
    }

    @Override
    public boolean isPageFor(RequestData requestData) {
        return false;
    }

    // Выбрать регион
    public void selectRegion(String region) {
        log().Action("Выбрать регион '%s'", region);
        browser().action.select(By.id("Address_City"), region);
    }

    // Ввести название улицы
    public void typeInStreet(String value) {
        log().Action("Ввести '%s' в поле 'Название улицы'", value);
        browser().action.typeIn(By.id("Address_Street"), value);
    }

    // Ввести номер дома
    public void typeInHouseNumber(int value) {
        log().Action("Ввести '%s' в поле 'Номер дома'", value);
        browser().action.typeIn(By.id("Address_House"), value);
    }

    // Ввести номер корпуса
    public void typeInBuilding(int value) {
        log().Action("Ввести '%s' в поле 'Номер корпуса'", value);
        browser().action.typeIn(By.id("Address_Building"), value);
    }

    // Клик по кнопке "Далее"
    public void ClickByNextButton() {
        log().Action("Клик по кнопке 'Продолжить'");
        browser().action.click(By.linkText("Продолжить"));
    }
}

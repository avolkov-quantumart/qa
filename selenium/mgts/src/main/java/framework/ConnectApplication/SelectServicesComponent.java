package framework.ConnectApplication;

import framework.page.Component;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by VolkovA on 27.02.14.
 */
public class SelectServicesComponent extends Component{
    private By _connectionMessage1Selector;
    private By _connectionMessage2Selector;

    public SelectServicesComponent() {
        _connectionMessage1Selector = By.cssSelector("#formRequest .intro-text p:nth-of-type(1)");
        _connectionMessage2Selector = By.cssSelector("#formRequest .intro-text p:nth-of-type(2)");
    }

    // Проверяет что отображается сообщение о том что GPON будет доступен к указанной дате
    public void assertGponWillBeAccessibleAt(Date date) {
        String msg1 = browser().get.text(_connectionMessage1Selector);
        String msg2 = browser().get.text(_connectionMessage2Selector);
        Assert.assertEquals(msg1,"Предлагаем подключить услуги Интернет, Домашнее цифровое ТВ по технологии ADSL и установить Домашний телефон.");
        String msg2Expected = String.format("До %s Ваш дом будет подключен к технологии GPON с возможностью пользоваться услугами Интернет на скорости до 350 Мбит/с и смотреть цифровое телевидение высокой четкости.",
                new SimpleDateFormat("dd.MM.yyyy").format(date));
        Assert.assertEquals(msg1,"Предлагаем подключить услуги Интернет, Домашнее цифровое ТВ по технологии ADSL и установить Домашний телефон.");
        Assert.assertEquals(msg2,msg2Expected);
    }
}
